import argparse
import time

import ange
import demon
import monde


# ID de l'Ange et du Démon
ANGE = 0
DEMON = 1
QUI = ('Ange', 'Démon')

DEFAULT_TYPE = 0
DEFAULT_K = 1
DEFAULT_ECH = 30


HELP_FILE = '''Un fichier qui contient les coordonnées de cases déjà détruites'''

HELP_K = '''Un entier nature strictement positif qui est la puissance de l'ange'''

HELP_ECH = '''Un entier représentant l'échelle ie le nombre de pixels pour 1 unité du modèle.
Il est recommandé de choisir cette valeur parmi 10, 20, 30, 40, 50'''


class Game:

    def __init__(self):
        self.__joueurs = []
        self.__id_joueur = None
        self.monde = None

    def __str__(self):
        s = "\nJEU DE L'ANGE\n" 
        s += f'* Ange : type {self.monde.ange.type()}, puissance {self.monde.ange.power()}\n'
        s += f'* Démon de type : {self.monde.demon.type()}\n'
        s += 'Joueurs :\n'
        for joueur in self.__joueurs:
            s += joueur.__str__()
        s += f'Monde centré en : {self.monde.centre()}\n'
        return s


    def settings(self):
        # Traitement des options
        #
        parser = argparse.ArgumentParser()
        parser.add_argument('-a', '--ange', help=ange.HELP, type=int)
        parser.add_argument('-d', '--demon', help=demon.HELP, type=int)
        parser.add_argument('-f', '--file', help=HELP_FILE)
        parser.add_argument('-k', '--power', help=HELP_K, type=int)
        parser.add_argument('-e', '--ech', help=HELP_ECH, type=int)

        ange_type = DEFAULT_TYPE
        k = DEFAULT_K
        demon_type = DEFAULT_TYPE
        ech = DEFAULT_ECH
        filename = None

        args = parser.parse_args()
        if args.ange:
            ange_type = int(args.ange)
        if args.demon:
            demon_type = int(args.demon)
        if args.file:
            filename = args.file
        if args.power:
            k = int(args.power)
        if args.ech:
            ech = int(args.ech)

        return ange_type, k, demon_type, ech, filename


    def genese(self, ange, demon, ech):
        self.__joueurs = [ange, demon]
        self.__id_joueur = ANGE
        self.monde = monde.Monde(ange, demon, ech, self)
        self.monde.se_revele()
        self.monde.se_dessine()
        self.monde.update_view()


    # --
    # -- MODELE
    # --

    def joueur(self):
        return self.__joueurs[self.__id_joueur]

    def end(self):
        return self.monde.end()

    def suivant(self):
        self.__id_joueur = 1 - self.__id_joueur

    def auto(self):
        return all(not self.__joueurs[id_joueur].humain() for id_joueur in (ANGE, DEMON)) 
    


    # --
    # -- CONTROLEUR
    # --

    def intro(self):
        titre = "JEU DE L'ANGE"
        print()
        print()
        print('-' * 30)
        print(f'-- {titre:^24} --')
        print('-' * 30)
        input('\n\nPress return to begin the game...')

    def bye(self):
        self.monde.bye()

    def unbind(self):
        self.monde.unbind()

    def bind(self):
        self.monde.bind()

    def stop(self, code=0):
        print('PARTIE TERMINNÉE... fermeture dans :')
        for i in range(4, -1, -1):
            print(f'{i}s', end='\b\b', flush=True)
            time.sleep(1)
        print(f'{len(self.monde.demon.cells_on_fire())} cases détruites')
        if code==0:
            self.bye()

    def iterative_loop(self):
        """la boucle de jeu quand il n'y a que la machine qui joue
        dès lors la version récursive n'est pas adaptée à cause de la
        limitation de la récursion"""
        exit_code = 0
        while not self.end():
            joueur = self.joueur()
            x, y = joueur.choix_coup()
            joueur.joue(x, y)
            try:
                self.monde.update_view()
            except:
                exit_code = 1
                break
            self.suivant()
        self.stop(exit_code)


    def loop(self, x=None, y=None):
        self.unbind()
        if x is not None:
            joueur = self.joueur()
            humain = joueur.humain()
            if humain:
                x, y = self.monde.click(x, y)
            if not humain or joueur.coup_valide(x, y):
                joueur.joue(x, y)
                self.monde.update_view()
                self.suivant()
        self.play()


    def play(self):
        print(f'{QUI[self.__id_joueur]} joue...')
        if self.end():
            self.stop()
        elif self.joueur().humain():
            self.bind()
        else:
            x, y = self.joueur().choix_coup()
            self.loop(x, y)


    def mainloop(self):
        print('Fermer le fenêtre Turtle pour terminer...')
        self.monde.mainloop()


# --------------------------------------------------
# -- MAIN
# --------------------------------------------------


jeu = Game()
jeu.intro()
ange_type, k, demon_type, ech, filename = jeu.settings()
ange = ange.birth(ange_type, k)
demon = demon.birth(demon_type)
jeu.genese(ange, demon, ech)
print(jeu.monde.rayon())
if jeu.auto():
    jeu.iterative_loop()
else:
    jeu.loop()
    jeu.mainloop()






