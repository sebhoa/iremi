import random

# ID des types d'anges

HUMAIN = 0
FOOL = 1
RANDOM = 2

HELP = '''
Un entier pour le type d'ange :\n
\t- type 0 : [défaut] ange humain, il suffit de cliquer sur une case accessible pour voler\n
\t- type 1 : ange autonome de type fool (cf. Conway)\n
\t- autre  : ange autonome qui joue au hasard'''

def birth(t_ange, k):
    """fonction qui crée un ange en fonction du type choisi"""
    if t_ange == HUMAIN:
        return Hange(k)
    elif t_ange == FOOL:
        return Fool(k)
    else:
        return Range(k)



class Ange():
    """Ange de base, regroupant les caractéristiques communes"""
    def __init__(self, type, k):
        self.__type = type
        self.__x = None
        self.__y = None
        self.__k = k
        self.__old_area = set()
        self.__area = set()
        self.monde = None

    def __str__(self):
        s = f'{self.__k}-ange de type {self.__type} posté en {self.position()}\n'
        s += f'{len(self.__area)} cases à portée\n'
        return s

    def type(self):
        return self.__type

    def position(self):
        return self.__x, self.__y

    def my_position(self, x, y):
        return x == self.__x and y == self.__y

    def power(self):
        return self.__k

    def set_monde(self, monde):
        self.monde = monde

    def end(self):
        return self.__area == set()

    def humain(self):
        return self.__type == HUMAIN

    def joue(self, x, y):
        old_position = self.__x, self.__y
        self.__x = x
        self.__y = y
        self.__update_area(old_position)


    def coup_valide(self, x, y):
        return (x, y) in self.__area

    def is_burning(self, x, y):
        if (x, y) in self.__area:
            self.__area.remove((x, y))

    def __update_area(self, old_position):
        self.__old_area = self.__area.copy() - {old_position}
        self.__area.clear()
        k = self.__k
        xa, ya = self.__x, self.__y
        self.__area = {(x, y) for x in range(xa-k, xa+k+1) for y in range(ya-k, ya+k+1)
            if not self.monde.on_fire(x, y) and not self.my_position(x, y)}

    def area(self):
        return self.__area

    def old_area(self):
        return self.__old_area



class Fool(Ange):
    """Ange idiot comme définit par Conway"""
    def __init__(self, *args):
        super(Fool, self).__init__(FOOL, *args)

    def se_prepare(self, monde):
        self.set_monde(monde)
        self.joue(0, 0)

    # In progress...


class Range(Ange):
    """Ange étourdit qui va au hasard"""
    def __init__(self, *args):
        super(Range, self).__init__(RANDOM, *args)
    
    def se_prepare(self, monde):
        self.set_monde(monde)
        self.joue(0, 0)

    def choix_coup(self):
        return random.choice(list(self.area()))


class Hange(Ange):
    """Ange humain : c'est un humain qui décide où poser l'Ange"""
    def __init__(self, *args):
        super(Hange, self).__init__(HUMAIN, *args)

    def se_prepare(self, monde):
        self.set_monde(monde)
        self.joue(0, 0)







