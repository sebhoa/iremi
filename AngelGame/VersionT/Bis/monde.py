import turtle

HUMAIN = 0
SCREENSIZE = 840
COULEUR_ANGE = 'blue'
COULEUR_A_PORTEE = 'lightgreen'
COULEUR_HORS_PORTEE = 'white'
COULEUR_FEU = 'red'


class Monde(turtle.Turtle):

    def __init__(self, ange, demon, ech, game):
        # Le modèle
        self.ange = ange
        self.demon = demon
        self.game = game
        # self.__x = None
        # self.__y = None
        self.__rayon = 0 # nombre de cellules visibles

        # Pour la vue
        self.__centre = None # le centre de la visualisation
        self.ech = ech
        super(Monde, self).__init__()

    def settings(self):
        self.__rayon = SCREENSIZE//2//self.ech
        self.ht()
        self.screen.tracer(400)

    def mainloop(self):
        self.screen.mainloop()

    def se_revele(self):
        """Réalise qq réglages de la turtle sous-jacente et informe
        l'ange et le démon de son existence"""
        self.settings()
        self.ange.se_prepare(self)
        self.__centre = self.ange.position()
        self.demon.se_prepare(self)



    # --
    # -- VUE
    # --

    def se_dessine(self):
        """Dessine le monde"""
        def ligne(x, y, vertical):
            self.up()
            self.goto(x, y)
            self.down()
            if vertical:
                self.right(90)
            self.fd(SCREENSIZE//2)
            self.right(180)
            self.fd(SCREENSIZE)
            if vertical:
                self.right(90)
            else:
                self.right(180)
        
        x =  0
        while x < SCREENSIZE//2:
            ligne(x, 0, True)
            ligne(-x, 0, True)
            ligne(0, x, False)
            ligne(0, -x, False)
            x += self.ech
        self.screen.update()

    def mark(self, x, y, color='white'):
        """Colorise un carré de PAS de côté avec la color"""
        self.up()
        self.goto(x, y)
        self.down()
        self.color('black', color)
        self.begin_fill()
        for _ in range(4):
            self.fd(self.ech)
            self.left(90)
        self.end_fill()

    def partial_refresh(self):
        """Met à jour la couleur des cases du monde"""

        area = self.angel_area()
        old_area = self.angel_old_area()

        # On met en vert les nouvelles cases atteignables
        for (x, y) in area:
            xv, yv = self.model_to_view(x, y)
            self.mark(xv, yv, COULEUR_A_PORTEE)
        
        # En blanc les anciennes cases atteignables qui ne le sont plus
        for (x, y) in old_area - area:
            if not self.demon.on_fire(x, y):
                xv, yv = self.model_to_view(x, y)
                self.mark(xv, yv, COULEUR_HORS_PORTEE)

        # En bleu le case de l'ange
        xa, ya = self.model_to_view(*self.ange.position())
        self.mark(xa, ya, COULEUR_ANGE)

        # En rouge la dernière case feu ajoutée
        # (les autres le sont déjà)
        if self.demon.est_pose():
            xf, yf = self.model_to_view(*self.demon.position())
            self.mark(xf, yf, COULEUR_FEU)
        self.screen.update()

    def total_refresh(self):
        for x, y in self.demon.cells_on_fire():
            xv, yv = self.model_to_view(x, y)
            self.mark(xv, yv, COULEUR_FEU)
        self.partial_refresh()


    # --
    # -- MODÈLE
    # --

    # def voisins(self, x, y):
    #     """Les 8 voisins d'une case (x, y) du modèle"""
    #     return [(x+dx, y+dy) for dx in (-1,0,1) for dy in (-1,0,1)
    #         if (x+dx, y+dy) != (x, y)]

    def distance(self, a, b):
        xa, ya = a
        xb, yb = b
        return max(abs(xa - xb), abs(ya - yb))

    def distance_to_angel(self, a):
        return self.distance(self.ange.position(), a)

    def centre(self):
        return self.__centre

    def rayon(self):
        return self.__rayon

    # def focused(self):
    #     return self.__x is not None

    # def focus(self):
    #     return self.__x, self.__y

    # def set_focus(self, x, y):
    #     self.__x = x
    #     self.__y = y


    def on_fire(self, x, y):
        return self.demon.on_fire(x, y)

    def is_burning(self, x, y):
        self.ange.is_burning(x, y)

    def angel_area(self):
        return self.ange.area()

    def angel_old_area(self):
        return self.ange.old_area()

    def recentrer(self):
        self.__centre = self.ange.position()


    def joueur(self):
        return self.game.joueur()

    def end(self):
        return self.ange.end()


    # -- 
    # -- CONTRÔLEUR
    # -- 

    def bye(self):
        self.screen.bye()

    def unbind(self):
        self.screen.onclick(None)

    def bind(self):
        self.screen.onclick(self.game.loop)


    def model_to_view(self, x, y):
        """Traduit des coordonnées du modèle en coordonnés de la vue"""
        xc, yc = self.__centre
        return (x - xc) * self.ech, (y - yc) * self.ech

    def view_to_model(self, xv, yv):
        xc, yc = self.__centre
        return xv // self.ech + xc, yv // self.ech + yc

    def hors_vision(self):
        x, y = self.ange.position()
        xc, yc = self.__centre
        limite = SCREENSIZE//self.ech//2 - self.ange.power()
        return abs(x - xc) >= limite or abs(y - yc) >= limite


    def update_view(self):
        if self.hors_vision():
            self.recentrer()
            self.clear()
            self.se_dessine()
            self.total_refresh()
        else:
            self.partial_refresh()


    def click(self, x, y):
        """La fonction qui récupère le clic utilisateur et en fait ce qu'il faut"""
        
        # Arrondir les valeurs en fonction de l'échelle du monde
        x_arrondi = int((x//self.ech) * self.ech)
        y_arrondi = int((y//self.ech) * self.ech)

        # Traduire en coordonnées du monde
        xm, ym = self.view_to_model(x_arrondi, y_arrondi)
        # self.set_focus(xm, ym)
        return xm, ym




