            self.__mem = {
                # cas 1
                (xa, ya+1): {
                    'rep':(x-1, y, False), 
                    'suite': {
                        (xa, ya+2): (x+1, y, True),
                        (xa-1, ya+2): (x-2, y, True), 
                        (xa+1, ya+2): (x+2, y, False),
                        (xa, ya+3): (x+1, y, True),
                        (xa+1, ya+3): (x+1, y, True),
                        (xa+2, ya+3): (x+3, y, False),
                        (xa+1, ya+4): (x+1, y, True),
                        (xa+2, ya+4): (x+1, y, True),
                        (xa+3, ya+4): (x+4, y, True)
                    }
                },
                # cas 2 et symétrique
                (xa+1, ya+1): {
                    'rep': (x+2, y, False),
                    'suite': {
                        (xa+1, ya+2): (x+1, y, True),
                        (xa+2, ya+2): (x+3, y, False),
                        (xa+3, ya+3): (x+4, y, True),
                        (xa+2, ya+3): (x+1, y, True),
                        (xa+1, ya+3): (x+1, y, True)
                    }
                },

                (xa-1, ya+1): {
                    'rep': (),
                    'suite': {

                    }
                }
            }



# in progess

def f(dx, dy, direction, etape, cas):
    # à ajouter : un mode : 
    # SAFE (on pioche le plus proche d'abord dans les coins)
    # ALERT : on met en place le bloc de 3
    # PUSH : le bloc est en place on push le long de la ligne
    ns_eo = ((0,1,'N'), (-1,0,'O'), (0,-1,'S'), (1,0,'E'))
    neso_nose_1 = ((1,1,'N'), (1,-1,'E'), (-1,-1,'S'), (-1,1,'O'))
    neso_nose_2 = ((-1,1,'N'), (1,1,'E'), (1,-1,'S'), (-1,-1,'O'))
    
    if (etape, cas) == 1, 1:
        if (dx, dy, direction) in ns_eo:
            return -dy, dx, (2,1) 
        if (dx, dy, direction) in neso_nose_1:
            return dx + dy, dy - dx, (2, 2)
        if (dx, dy, direction) in neso_nose_2:
            return dx - dy, -dy * (dx+dy), (2, 3)
    
    if (etape, cas) == 2, 1:
        if (dx, dy, direction) in neso_nose_2:
            return (dx - dy) // 2, (dx + dy) // 2, None # mode -> PUSH
        if (dx, dy, direction) in ns_eo:
            return -dy, dx, None # mode -> PUSH
        if (dx, dy, direction) in neso_nose_1:
            return dx + dy, dy - dx, (3, 1)
    
    if (etape, cas) == 3, 1:
        if (dx, dy, direction) in neso_nose_2:
            return (dy - dx) // 2, -(dx + dy) // 2, None # mode -> PUSH
        if (dx, dy, direction) in ns_eo:
            return dy, -dx, None # mode -> PUSH
        if (dx, dy, direction) in neso_nose_1:
            return (dx + dy) // 2 * 3, (dy - dx) // 2 * 3, (4, 1)
