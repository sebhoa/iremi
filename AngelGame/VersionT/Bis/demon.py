import random

# ID des types de démons
#
HUMAIN = 0
KING_BLOCKER = 1
RANDOM = 2

INFINITY = float('inf')
DIRECTIONS = {(0,1):'N', (1,1):'NE', (1,0):'E', (1,-1):'SE',
    (0,-1):'S', (-1,-1):'SO', (-1,0):'O', (-1,1):'NO'}


HELP = '''
Le type de démon est un entier qui détermine quel démon est en jeu :\n
\t- type 0 : démon joué par un humain\n
\t- type 1 : démon autonome avec stratégie pour bloquer un 1-ange\n
\t- autre  : démon autonome qui joue au hasard, autours de l'ange\n'''


def birth(t_demon):
    if t_demon == HUMAIN:
        return Hdemon()
    elif t_demon == KING_BLOCKER:
        return KingBlocker()
    else:
        return Rdemon()


class Demon:

    def __init__(self, type):
        self.__type = type
        self.__cells_on_fire = set()
        self.__x = None
        self.__y = None 
        self.monde = None

    def __str__(self):
        return f'Un démon de type {self.__type}\n'

    def joue(self, x, y):
        self.__x = x
        self.__y = y
        self.__cells_on_fire.add((x, y))
        self.monde.is_burning(x, y)

    def est_pose(self):
        return self.__x is not None

    def position(self):
        return self.__x, self.__y

    def type(self):
        return self.__type

    def humain(self):
        return self.__type == HUMAIN

    def set_monde(self, monde):
        self.monde = monde

    def on_fire(self, x, y):
        return (x, y) in self.__cells_on_fire

    def cells_on_fire(self):
        return self.__cells_on_fire

    def coup_valide(self, x, y):
        return not self.monde.ange.my_position(x, y) and\
             not self.on_fire(x, y)

    def large_area_random_move(self, lower_left, upper_right):
        x = random.randint(lower_left[0], upper_right[0])
        y = random.randint(lower_left[1], upper_right[1])
        return x, y

    def random_move(self):
        return random.choice(list(self.monde.angel_area()))        


class Hdemon(Demon):
    """Démon joué par un humain"""
    def __init__(self):
        super(Hdemon, self).__init__(HUMAIN)

    def se_prepare(self, monde):
        self.set_monde(monde)


class Rdemon(Demon):
    """Démon autonome qui joue au hasard"""
    def __init__(self):
        super(Rdemon, self).__init__(RANDOM)

    def se_prepare(self, monde):
        self.set_monde(monde)

    def choix_coup(self):
        return self.random_move()        


class KingBlocker(Demon):

    SECURITY = 16
    SAFE = 0
    CRITICAL = 1
    PUSH = 2

    """Démon autonome avec stratégie pour bloquer un 1-ange"""
    def __init__(self):
        self.__wall = {direction:set() for direction in DIRECTIONS.values()}
        self.__corners = set()
        self.__walls = set()
        self.__inside = set()
        self.__mem_block = None, None
        self.__mem_ange = None, None
        self.__mode = KingBlocker.SAFE
        self.__etape = None
        self.__d_mur = None
        self.__mur = {'N':None, 'S':None, 'O':None, 'E':None}
        super(KingBlocker, self).__init__(KING_BLOCKER)


    def se_prepare(self, monde):
        self.set_monde(monde)
        self.corners_init()
        self.walls_init()
        self.inside_init()

    def corners_init(self):
        marge = self.SECURITY
        xa, ya = self.monde.ange.position()
        for dx, dy in ((-1, 1), (1, 1), (-1, -1), (1, -1)):
            xco, yco = xa + dx*marge, ya + dy*marge
            self.__corners.add((xco, yco))
            for d in range(1,5):
                self.__corners.add((xco+dx*d, yco))
                self.__corners.add((xco, yco+dy*d))

    def walls_init(self):
        marge = self.SECURITY
        xa, ya = self.monde.ange.position()
        for dx, dy in ((1, 0), (0, 1)):
            for d in range(-marge, marge+1):
                self.__walls.add((xa+dx*d+dy*4+dy*marge, ya+dx*marge+dx*4+dy*d))
                self.__walls.add((xa+dx*d-dy*4-dy*marge, ya-dx*marge-dx*4+dy*d))

    def inside_init(self):
        marge = self.SECURITY
        self.__inside = {(x, y) for x in range(-marge-4,marge+5) 
                for y in range(-marge-4, marge+5)
                    if not (y > marge and (x < -marge or x > marge)) 
                        and not (y < -marge and (x < -marge or x > marge)) 
                        and not (x, y) in self.__corners
                        and not (x, y) in self.__walls}

    def in_security_zone(self, x, y):
        m = self.SECURITY
        return -m+1 < x < m-1 and -m+1 < y < m-1

    def memorise_block(self, x, y):
        self.__mem_block = x, y

    def memorise_ange(self, x, y):
        self.__mem_ange = x, y

    def memorise_mur(self, coup):
        self.__mur[self.__d_mur] = coup

    def last_bloc(self):
        return self.__mur[self.__d_mur]

    def nearest(self, cells):
        best = None
        best_d = INFINITY
        xa, ya = self.monde.ange.position()
        for x, y in cells:
            current_d = self.monde.distance_to_angel((x,y))
            if current_d < best_d or current_d == best_d and (x == xa or y == ya):
                best_d = current_d
                best = x, y
        return best

    def __vector(self, a, b):
        def delta(u, v):
            return 0 if u == v else int((v - u) // abs(v - u)) 
        return delta(a[0], b[0]), delta(a[1], b[1])

    def debug_msg(self, dx, dy):
        print(f'etape:{self.__etape}, mur:{self.__d_mur}, ange:{(dx, dy)}')

    def three_blocks_wall(self, xa, ya):
        """Met en place un mur de 3 suivant la stratégie énoncée
        dans la thèse de Martin Kutz"""
        ns_eo = ((0,1,'N'), (-1,0,'O'), (0,-1,'S'), (1,0,'E'))
        neso_nose_1 = ((1,1,'N'), (1,-1,'E'), (-1,-1,'S'), (-1,1,'O'))
        neso_nose_2 = ((-1,1,'N'), (1,1,'E'), (1,-1,'S'), (-1,-1,'O'))
        x, y = self.__mem_block
        coup = None
        dx, dy = self.__vector(self.__mem_ange, (xa, ya))
        self.debug_msg(dx, dy)
        print(f'ancienne position de ange : {self.__mem_ange}')
        print(f'nouvelle position de ange : {xa}, {ya}')
        if self.__etape is None:
            print('DEBUT 3-BLOC')
            coup = self.nearest(self.__walls)
            self.memorise_block(*coup)
            print(coup)
            self.__d_mur = DIRECTIONS[self.__vector((xa, ya), coup)] 
            self.__etape = 1, 1
        elif self.__etape == (1, 1):
            if (dx, dy, self.__d_mur) in ns_eo:
                coup = x - dy, y + dx                       # ok
                self.__etape = 2, 1
            elif (dx, dy, self.__d_mur) in neso_nose_1:
                coup = x + dx + dy, y + dy - dx
                self.__etape = 2, 2
            elif (dx, dy, self.__d_mur) in neso_nose_2:
                coup = x + dx - dy, y + dx + dy             # ok
                self.__etape = 2, 3

        elif self.__etape == (2, 1):
            if (dx, dy, self.__d_mur) in neso_nose_2:
                coup = x + (dx - dy), y + (dx + dy)
                self.__etape = None
                self.__mode = KingBlocker.PUSH  
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.__etape = None
                self.__mode = KingBlocker.PUSH              # ok
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in neso_nose_1:
                coup = x + dx + dy, y + dy - dx
                self.__etape = 3, 1                         # ok

        elif self.__etape == (2, 2):
            if (dx, dy, self.__d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.__etape = None
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in neso_nose_2:
                coup = x + dx - dy, y + dx + dy
                self.__etape = 3, 2
            elif (dx, dy, self.__d_mur) in neso_nose_1:
                coup = x + dx + dy, y + dy - dx
                self.__etape = 3, 3

        elif self.__etape == (2, 3):
            if (dx, dy, self.__d_mur) in ns_eo:
                coup = x + (dx - dy) // 2, y + (dx + dy) // 2
                self.__etape = None
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in neso_nose_1:
                coup = x + dx + dy, y + dy - dx
                self.__etape = 3, 2
            elif (dx, dy, self.__d_mur) in neso_nose_2:
                coup = x + (dx - dy) // 2 * 3, y + (dx + dy) // 2 * 3 
                self.__etape = 3, 4

        elif self.__etape == (3, 1):                        # ok
            if (dx, dy, self.__d_mur) in neso_nose_2:
                coup = x + (dy - dx) // 2, y - (dx + dy) // 2
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in neso_nose_1:
                coup = x + (dx + dy) // 2 * 3, y + (dy - dx) // 2 * 3
                self.__etape = 4, 1 
 
        elif self.__etape == (3, 2):
            if (dx, dy, self.__d_mur) in ns_eo:
                coup = x - dy, y + dx
                self.__etape = 4, 2 
            elif (dx, dy, self.__d_mur) in neso_nose_2:
                coup = x + (dx - dy) // 2, y + (dx + dy) // 2
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in neso_nose_1:
                coup = x + (dx + dy) // 2, y + (dy - dx) // 2
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 

        elif self.__etape == (3, 3):
            if (dx, dy, self.__d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in neso_nose_1:
                coup = x + (dy - dx) // 2, y - (dx + dy) // 2
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in neso_nose_2:
                coup = x + 2 * (dx + dy), y + 2 * (dy - dx)
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 

        elif self.__etape == (3, 4):
            if (dx, dy, self.__d_mur) in neso_nose_2:
                coup = x + 2 * (dx - dy), y + 2 * (dx + dy)
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in neso_nose_1:
                coup = x - (dx + dy) // 2, y + (dx - dy) // 2
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in ns_eo:
                coup = x - dy, y + dx
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 

        elif self.__etape == (4, 1):                    # ok
            if (dx, dy, self.__d_mur) in neso_nose_2:
                coup = x + (dy - dx) // 2, y - (dx + dy) // 2
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.__d_mur) in neso_nose_1:
                coup = x + 2 * (dx + dy), y + 2 * (dy - dx)
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 

        elif self.__etape == (4, 2):
            if (dx, dy , self.__d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy , self.__d_mur) in neso_nose_2:
                # a priori on a un coup d'avance donc coup peut rester à None
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy , self.__d_mur) in neso_nose_1:
                coup = x + (dx + dy) // 2, y + (dy - dx) // 2
                self.__etape = None 
                self.__mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
        print(f'Démon joue {coup}')
        return coup


        

    def push_the_line(self, xa, ya):
        coup = None
        m = self.SECURITY
        dx, dy = self.__vector(self.__mem_ange, (xa, ya))
        if self.__d_mur in 'NS':
            dy = 0
        else:
            dx = 0
        if (dx, dy) != (0, 0):
            x, y = self.last_bloc()
            x += dx
            y += dy
            while self.on_fire(x, y) and ((-m+1 < x < m) or (-m+1 < y < m)):
                #print(f'dans push... on checke {x}, {y}')
                x += dx
                y += dy
            if not self.on_fire(x, y) and ((-m+1 < x < m) or (-m+1 < y < m)):
                coup = x, y            
        return coup
        
    def choix_coup(self):
        coup = None
        xa, ya = self.monde.ange.position()
        if self.__mode == KingBlocker.SAFE: 
            if not self.in_security_zone(xa, ya):
                self.__mode = KingBlocker.CRITICAL
        else:
            if self.in_security_zone(xa, ya):
                self.__mode = KingBlocker.SAFE
                self.__etape = None

        if self.__mode == KingBlocker.SAFE:
            if self.__corners:
                coup = self.nearest(self.__corners)
                self.__corners.remove(coup)
            elif self.__walls:
                coup = self.nearest(self.__walls)
                self.__walls.remove(coup)
        elif self.__mode == KingBlocker.CRITICAL:
            # on met en place le bloc de 3
            coup = self.three_blocks_wall(xa, ya)
            if coup in self.__walls:
                self.__walls.remove(coup)
        elif self.__mode == KingBlocker.PUSH:
            # on doit pousser le long du bloc de 3
            print(f'On pushe... dernier coup joué {self.position()}')
            coup = self.push_the_line(xa, ya)
            if coup in self.__walls:
                self.__walls.remove(coup)
            elif coup in self.__corners:
                self.__corners.remove(coup)
        if coup is None:
            coup = self.__inside.pop()
        self.memorise_ange(xa, ya)
        return coup


