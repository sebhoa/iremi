"""
Implémentation du jeu de l'ange et du démon de J. Conway
Il s'agit d'une implémentation d'un jeu à 2 joueurs humain
ou machine

Auteur : Sébastien Hoarau
Date   : 2020-09-24
"""


import turtle
import argparse
import time
import game2p
import ange
import demon


HUMAIN = 0
SCREENSIZE = 840
COULEUR_ANGE = 'blue'
COULEUR_A_PORTEE = 'lightgreen'
COULEUR_HORS_PORTEE = 'white'
COULEUR_FEU = 'red'
COULEUR_SPECIALE = 'orange'

DEFAULT_TYPE = 0
DEFAULT_K = 1
DEFAULT_ECH = 30


HELP_FILE = '''Un fichier qui contient les coordonnées de cases déjà détruites'''
HELP_K = '''Un entier nature strictement positif qui est la puissance de l'ange'''
HELP_ECH = '''Un entier représentant l'échelle ie le nombre de pixels pour 1 unité du modèle.
Il est recommandé de choisir cette valeur parmi 10, 20, 30, 40, 50'''


#class Monde(turtle.Turtle):
class Monde:

    def __init__(self):
        # Le modèle
        self.ange = None
        self.demon = None
        self.game2p = None

        # Pour la vue
        self.vue = turtle.Turtle()
        self._centre = 0, 0 # point de départ
        self._rayon = 0 # nombre de cellules visibles
        self.ech = DEFAULT_ECH
        super(Monde, self).__init__()


    def setup(self):
        # Traitement des options
        #
        parser = argparse.ArgumentParser()
        parser.add_argument('-a', '--ange', help=ange.HELP, type=int)
        parser.add_argument('-d', '--demon', help=demon.HELP, type=int)
        parser.add_argument('-f', '--file', help=HELP_FILE)
        parser.add_argument('-k', '--power', help=HELP_K, type=int)
        parser.add_argument('-e', '--ech', help=HELP_ECH, type=int)

        ange_type = DEFAULT_TYPE
        k = DEFAULT_K
        demon_type = DEFAULT_TYPE
        filename = None

        args = parser.parse_args()
        if args.ange:
            ange_type = int(args.ange)
        if args.demon:
            demon_type = int(args.demon)
        if args.file:
            filename = args.file
        if args.power:
            k = int(args.power)
        if args.ech:
            self.ech = int(args.ech)

        # Initialisation des joueurs (ange et démon)
        self.ange = ange.birth(ange_type, k, self)
        self.demon = demon.birth(demon_type, self)
        self.ange.se_prepare()
        self.demon.se_prepare()

        # Initialisation du contrôleur de 2P-Game
        self.game2p = game2p.Game()
        self.game2p.setup(self.ange, self.demon, self)

        # Réglages techniques
        self._rayon = SCREENSIZE//2//self.ech
        self.vue.ht()
        self.vue.screen.tracer(400)

        # Initialisation de la vue
        self.init_view()


    def turtleloop(self):
        self.vue.screen.mainloop()



    # --
    # -- VUE
    # --

    def se_dessine(self):
        """Dessine le monde"""
        def ligne(x, y, vertical):
            self.vue.up()
            self.vue.goto(x, y)
            self.vue.down()
            if vertical:
                self.vue.right(90)
            self.vue.fd(SCREENSIZE//2)
            self.vue.right(180)
            self.vue.fd(SCREENSIZE)
            if vertical:
                self.vue.right(90)
            else:
                self.vue.right(180)
        
        x =  0
        while x < SCREENSIZE//2:
            ligne(x, 0, True)
            ligne(-x, 0, True)
            ligne(0, x, False)
            ligne(0, -x, False)
            x += self.ech
        self.vue.screen.update()

    def mark(self, x, y, color='white'):
        """Colorise un carré de PAS de côté avec la color"""
        self.vue.up()
        self.vue.goto(x, y)
        self.vue.down()
        self.vue.color('black', color)
        self.vue.begin_fill()
        for _ in range(4):
            self.vue.fd(self.ech)
            self.vue.left(90)
        self.vue.end_fill()

    def partial_refresh(self):
        """Met à jour la couleur des cases du monde"""

        area = self.angel_area()
        old_area = self.angel_old_area()

        # l'ancienne position revient en blanc
        x, y = self.ange.old_position()
        xv, yv = self.model_to_view(x, y)
        self.mark(xv, yv, COULEUR_HORS_PORTEE)

        # En blanc les anciennes cases atteignables qui ne le sont plus
        for (x, y) in old_area - area:
            if not self.demon.on_fire(x, y):
                xv, yv = self.model_to_view(x, y)
                self.mark(xv, yv, COULEUR_HORS_PORTEE)
        
        # Les éventuelles cases spéciales
        special = self.ange.special()
        for (x, y) in special - self.demon.cells_on_fire():
            xv, yv = self.model_to_view(x, y)
            self.mark(xv, yv, COULEUR_SPECIALE)

        # On met en vert les nouvelles cases atteignables
        for (x, y) in area:
            xv, yv = self.model_to_view(x, y)
            self.mark(xv, yv, COULEUR_A_PORTEE)
        
        # En bleu le case de l'ange
        xa, ya = self.model_to_view(*self.ange.position())
        self.mark(xa, ya, COULEUR_ANGE)

        # En rouge la dernière case feu ajoutée
        # (les autres le sont déjà)
        if self.demon.est_pose():
            xf, yf = self.model_to_view(*self.demon.position())
            self.mark(xf, yf, COULEUR_FEU)
        self.vue.screen.update()
        time.sleep(0.1)

    def total_refresh(self):
        for x, y in self.demon.cells_on_fire():
            xv, yv = self.model_to_view(x, y)
            self.mark(xv, yv, COULEUR_FEU)
        self.partial_refresh()


    # --
    # -- MODÈLE
    # --

    def distance(self, a, b):
        xa, ya = a
        xb, yb = b
        return max(abs(xa - xb), abs(ya - yb))

    def distance_to_angel(self, a):
        return self.distance(self.ange.position(), a)

    def centre(self):
        return self._centre

    def rayon(self):
        return self._rayon

    def auto(self):
        return not self.ange.human() and not self.demon.human()

    def on_fire(self, x, y):
        return self.demon.on_fire(x, y)

    def is_burning(self, x, y):
        self.ange.is_burning(x, y)

    def angel_area(self):
        return self.ange.area()

    def angel_old_area(self):
        return self.ange.old_area()

    def recentrer(self):
        self._centre = self.ange.position()

    def end(self):
        return self.ange.end()


    # -- 
    # -- CONTRÔLEUR
    # -- 

    def bye(self):
        self.vue.screen.exitonclick()

    def unbind(self):
        self.vue.screen.onclick(None)

    def bind(self):
        self.vue.screen.onclick(self.game2p.loop)

    def stop(self, code=0):
        print('PARTIE TERMINNÉE...', end='')
        if code == 0:
            print('fermer la fenêtre Turtle.')
            self.bye()
        else:
            print()
        print(f'{len(self.demon.cells_on_fire())} cases détruites')


    def model_to_view(self, x, y):
        """Traduit des coordonnées du modèle en coordonnés de la vue"""
        xc, yc = self.centre()
        return (x - xc) * self.ech, (y - yc) * self.ech

    def view_to_model(self, xv, yv):
        xc, yc = self.centre()
        return xv // self.ech + xc, yv // self.ech + yc

    def hors_vision(self):
        x, y = self.ange.position()
        xc, yc = self.centre()
        limite = self.rayon() - self.ange.power()
        return abs(x - xc) >= limite or abs(y - yc) >= limite

    def init_view(self):
        self.se_dessine()
        self.total_refresh()

    def update_view(self):
        if self.hors_vision():
            self.recentrer()
            self.vue.clear()
            self.se_dessine()
            self.total_refresh()
        else:
            self.partial_refresh()


    def click(self, x, y):
        """La fonction qui récupère le clic utilisateur et en fait ce qu'il faut"""
        
        # Arrondir les valeurs en fonction de l'échelle du monde
        x_arrondi = int((x//self.ech) * self.ech)
        y_arrondi = int((y//self.ech) * self.ech)

        # Traduire en coordonnées du monde
        xm, ym = self.view_to_model(x_arrondi, y_arrondi)
        return xm, ym


# --
# -- MAIN
# --

def main():
    angel_game = Monde()
    angel_game.setup()
    # angel_game.game.burn()
    if angel_game.auto():
        input('Press return to begin...')
        angel_game.game2p.iterative_loop()
    else:
        angel_game.game2p.loop()
        angel_game.turtleloop()


if __name__ == '__main__':
    main()
