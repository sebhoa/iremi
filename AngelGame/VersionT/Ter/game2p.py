"""
Module qui implémente un jeu à 2 joueurs. Les joueurs peuvent
être humain ou machine. Le joueur humain joue en cliquant : le
jeu utilise donc une interface graphique, turtle par exemple

Auteur : Sébastien Hoarau
Date   : 2020-09-24
"""


class Game:

    """La classe Game permet d'implémenter la mécanique d'un jeu 2 joueurs
    elle a besoin de la liste des 2 joueurs, de l'identifiant du joueur 
    courant ainsi que du jeu mis en oeuvre

    Le jeu mis en oeuvre doit implémenter certaines méthodes :
    - bind et unbind pour lancer et interrompre le listener sur clic
    - end qui retourne True ssi la partie est finie
    - stop pour arrêter la partie et éventuellement afficher des résultats
    - update_view : pour mettre à jour la vue du jeu
    - click qui traduit des coordonnées de clic en coup du jeu

    Le jeu doit utiliser une class joueur qui dit implémenter les méthodes :
    - select_move : pour choisir un coup
    - valid_move : retourne True ssi le coup en argument est valide
    - play : pour jouer le coup choisi
    - human : qui retourne True ssi le joueur est contrôlé par un humain


    """

    def __init__(self):
        self.__players = [None, None]
        self.__player_id = 0
        self.game = None

    def setup(self, p0, p1, game):
        self.__players[0] = p0
        self.__players[1] = p1
        self.game = game 

    def player(self):
        """Retourne le joueur courant"""
        return self.__players[self.__player_id]


    def next(self):
        """Passer au joueur suivant"""
        self.__player_id = 1 - self.__player_id


    def iterative_loop(self):
        """la boucle de jeu quand il n'y a que la machine qui joue
        dès lors la version récursive n'est pas adaptée à cause de la
        limitation de la récursion"""
        exit_code = 0
        while not self.game.end():
            p = self.player()
            x, y = p.select_move()
            p.play(x, y)
            try:
                self.game.update_view()
            except:
                exit_code = 1
                break
            self.next()
        self.game.stop(exit_code)


    def loop(self, x=None, y=None):
        self.game.unbind()
        if x is not None:
            p = self.player()
            human_play = p.human()
            if human_play:
                x, y = self.game.click(x, y)
            if not human_play or p.valid_move(x, y):
                p.play(x, y)
                self.game.update_view()
                self.next()
        self.play()


    def play(self):
        if self.game.end():
            self.game.stop()
        elif self.player().human():
            self.game.bind()
        else:
            x, y = self.player().select_move()
            self.loop(x, y)









