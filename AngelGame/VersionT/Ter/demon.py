import random

# ID des types de démons
#
HUMAIN = 0
KING_BLOCKER = 1
RANDOM = 2

INFINITY = float('inf')
DIRECTIONS = {(0,1):'N', (1,1):'NE', (1,0):'E', (1,-1):'SE',
    (0,-1):'S', (-1,-1):'SO', (-1,0):'O', (-1,1):'NO'}


HELP = '''
Le type de démon est un entier qui détermine quel démon est en jeu :\n
\t- type 0 : démon joué par un humain\n
\t- type 1 : démon autonome avec stratégie pour bloquer un 1-ange\n
\t- autre  : démon autonome qui joue au hasard, autours de l'ange\n'''


def birth(t_demon, monde):
    if t_demon == HUMAIN:
        return Hdemon(monde)
    elif t_demon == KING_BLOCKER:
        return KingBlocker(monde)
    else:
        return Rdemon(monde)


class Demon:

    def __init__(self, type, monde):
        self._type = type
        self._cells_on_fire = set()
        self.x = None
        self.y = None 
        self.monde = monde

    def __str__(self):
        return f'Un démon de type {self.type}\n'

    def est_pose(self):
        return self.x is not None

    def position(self):
        return self.x, self.y

    def type(self):
        return self._type

    def set_monde(self, monde):
        self.monde = monde

    def on_fire(self, x, y):
        return (x, y) in self._cells_on_fire

    def cells_on_fire(self):
        return self._cells_on_fire

    def large_area_random_move(self, lower_left, upper_right):
        x = random.randint(lower_left[0], upper_right[0])
        y = random.randint(lower_left[1], upper_right[1])
        return x, y

    def random_move(self):
        return random.choice(list(self.monde.angel_area()))


    def se_prepare(self):
        """Permet au démon de se préparer... éventuellement"""
        pass


    # Les méthodes obligatoires, utilisées par le module Game2P

    def valid_move(self, x, y):
        return not self.monde.ange.my_position(x, y) and\
             not self.on_fire(x, y)

    def play(self, x, y):
        self.x = x
        self.y = y
        self._cells_on_fire.add((x, y))
        self.monde.is_burning(x, y)

    def human(self):
        return self._type == HUMAIN



class Hdemon(Demon):
    """Démon joué par un humain"""
    def __init__(self, *args):
        super(Hdemon, self).__init__(HUMAIN, *args)


class Rdemon(Demon):
    """Démon autonome qui joue au hasard"""
    def __init__(self, *args):
        super(Rdemon, self).__init__(RANDOM, *args)

    def select_move(self):
        return self.random_move()        


class KingBlocker(Demon):

    SECURITY = 16
    SAFE = 0
    CRITICAL = 1
    PUSH = 2

    """Démon autonome avec stratégie pour bloquer un 1-ange"""
    def __init__(self, *args):
        self.wall = {direction:set() for direction in DIRECTIONS.values()}
        self.corners = set()
        self.walls = set()
        self.inside = set()
        self.mem_block = None, None
        self.mem_ange = None, None
        self.mode = KingBlocker.SAFE
        self.etape = None
        self.d_mur = None
        self.mur = {'N':None, 'S':None, 'O':None, 'E':None}
        super(KingBlocker, self).__init__(KING_BLOCKER, *args)


    def se_prepare(self):
        self.corners_init()
        self.walls_init()
        self.inside_init()

    def corners_init(self):
        marge = self.SECURITY
        xa, ya = self.monde.ange.position()
        for dx, dy in ((-1, 1), (1, 1), (-1, -1), (1, -1)):
            xco, yco = xa + dx*marge, ya + dy*marge
            self.corners.add((xco, yco))
            for d in range(1,5):
                self.corners.add((xco+dx*d, yco))
                self.corners.add((xco, yco+dy*d))

    def walls_init(self):
        marge = self.SECURITY
        xa, ya = self.monde.ange.position()
        for dx, dy in ((1, 0), (0, 1)):
            for d in range(-marge+1, marge):
                self.walls.add((xa+dx*d+dy*4+dy*marge, ya+dx*marge+dx*4+dy*d))
                self.walls.add((xa+dx*d-dy*4-dy*marge, ya-dx*marge-dx*4+dy*d))

    def inside_init(self):
        marge = self.SECURITY
        self.inside = {(x, y) for x in range(-marge-4,marge+5) 
                for y in range(-marge-4, marge+5)
                    if not (y > marge and (x < -marge or x > marge)) 
                        and not (y < -marge and (x < -marge or x > marge)) 
                        and not (x, y) in self.corners
                        and not (x, y) in self.walls}

    def in_security_zone(self, x, y):
        m = self.SECURITY
        return -m+1 < x < m-1 and -m+1 < y < m-1

    def memorise_block(self, x, y):
        self.mem_block = x, y

    def memorise_ange(self, x, y):
        self.mem_ange = x, y

    def memorise_mur(self, coup):
        self.mur[self.d_mur] = coup

    def last_bloc(self):
        return self.mur[self.d_mur]

    def nearest(self, cells):
        best = None
        best_d = INFINITY
        xa, ya = self.monde.ange.position()
        for x, y in cells:
            current_d = self.monde.distance_to_angel((x,y))
            if current_d < best_d or current_d == best_d and (x == xa or y == ya):
                best_d = current_d
                best = x, y
        return best

    def debut_bloc(self, dx, dy):
        x, y = self.monde.ange.position()
        return x + 5*dx, y + 5*dy

    def __vector(self, a, b):
        def delta(u, v):
            return 0 if u == v else int((v - u) // abs(v - u)) 
        return delta(a[0], b[0]), delta(a[1], b[1])

    def debug_msg(self, dx, dy):
        print(f'etape:{self.etape}, mur:{self.d_mur}, ange:{(dx, dy)}')

    def three_blocks_wall(self, xa, ya):
        """Met en place un mur de 3 suivant la stratégie énoncée
        dans la thèse de Martin Kutz"""
        ns_eo = ((0,1,'N'), (-1,0,'O'), (0,-1,'S'), (1,0,'E'))
        neso_nose_1 = ((1,1,'N'), (1,-1,'E'), (-1,-1,'S'), (-1,1,'O'))
        neso_nose_2 = ((-1,1,'N'), (1,1,'E'), (1,-1,'S'), (-1,-1,'O'))
        x, y = self.mem_block
        coup = None
        dx, dy = self.__vector(self.mem_ange, (xa, ya))
        self.debug_msg(dx, dy)
        print(f'ancienne position de ange : {self.mem_ange}')
        print(f'nouvelle position de ange : {xa}, {ya}')
        if self.etape is None:
            print('DEBUT 3-BLOC')
            coup = self.nearest(self.walls)
            # coup = self.debut_bloc(dx, dy)
            if coup is not None:
                self.memorise_block(*coup)
                print(coup)
                self.d_mur = DIRECTIONS[self.__vector((xa, ya), coup)] 
                self.etape = 1, 1
        elif self.etape == (1, 1):
            if (dx, dy, self.d_mur) in ns_eo:
                coup = x - dy, y + dx                       # ok
                self.etape = 2, 1
            elif (dx, dy, self.d_mur) in neso_nose_1:
                coup = x + dx + dy, y + dy - dx
                self.etape = 2, 2
            elif (dx, dy, self.d_mur) in neso_nose_2:
                coup = x + dx - dy, y + dx + dy             # ok
                self.etape = 2, 3

        elif self.etape == (2, 1):
            if (dx, dy, self.d_mur) in neso_nose_2:
                coup = x + (dx - dy), y + (dx + dy)
                self.etape = None
                self.mode = KingBlocker.PUSH  
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.etape = None
                self.mode = KingBlocker.PUSH              # ok
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in neso_nose_1:
                coup = x + dx + dy, y + dy - dx
                self.etape = 3, 1                         # ok

        elif self.etape == (2, 2):
            if (dx, dy, self.d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.etape = None
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in neso_nose_2:
                coup = x + dx - dy, y + dx + dy
                self.etape = 3, 2
            elif (dx, dy, self.d_mur) in neso_nose_1:
                coup = x + dx + dy, y + dy - dx
                self.etape = 3, 3

        elif self.etape == (2, 3):
            if (dx, dy, self.d_mur) in ns_eo:
                coup = x + - dy, y + dx
                self.etape = None
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in neso_nose_1:
                coup = x + dx + dy, y + dy - dx
                self.etape = 3, 2
            elif (dx, dy, self.d_mur) in neso_nose_2:
                coup = x + (dx - dy) // 2 * 3, y + (dx + dy) // 2 * 3 
                self.etape = 3, 4

        elif self.etape == (3, 1):                        # ok
            if (dx, dy, self.d_mur) in neso_nose_2:
                coup = x + (dy - dx) // 2, y - (dx + dy) // 2
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in neso_nose_1:
                coup = x + (dx + dy) // 2 * 3, y + (dy - dx) // 2 * 3
                self.etape = 4, 1 
 
        elif self.etape == (3, 2):
            if (dx, dy, self.d_mur) in ns_eo:
                coup = x - dy, y + dx
                self.etape = 4, 2 
            elif (dx, dy, self.d_mur) in neso_nose_2:
                coup = x + (dx - dy) // 2, y + (dx + dy) // 2
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in neso_nose_1:
                coup = x + (dx + dy) // 2, y + (dy - dx) // 2
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 

        elif self.etape == (3, 3):
            if (dx, dy, self.d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in neso_nose_1:
                coup = x + (dy - dx) // 2, y - (dx + dy) // 2
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in neso_nose_2:
                coup = x + 2 * (dx + dy), y + 2 * (dy - dx)
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 

        elif self.etape == (3, 4):
            if (dx, dy, self.d_mur) in neso_nose_2:
                coup = x + 2 * (dx - dy), y + 2 * (dx + dy)
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in neso_nose_1:
                coup = x - (dx + dy) // 2, y + (dx - dy) // 2
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in ns_eo:
                coup = x - dy, y + dx
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 

        elif self.etape == (4, 1):                    # ok
            if (dx, dy, self.d_mur) in neso_nose_2:
                coup = x + (dy - dx) // 2, y - (dx + dy) // 2
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy, self.d_mur) in neso_nose_1:
                coup = x + 2 * (dx + dy), y + 2 * (dy - dx)
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 

        elif self.etape == (4, 2):
            if (dx, dy , self.d_mur) in ns_eo:
                coup = x + dy, y - dx
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy , self.d_mur) in neso_nose_2:
                # a priori on a un coup d'avance donc coup peut rester à None
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
            elif (dx, dy , self.d_mur) in neso_nose_1:
                coup = x + (dx + dy) // 2, y + (dy - dx) // 2
                self.etape = None 
                self.mode = KingBlocker.PUSH
                self.memorise_mur(coup)            # ok 
        print(f'Démon joue {coup}')
        return coup


        

    def push_the_line(self, xa, ya):
        coup = None
        m = self.SECURITY
        dx, dy = self.__vector(self.mem_ange, (xa, ya))
        if self.d_mur in 'NS':
            dy = 0
        else:
            dx = 0
        if (dx, dy) != (0, 0):
            x, y = self.last_bloc()
            x += dx
            y += dy
            while self.on_fire(x, y) and ((-m < x < m) or (-m < y < m)):
                #print(f'dans push... on checke {x}, {y}')
                x += dx
                y += dy
            if not self.on_fire(x, y) and ((-m < x < m) or (-m < y < m)):
                coup = x, y
            else:
                self.mode = KingBlocker.SAFE            
        else:
            self.mode = KingBlocker.SAFE            
        return coup
        
    def select_move(self):
        coup = None
        xa, ya = self.monde.ange.position()
        if self.mode == KingBlocker.SAFE: 
            if not self.in_security_zone(xa, ya):
                self.mode = KingBlocker.CRITICAL
        else:
            if self.in_security_zone(xa, ya):
                print('En SECURITE')
                self.mode = KingBlocker.SAFE
                self.etape = None

        if self.mode == KingBlocker.SAFE:
            if self.corners:
                coup = self.nearest(self.corners)
                self.corners.remove(coup)
            elif self.walls:
                coup = self.nearest(self.walls)
                self.walls.remove(coup)
        elif self.mode == KingBlocker.CRITICAL:
            # on met en place le bloc de 3
            coup = self.three_blocks_wall(xa, ya)
            if coup in self.walls:
                self.walls.remove(coup)
        elif self.mode == KingBlocker.PUSH:
            # on doit pousser le long du bloc de 3
            print(f'On pushe... dernier coup joué {self.position()}')
            coup = self.push_the_line(xa, ya)
            if coup in self.walls:
                self.walls.remove(coup)
            elif coup in self.corners:
                self.corners.remove(coup)
        if coup is None:
            self.mode == KingBlocker.SAFE
            if self.corners:
                coup = self.corners.pop()
            elif self.walls:
                coup = self.walls.pop()
            else:
                coup = self.inside.pop()
        self.memorise_ange(xa, ya)
        return coup


    def burn(self):
        for x, y in self.walls:
            self.play(x, y)
            self.monde.update_view()
        for x, y in self.corners:
            self.play(x, y)
            self.monde.update_view()
