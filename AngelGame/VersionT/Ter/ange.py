import random

# ID des types d'anges

HUMAIN = 0
FOOL = 1
SPEFOOL = 2
RANDOM = 3

INFINITY = float('inf')


HELP = '''
Un entier pour le type d'ange :\n
\t- type 0 : [défaut] ange humain, il suffit de cliquer sur une case accessible pour voler\n
\t- type 1 : ange autonome de type fool (cf. Conway)\n
\t- type 2 : ange autonome de type fool qui essaie chacune des 4 directions\n
\t- autre  : ange autonome qui joue au hasard'''

def birth(t_ange, k, monde):
    """fonction qui crée un ange en fonction du type choisi"""
    if t_ange == HUMAIN:
        return Hange(k, monde)
    elif t_ange == FOOL:
        return Fool(k, monde)
    elif t_ange == SPEFOOL:
        return SpecialFool(k, monde)
    else:
        return Range(k, monde)



class Ange:
    """Ange de base, regroupant les caractéristiques communes"""
    def __init__(self, type, k, monde):
        self._type = type
        self.x = 0
        self.y = 0
        self._k = k
        self._old_area = set()
        self._old_position = 0, 0
        self._area = set()
        self.monde = monde

    def __str__(self):
        s = f'{self.k}-ange de type {self.type} posté en {self.position()}\n'
        s += f'{len(self.area)} cases à portée\n'
        return s

    def type(self):
        return self._type

    def position(self):
        return self.x, self.y

    def old_position(self):
        return self._old_position

    def my_position(self, x, y):
        return x == self.x and y == self.y

    def power(self):
        return self._k

    def end(self):
        return self._area == set()

    def is_burning(self, x, y):
        if (x, y) in self._area:
            self._area.remove((x, y))

    def update_area(self):
        self._old_area = self._area.copy()
        self._area.clear()
        k = self._k
        xa, ya = self.x, self.y
        self._area = {(x, y) for x in range(xa-k, xa+k+1) for y in range(ya-k, ya+k+1)
            if not self.monde.on_fire(x, y) and not self.my_position(x, y)}

    def area(self):
        return self._area

    def old_area(self):
        return self._old_area

    # Méthodes obligatoires utilisées par le module Game2P

    def human(self):
        return self._type == HUMAIN

    def play(self, x, y):
        self._old_position = self.x, self.y
        self.x = x
        self.y = y
        self.update_area()

    def valid_move(self, x, y):
        return (x, y) in self._area

    def se_prepare(self):
        """Permet à l'ange de se préparer... éventuellement"""
        self.update_area()

    def special(self):
        """Permet de retourner la valeur d'un ensemble spécial de cellules"""
        return set()

    # et select_move propre à chaque type d'ange joué par la machine


class Fool(Ange):
    """Ange idiot comme définit par Conway"""
    def __init__(self, *args):
        super(Fool, self).__init__(FOOL, *args)
        self._special = set()
        self._distance = 2**(4 * self._k ** 3)

    def special(self):
        return self._special

    # def se_prepare(self):
    #     self.play(0, 0)
        # if self._k == 1:
        #     self.update_speciale()
 
    def update_speciale(self):
        x1, y1 = self.x, self.y
        x2, y2 = self.x, self.y
        for _ in range(self._distance):
            x1, y1 = x1 - 1, y1 + 1
            x2, y2 = x2 + 1, y2 + 1
            self._special.add((x1, y1))
            self._special.add((x2, y2))

    def update_area(self):
        self._old_area = self._area.copy()
        self._area.clear()
        k = self._k
        xa, ya = self.x, self.y
        self._area = {(x, y) for x in range(xa-k, xa+k+1) for y in range(ya+1, ya+k+1)
            if not self.monde.on_fire(x, y) and not self.my_position(x, y)}
        if self._k == 1 and self._distance in (16, 8, 4):
            self.update_speciale()

    def select_move(self):
        # restricted_moves = list((x, y) for (x, y) in self._area if y > self.y)
        # return random.choice(restricted_moves)
        self._distance -= 1
        return random.choice(list(self._area))


class SpecialFool(Ange):

    NORD = 0
    EST = 1
    SUD = 2
    OUEST = 3

    def __init__(self, *args):
        super(SpecialFool, self).__init__(SPEFOOL, *args)
        self._directions = [(SpecialFool.EST, SpecialFool.NORD),
                (SpecialFool.SUD, SpecialFool.EST),
                (SpecialFool.OUEST, SpecialFool.SUD)]
        self._main_dir = SpecialFool.NORD 
        self._dir_bis = SpecialFool.OUEST
        self._func = [lambda x, y, nx, ny, bis: ny > y and (not bis or x == nx), 
                      lambda x, y, nx, ny, bis: nx > x and (not bis or y == ny), 
                      lambda x, y, nx, ny, bis: y > ny and (not bis or x == nx), 
                      lambda x, y, nx, ny, bis: x > nx and (not bis or y == ny)]
        self._random = False
        self._bis = False


    def random_move(self):
        return random.choice(list(self._area))

    def move_forward(self, direction, bis):
        x, y = self.x, self.y
        f = self._func[direction]
        moves = [(nx, ny) for (nx, ny) in self._area if f(x, y, nx, ny, bis)]
        if moves != []:
            return random.choice(moves)
        return None

    def coince(self):
        return self._directions == []

    def change_directions(self):
        self._bis = False
        self._main_dir, self._dir_bis = self._directions.pop()

    def select_move(self):
        if self._random:
            return self.random_move()
        coup = self.move_forward(self._main_dir, bis=False)
        if coup is not None:
            return coup
        coup = self.move_forward(self._dir_bis, bis=True)
        if coup is not None:
            return coup
        if self.coince():
            self._random = True
            coup = self.random_move()
            return coup
        self.change_directions()
        coup = self.move_forward(self._main_dir, False)
        if coup is not None:
            return coup
        coup = self.move_forward(self._dir_bis, True)
        if coup is not None:
            return coup
        return None


class Range(Ange):
    """Ange étourdit qui va au hasard"""
    def __init__(self, *args):
        super(Range, self).__init__(RANDOM, *args)
    
    def select_move(self):
        return random.choice(list(self._area))


class Hange(Ange):
    """Ange humain : c'est un humain qui décide où poser l'Ange"""
    def __init__(self, *args):
        super(Hange, self).__init__(HUMAIN, *args)







