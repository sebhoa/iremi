import turtle
import time
import sys

OK = 0
FIRE = 1
ANGE = 2
KO = 3
COLORS = ('green', 'red', 'blue', 'white')
SCREENSIZE = 820
DIRECTIONS = {(-1,1):'NO', (0,1):'N', (1,1):'NE', (1,0):'E',
        (1,-1):'SE', (0,-1):'S', (-1,-1):'SO', (-1,0):'O'}

def settings():
    try:
        k = int(sys.argv[1])
    except:
        k = 2
    try:
        ech = int(sys.argv[2])
    except:
        ech = 10
    return k, ech


class Demon():

    def __init__(self, monde):
        self.x = None
        self.y = None
        self.fire_cells = set()
        self.monde = monde
        self.next_move = None

    def position(self):
        return self.x, self.y

    def there(self):
        return self.x is not None

    def burn(self, x, y):
        self.x = x
        self.y = y
        self.fire_cells.add((x, y))

    def play(self):
        if self.next_move is not None:
            self.burn(*self.next_move)


class KingBlocker(Demon):

    def __init__(self):
        self.first_moves = {}
        self.moves = {}

    def preparation(self, marge):
        xa, ya = self.monde.ange.position()
        for dx, dy in ((-1, 1), (1, 1), (-1, -1), (1, -1)):
            xco, yco = xa+dx*marge, ya+dy*marge
            self.moves[DIRECTIONS[dx,dy]].add((xco, yco))
            for d in range(1,4):
                self.next_moves[DIRECTIONS[dx,dy]].add((xco+dx*d, yco))
                self.next_moves[DIRECTIONS[dx,dy]].add((xco, yco+dy*d))






class Ange():

    def __init__(self, k, monde):
        self.k = k
        self.x = None
        self.y = None
        self.nouveaux = set()
        self.anciens = set()
        self.monde = monde

    def __str__(self):
        s = f'Position : {self.position()}\n'
        s += f'Atteignables : {len(self.nouveaux)}\n'
        for pos in self.nouveaux:
            s += f'{pos}, '
        return s[:-2] + '\n'

    def position(self):
        return self.x, self.y

    def my_position(self, x, y):
        return self.x == x and self.y == y

    def se_pose(self, x, y):
        old_position = self.x, self.y
        self.x = x
        self.y = y
        self._update_reachable(old_position)

    def distance(self, x, y):
        return max(abs(self.x - x), abs(self.y - y))

    def is_burning(self, x, y):
        if (x, y) in self.nouveaux:
            self.nouveaux.remove((x, y))

    def _update_reachable(self, old_position):
        self.anciens = self.nouveaux.copy() - {old_position}
        self.nouveaux.clear()
        a_traiter = [(self.x, self.y)]
        distances = {(self.x, self.y):0}
        while a_traiter:
            x, y = a_traiter.pop(0)
            d = distances[x, y]
            if d <= self.k:
                if d > 0 and not self.monde.fire(x, y) and not self.my_position(x, y):
                    self.nouveaux.add((x, y))
                for xx, yy in self.monde.voisins(x, y):
                    if (xx, yy) not in a_traiter:
                        a_traiter.append((xx, yy))
                        distances[xx, yy] = d+1
            # else:
            #     if self.monde.inconnu(x, y) or not self.monde.ko(x, y):
            #         self.cells[x, y] = KO
                #self.delete(x, y)

    def a_portee(self, x, y):
        return self.distance(x, y) <= self.k



class Monde(turtle.Turtle):
    
    def __init__(self, x=0, y=0, k=2, ech=10):
        # Le modèle
        self.ange = Ange(k, self)
        self.demon = Demon(self)

        # Pour la vue
        self.centre = x, y # le centre de la visualisation
        self.ech = ech
        turtle.Turtle.__init__(self)
        
    # petits utilitaires
    # -----------------------------------------------------

    def setup(self):
        """Quelques réglages"""
        self.ht()
        self.screen.tracer(400)
        #self.speed(0)
        self.screen.onclick(self.traiter_click)

    def mainloop(self):
        self.screen.mainloop()


    # Méthodes VUE
    # -----------------------------------------------------


    def draw_world(self):
        """Dessine le monde"""
        def ligne(x, y, vertical):
            self.up()
            self.goto(x, y)
            self.down()
            if vertical:
                self.right(90)
            self.fd(SCREENSIZE//2)
            self.right(180)
            self.fd(SCREENSIZE)
            if vertical:
                self.right(90)
            else:
                self.right(180)
        
        x =  0
        while x < SCREENSIZE//2:
            ligne(x, 0, True)
            ligne(-x, 0, True)
            ligne(0, x, False)
            ligne(0, -x, False)
            x += self.ech
        self.screen.update()

    def mark(self, x, y, color='white'):
        """Colorise un carré de PAS de côté avec la color"""
        self.up()
        self.goto(x, y)
        self.down()
        self.color('black', color)
        self.begin_fill()
        for _ in range(4):
            self.fd(self.ech)
            self.left(90)
        self.end_fill()

    def update_view(self):
        """Met à jour la couleur des cases du monde"""

        # On met en vert les nouvelles cases atteignables
        for (x, y) in self.ange.nouveaux:
            xv, yv = self.model_to_view(x, y)
            self.mark(xv, yv, COLORS[OK])
        
        # En blanc les anciennes cases atteignables qui ne le sont plus
        for (x, y) in self.ange.anciens - self.ange.nouveaux:
            xv, yv = self.model_to_view(x, y)
            self.mark(xv, yv, COLORS[KO])

        # En bleu le case de l'ange
        xa, ya = self.model_to_view(*self.ange.position())
        self.mark(xa, ya, COLORS[ANGE])

        # En rouge la dernière case feu ajoutée
        # (les autres le sont déjà)
        if self.demon.there():
            xf, yf = self.model_to_view(*self.demon.position())
            self.mark(xf, yf, COLORS[FIRE])
        self.screen.update()

    def update_fire_view(self):
        # pour mettre en rouges toutes les cases feu ajoutées
        for x, y in self.demon.fire_cells:
            xv, yv = self.model_to_view(x, y)
            self.mark(xv, yv, COLORS[FIRE])
        self.screen.update()

    def __str__(self):
        nb_ok = 0
        nb_ko = 0
        nb_fire = 0
        s_feu = 'Cases en feu : '
        for x, y in self.cells:
            if self.ok(x, y):
                nb_ok += 1
            elif self.ko(x, y):
                nb_ko += 1
            elif self.fire(x, y):
                nb_fire += 1
        return f'CASES OK : {nb_ok}\nCASES KO : {nb_ko}\nFEU : {nb_fire}'



    # Méthodes du MODÈLE
    # -----------------------------------------------------

    def voisins(self, x, y):
        """Les 8 voisins d'une case (x, y) du modèle"""
        return [(x+dx, y+dy) for dx in (-1,0,1) for dy in (-1,0,1)
            if (x+dx, y+dy) != (x, y)]


    def burn(self, x, y):
        self.demon.burn(x, y)
        self.ange.is_burning(x, y)
        

    def fire(self, x, y):
        return (x, y) in self.demon.fire_cells



    # Méthodes du CONTROLEUR
    # -----------------------------------------------------

    def model_to_view(self, x, y):
        """Traduit des coordonnées du modèle en coordonnés de la vue"""
        xc, yc = self.centre
        return (x - xc) * self.ech, (y - yc) * self.ech

    def view_to_model(self, xv, yv):
        xc, yc = self.centre
        return xv // self.ech + xc, yv // self.ech + yc

    def hors_vision(self):
        x, y = self.ange.position()
        xc, yc = self.centre
        limite = SCREENSIZE//self.ech//2 - self.ange.k
        return abs(x - xc) >= limite or abs(y - yc) >= limite

    def recentrer(self):
        self.centre = self.ange.position()


    def traiter_click(self, x, y):
        x_arrondi = int((x//self.ech) * self.ech)
        y_arrondi = int((y//self.ech) * self.ech)
        xm, ym = self.view_to_model(x_arrondi, y_arrondi)
        # print('Dans la vue :')
        # print(f'CLIC : {x_arrondi}, {y_arrondi}')
        # print('Dans le modèle :')
        # print(f'CLIC : {xm}, {ym}')

        if self.ange.a_portee(xm, ym) and not self.fire(xm, ym):
            self.ange.se_pose(xm, ym)
            if self.hors_vision():
                self.recentrer()
                self.clear()
                self.draw_world()
                self.update_fire_view()
        self.update_view()
        print(self.ange)


# --
# -- MAIN
# --

ltest = [(-4, 5),(-4, 3),(0, 3),(-2, 1),(-1, 1),
    (0, 1),(1, 1),(2, 1),(-2, 0),(2, 0),(-2, -1),
    (2, -1),(-1, -2),(0, -2),(1, -2),(2, -2)]

k, ech = settings()
test = Monde(k=k, ech=ech)
test.setup()
test.draw_world()

# On pose l'Ange
test.ange.se_pose(0, 0)

# Demon se prépare
#
# test.demon.preparation(15)
# for l in test.demon.next_moves.values():
#     for x, y in l:
#         test.burn(x, y)
# test.update_fire_view()


# On enflamme qq cases
for x, y in ltest:
    test.burn(x, y)
test.update_fire_view()

print(test.ange)

# time.sleep(3)
# test.ange.se_pose(-10,30)
# test.atteignables()
# test.plus_atteignables()
test.update_view()
test.mainloop()



