---
title: "Jeux et puzzles pour le développement de la pensée informatique"
output:
  html_document:
    toc: yes
pagetitle: Hop
author: Sébastien Hoarau
---

Cette série vise à présenter des jeux combinatoires à deux joueurs ou à un joueur (puzzle) sous l'angle de la modélisation informatique. Manipuler ces jeux, les comprendre, savoir les modéliser à des fins de programmation : trouver ou coder des stratégies gagnantes, ou plus modestement simuler des débuts de parties. Le but étant de développer la pensée informatique d'un apprenant niveau académique BAC-2 -- BAC+2

Les compétences développées 

# Le Jeu de l'Ange

## Introduction

_The Angel Game_ est un jeu à deux joueurs dit combinatoire c'est-à-dire à information complètement connue. Parmi ces jeux les plus célébres sont le jeu d'Échecs, de Go qui sont des jeux non triviaux ou encore le morpion ou tic-tac-toe qui lui est un jeu trivial (la solution est connue).

Une branche des Mathématiques et de l'Informatique théorique consiste à étudier ce type de jeu pour en trouver des stratégies gagnantes.

### But du jeu et représentations

Le jeu de l'Ange se joue sur une grille infinie : un _k-ange_ qui peut se déplacer d'une distance d'au plus _k_ cases suivant une certaine règle que nous préciserons plus tard doit ne pas se faire enfermer par les flammes d'un démon.

Pour enfermer l'ange, le démon détruit le sol d'une case de la grille, faisont apparaître des flammes. L'ange peut, en volant, sauter au-dessus des flammes, mais ne peut en aucun cas y stationner. 

Le jeu peut se jouer sur les intersections, comme dans la figue 1.1 ci-dessous tirée de wikipedia et montrant les positions atteignables par un 3-ange (les intersection à l'intérieur de la zone délimitée). Utiliser les cases comme dans la figure 1.2 illustrant la même configuration et réalisée à l'aide d'un simple tableur.

Enfin, la figure 1.3 montre une représentation obtenue par un script Python, qui, en plus de produire l'affichage, a calculé les cases atteignables (et donc celles non atteignables) par notre 3-ange.

![Diverses représentations d'un 3-ange](representations.png)


## Le problème de J. H. Conway

Le jeu est dû au mathématicien britannique John Horton Conway [1]. Le problème posé par Conway est simple, au moins par sa formulation :

> Déterminer si un ange d'une certaine puissance bat le démon

Autrement dit, existe-t-il un entier $k \leq 1$ tel qu'un k-ange ait une stratégie gagnante ? Mais avant d'aller plus avant dans la présentation de ce problème et des résultats obtenus par divers chercheurs, revenons sur la notion de distance mise en oeuvre dans ce jeu.

### La distance $L_{\infty}$


## Références

[1]: John H. Conway, [The angel problem](http://library.msri.org/books/Book29/files/conway.pdf), in: Richard Nowakowski (editor) _Games of No Chance_, volume 29 of MSRI Publications, pages 3–12, 1996.