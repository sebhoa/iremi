"""
Jeu de l'Ange
version non objet -- NSI 1re
Auteur : S. Hoarau
Date   : 2020.06.16
"""

# flamme : '\U0001F525'
# ange : '\U0001f607'

EMOJIS = ('✳️ ', '🔥', '😇', '❌') 

OK = 0
FLAMME = 1
ANGE = 2
KO = -1

def init_grille(x, y):
    return {(x, y): ANGE} 


def enflammer(grid, l_flammes):
    for x, y in l_flammes:
        grid[x,y] = FLAMME


def voisins_libres(x, y, grid):
    return [(x+dx, y+dy) for dx in (-1,0,1) for dy in (-1,0,1)
        if grid.get((x+dx, y+dy), OK) in (OK, FLAMME)]

def atteignables(grid, ange):
    max_d = ange['k']
    a_traiter = [(ange['x'], ange['y'], 0)]
    while a_traiter:
        x, y, d = a_traiter.pop(0)
        if d <= max_d:
            grid[(x,y)] = grid.get((x, y), OK)
            if d < max_d:
                for xx, yy in voisins_libres(x, y, grid):
                    if (xx, yy, d+1) not in a_traiter:
                        a_traiter.append((xx, yy, d+1))
        else:
            grid[(x,y)] = KO



def afficher_grille(grid, largeur, hauteur):
    print()
    for x in range(hauteur):
        ligne = ''.join([EMOJIS[grid.get((x,y), KO)] for y in range(largeur)])
        print(ligne)
    print()


ange = {'x':5, 'y':4, 'k':3}
ltest = [(0,0), (2,0), (2,4), (4,2), (4,3), (4,4), (4,5), (4,6), (5,2), (5,6), 
    (6,2), (6,6), (7,3), (7,4), (7,5), (7,6)]
grid = init_grille(ange['x'], ange['y'])
enflammer(grid, ltest)
atteignables(grid, ange)
afficher_grille(grid, 9, 10)
