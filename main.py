

def define_env(env):
    
    env.variables['list_solution'] = {
        1: [(45,65), (43,45), (35,55), (65,45), (46,44)],
        2: [(43,41), (45,43), (24,44), (44,42), (64,44), (41,43), (43,45), (46,44)],
        3: [(53,55), (55,35), (33,53), (63,43), (44,42), (35,33), (23,43), (42,44)],
        4: [(45,25), (37,35), (57,37), (34,36), (37,35), (25,45), (46,66), (54,56), (66,46), (46,44)],
        5: [(31,33), (51,53), (43,63), (41,43), (33,53), (63,43), (44, 42), (46, 44), (25, 45), (45, 43), (65, 45), (42, 44), (44, 46), (47, 45)],
        6: [(55,53), (74,54), (53,55), (55,57), (57,37), (35,33), (14,34), (33,35), (36,56), (44,46), (56,36), (25,45), (37,35), (35,55), (65,45)],
        7: [(54,52), (52,32), (22,42), (33,53), (41,43), (43,63), (74,54), (62,64), (45,65), (54,74), (66,64), (74,54), (35,33), (54,34), (33,35), (47,45), (45,25), (14,34), (26,24), (24,44)],
        8: [(64,62), (44,64), (74,54), (46,66), (66,64), (64,44), (44,46), (47,45), (24,26), (26,46), (46,44), (44,24), (14,34), (42,22), (22,24), (24,44), (44,42), (41,43), (62,42), (42,44)],
        9: [(53,51), (32,52), (51,53), (44,42), (23,43), (42,44), (63,43), (25,23), (45,25), (43,45), (55,35), (35,33), (33,13), (13,15), (15,35), (35,37), (37,57), (57,55), (55,53), (74,54), (53,55), (65,45), (46,44)],
        10: [(53,51), (51,31), (55,75), (75,73), (73,53), (64,44), (35,37), (37,57), (57,55), (55,35), (34,36), (46,26), (14,34), (26,24), (23,25), (44,24), (25,23), (32,34), (53,33), (34,32), (31,33), (23,43), (42,44)],
        11: [(53,51), (32,52), (51,53), (54,52), (74,54), (44,42), (52,32), (22,42), (41,43), (24,22), (43,23), (22,24), (62,64), (64,44), (34,54), (14,34), (66,64), (64,44), (56,54), (35,33), (54,34), (33,35), (35,55), (47,45), (55,35), (25,45), (26,46), (46,44)],
        12: [(42,62), (54,52), (51,53), (74,54), (54,52), (62,42), (73,53), (32,52), (31,51), (43,63), (51,53), (63,43), (56,54), (75,55), (54,56), (35,55), (47,45), (45,65), (57,55), (65,45), (37,35), (34,32), (13,33), (15,13), (43,23), (13,33), (32,34), (24,26), (34,36), (26,46), (46,44)],
        13: [(64,44), (62,64), (42,62), (22,42), (24,22), (26,24), (46,26), (66,46), (64,66), (34,54), (54,52), (52,32), (32,34), (24,44)],
        14: [(42,44), (22,42), (24,22), (63,43), (33,53), (65,63), (62,64), (42,62), (26,24), (46,26), (34,36), (55,35), (36,34), (53,55), (56,54)],
        15: [(24,44), (36,34), (55,35), (25,45), (33,35), (53,33), (23,43), (56,36), (36,34), (73,53), (65,63), (53,73), (51,53), (32,52), (53,51)],
        16: [(64,44), (52,54), (33,53), (54,52), (66,64), (46,66), (44,46), (24,44), (26,24), (46,26), (14,34), (22,24), (34,14), (42,22), (62,42), (64,62)],
        17: [(24,44), (54,34), (74,54), (42,44), (44,64), (46,44), (34,54), (54,74), (62,42), (63,43), (32,34), (22,24), (25,45), (26,46), (56,54), (66,64)],
        18: [(42,44), (45,43), (64,44), (43,45), (66,64), (46,66), (26,46), (46,44), (24,26), (44,24), (22,42), (24,22), (41,43), (62,42), (43,41), (64,62)],
        19: [(64,44), (34,54), (42,44), (44,64), (36,34), (56,36), (26,46), (46,44), (65,45), (24,26), (24,26), (44,24), (22,42), (24,22), (41,43), (62,42), (43,41), (64,62)],
        20: [(46,44), (43,45), (23,43), (25,23), (45,25), (42,44), (63,43), (43,45), (65,63), (45,65), (22,42), (26,24), (66,46), (62,64), (42,62), (24,22), (46,26), (64,66)],
        21: [(64,44), (62,64), (42,62), (22,42), (24,22), (26,24), (46,26), (66,46), (64,66), (34,54), (54,52), (52,32), (32,34), (24,44), (45,65), (43,45), (35,55), (65,45), (46,44)],
        22: [(24,44), (32,34), (52,32), (35,33), (32,34), (55,35), (57,55), (65,45), (63,65), (43,63), (36,56), (35,55), (55,57), (13,33), (15,13), (33,35), (35,15), (73,53), (75,73), (53,55), (55,75)],
        23: [(24,44), (32,34), (53,33), (34,32), (31,33), (52,32), (33,31), (45,43), (64,44), (43,45), (56,54), (35,55), (54,56), (66,64), (63,65), (26,24), (23,25), (37,35), (47,45), (57,55)],
        24: [(42,44), (62,42), (64,62), (44,64), (41,43), (74,54), (46,44), (44,64), (66,46), (64,66), (47,45), (24,44), (44,46), (26,24), (46,26), (14,34), (22,24), (24,44), (44,42), (42,22)],
        25: [(42,44), (45,43), (24,44), (43,45), (64,44), (56,54), (54,34), (36,56), (34,36), (15,35), (57,55), (37,57), (35,37), (13,15), (33,13), (31,33), (51,31), (53,51), (73,53), (75,73), (55,75)],
        26: [(42,44), (63,43), (51,53), (31,51), (33,31), (53,33), (23,43), (35,33), (33,53), (14,34), (44,24), (46,44), (26,46), (24,26), (47,45), (66,46), (54,56), (46,66), (74,54), (75,55), (45,65), (53,55), (55,75)],
        27: [(46,44), (65,45), (57,55), (37,57), (54,56), (57,55), (45,65), (52,54), (32,52), (34,32), (36,34), (15,35), (22,42), (13,33), (34,32), (14,34), (75,55), (55,53), (74,54), (53,55), (73,53), (52,54), (32,52), (62,42)],
        28: [(46,44), (25,45), (37,35), (45,25), (15,35), (34,36), (26,46), (54,34), (65,45), (57,55), (45,65), (75,55), (74,54), (54,56), (47,45), (66,46), (52,54), (32,52), (34,32), (14,34), (62,42), (42,44), (73,53), (54,52), (22,42), (13,33), (34,32), (45,43), (31,33), (43,23), (41,43), (51,53), (43,63)],
        29: [(46,44), (25,45), (37,35), (45,25), (15,35), (34,36), (26,46), (54,34), (65,45), (57,55), (45,65), (75,55), (74,54), (54,56), (47,45), (66,46), (52,54), (32,52), (34,32), (14,34), (45,47), (73,53), (62,42), (54,52), (51,53), (53,33), (41,43), (22,42), (34,32), (13,33), (43,23), (31,33), (23,43), (43,41)],
        30: [(53,51), (73,53), (65,63), (62,64), (75,73), (54,52), (51,53), (43,63), (73,53), (23,43), (25,23), (45,25), (47,45), (31,33), (33,35), (13,33), (43,23), (22,24), (14,34), (35,33), (15,35), (45,25), (26,24), (37,35), (66,46), (41,43), (43,23), (23,25), (25,45), (45,65), (65,63), (63,43), (43,45), (45,47), (57,37)] 
    }

    @env.macro
    def entete(numero):
        #titre = env.variables.p_titre[numero]
        ligne = '=== "P'+str(numero)+'"\n\n'
        return ligne

    @env.macro
    def enonce(numero):
        s = '\t??? question "Enoncé et solution de E.Lucas"\n\n'
        s += f'\t\t![p{numero}](../assets/images/solitaire/p{numero:02}.png)\n\n'
        return s

    @env.macro
    def charger(pb, numero):
        s = "\t### Charger le problème et afficher l'objectif\n\n"

        s += '\t```python\n'
        s += f'\t>>> p{numero} = Solitaire("EL{numero:02}")\n'
        s += f'\t>>> p{numero}.goal()\n'
        s += '\t```\n\n'

        s += '\t```\n'
        s += pb.goal(1)
        s += '\n\t```\n'
        return s
    
    @env.macro
    def solution(pb, numero):
        sol = env.variables['list_solution'][numero]
        s = '\t### Tester la solution de E. Lucas\n\n'

        s += '\t```python\n'
        s += f'\t>>> p{numero}.solution({sol})\n'
        s += f'\t{pb.solution(sol)}\n'
        s += f'\t>>> p{numero}.coups()\n'
        s += f'\t{pb.coups()}\n'
        s += '\t```\n\n'
        return s

    @env.macro
    def notre_solution(pb, numero, limit=INF):
        rep = pb.solve(limit=limit)
        coups = pb.coups()
        explored = pb.explored

        s = '\t### Notre solution\n\n'
        #s += f"\tOn a exploré {explored} motifs pour y arriver.\n\n"

        s += '\t```python\n'
        s += f"\t>>> p{numero}b = Solitaire('EL{numero:02}')\n"
        s += f'\t>>> p{numero}b.solve()\n'
        s += f'\t{rep}\n'
        s += f'\t>>> p{numero}b.coups()\n'
        s += f'\t{coups}\n'
        s += f'\t>>> p{numero}b.explored\n'
        s += f'\t{explored}\n'
        s += '\t```\n\n'
        return s

    @env.macro
    def trace(p, numero):
        str_trace = p.trace(2)

        s = '\t??? done "La trace complète"\n\n'

        s += '\t\t```python\n'
        s += f'\t\t>>> p{numero}.trace()\n'
        s += '\t\t```\n\n'

        s += '\t\t```\n'
        s += str_trace 
        s += '\n'
        s += '\t\t```\n'
        return s


    @env.macro
    def probleme(numero, notre_sol=False, limit=INF):
        racine = f'EL{numero:02}'
        p = Solitaire(racine)
        pb = Solitaire(racine)
        s = entete(numero)
        s += enonce(numero)
        s += charger(p, numero)
        s += solution(p, numero)
        if notre_sol:
            s += notre_solution(pb, numero, limit)
            s += trace(pb, numero)
        return s



# ---

"""
SOLITAIRE

recherche de solution.
"""

VIDE = 0
PLEIN = 1
VIDE_STR = '.'
PLEIN_STR = 'x'
JETONS = '○', '●'
DIRECTIONS = {'N':(0,1), 'S':(0,-1), 'E':(1,0), 'W':(-1,0)}
NUMS_TO_DIR = {20:'E', -20:'W', 2:'N', -2:'S'}
INFINI = float('Inf')
DEFAULT_REP = 'Solitaire/Problemes'
INF = float('Inf')

# === FONCTIONS UTILITAIRES

def create_file(name, liste_of_nums, extension='I', rep=DEFAULT_REP):
    """Crée un fichier texte pour une configuration initiale (extension = 'I') ou finale (extension='F') pour un solitaire européen, avec la numérotation
    de E. Lucas en remplissant les encoches dont le numéro figure dans la liste_of_nums et dont la partie commune du nom est name"""
    nb_of_e = [3, 2, 1, 1, 1, 2, 3]
    coords_of_x = {divmod(num, 10) for num in liste_of_nums}
    full_of_e = 'E'*9 + '\n'
    filename = f'{rep}/{name}{extension}.txt'
    with open(filename, 'w', encoding='utf-8') as output:
        output.write(full_of_e)
        for y in range(7, 0, -1):
            nb_e = nb_of_e[7-y]
            s = 'E'*nb_e
            for x in range(nb_e, 9-nb_e):
                s += PLEIN_STR if (x, y) in coords_of_x else VIDE_STR
            s += 'E'*nb_e + '\n'
            output.write(s)
        output.write(full_of_e)

def to_num(x, y):
    return 10*x + y

def to_coords(*args):
    return args if len(args) == 2 else divmod(args[0], 10)



# === 
# === ENCOCHES ET MOTIFS

class Encoche:

    def __init__(self, motif, *args, etat=VIDE):
        self.motif = motif
        if len(args) == 2:
            self.x, self.y = args
            self.num = to_num(self.x, self.y)
        else:
            self.num = args[0]
            self.x, self.y = to_coords(self.num) 
        self.etat = etat

    def switch(self):
        self.etat = 1 - self.etat
        
    def encode(self):
        return 1 << self.num
    
    def on(self):
        return self.etat == PLEIN

    def off(self):
        return self.etat == VIDE

    def blocked(self):
        return all(not self.motif.coup_possible(d, self.x, self.y) for d in DIRECTIONS)
    
    def jeton(self):
        return JETONS[self.etat]


class Motif:
    
    def __init__(self, solitaire, dico_encoches):
        self.solitaire = solitaire
        self.encoches = {(x, self.solitaire.h - 1 - y):Encoche(self, x, self.solitaire.h - 1 - y, etat=dico_encoches[x, y]) for x, y in dico_encoches}

    def copy(self):
        motif = Motif(self.solitaire, {})
        for x, y in self.encoches:
            motif.encoches[x, y] = Encoche(motif, x, y, etat=self.encoche(x, y).etat)
        return motif

    def __iter__(self):
        return self.encoches.__iter__()

    def encoche(self, *args):
        return self.encoches[to_coords(*args)]

    def nb_pleins(self):
        return sum(encoche.on() for encoche in self.encoches.values())
    
    def nb_blocked(self):
        return sum(encoche.on() and encoche.blocked() for encoche in self.encoches.values())

    def score(self):
        nb_pleins, nb_blocked = 0, 1
        for encoche in self.encoches.values():
            if encoche.on():
                nb_pleins += 1
                if encoche.blocked():
                    nb_blocked += 1
        return nb_pleins / nb_blocked

    def __str__(self):
        s = ''
        w, h = self.solitaire.w, self.solitaire.h
        width = len(str(to_num(w, h)))
        for y in range(h-1, -1, -1):
            s += f'{y} ' # numéro de la ligne
            for x in range(w):
                if self.inside(x, y):
                    s += f' {self.encoche(x, y).jeton():2}'
                else:
                    s += '   '
            s += '\n'
        # ajout des numéros de colonne
        s += f"  {' '.join(f'{x:{width}}' for x in range(w))}"
        return s

    def aff(self, tab=0):
        str_tab = '\t'*tab
        s = ''
        w, h = self.solitaire.w, self.solitaire.h
        width = len(str(to_num(w, h)))
        for y in range(h-1, -1, -1):
            s += str_tab + f'{y} ' # numéro de la ligne
            for x in range(w):
                if self.inside(x, y):
                    s += f' {self.encoche(x, y).jeton():2}'
                else:
                    s += '   '
            s += '\n'
        # ajout des numéros de colonne
        s += str_tab + f"  {' '.join(f'{x:{width}}' for x in range(w))}"
        return s

    def __eq__(self, m):
        return self.num() == m.num()
    
    def num(self):
        return sum(encoche.encode() for encoche in self.encoches.values() if encoche.on())

    def valide(self, direction, *args):
        direction = direction.upper()
        if direction in DIRECTIONS:
            x, y = to_coords(*args)
            dx, dy = DIRECTIONS[direction]
            nx, ny = x+dx, y+dy
            return self.on(x, y) and self.off(nx, ny)
        return False
    
    def switch(self, *args):
        self.encoche(*args).switch()
        
    def on(self, *args):
        return self.inside(*args) and self.encoche(*args).on()

    def off(self, *args):
        return self.inside(*args) and self.encoche(*args).off()
    
    # -- Voisins d'une case

    def coords_voisines(self, x, y, n=1):
        return {(x+i*dx, y+i*dy) for dx, dy in DIRECTIONS.values() for i in range(n)}

    # -- from solitaire

    def coordonnees(self):
        return self.solitaire.coordonnees

    def inside(self, *args):
        return self.solitaire.inside(*to_coords(*args))


    # ---
    # --- Coups simples et composés

    def coup_possible(self, direction, x, y):
        dx, dy = DIRECTIONS[direction.upper()]
        voisin = x+dx, y+dy
        arrivee = x+2*dx, y+2*dy
        return self.on(*voisin) and self.off(*arrivee)            

    def joue_un_coup(self, direction, *args):
        x, y = to_coords(*args)
        motif = None
        dx, dy = DIRECTIONS[direction.upper()]
        voisin = x+dx, y+dy
        arrivee = x+2*dx, y+2*dy
        if self.on(*voisin) and self.off(*arrivee):            
            motif = self.copy()
            motif.switch(x, y)
            motif.switch(*voisin)
            motif.switch(*arrivee)
        return motif, arrivee

    def joue_coups(self, directions, *args):
        motif = self.copy()
        arrivee = to_coords(*args)
        for direction in directions.upper():
            motif, arrivee = motif.joue_un_coup(direction, *arrivee)
            if motif is None:
                break
        return motif

    # --- Les 3 fonctions au coeur de la résolution

    def coup_simple(self, *args):
        """calcul la liste des motifs obtenus par un coup dans chacune des directions possibles (ie qui ne font pas sortir du tablier)"""
        coups = []
        if self.on(*args):
            x, y = to_coords(*args)
            for direction in DIRECTIONS:
                motif, arrivee = self.joue_un_coup(direction, x, y)
                if motif is not None:
                    coups.append((motif, direction, arrivee))
        return coups 
                    
    def coup_compose(self, *args):
        coups = []
        if self.on(*args):
            a_traiter = self.coup_simple(*args)
            while a_traiter:
                coup, direction, arrivee = a_traiter.pop(0)
                coups.append((coup, direction, arrivee))
                for motif, one_dir, arr in coup.coup_simple(*arrivee):
                    voisin = motif, direction+one_dir, arr
                    if voisin not in a_traiter and voisin not in coups:
                        a_traiter.append(voisin)        
        return coups
    
    def mouvements(self):
        return [(m1, (x, y), directions) for (x, y) in self.encoches for (m1, directions, _) in self.coup_compose(x, y)]
    

# ===
# === SOLITAIRE 

class Solitaire:

    def __init__(self, racine_fichier, rep=DEFAULT_REP):
        init_filename = f'{rep}/{racine_fichier}I.txt'
        final_filename = f'{rep}/{racine_fichier}F.txt' 
        w, h, dico_initial = self.load(init_filename)
        w2, h2, dico_final = self.load(final_filename)
        if w == w2 and h == h2 and dico_initial.keys() == dico_final.keys():
            self.w, self.h = w, h
            self.coordonnees = dico_initial.keys()
            self.initial = Motif(self, dico_initial)
            self.final = Motif(self, dico_final)
            self.current_id = 0
            self.partie = [(self.initial, None, '')]
            self.explored = 0
        else:
            raise Exception('Fichiers incohérents')
        
    
    def numeros(self):
        s = ''
        width = len(str(to_num(self.w, self.h)))
        for y in range(self.h-1, -1, -1):
            s += f'{y} '
            for x in range(self.w):
                if self.inside(x, y):
                    s += f' {to_num(x,y):2}'
                else:
                    s += '   '
            s += '\n'
        s += f"   {' '.join(f'{x:{width}}' for x in range(self.w))}"
        print(s)

    def __str__(self):
        motif, coords, directions = self.partie[self.current_id]
        s = f'{self.current_id + 1}/{len(self.partie)} -- {coords}:{directions}\n'
        return s+motif.__str__()

    def aff(self, tab=0):
        motif, coords, directions = self.partie[self.current_id]
        s = '\t'*tab + f'{self.current_id + 1}/{len(self.partie)} -- {coords}:{directions}\n'
        s += motif.aff(tab)
        s += '\n'
        return s


    def inside(self, x, y):
        return (x, y) in self.coordonnees

    def goal(self, tab=0):
        """Affiche côte à côte le motifs initial et final"""
        lI = self.initial.aff(tab).split('\n')
        lF = self.final.__str__().split('\n')
        width = len(lI[0])
        str_tab = '\t' * tab
        fusion = [f'{str_tab}{" Initial ":-^{width}}     {" Final ":-^{width}}'] + [f'{lI[i]}     {lF[i]}' for i in range(len(lI))]
        return '\n'.join(fusion)

    def load(self, filename):
        """A partir d'un fichier texte décrivant un motif, retourne la largeur et la hauteur ainsi qu'un dictionnaire des encoches pleines et vides"""
        with open(filename) as entree:
            encoches = {}
            w, h = 0, 0
            for y, ligne in enumerate(entree):
                for x, val in enumerate(ligne[:-1]):
                    if val == VIDE_STR:
                        encoches[x, y] = VIDE
                    elif val.lower() == PLEIN_STR:
                        encoches[x, y] = PLEIN
                    w = max(w, x+1)
            h = y+1
            return w, h, encoches


    # --- La résolution ---

    def add_and_mem(self, dico, motif, s):
        num = motif.num()
        if num not in dico:
            dico[num] = s
            return True
        return False

    def strate(self, dico, motifs):
        ajoutes = []
        for m in motifs:
            for new_m, coords, direction in m.mouvements():
                if self.add_and_mem(dico, new_m, (m, coords, direction)):
                    ajoutes.append(new_m)
        return ajoutes

    def solve(self, limit=INFINI, debug=False):
        d_resultat = {}
        d_pred = {self.initial.num():(None, None, '')}
        file_a_traiter = [self.initial]
        num_final = self.final.num()
        while num_final not in d_resultat and file_a_traiter:
            self.explored += 1
            m = file_a_traiter.pop(0)
            if debug:
                print(f'On explore (score {m.score()})')
                print(m)
            num = m.num()
            pred, coords, directions = d_pred.pop(num)
            if num not in d_resultat:
                d_resultat[num] = pred, coords, directions
            next_moves = sorted(self.strate(d_pred, [m]), key=lambda t: t.score(), reverse=True)
            n = min(limit, len(next_moves))
            if debug:
                print('--- Non suivi :')
                for i in range(n, len(next_moves)):
                    print(next_moves[i])
                print('--- Suivi')
            for i in range(n):
                if debug:
                    print(next_moves[i])                    
                file_a_traiter.append(next_moves[i])
            if debug:
                print('--- Fin ---')
        if num_final in d_resultat:
            self.create_path(d_resultat)
            return True
        else:
            return False
    


    # --- Mettre en forme la solution ---

    def create_path(self, d_result):
        reverse_path = []
        motif = self.final
        motif_num = motif.num()
        initial_num = self.initial.num()
        while motif_num != initial_num:
            pred, coords, directions = d_result[motif_num]
            reverse_path.append((motif.copy(), coords, directions))
            motif = pred
            motif_num = motif.num()
        reverse_path.reverse()
        self.partie.extend(reverse_path)

    def trace(self, tab=0):
        nb = len(self.partie)
        s = ''
        str_tab = '\t' * tab
        for i, (m, coords, d) in enumerate(self.partie):
            s += f'{str_tab}--- motif {i+1}/{nb} {coords}:{d}\n'
            s += m.aff(tab)
            s += '\n'
        return s

    def nb_coups(self):
        return sum(len(d) for _, _, d in self.partie)

    def coups(self):
        return [f'{x},{y}:{d}' for (_, (x, y), d) in self.partie[1:]]

    def last(self):
        return self.partie[-1]

    def move(self, directions, *args):
        last_move = self.last()
        motif = last_move.joue_coups(directions.upper(), *to_coords(*args))
        if motif is not None:
            self.partie.append(motif)
            self.fd()
        else:
            print('Coup invalide... motif inchangé')
            print(self)

    def bk(self):
        n = len(self.partie)
        self.current_id = (self.current_id - 1) % n
        print(f'{self.current_id}/{n-1}')
        print(self)

    def fd(self):
        n = len(self.partie)
        self.current_id = (self.current_id + 1) % n
        print(f'{self.current_id}/{n-1}')
        print(self)

    def solution(self, list_of_nums):
        """list_of_nums etant une liste de couple num_depart, num_arrivee cette fonction joue les coups de cette liste et range les motifs obtenus dans self.partie... si le dernier motif correspond à self.final retourne True, False sinon"""
        for num_dep, num_arr in list_of_nums:
            d = NUMS_TO_DIR[num_arr - num_dep]
            coords_dep = to_coords(num_dep)
            motif, coords_arr = self.partie[self.current_id][0].joue_un_coup(d, *coords_dep)
            self.partie.append((motif, coords_dep, d))
            self.current_id += 1
        self.current_id = 0
        return self.partie[-1][0] == self.final

    
