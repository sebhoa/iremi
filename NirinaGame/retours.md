# Retours d'expériences pédagogiques

## Mode Hybride

### Georges Jonkisz -- cours Electrocinétique

- Matériel : tablette connectée à l'ordinateur + Document word annoté en direct (plus d'espace que le tableau blanc de Zoom trop limité)
- Plus : plus visible que le tableau noir filmé
- Moins : Résolution du vidéo projecteur trop basse implique une perte d'espace. Manipulations nombreuses : il faudrait une salle installée en permanence, avec 