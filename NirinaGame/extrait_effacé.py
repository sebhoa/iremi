                # La portion de code ci-après permet de simplifier les parties
                # trop longues puis de mémoriser dans un fichier texte
                #
                # if self.last_config() in self.blocked():
                #     print(f'Avant simplification : {len(self.configs())} coups')
                #     self.model.simplify()
                #     print(f'Après simplification : {len(self.configs())} coups')
                #     self.memorize()
                #     print(f'Une partie sauvée')
                #     pause()
                # met à jour un fichier json qui contient le décompte des
                # des victoires de rouge en fonction de la config finale
                # ensuite on lance le script red.py pour voir le % de victoires
                # par blocage et % par atteinte position finale