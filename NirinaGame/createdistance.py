import json


ALLCFGS = 'all_configs.json'

def final(cfg):
    player = int(cfg[-1])
    if allcfgs[cfg] != [] or player == 1 and cfg.startswith('0,1,2'):
        return -1
    else:
        return int(cfg[-1])

def nearest(cfg, d_dist):
    crt_cfg = cfg
    dist = 0
    a_traiter = [(cfg, 0)]
    s_a_traiter = set(cfg)
    deja_vu = set()
    winner = 1 - final(crt_cfg)
    while a_traiter and winner != int(cfg[-1]):
        # if cfg == '1,6,11,4,7,9,1':
        #     print(f'NB A traiter : {len(a_traiter)}')
        crt_cfg, dist = a_traiter.pop(0)
        deja_vu.add(crt_cfg)
        # print(f'On traite {crt_cfg}')
        # input()
        winner = 1 - final(crt_cfg)
        if winner != int(cfg[-1]):
            s_a_traiter.discard(crt_cfg)
            voisins = {ncfg for ncfg in allcfgs[crt_cfg] if ncfg not in deja_vu and ncfg not in s_a_traiter}
            for ncfg in voisins:
                a_traiter.append((ncfg, dist+1))
                s_a_traiter.add(ncfg)
    d_dist[cfg] = (crt_cfg, dist)


with open(ALLCFGS) as f_in:
    allcfgs = json.load(f_in)


d_dist = {}
# nearest('1,6,11,4,7,9,1', d_dist)
# print(d_dist['1,6,11,4,7,9,1'])
# input()
for index, cfg in enumerate(allcfgs):
    print(f'{index:05}',end='\b'*5, flush=True)
    # if index in (25231, ):
    #     print(f'\n{cfg}')
    # else:
    nearest(cfg, d_dist)
    # print(cfg, d_dist[cfg])
    # input()

with open('nearest_dist.json', 'w') as f_out:
    json.dump(d_dist, f_out, indent=3)




