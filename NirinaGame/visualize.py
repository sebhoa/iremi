import sys

NAMES = ['RED', 'BLUE']
SEPS = [    ' | \\    |     / |', 
            ' |    \\ |  /    |',
            ' |    / |  \\    |',
            ' | /    |     \\ |' ]
MARKERS = [{'open':'(', 'close':')'}, {'open':'[', 'close':']'}]

def player(cfg):
    return int(cfg[-1])

def move_played(from_cfg, to_cfg):
    """
    Retourne le couple d'entier correspondant au coup joué
    pur passer de la config from_cfg à la config to_cfg
    """
    who = player(from_cfg)
    from_set = set(from_cfg.split(',')[who*3:(who+1)*3])
    to_set = set(to_cfg.split(',')[who*3:(who+1)*3])
    dep = int((from_set - to_set).pop())
    arr = int((to_set - from_set).pop())
    return dep, arr

def view_one(cfg):
    positions = [int(e) for e in cfg.split(',')[:-1]]
    s_print = ''
    l_elts = [[0,5,10],0,[3,8],1,[1,6,11],2,[4,9],3,[2,7,12]]
    l = []
    for elt in l_elts:
        if type(elt) == int:
            l.append(SEPS[elt])
        elif len(elt) == 3:
            l.append('---'.join([f' {e:02} ' for e in elt]))
        else:
            s = ' |  '.join([f' {e:02} ' for e in elt])
            l.append(f' | {s} |')
    s_print += '\n'.join(l)
    for index, pos in enumerate(positions):
        player = index // 3
        o = MARKERS[player]["open"]
        c = MARKERS[player]["close"]
        pattern = f' {pos:02} '
        sub = f'{o}{pos:02}{c}'
        s_print = s_print.replace(pattern, sub)
    print(s_print)



def view_all(cfgs):
    move = ''
    for index, cfg in enumerate(cfgs[:-1]):
        print(f'-- {index:02} / {len(cfgs):02}')
        p = 1 - player(cfg)
        if move:
            print(f'{NAMES[p]} played {move[0]} -> {move[1]}')
        view_one(cfg)
        move = move_played(cfg, cfgs[index+1]) 
        input()
    view_one(cfgs[-1])


filename = sys.argv[1]
with open(filename) as f_in:
    cfgs = [line[:-1] for line in f_in]
view_all(cfgs)
