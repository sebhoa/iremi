"""
Jeu des parkings
Version objet et avec déjà toutes le configs possibles
Config initiale [ ] = Blue, ( ) = Red

     [00]--- 05 ---(10)
      | \    |     / |
      |  03  |   08  |
      |    \ |  /    |
     [01]--- 06 ---(11)
      |    / | \     |
      |  04  |   09  |   
      | /    |    \  |
     [02]--- 07 ---(12)

module à importer
"""


import json
import sys
import argparse
import random
import datetime

# -------------
# -- CONSTANTES
# -------------


RED = 0
BLUE = 1
NULL = 2
NAMES = ['Red', 'Blue', 'Nulle']

INIT_CFG = '10,11,12,0,1,2,0'
# INIT_CFG = '1,5,12,0,2,6,0'
# INIT_CFG = '4,5,6,7,10,11,0'
# INIT_CFG = '2,3,9,7,10,11,0'


INF = float('inf')

def pause():
    input('Press return to continue...')

def tirage(distribution):
    total = sum(nb for _, nb in distribution)
    rnd = random.randint(0, total-1)
    s = 0
    for value, nb in distribution:
        if s <= rnd < s+nb:
            return value
        s += nb

# ----------
# -- CLASSES
# ----------

class Game:

    """ 
    En charge de la mécanique de jeu pour 1 partie : 
    """

    def __init__(self, red_strat, blue_strat, allcfgs, memories, initial_cfg):
        # board code le graphe du jeu par listes d'adjacence
        self.board = [  {1,3,5},{0,2,6},{1,4,7},{0,6},
                        {2,6},{0,6,10},{1,3,4,5,7,8,9,11},{2,6,12},
                        {6,10},{6,12},{5,8,11},{6,10,12},{7,9,11} ]
        # allcfgs code toutes les configurations possibles du jeu
        # a été calculé précédemment et stocké dans un fichier json
        self.allcfgs = allcfgs
        self.blockedcfgs = {k for k in self.allcfgs if self.allcfgs[k] == set() 
                                and int(k[-1]) == BLUE and not k.startswith('0,1,2')}
        self.memories = memories

        strategies = {
            'alea': self.alea,
            'oriented': self.oriented,
            'liberties': self.less_liberties,
            'bayesien': self.bayesien
        }

        self.initial_cfg = initial_cfg
        self.strategies = [strategies[red_strat], strategies[blue_strat]] 
        self.reset()


    def reset(self):
        self.config = self.initial_cfg  # la configuration courante
        self.player = RED # ID du joueur courant (ROUGE commence)
        self.gameover = False
        self.trace = [INIT_CFG]   # pile des configs     



    # -- SMALLS FUNCTIONS
    # --

    def move_played(self, from_cfg, to_cfg):
        """
        Retourne le couple d'entier correspondant au coup joué
        pour passer de la config from_cfg à la config to_cfg
        notez la puissance des opérations ensemblistes
        """
        who = self.player(from_cfg)
        from_set = self.positions(from_cfg, who)
        to_set = self.positions(to_cfg, who)
        dep = (from_set - to_set).pop()
        arr = (to_set - from_set).pop()
        return dep, arr

    def whos_next(self, config=''):
        if config:
            return int(config[-1])
        else:
            return int(self.config[-1])

    def losing(self, cfg):
        """
        cfg est une configuration perdante si parmi les
        coups possibles après, pour l'adversaire il y a
        un coup gagnant
        """
        for ncfg in self.allcfgs[cfg]:
            if not self.allcfgs[ncfg]:
                return True
        return False

    def positions(self, cfg, player):
        """
        Retourne l'ensemble des 3 entiers qui
        représentent les positions occupées par
        player dans la configuration cfg
        """
        return {int(e) for e in cfg.split(',')[player*3:(player+1)*3]}

    def liberties(self, cfg, player):
        """ 
        Retourne le nombre de libertés de la configuration cfg
        pour le joueur player, ie le nombre de cases jouables
        """
        pos = self.positions(cfg, player)
        pos_op = self.positions(cfg, 1 - player)
        adj_pos = {v for p in pos for v in self.board[p]}
        return len(adj_pos - pos - pos_op)

    def final_cfg(self):
        return self.allcfgs[self.config] == []

    def winning_next(self, possibilities):
        for cfg, _ in possibilities:
            if self.allcfgs[cfg] == set():
                return cfg

    def next_player(self):
        self.player = 1 - self.player

    # -- STRATEGIES FUNCTIONS
    # --

    def alea(self):
        cfg = self.config
        possibilities = [(ncfg, 0) for ncfg in self.allcfgs[cfg]]
        return random.choice(possibilities)[0]

    def oriented(self):
        cfg = self.config
        possibilities = [(ncfg, self.move_played(cfg, ncfg)[1]) 
                                        for ncfg in self.allcfgs[cfg]]
        possibilities.sort(key=lambda e: e[1], reverse=bool(p))
        winning_cfg = self.winning_next(possibilities)
        if winning_cfg is not None:
            return winning_cfg
        else:
            index = int(random.triangular(0, len(possibilities), 0))
            return possibilities[index][0]

    
    def centrale(self, cfg, player):
        return 6 in self.positions(cfg, player)
    
    def minimum_liberties(self, cfg, player, possibilities):
        nb_liberties = INF
        best_cfg = None
        for ncfg, liberties in possibilities:
            if not self.losing(ncfg) and (nb_liberties > liberties\
                or nb_liberties == liberties and self.centrale(cfg, player)):
                best_cfg = ncfg
                nb_liberties = liberties
    
    def less_liberties(self):
        p = self.player
        cfg = self.config
        possibilities = [(new_cfg, self.liberties(new_cfg, 1 - p)) 
                                        for new_cfg in self.allcfgs[cfg]]
        best_cfg = self.minimum_liberties(cfg, possibilities)
        if best_cfg is None:
            best_cfg = self.alea()
        return best_cfg

    def bayesien(self):
        cfg = self.config
        chance_to_win = self.memories[self.player]
        possibilities = [(ncfg, chance_to_win[ncfg]) 
                                for ncfg in self.allcfgs[cfg]]
        possibilities.sort(key=lambda e: e[1], reverse=True)
        try:
            return tirage(possibilities)
        except:
            return random.choice(possibilities)[0]

    
    # -- LA FONCTION QUI JOUE 
    # -- une partie et un coup

    def play(self):
        new_cfg = self.strategies[self.player]()
        self.trace.append(new_cfg)
        self.config = new_cfg
        return self.final_cfg()

    def a_game(self):
        while not self.gameover:
            self.gameover = self.play()
            self.next_player()
        return 1 - self.player, self.trace

