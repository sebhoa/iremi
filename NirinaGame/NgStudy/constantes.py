RED = 0
BLUE = 1
NULL = 2
NAMES = ['Red', 'Blue', 'Nulle']

INIT_CFG = 'ABC0120'

INF = float('inf')

# CFGS_FILE = '../all_configs.json'
# MEMORIES_FILES = ('red_memory.json', 'blue_memory.json')
CFGS_FILE = '../brassards.json'
MEMORIES_FILES = ('red_memory_b.json', 'blue_memory_b.json')
MOTIFS = ('◾', '◽', '_')
# MOTIFS = ('°', '*')
NODES = '0123456789ABC'

GRAPH = {'0':'135', '1':'026', '2':'147', '3':'06', '4':'26', '5':'06A', '6':'1345789B', '7':'26C', '8':'6A', '9':'6C', 'A':'58B', 'B':'6AC', 'C':'79B'}
ADJ = {'0':'135', '1':'26', '2':'47', '3':'6', '4':'6', '5':'6A', '6':'789B', '7':'C', '8':'A', '9':'C', 'A':'B', 'B':'C', 'C':''}
DISTANCES = [
    {'0':0, '1':0, '2':0, 
     '3':1, '4':1, '5':1, '6':1, '7':1, 
     '8':2, '9':2, 'A':2, 'B':2, 'C':2},
    {'A':0, 'B':0, 'C':0, 
     '5':1, '6':1, '7':1, '8':1, '9':1, 
     '0':2, '1':2, '2':2, '3':2, '4':2},
]