import ngm
import time
import json
import random
import graphviz
from IPython.display import clear_output


CFGS_FILE = '../all_configs.json'
MEMORIES_FILES = ('red_memory.json', 'blue_memory.json')
MOTIFS = ('◾', '◽')
# MOTIFS = ('°', '*')


class NgStudy:
    """Pour etudier le jeu de Nirina les paramètres sont :
    - red_strat : stratégie de ROUGE parmi alea, oriented, liberties et bayesien
    - blue_strat : stratégie de BLEU
    - nb_rounds : nombre de manches (1 par défaut)
    - nb_games : nombre de parties par manche (100 par défaut)
    - initial : la configuration initiale par défaut '10,11,12,0,1,2,0'
    """
        
    def __init__(self, red_strat='alea', blue_strat='alea', nb_rounds=1,nb_games=100, initial=ngm.INIT_CFG, learning=False, live=False):
        self.initial = initial
        self.nb_rounds = nb_rounds
        self.nb_games = nb_games
        self.scores = [0, 0]
        self.allcfgs = {}
        
        self.learning = learning
        self.live = live
        self.trace = []
        self.results_file = f"results_{time.strftime('%Y%m%d%H%H')}.csv"        
        self.results = []
        
        self.memories = [None, None]
        self.load_json()
        self.game = ngm.Game(red_strat, blue_strat, self.allcfgs, self.memories, self.initial)

        # Pour la visualisation via graphviz
        self.init_vue()
        
    def load_json(self):
        with open(CFGS_FILE) as jsonfile:
            self.allcfgs = json.load(jsonfile)
        for player in (ngm.RED, ngm.BLUE):
            try:
                with open(MEMORIES_FILES[player]) as jsonfile:
                    self.memories[player] = json.load(jsonfile)
            except FileNotFoundError:
                self.init_json(player, MEMORIES_FILES[player])

    def init_json(self, player, filename):
        self.memories[player] = {cfg:1 for cfg in self.allcfgs if int(cfg[-1]) 
                                                                == 1 - player}
        with open(filename, 'w') as jsonfile:
            json.dump(self.memories[player], jsonfile, indent=3) 
    
    def init_vue(self):
        l_adj = [ {1,3,5},{2,6},{4,7},{6},{6},{6,10},{7,8,9,11},{12},
        {10},{12},{11},{12},{}]
        g = graphviz.Graph(engine='neato', graph_attr={'size':'2.2,2.2!'}, node_attr={'shape': 'circle', 'fixedsize':'true'})
        for node_id in range(13):
            g.node(str(node_id))
            for voisin in l_adj[node_id]:
                g.edge(str(node_id), str(voisin))
        self.vue = g

    def reset_game(self):
        self.trace = []
        self.trace_file = f"traces_{time.strftime('%Y%m%d%H%M%S')}.json"
        self.game.reset()
            
    def reset_round(self):
        self.scores = [0, 0]

    def whos_played(self, cfg):
        return 1 - int(cfg[-1])


    def learn(self):
        winner = self.winner()
        for cfg in self.trace:
            if self.whos_played(cfg) == winner:
                self.memories[winner][cfg] += 1

    def save_scores(self):
        self.results.append(tuple(self.scores))
            
    def save_game(self):
        with open(self.trace_file, 'w') as jsonfile:
            json.dump(self.trace, jsonfile, indent=3)

    def save_memories(self):
        for player in (ngm.RED, ngm.BLUE):
            with open(MEMORIES_FILES[player], 'w') as jsonfile:
                json.dump(self.memories[player], jsonfile, indent=3)
         
    def print_scores(self, verbose):
        barre_rouge = self.scores[ngm.RED] * MOTIFS[ngm.RED]
        barre_bleue = self.scores[ngm.BLUE] * MOTIFS[ngm.BLUE]
        if verbose:
            print(f'{barre_rouge}{barre_bleue}')
        else:
            back = '\b' * self.nb_games
            clear_output(True)
            print(f'{barre_rouge}{barre_bleue}', end=back, flush=True)



    # -------
    # --- API
    # -------

    # Modifier l'objet

    def simplify(self):
        """
        Simplifie la liste des configurations : qd on rencontre
        une configuration déjà rencontrée, on peut dépiler toutes
        les configs entre : c'était une grande boucle pour rien :)
        """
        i = 1
        cfgs = self.trace
        while i < len(cfgs):
            if cfgs[i] in cfgs[:i]:
                k = cfgs.index(cfgs[i])
                cfgs[k+1:i+1] = []
                i = k+1
            else:
                i += 1

    def more_game(self, nb_games):
        self.nb_games = nb_games

    def more_rounds(self, nb_rounds):
        self.nb_rounds = nb_rounds

    def colorise(self, cfg_id):
        cfg = self.trace[min(self.size(), cfg_id)]
        colors = ('coral1', 'cyan3')
        positions = cfg.split(',')[:-1]
        for pos in range(13):
            pos = str(pos)
            try:
                indice = positions.index(pos)
                color = colors[indice//3]
                self.vue.node(pos, style='filled', fillcolor=color)
            except:
                self.vue.node(pos, style='filled', fillcolor='white')

    def reset_memories(self):
        for player in (ngm.RED, ngm.BLUE):
            self.init_json(player, MEMORIES_FILES[player])

    # Infos

    def size(self):
        return len(self.trace)

    def winner(self):
        return 1 - int(self.trace[-1][-1])


    def start(self, verbose=False):
        for round_id in range(self.nb_rounds):
            for game_id in range(self.nb_games):
                self.reset_game()
                winner, self.trace = self.game.a_game()
                self.scores[winner] += 1
                if self.learning:
                    self.simplify()
                    self.learn()
                #self.save_game()
            if self.live:
                self.print_scores(verbose)
                #time.sleep(0.2)
            self.save_scores()
            self.save_memories()
            self.reset_round()
                