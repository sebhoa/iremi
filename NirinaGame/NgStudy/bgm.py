"""
Jeu des parkings
Version objet et avec déjà toutes le configs possibles
Config initiale [ ] = Blue, ( ) = Red

     [0]--- 5 ---(A)
      | \   |   / |
      |  3  |  8  |
      |   \ | /   |
     [1]--- 6 ---(B)
      |   / | \   |
      |  4  |  9  |   
      | /   |   \ |
     [2]--- 7 ---(C)

module à importer
variante : 5 coups possibles par jeton
"""


import json
import sys
import argparse
import random
import datetime

# -------------
# -- CONSTANTES
# -------------

from constantes import *


def pause():
    input('Press return to continue...')

def tirage(distribution):
    total = sum(nb for _, nb in distribution)
    rnd = random.randint(0, total-1)
    s = 0
    for value, nb in distribution:
        if s <= rnd < s+nb:
            return value
        s += nb

# ----------
# -- CLASSES
# ----------


class Config:
    
    def __init__(self, nb=5, pions=None, joueur=RED):
        self.nb = nb
        if pions is None:
            self.pions = [[[pos, nb] for pos in 'ABC'], [[pos, nb] for pos in '012']]
        else:
            self.pions = pions
        self.joueur = joueur
    
    def __str__(self):
        red = ' '.join(pos+str(n) for pos, n in self.pions[RED])
        blue = ' '.join(pos+str(n) for pos, n in self.pions[BLUE])
        return f'{red} -- {blue} [{("ROUGE", "BLEU")[self.joueur]}]'
    
    def cle(self):
        red = ''.join(sorted(pos for pos, _ in self.pions[RED]))
        blue = ''.join(sorted(pos for pos, _ in self.pions[BLUE]))
        return f'{red}{blue}{self.joueur}'
    
    def copy(self):
        return Config(self.nb, [[[pos, n] for pos, n in self.pions[j]] for j in (RED, BLUE)], self.joueur)
    
    def position(self, pion_id, joueur=None):
        if joueur is None:
            joueur = self.joueur
        # print(self.pions, pion_id)
        return self.pions[joueur][pion_id][0]
        
    def nb_move(self, pion_id, joueur=None):
        if joueur is None:
            joueur = self.joueur
        return self.pions[joueur][pion_id][1]
    
    def suivant(self):
        self.joueur = 1 - self.joueur
    
    def occupees(self):
        return ''.join(pos for j in (RED, BLUE) for pion_id in range(3) for pos in self.position(pion_id, j))
    
    def candidats(self, pion_id):
        if self.nb_move(pion_id) > 0:
            pos = self.position(pion_id)
            # print(pos)
            oqp = self.occupees()
            return ''.join(voisin for voisin in GRAPH[pos] if voisin not in oqp)
        else:
            return ''
    
    def red_final(self):
        return self.cle().startswith('012')
    
    def blue_final(self):
        return self.cle().endswith('ABC0')
    
    def blocked(self):
        return all(self.candidats(pion_id) == '' for pion_id in range(3))

    def blocked_by(self):
        return all(self.candidats(pion_id) == '' for pion_id in range(3))    
    
    def no_more_move(self):
        return all(self.nb_move(pion_id) == 0 for pion_id in range(3))
    
    def final(self):
        return self.red_final() or self.blue_final() or self.blocked()
    
    

    def best_distance(self):
        red_dist = sum(DISTANCES[RED][pos] for pion_id in range(3) for pos in self.position(pion_id, RED))
        blue_dist = sum(DISTANCES[BLUE][pos] for pion_id in range(3) for pos in self.position(pion_id, BLUE))
        if red_dist < blue_dist:
            return RED
        elif blue_dist < red_dist:
            return BLUE
        else:
            return NULL

    
    def can_move(self, joueur):
        return any(self.nb_move(pion_id, joueur) > 0 for pion_id in range(3))

    def winner(self):
        if self.red_final() or self.can_move(RED):
            return RED
        elif self.blue_final() or self.can_move(BLUE):
            return BLUE
        else:
            return self.best_distance()

    def random_move(self):
        c = self.copy()
        ids = list(range(3))
        random.shuffle(ids)
        for pion_id in ids:
            candidats = self.candidats(pion_id)
            if candidats:
                break
        if candidats:
            choix = random.randrange(len(candidats))
            c.pions[self.joueur][pion_id] = [candidats[choix], self.nb_move(pion_id) - 1]
            c.suivant()
        return c



class Game:

    """ 
    En charge de la mécanique de jeu pour 1 partie : 
    """

    def __init__(self, red_strat, blue_strat, allcfgs, memories, initial_cfg):
        # board code le graphe du jeu par listes d'adjacence
        self.board =  {'0':'135', '1':'026', '2':'147', '3':'06', '4':'26', '5':'06A', '6':'345789', '7':'26C', '8':'6A', '9':'6C', 'A':'58B', 'B':'6AC', 'C':'79B'}
        # allcfgs code toutes les configurations possibles du jeu
        # a été calculé précédemment et stocké dans un fichier json
        self.allcfgs = allcfgs
        # self.blockedcfgs = {k for k in self.allcfgs if self.allcfgs[k] == set() 
        #                         and int(k[-1]) == BLUE and not k.startswith('0,1,2')}
        self.memories = memories

        strategies = {
            'alea': self.alea,
            'oriented': self.oriented,
            'liberties': self.less_liberties,
            'bayesien': self.bayesien
        }

        self.initial_cfg = Config()
        self.strategies = [strategies[red_strat], strategies[blue_strat]] 
        self.reset()


    def reset(self):
        self.config = self.initial_cfg  # la configuration courante
        self.player = RED # ID du joueur courant (ROUGE commence)
        self.gameover = False
        self.trace = [self.config.copy()]

    def memorise(self):
        self.trace.append(self.config.copy())


    # -- SMALLS FUNCTIONS
    # --

    # def move_played(self, from_cfg, to_cfg):
    #     """
    #     Retourne le couple d'entier correspondant au coup joué
    #     pour passer de la config from_cfg à la config to_cfg
    #     notez la puissance des opérations ensemblistes
    #     """
    #     who = int(from_cfg[-1])
    #     from_set = self.positions(from_cfg, who)
    #     to_set = self.positions(to_cfg, who)
    #     dep = (from_set - to_set).pop()
    #     arr = (to_set - from_set).pop()
    #     return dep, arr

    # def whos_next(self, config=''):
    #     if config:
    #         return int(config[-1])
    #     else:
    #         return int(self.config[-1])

    # def losing(self, cfg):
    #     """
    #     cfg est une configuration perdante si parmi les
    #     coups possibles après, pour l'adversaire il y a
    #     un coup gagnant
    #     """
    #     for ncfg in self.allcfgs[cfg]:
    #         if not self.allcfgs[ncfg]:
    #             return True
    #     return False

    # def positions(self, cfg, player):
    #     """
    #     Retourne l'ensemble des 3 entiers qui
    #     représentent les positions occupées par
    #     player dans la configuration cfg
    #     """
    #     return {int(e) for e in cfg.split(',')[player*3:(player+1)*3]}

    def liberties(self, cfg, player):
        """ 
        Retourne le nombre de libertés de la configuration cfg
        pour le joueur player, ie le nombre de cases jouables
        """
        pos = self.positions(cfg, player)
        pos_op = self.positions(cfg, 1 - player)
        adj_pos = {v for p in pos for v in self.board[p]}
        return len(adj_pos - pos - pos_op)

    def final(self):
        return self.config.final()

    def winning_next(self, possibilities):
        for cfg, _ in possibilities:
            if self.allcfgs[cfg] == set():
                return cfg

    def next_player(self):
        self.player = 1 - self.player

    def winner(self):
        return self.config.winner()

    def possibilities(self):
        """Retourne la liste des mouvements possibles à partir de la cfg courante"""
        cfg = self.config
        results = []
        for pion_id in range(3):
            candidats = cfg.candidats(pion_id)
            for choix in candidats:
                new_cfg = cfg.copy()
                # print(new_cfg)
                # print(new_cfg.pions)
                # print('joueur (frm cfg):', cfg.joueur)
                # print('joueur (from game):', self.player)
                new_cfg.pions[cfg.joueur][pion_id] = [choix, cfg.nb_move(pion_id) - 1]
                new_cfg.suivant()
                results.append(new_cfg)
        return results


    # -- STRATEGIES FUNCTIONS
    # --

    # Attention suite aux changements pour intégrer la version
    # brassards, seuls les stratégies alea et bayesien fonctionnent 


    def alea(self):
        return random.choice(self.possibilities())

    def oriented(self):
        cfg = self.config
        possibilities = [(ncfg, self.move_played(cfg, ncfg)[1]) 
                                        for ncfg in self.allcfgs[cfg]]
        winning_cfg = self.winning_next(possibilities)
        if winning_cfg is not None:
            return winning_cfg
        else:
            possibilities.sort(key=lambda e: e[1], reverse=bool(self.player))
            index = int(random.triangular(0, len(possibilities), 0))
            return possibilities[index][0]

    
    def centrale(self, cfg, player):
        return 6 in self.positions(cfg, player)
    
    def minimum_liberties(self, cfg, player, possibilities):
        nb_liberties = INF
        best_cfg = None
        for ncfg, liberties in possibilities:
            if not self.losing(ncfg) and (nb_liberties > liberties\
                or nb_liberties == liberties and self.centrale(cfg, player)):
                best_cfg = ncfg
                nb_liberties = liberties
        return best_cfg
    
    def less_liberties(self):
        p = self.player
        cfg = self.config
        possibilities = [(new_cfg, self.liberties(new_cfg, 1 - p)) 
                                        for new_cfg in self.allcfgs[cfg]]
        best_cfg = self.minimum_liberties(cfg, p, possibilities)
        if best_cfg is None:
            best_cfg = self.alea()
        return best_cfg

    def bayesien(self):
        chance_to_win = self.memories[self.player]
        possibilities = [(c, chance_to_win.get(c.cle(), 1)) for c in self.possibilities()]
        possibilities.sort(key=lambda e: e[1], reverse=True)
        try:
            return tirage(possibilities)
        except:
            return random.choice(possibilities)[0]

    
    # -- LA FONCTION QUI JOUE 
    # -- une partie et un coup

    def play(self):
        new_cfg = self.strategies[self.player]()
        self.config = new_cfg
        self.memorise()
        return self.final()

    def a_game(self):
        while not self.gameover:
            self.gameover = self.play()
            self.next_player()
        return self.winner(), self.trace

