ALLCFGS = 'all_configs.json'

RED = 0

import json

def load_json_file():
    with open(ALLCFGS) as f_in:
        allcfgs = json.load(f_in)
    for cfg in allcfgs:
        allcfgs[cfg] = set(allcfgs[cfg])
    return allcfgs




allcfgs = load_json_file()
# red_win = [cfg for cfg in allcfgs if allcfgs[cfg] == set() and cfg[-1] == '1']
# red_win.sort()
d_stat = {cfg:0 for cfg in allcfgs if allcfgs[cfg] == set() and cfg[-1] == '1'}
with open('red_win_cfg.json', 'w') as f_out:
    json.dump(d_stat, f_out, indent=3)

