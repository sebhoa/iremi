#! /usr/bin/env python3

"""
Jeu des parkings
Version objet et avec déjà toutes le configs possibles
Config initiale [ ] = Blue, ( ) = Red

     [00]--- 05 ---(10)
      | \    |     / |
      |  03  |   08  |
      |    \ |  /    |
     [01]--- 06 ---(11)
      |    / | \     |
      |  04  |   09  |   
      | /    |    \  |
     [02]--- 07 ---(12)

"""


import json
import sys
import argparse
import random
import datetime

# -------------
# -- CONSTANTES
# -------------

ALLCFGS = 'all_configs.json'
REDWINCFGS = 'red_win_cfg.json'
NEAREST = 'nearest_dist.json'

HUMAN = 0
MACHINE = 1
FCTSNAMES = ('alea', 'oriented', 'liberties')
DEFAULTSTRAT = 'alea'
RED = 0
BLUE = 1
NAMES = ['Red', 'Blue', 'Nulle']
NULL = 2

INIT_CFG = '10,11,12,0,1,2,0'
# INIT_CFG = '1,6,10,3,5,8,0'
# INIT_CFG = '2,3,9,7,10,11,0'

SEPS = [    ' | \\    |     / |', 
            ' |    \\ |  /    |',
            ' |    / |  \\    |',
            ' | /    |     \\ |' ]
MARKERS = [{'open':'(', 'close':')'}, {'open':'[', 'close':']'}]

INF = float('inf')

def pause():
    input('Press return to continue...')

# ----------
# -- CLASSES
# ----------

class GameView:
    """ La classe en charge de l'affichage en général """

    def __init__(self, controller, model):
        self.model = model
        self.controller = controller

    def view(self):

        if self.verbose():
            s_print = '\n'
            move = self.last_move()
            if move:
                s_print += f'{NAMES[1 - self.player()]} moved {move[0]} -> {move[1]}\n'
            l_elts = [[0,5,10],0,[3,8],1,[1,6,11],2,[4,9],3,[2,7,12]]
            l = []
            for elt in l_elts:
                if type(elt) == int:
                    l.append(SEPS[elt])
                elif len(elt) == 3:
                    l.append('---'.join([f' {e:02} ' for e in elt]))
                else:
                    s = ' |  '.join([f' {e:02} ' for e in elt])
                    l.append(f' | {s} |')
            s_print += '\n'.join(l)
            s_print += f'\n {NAMES[int(self.config()[-1])]} plays\n'
            for index, pos in enumerate(self.l_config()[:-1]):
                player = index // 3
                o = MARKERS[player]["open"]
                c = MARKERS[player]["close"]
                pattern = f' {int(pos):02} '
                sub = f'{o}{int(pos):02}{c}'
                s_print = s_print.replace(pattern, sub)
        else:
            s_print = self.config()
        print(s_print)
        if self.model.winner is not None:
            if self.model.winner == NULL:
                print('PARTIE NULLE')
            else:
                print(f'{NAMES[self.model.winner]} WINS') 

    def stop(self):
        """ Affiche les résultats de plusieurs games """
        s = '\n'
        for p in [RED, BLUE, NULL]:
            s += f'{NAMES[p]} {self.score(p)} - '
        print(s[:-2])


    def verbose(self):
        """ demande au contrôleur quel type d'affichage doit être fait """
        return self.controller.verbose

    def config(self):
        """ demande au modèle la config courante, celle qu'on doit afficher """
        return self.model.config

    def l_config(self):
        """ obtenir la config sous forme de liste """
        return self.config().split(',')

    def player(self):
        """ obtenir le joueur courant """
        return self.model.player()

    def last_move(self):
        """ obtenir le dernier coup joué """
        return self.model.last_move()

    def score(self, player):
        """ 
        Demander au contrôleur le nombre de victoire de
        player... player au sens large : player = 2 signifie
        partie Nulle
        """
        return self.controller.scores[player]


class GameModel:

    """ 
    En charge de la mécanique de jeu pour 1 coup : 
    choisir le coup, le jouer, mettre à jour les résultats...
    Et transmettre au contrôleur les infos pour décider de 
    la suite de la partie
    """


    def __init__(self, controller):
        self.controller = controller
        # board code le graphe du jeu par listes d'adjacence
        self.board = [  {1,3,5},{0,2,6},{1,4,7},{0,6},
                        {2,6},{0,6,10},{1,3,4,5,7,8,9,11},{2,6,12},
                        {6,10},{6,12},{5,8,11},{6,10,12},{7,9,11} ]
        # allcfgs code toutes les configurations possibles du jeu
        # a été calculé précédemment et stocké dans un fichier json
        self.allcfgs = {}
        self.blockedcfgs = {}

        self.known_strat = {
            'alea': self.alea,
            'oriented': self.oriented,
            'liberties': self.less_liberties
        }

        self.config = INIT_CFG  # la configuration courante
        # les stratégies de jeu pour chaque joueur
        self.strategies = [None, None] 
        self.seen = {}          # dico des configs rencontrées
        self.nb_seen = 0
        self.winner = None      # ID du gagnant, 2 si partie NULLE
        self.moves = []         # la liste des coups joués



    def load_json_file(self):
        with open(ALLCFGS) as f_in:
            self.allcfgs = json.load(f_in)
        for cfg in self.allcfgs:
            self.allcfgs[cfg] = set(self.allcfgs[cfg])
        self.blockedcfgs = {k for k in self.allcfgs if self.allcfgs[k] == set() 
                                    and k[-1] == '1' and not k.startswith('0,1,2')}

    def reset(self):
        self.config = INIT_CFG  
        self.seen = {}          
        self.nb_seen = 0
        self.winner = None
        self.configs = [INIT_CFG]   # pile des configs     
        self.moves = []     # pile des coups         



    # -- TECHNICAL FUNCTIONS (TRAD, ETC.)
    # --

    def move_played(self, from_cfg, to_cfg):
        """
        Retourne le couple d'entier correspondant au coup joué
        pour passer de la config from_cfg à la config to_cfg
        notez la puissance des opérations ensemblistes
        """
        who = self.player(from_cfg)
        from_set = self.positions(from_cfg, who)
        to_set = self.positions(to_cfg, who)
        dep = (from_set - to_set).pop()
        arr = (to_set - from_set).pop()
        return dep, arr


    def simplify(self):
        """
        Simplifie la liste des configurations : qd on rencontre
        une configuration déjà rencontrée, on peut dépiler toutes
        les configs entre : c'était une grande boucle pour rien :)
        """
        i = 1
        cfgs = self.configs
        while i < len(cfgs):
            if cfgs[i] in cfgs[:i]:
                k = cfgs.index(cfgs[i])
                cfgs[k+1:i+1] = []
                i = k+1
            else:
                i += 1



    # -- INFORMATIONS
    # --

    def player(self, config=''):
        if config:
            return int(config[-1])
        else:
            return int(self.config[-1])


    def last_move(self):
        if self.moves:
            return self.moves[-1]
        else:
            return None

    
    def positions(self, cfg, player):
        """
        Retourne l'ensemble des 3 entiers qui
        représentent les positions occupées par
        player dans la configuration cfg
        """
        return {int(e) for e in cfg.split(',')[player*3:(player+1)*3]}

    def liberties(self, cfg, player):
        """ 
        Retourne le nombre de libertés de la configuration cfg
        pour le joueur player, ie le nombre de cases jouables
        """
        pos = self.positions(cfg, player)
        pos_op = self.positions(cfg, 1 -player)
        adj_pos = {v for p in pos for v in self.board[p]}
        return len(adj_pos - pos - pos_op)

    # -- LES UPDATES
    # --

    def update_result(self):
        """ met à jour le gagnant et retour True ssi la partie est finie """
        if self.nb_seen == -1:
            self.winner = NULL
        else:
            cfg = self.config
            p = self.player()
            if self.allcfgs[cfg] == set():
                self.winner = 1 - p
        return self.winner is not None


    def update_strategies(self, choices):
        for p in [RED, BLUE]:
            self.strategies[p] = self.known_strat[choices[p]]

    # -- STRATEGIES FUNCTIONS
    # --


    def alea(self):
        cfg = self.config
        player = self.player()
        possibilities = list(self.allcfgs[cfg])
        return random.choice(possibilities)

    def oriented(self):
        cfg = self.config
        player = self.player()
        possibilities = list(self.allcfgs[cfg])
        possible_moves = [self.move_played(cfg, ncfg)[1] for ncfg in possibilities]
        combine = list(zip(possibilities, possible_moves))
        combine.sort(key=lambda e: e[1], reverse=bool(player))
        nb = len(combine)
        return combine[int(random.triangular(0,nb,0))][0]
    

    
    def losing(self, cfg):
        """
        cfg est une configuration perdante si parmi les
        coups possibles après, pour l'adversaire il y a
        un coup gagnant
        """
        for ncfg in self.allcfgs[cfg]:
            if not self.allcfgs[ncfg]:
                return True
        return False

    def less_liberties(self):
        p = self.player()
        cfg = self.config
        possibilities = [(new_cfg, self.liberties(new_cfg, 1 - p)) 
                                        for new_cfg in self.allcfgs[cfg]]
        nb_liberties = INF
        best_cfg = None
        for ncfg, liberties in possibilities:
            # print(f'{ncfg} {liberties} degrés')
            if not self.losing(ncfg) and (nb_liberties > liberties\
                or nb_liberties == liberties and 6 in self.positions(ncfg, p)):
            # if not self.losing(ncfg) and nb_liberties > liberties:
                best_cfg = ncfg
                nb_liberties = liberties
        if not best_cfg:
            best_cfg = self.alea()
        return best_cfg



    # -- LA FONCTION QUI JOUE UN COUP
    # --

    def play(self):
        new_cfg = self.strategies[self.player()]()
        # ce test permet de mettre en place les parties nulles
        # au bout de x répétitions de la même configuration
        if new_cfg in self.seen:
            self.seen[new_cfg] += 1
            self.nb_seen = max(self.nb_seen, self.seen[new_cfg])
        else:
            self.seen[new_cfg] = 1
        move = self.move_played(self.config, new_cfg)
        self.moves.append(move)
        self.configs.append(new_cfg)
        self.config = new_cfg


class GameController:
    """
    Le contrôleur principal en charge de l'organisation de plusieurs parties :
    initialiser ce qu'il faut puis lancer la boucle de jeu pour chaque partie
    """

    def __init__(self):
        self.count_games = 1
        self.players = [MACHINE, MACHINE]
        self.scores = [0, 0, 0]
        self.verbose = False
        self.debug = False
        self.gameover = False
        self.player = RED # joueur courant

        self.model = GameModel(self)
        self.view = GameView(self, self.model)

    # -- Pour initialiser les parties
    # --

    def settings(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-r', '--red', help='Who plays RED ? 0 = Human, 1 = Machine')
        parser.add_argument('-R', help='Choice function for RED : alea, oriented, liberties')
        parser.add_argument('-b', '--blue', help='Who plays BLUE ? 0 = Human, 1 = Machine')
        parser.add_argument('-B', help='Choice function for BLUE : alea, oriented, liberties')
        parser.add_argument('-n', help='How many games ?')
        parser.add_argument('-d', '--debug', help='Print config each turn',
                                action='store_true')                
        parser.add_argument('-v', '--verbose', help='Print the board as a graph',
                                action='store_true')
        args = parser.parse_args()

        if args.red:
            self.players[RED] = args.red   
        if args.blue:
            self.players[BLUE] = args.blue
        if args.n:
            self.count_games = int(args.n)
        self.debug = args.debug
        if args.verbose:
            self.verbose = args.verbose
            self.debug = True
        strats_choices = [DEFAULTSTRAT, DEFAULTSTRAT]
        if args.R in self.known_strat():
            strats_choices[RED] = args.R
        if args.B in self.known_strat():
            strats_choices[BLUE] = args.B
        self.model.update_strategies(strats_choices)


    def load(self):
        """ Chargement de toutes les configurations possibles """
        self.model.load_json_file()


    def memorize(self):
        stamp = f'mem_{datetime.datetime.today():%Y%m%d_%H%M%S}'
        with open(stamp, 'w') as f_out:
            for cfg in self.configs():
                f_out.write(f'{cfg}\n')

    
    # -- Demande d'infos
    # --

    def known_strat(self):
        return self.model.known_strat

    def winner(self):
        return self.model.winner

    def config(self):
        return self.model.config
    
    def moves(self):
        return self.model.moves

    def last_config(self):
        return self.model.configs[-1]

    def configs(self):
        return self.model.configs

    def blocked(self):
        return self.model.blockedcfgs

    # -- Les mises à jour
    # --

    def update_scores(self):
        self.scores[self.winner()] += 1

    def update_result(self):
        self.gameover = self.model.update_result()

    def next_player(self):
        self.player = 1 - self.player

    def reset(self):
        self.gameover = False
        self.player = RED # joueur courant
        self.model.reset()

    def deep(self, level):
        self.model.deep = level


    # -- Boucle pour 1 partie
    # -- 

    def loop(self):
        """
        La fonction qui joue une partie complète 
        """
        while not self.gameover:
            if self.debug:
                self.view.view()
                pause()
            self.update_result()
            if not self.gameover:
                self.model.play()
                self.next_player()
            else:
                self.update_scores()
        
        # self.view.view()


    # -- Démarrage et arrêt de plusieurs parties
    # --

    def stats_red_win(self):
        with open(REDWINCFGS) as f_in:
            red_win_cfg = json.load(f_in)
        if self.winner() == RED:
            red_win_cfg[self.config()] += 1
        with open(REDWINCFGS, 'w') as f_out:
            json.dump(red_win_cfg, f_out, indent=3)


    def start(self):
        self.settings()
        self.load()
        nb_car = len(str(self.count_games))
        for id_game in range(self.count_games):
            if not self.debug:
                print(f'{id_game:0{nb_car}}',end='\b'*nb_car, flush=True)
            self.reset()
            self.loop()
            # La portion de code ci-après permet de simplifier les parties
            # trop longues puis de mémoriser dans un fichier texte
            #
            # if self.last_config() in self.blocked():
            #     print(f'Avant simplification : {len(self.configs())} coups')
            #     self.model.simplify()
            #     print(f'Après simplification : {len(self.configs())} coups')
            #     self.memorize()
            #     print(f'Une partie sauvée')
            #     pause()
            # met à jour un fichier json qui contient le décompte des
            # des victoires de rouge en fonction de la config finale
            # ensuite on lance le script red.py pour voir le % de victoires
            # par blocage et % par atteinte position finale
            self.stats_red_win()

    def stop(self):
        self.view.stop()



my_game = GameController()
my_game.start()
my_game.stop()