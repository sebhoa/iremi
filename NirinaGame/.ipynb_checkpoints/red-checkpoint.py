import json



REDWINCFGS = 'red_win_cfg.json'

with open(REDWINCFGS) as f_in:
    red_win_cfg = json.load(f_in)

total = sum(red_win_cfg.values())

count = {'bloquantes':0, 'finales':0}
for cfg in red_win_cfg:
	if cfg.startswith('0,1,2'):
		count['finales'] += red_win_cfg[cfg]
	else:
		count['bloquantes'] += red_win_cfg[cfg]

print(f"Bloquantes = {count['bloquantes']} {count['bloquantes']/total:.1%}")
print(f"Finales = {count['finales']} {count['finales']/total:.1%}")

# liste = list(red_win_cfg.items())
# liste.sort(key=lambda e: e[1], reverse=True)

# for cfg, nb in liste:
# 	print(f'{cfg} {nb} {nb/total:.1%}')

