from graphviz import *

graphe = [[1,3,5],[2,6],[4,7],[6],[6],[6,10],[7,8,9,11],[12],[10],[12],[11],[12],[]]
def voisins(a,b):
    return a in graphe[b] or b in graphe[a]

def colorieGraphe(numero,posbleus,posrouges):
    g = Graph(format='png')
    g.body.extend(['rankdir=LR','size="2"'])
    for n in range(13):
        if n in posbleus:
            g.node(str(n),' ',shape='circle',color='blue',style='filled')
        elif n in posrouges:
            g.node(str(n),' ',shape='circle',color='red',style='filled')
        else:
            g.node(str(n),' ',shape='circle')
    for i in range(13):
        for j in range(i,13):
            if voisins(i,j):
                g.edge(str(i),str(j))
    g.render('g'+str(10000+numero),view=False)

with open('rouge_en_40_coups.txt','r') as f:
    for n, line in enumerate(f):
        ligne = [int(e) for e in line.split(',')[:-1]]
        r=ligne[:3]
        b=ligne[3:6]
        colorieGraphe(n,b,r)

#colorieGraphe(0,bleus,rouges)
