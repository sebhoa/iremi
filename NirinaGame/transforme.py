import json

def rename(cle):
    dico = {'10':'A', '11':'B', '12':'C'}
    for d in dico:
        cle = cle.replace(d, dico[d])
    return cle.replace(',','')

d_original = {}
with open('all_configs.json', 'r', encoding='utf-8') as csvfile:
    d_original = json.load(csvfile)

new_d = {rename(cle):[rename(v) for v in d_original[cle]] for cle in d_original}

with open('original.json', 'w', encoding='utf-8') as csvfile:
    json.dump(new_d, csvfile, indent=3)