
"""
    31.07.2018
    seb

    Dans cette version du NirinaGame, on va utiliser :
    - minmax (peut-être)
    - dijkstra
    - fonctions de distance, de degré etc.

    une config ou environnement sera un dictionnaire :
    {
        'pos' : [ liste positions de ROUGE, liste positions de BLEU ]
        'player' : O ou 1 l'ID du joueur qui doit jouer
        'to' : [ listes d'adjacences s <- [s'1...] ]
        'from' : [ listes d'adjacences s -> [s1...] ]
        'result' : -1 si partie Nulle, 0 si ROUGE a gagné, 1 si BLEU a gagné, None si partie en cours
        'seen' : la liste des configs vues ((positions ROUGE), (positions BLEU), joueur) pour voir si
                on retombe sur une deja vue... ne sera pas utilisée pour le moment
        'degres' : [ liste des degrés de libertés ROUGE, liste des degrés de libertés BLEU ]
    }

"""

FIELD_LISTS = ['pos','to','from','seen','degres']
FIELD_SIMPLE = ['player','result']

import sys
import random
import shelve

# Graphe initial sans les 'voitures', non orienté donc
#
#      [00]--- 05 ---(10)
#       | \    |     / |
#       |  03  |   08  |
#       |    \ |  /    |
#      [01]--- 06 ---(11)
#       |    / |  \    |
#       |  04  |   09  |   
#       | /    |     \ |
#      [02]--- 07 ---(12)
#
# Modélisé ici avec des listes d'adjacences :

GRAPHE = [  [1,3,5],[0,2,6],[1,4,7],[0,6],[2,6],[0,6,10],[1,3,4,5,7,8,9,11],[2,6,12],
            [6,10],[6,12],[5,8,11],[6,10,12],[7,9,11] ]

# ID des joueurs
#
ROUGE = 0
BLEU = 1
NAMES = ['rouge', 'bleu']
DEPARTS = [[10,11,12], [0,1,2]]
GAGNANTES = [DEPARTS[BLEU], DEPARTS[ROUGE]]
GAGNANTS = ((0,1,2), (10,11,12))

INF = 100 # largement suffisant compte tenu des distances
SOMMETS = range(0,13)

NULLE = -1
SEUIL = 3

FILESTATS = 'filestats.csv'

# --- UTILITIES
# ---

def freq(e):
    return e[1]

def my_copy(listes):
    """effectue une deep copy d'une liste de listes car utiliser
        copy.deepcopy est trop couteux"""
    l_copy = []
    for l in listes:
        l_copy.append(l[:])
    return l_copy

def str_positions(env):
    str_p = ''
    for p in [ROUGE, BLEU]:
        l = sorted(env['pos'][p])
        for pos in l:
            str_p += '{}-'.format(pos)
    return str_p[:-1]

def lecture_stats(filename):
    try:
        fp = open(filename, 'r')
    except:
        fp = -1
    if fp == -1:
        return {}, 0
    else:
        d = {}
        n = int(fp.readline())
        ligne = fp.readline()
        while ligne:
            pos, index_dep, arr, nb = ligne.split()
            coup = int(index_dep), int(arr)
            if pos in d:
                d[pos][coup] = d[pos].get(coup, 0) + int(nb)
            else:
                d[pos] = {coup:int(nb)}
            ligne = fp.readline()
        fp.close()
        return d, n

def update_stats(d_stats, d_coups):
    for pos, coup in d_coups.items():
        if pos in d_stats:
            d_stats[pos][coup] = d_stats.get(coup, 0) + 1
        else:
            d_stats[pos] = {coup:1}

def ecriture_stats(d_stats, nb, filename):
    with open(filename, 'w') as fp:
        fp.write('{}\n'.format(nb))
        for pos in d_stats:
            for ((index_dep, arr), nb) in d_stats[pos].items(): 
                fp.write('{} {} {} {}\n'.format(pos, index_dep, arr, nb))

# --- SMALL FUNCTIONS AND GET FUNCTIONS
# ---

def from_s(s, env):
    """Retourne les sommets atteignables en 1 coup d'un sommet s dans la config env"""
    return env['from'][s]

def to_s(s, env):
    """ Retourne les sommets qui peuvent atteindre le sommet s dans l'environnement env """
    return env['to'][s]

def player(env):
    """ Retourne l'ID du joueur courant """
    return env['player']

def positions(env, joueur):
    """ Retourne les positions du joueur joueur """
    return env['pos'][joueur]

def current_positions(env):
    """ Retourne les positions du joueur courant """
    return env['pos'][env['player']]

def degres(env, joueur):
    """ Retourne la liste des degrés du joueur joueur """
    return env['degres'][joueur]

def result(env):
    """ Retourne la valeur du résultat de la partie None, -1, 0 ou 1 """
    return env['result']

def vide(s, env):
    """ Renvoie True ssi aucune voiture de l'environnement env n'est sur le sommet s """
    p = player(env)
    return s not in env['pos'][p] and s not in env['pos'][1 - p]
 


def in_progress(env):
    return result(env) is None

def is_nulle(env):
    return result(env) == NULLE

def name(env, who='player'):
    return NAMES[env[who]].upper()


# --- INIT AND UPDATE DATAS
# ---

def copy_env(env):
    new_env = {}
    for field in FIELD_LISTS:
        new_env[field] = my_copy(env[field])
    for field in FIELD_SIMPLE:
        new_env[field] = env[field]
    return new_env

def update_libere(s, env):
    """ Mise à jour de l'environnement env si on libère le sommet s """
    env['to'][s].extend(GRAPHE[s])
    for v in env['to'][s]:
        env['from'][v].append(s)

def update_occupe(s, env):
    """ Mise à jour de l'environnement env si on occupe le sommet s """
    for v in env['to'][s]:
        env['from'][v].remove(s)
    env['to'][s].clear()

def update_degres(env):
    for p in [ROUGE, BLEU]:
        env['degres'][p] = [len(from_s(s, env)) for s in env['pos'][p]]


def init_env():
    env = {}
    env['pos'] = my_copy(DEPARTS)
    env['player'] = ROUGE
    env['result'] = None
    env['from'] = my_copy(GRAPHE)
    env['to'] = my_copy(GRAPHE)
    for p in [ROUGE, BLEU]:
        for s in env['pos'][p]:
            update_occupe(s, env)
    env['seen'] = []
    env['degres'] = [0, 0]
    update_degres(env)
    return env

def change_player(env):
    """ Modifie env en changeant le joueur courant """
    env['player'] = 1 - env['player']

def set_positions(env, joueur, index_dep, arr):
    """ On déplace la voiture du sommet à l'indice index_dep vers le sommet arr 
        pour le joueur joueur de env """
    env['pos'][joueur][index_dep] = arr

def set_result(env, value):
    env['result'] = value


# --- PRINT FUNCTIONS
# ---

def the_winner_is(env,tab=0):
    tabs = '\t'*tab
    result_game = result(env)
    if result_game is None:
        print('{}PARTIE EN COURS...'.format(tabs))
    elif result_game == NULLE:
        print('{}PARTIE NULLE.'.format(tabs))
    else:
        print('{}{} GAGNE'.format(tabs, name(env,'result')))


def print_env(env, details=False, tab=0):
    """ Afficher un environnement de façon un peu lisible """
    tabs = '\t'*tab
    print('{}POS : {} {} {} joue'.format(tabs,env['pos'][ROUGE], env['pos'][BLEU], name(env)), end=' ')
    if details:
        print()
        print('{}{:=>25s}'.format(tabs,' FROM_S :'))
        for s in SOMMETS:
            print('{}{}'.format(tabs,from_s(s, env)))
        print('{}{:=>25s}'.format(tabs, ' TO_S :'))
        for s in SOMMETS:
            print('{}{}'.format(tabs, to_s(s, env)))
        print('{}DEGRÉS : {} {}'.format(tabs, degres(env, ROUGE), degres(env, BLEU)))
    print()
    #the_winner_is(env)


# ------------------
# --- GAME FUNCTIONS
# ------------------


# Fonctions auxiliaires
# --
def choix(env):
    """Retourne la liste des coups possibles à partir de l'environnement
        env. Le résultat est un couple (position départ, position arrivée)"""
    return [(index_dep, arr) for index_dep, dep in enumerate(current_positions(env)) for arr in from_s(dep, env)]

def est_bloque(env):
    """ Retourne True ssi le joueur courant de l'environnement env est bloqué """
    return choix(env) == []

def nulle_game(env):
    #env[str_env(env)] >= SEUIL
    return False

def win_game(env):
    winner = 1 - player(env)
    return est_bloque(env) or sorted(env['pos'][winner]) == GAGNANTES[winner]



def simulation(env, index_dep, arr):
    new_env = copy_env(env)
    play_one_move(new_env, index_dep, arr)
    return new_env

def dist_calculation(arr, env):
    """ calcule la distance minimale de tout sommet au
        sommet arr dans l'environnement env """
    d_dist = {}
    s_a_traiter = [arr]
    l_dist = [0]
    while s_a_traiter:
        s = s_a_traiter.pop(0)
        d = l_dist.pop(0)
        d_dist[s] = d
        v_de_s = [v for v in to_s(s, env) if v not in d_dist and v not in s_a_traiter]
        for v in v_de_s:
            s_a_traiter.append(v)
            l_dist.append(d+1)
    return d_dist

def optimise_distances(l_distances):
    """ l_distances = [[d0, d1, d2], [d'0, d'1, d'2], [d"0, d"1, d"2]]
        optimise va créer D, D', D" tq D in [d], D' in [d'] et D" in [d"]
        et D+D'+D" est minimale """
    dmin = INF * INF
    tmin = [-1, -1, -1]
    for i, d in enumerate(l_distances[0]):
        iprimes = list(range(3))
        iprimes.remove(i)
        for iprime in iprimes:
            isecondes = iprimes[:]
            isecondes.remove(iprime)
            iseconde = isecondes[0]
            somD = l_distances[0][i] + l_distances[1][iprime] + l_distances[2][iseconde]
            if somD < dmin:
                dmin = somD
                tmin = [i, iprime, iseconde]
    a, b ,c = tmin
    return l_distances[0][a], l_distances[1][b], l_distances[2][c]

def distances_to_final(env, joueur):
    """ Calcule la liste des listes de distances entre les positions 
        courantes du joueur et ses positions finales """
    final_positions = GAGNANTES[joueur]
    l_distances = [[-1, -1, -1], [-1, -1, -1], [-1, -1, -1]]
    joueur_pos = positions(env, joueur)
    for i_final in range(3):
        d_dist = dist_calculation(final_positions[i_final], env)
        for i_pos in range(3):
            l_distances[i_pos][i_final] = d_dist.get(joueur_pos[i_pos], INF)
    return l_distances

def qualite(env):
    advers = player(env)
    joueur = 1 - advers
    l_dists = [-1, -1]
    for p in [ROUGE, BLEU]:
        l_dists[p] = optimise_distances(distances_to_final(env, p))
    di_adv = sum(l_dists[advers])
    di_jou = sum(l_dists[joueur])
    de_adv = sum(degres(env, advers))
    de_jou = sum(degres(env, joueur)) 
    if win_game(env):
        return INF * INF
    else:
        return de_jou - de_adv + di_adv - di_jou 


# - STRATEGIE BASEE SUR LA CONNAISSANCE DE TOUT LE GRAPHE
# - les fonctions ci-dessous sont tirées de test.py et ne 
# - sont pas super opti pour l'intégration ici... à revoir éventuellement

# Ca c'est le graphe des 34308 configurations possibles
#
FILE_GRAPH = 'graphe.csv'

# - LECTURE GRAPHE
# - et autres fonctions

def initGraph():
    d_graph = {}
    with open(FILE_GRAPH, 'r') as fp:
        for ligne in fp:
            infos = ligne.split()
            l = [int(e) for e in tuple(infos[0].split(','))]
            cle = (tuple(l[:3]),tuple(l[3:-1]),l[-1])
            d_graph[cle] = []
            for s in infos[1:]:
                l = [int(e) for e in tuple(s.split(','))]
                d_graph[cle].append((tuple(l[:3]),tuple(l[3:-3]),l[-3],tuple(l[-2:])))
    return d_graph

def bloquant(config):
    return d_graph[config] == []

def gagnant(config):
    winner = 1 - config[-1]
    return config[winner] == GAGNANTS[winner]

def to_str_key(cle):
    return str(cle).replace('(','').replace(')','')

def theNearest(config):
    joueur = 1 - config[-1]
    dmin = INF
    finale = None
    suivant = None
    for cfg in configs_gagnantes[joueur]:
        d, c = d_mins[joueur][cfg].get(config,(0,None))
        if d < dmin:
            dmin, finale, suivant = d, cfg, c
    return dmin, finale, suivant


def danger(cfg):
    for c in d_graph[cfg]:
        if bloquant(c[:-1]):
            return True
    return False

def good_choice(config):
    voisins = d_graph[config]
    dmin = INF
    cmin = None
    for cfg in voisins:
        cfg = cfg[:-1]
        d, _, _ = theNearest(cfg)
        if not danger(cfg):
            if d <= dmin:
                dmin, cmin = d, cfg
    if cmin is None:
        cmin = voisins[0][:-1]
    return cmin

# fonctions pour faire le lien entre anciennes versions de config
# (issu de test.py) et la version de ce fichier d'environnement
#
def env_to_config(env):
    config = tuple()
    for p in [ROUGE, BLEU]:
        pos = tuple(sorted(env['pos'][p]))
        config = config + (pos,)
    return config + (player(env),)

def coup_to_move(config, new_config, env):
    joueur = config[-1]
    dep = [c for c in config[joueur] if c not in new_config[joueur]][0]
    arr = [c for c in new_config[joueur] if c not in config[joueur]][0]
    index_dep = env['pos'][joueur].index(dep)
    return index_dep, arr


# Fonctions de sélection d'un coup
#

# 1re fonction : choix aléatoire
#
def alea(env):
    """ Le joueur courant va jouer un coup au hasard """
    possibilites = choix(env)
    return random.choice(possibilites)


# 2e fonction de choix : basée sur une heuristique (maximiser la qualité)
#
def ia(env):
    """ Sélectionne un bon coup... ie qui maximise une certaine fonction """
    possibilites = choix(env)
    qmax = - INF * INF
    index_opti, arr_opti = None, None
    #print('Simulation...')
    for index_dep, arr in possibilites:
        new_env = simulation(env, index_dep, arr)
        #print('\t',end='')
        #print_env(new_env, tab=1)
        q = qualite(new_env)
        #print(q)
        if q > qmax:
            qmax = q
            index_opti, arr_opti = index_dep, arr
    return index_opti, arr_opti


# 3e fonction choix : meilleur candidat au sens de la 
# meilleure distance à une config gagnante
#
def best(env):
    #print_env(env)
    config = env_to_config(env)
    new_config = good_choice(config)
    return coup_to_move(config, new_config, env)


# 4e fonction de choix : humain qui choisit
# (si on veut jouer contre la machine)
#
def human(env):
    dep, arr = [int(x) for x in input('YOUR TURN : ').split()]
    index_dep = env['pos'][player(env)].index(dep)
    return index_dep, arr

# 5e fonction de choix (en cours de développement)
# choix statistique censé aller en apprenant
#
def machine_learning(env):
    """ Sélectionne un bon coup statistiquement """
    pos = str_positions(env)
    if pos in d_stats:
        #l_possibilites = [(coup, int(100 * nb/nb_winning_games)) for coup, nb in d_stats[pos].items()]
        l_possibilites = list(d_stats[pos].items())
        l_possibilites.sort(key=freq, reverse=True)
        #return l_possibilites[0][0]
        #print(l_possibilites)
        tirage = random.randint(0,100)
        start = 0
        for coup, f in l_possibilites:
            if start + f > tirage:
                print("STATS")
                return coup
            start += f
        return alea(env)
    else:
        return alea(env)








# -- Les fonctions pour gérer une partie
# --

def play_one_move(env, index_dep, arr):
    """ Joue le coup dep -> arr dans l'environnement env """
    dep = current_positions(env)[index_dep]
    set_positions(env, player(env), index_dep, arr)
    update_libere(dep, env)
    update_occupe(arr, env)
    update_degres(env)
    change_player(env)


def play(env, debug=False):
    """ La fonction qui joue une partie complète """
    d_coups = {} # pour retenir les coups de la partie pour une éventuelle utilisation statistique
    while in_progress(env):
        if debug:
            print_env(env)
        p = player(env)
        if win_game(env):
            set_result(env, 1 - p)
        elif nulle_game(env):
            set_result(env, NULLE)
        else:
            # la sélection d'un coup à jouer
            #
            index_dep, arr = SELECTION_FCTS[p](env)
            
            if p == BLEU:
                str_p = str_positions(env)
                d_coups[str_p] = (env['pos'][p][index_dep], arr)
            
            # Et on joue le coup sélectionné
            #
            play_one_move(env, index_dep, arr)
    #print(len(d_coups))
    return d_coups




# --- MAIN
# ---

SELECTION_FCTS = [alea, best]

def main():
    global d_stats
    global nb_winning_games
    global d_mins, d_graph, configs_gagnantes
    scores = [0, 0]
    nb_games = 1
    debug = False
    learner = BLEU
    if len(sys.argv) > 1:
        nb_games = int(sys.argv[1])
    if len(sys.argv) > 2:
        debug = True
    width = len(str(nb_games))




    for game_id in range(nb_games):
        env = init_env()
        d_coups = play(env, debug)
        scores[result(env)] += 1

        # Pour les stats
        #if result(env) == learner:
            #print('on met à jour')
            #update_stats(d_stats, d_coups)
            #print(d_stats)
        print('{:0>{width}d} -- {:0>{width}d}'.format(scores[ROUGE], scores[BLEU], width=width), 
                end='\b'*(2*width + 4), flush=True)
    #ecriture_stats(d_stats, scores[learner]+nb_winning_games, FILESTATS)
    print()
    print('Global scores : {}'.format(scores))



if __name__ == '__main__':
    #d_stats, nb_winning_games = lecture_stats(FILESTATS)
    # for k in d_stats:
    #     print(k)
    #     print('\t',d_stats[k])

    # Mise en place pour l'algo de ROUGE :
    # On récupère le graphe total (34308 noeuds)
    #
    d_graph = initGraph()
    #print(d_graph[((0, 6, 7), (2, 3, 4), 1)]) 
    #print(d_graph[((0, 6, 7), (1, 3, 4), 0)])  
    #input()    

    # On récupère les configs gagnantes et bloquantes
    #
    configs_gagnantes = [[], []]
        
    for config in d_graph:
        winner = 1 - config[-1]
        if gagnant(config) or bloquant(config):
            configs_gagnantes[winner].append(config)

    # Les distances des configs aux configs gagnantes
    # dbs shelve sont gros et lents on transfert dans
    # un dictionnaire classique
    #
    dbs = [shelve.open('db_rouge'), shelve.open('db_bleu')]

    d_mins = [{}, {}]
    for joueur in [ROUGE, BLEU]:
        for cfg in configs_gagnantes[joueur]:
            d_mins[joueur][cfg] = dbs[joueur][to_str_key(cfg)] 

    main()












