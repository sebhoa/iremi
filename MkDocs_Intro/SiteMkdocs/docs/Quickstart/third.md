---
hide:
    - toc
---

# Troisième page

## Programmation

Un peu de code Python

??? example "Exemple en Python3"

    ```python
    def premier(n):
        return n > 1 and all(n%d != 0 for d in range(2, n))
    ```

## Détente

- Un petit jeu : [sutom.nocle.fr](sutom.nocle.fr)