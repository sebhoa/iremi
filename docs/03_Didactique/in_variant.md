# Variants et invariants en Première NSI

## Introduction

La correction de programme (correction partielle et terminaison) est un concept simple à comprendre mais difficile à prouver. Ce thème est au programme des contenus d'Informatique de classes préparatoires[^1] (premier semestre de la classe MP2I, second semestre de MPSI) et en $2^e$ voir en $3^e$ année de Licence Informatique. Les références à ce concept sont mentionnés à divers endroits du programme officiel de NSI en classe de Première :

???+ info "Langages et programmation"

    | Contenus | Capacités attendues | Commentaires |
    | -------- | ------------------- | ------------ |
    | Spécification | Décrire les préconditions sur les arguments.<br>Décrire les postconditions sur les résultats. | Des assertions peuvent être utilisées pour garantir les préconditions ou les postconditions. |
    | Mise au point de programmes | Utiliser des jeux de tests | L'importance de la qualité et du nombre de tests est mise en évidence.<br>Le succès d'un jeu de test ne garantit pas la correction d'un programme. |

???+ info "Algorithmique"

    | Contenus | Capacités attendues | Commentaires |
    | -------- | ------------------- | ------------ |
    | Tris par insertion, par sélection | Écrire un algorithme de tri.<br>Décrire un **invariant** de boucle qui prouve la correction des tris. | La terminaison de ces algorithmes est à justifier |
    | Recherche dichotomique dans un tableau trié | Montrer la terminaison de la recherche dichotomique à l'aide d'un **variant** de boucle.| Des assertions peuvent être utilisées. La preuve de la correction peut être présentée par le professeur. |

Dès lors, comment aborder un tel sujet, avec la certitude qu'il ne faudra pas essayer de faire des preuves formelles ou en tout cas incluant trop de formalisme mathématique puisque les élèves n'ont pas le bagage nécessaire. 

???+ warning "Remarque" 

    Ce thème ne fait l'objet d'aucun exercice au Bac et il n'est pas non plus traité par les ressources présentées sur Éduscol.

Nous montrons dans cet article une façon possible d'introduire et d'écrire en langage Python les différents algorithmes _simples_ proposés dans le programme : les algorithmes de parcours séquentiel de tableau (la somme des valeurs par exemple), les tris par sélection et par insertion, la recherche dichotomique pour ensuite traiter dans cet ordre :

1. l'idée qu'un ensemble de tests ne peut pas suffire
2. qu'il y a potentiellement deux problèmes : un appel à la fonction ne donne pas de réponse en un temps raisonnable (problème de terminaison), qu'une réponse donnée est fausse (problème de correction)
3. définir les notions de variant et d'invariant et leurs rôles respectifs
4. montrer sur nos exemples d'algorithmes des preuves de correction et de terminaison, faisant intervenir les deux notions précédentes

## Quelques algorithmes classiques

Les algorithmes introduits ici sont des classiques qui manipulent des tableaux de données (en général entières pour plus de simplicité). Ces algorithmes sont bien calibrés pour ce type d'étude : pas trop triviaux (ils incluent une boucle, bornée ou pas, des manipulations sur les valeurs du tableau) mais pas non plus trop compliqués pour pouvoir raisonner sur quelques lignes de code.

### Somme des valeurs d'un tableau

???+ note "Spécification"

    Il s'agit d'écrire une fonction qui prend un tableau de valeurs entières en paramètre. La fonction calcule et renvoie la somme de toutes les valeurs du tableau. Un tableau vide renverra une somme nulle.

???+ abstract "Algorithme"

    On parcourt le tableau de _gauche à droite_ et on mémorise la somme partielle des éléments rencontrés. À la fin du parcours on renvoie cette somme.

???+ tip "Mise en œuvre"

    ```python
    def somme(tab):
        som = 0
        for i in range(len(tab)):
            som += tab[i]
        return som
    ```

???+ warning "Autre version"

    La boucle `for` de Python autorise à une écrire plus simple de la fonction `somme` :

    ```python title="version simplifiée"
    def somme(tab):
        som = 0
        for elt in tab:
            som += elt
        return som
    ```

    Mais pour les besoins des preuves de correction, nous utiliserons la version manipulant explicitement les indices du tableau.

### Tri pas sélection

???+ note "Spécification"

    Ce tri porte mal son nom en français. En anglais il s'agit du _minsort_ qui renseigne mieux la spécification :

    > Tri par recherche itérée du minimum.

???+ abstract "Algorithme"

    En entrée un tableau d'entiers. L'algorithme ne renvoie rien et effectue un tri en place du tableau de la façon suivante :

    On parcourt le tableau de _gauche à droite_, :
    
    - on repère la position du minimum entre la position courante et la fin du tableau ;
    - on échange les valeurs associées dans le tableau 

???+ tip "Mise en œuvre"

    ```python
    def tri_selection(tab):
        for i in range(len(tab)):
            ind_min = indice_du_min(tab, i):
            tab[i], tab[ind_min] = tab[ind_min], tab[i]
    ```

    Avec la fonction qui permet de repérer la position du minimum, c'est-à-dire trouver et renvoyer l'indice de ce minimum :

    ```python
    def indice_du_min(tab, i):
        ind_min = i
        for j in range(i+1, len(tab)):
            if tab[j] < tab[ind_min]:
                ind_min = j
        return ind_min
    ```    

### Tri par insertion

_TODO_

### Recherche dichotomique dans un tableau trié

_TODO_

## Les tests indispensables, ne peuvent pas suffire

Si on doit insister auprès des élèves apprenants sur l'importance d'un _bon_ jeu de tests, il faut aussi rappeler que la validation de ces tests ne garantit pas la correction d'un algorithme, d'une fonction.

???+ fail "Une recherche dichotomique fausse"

    Supposons que l'élève fournit cette fonction pour la recherche dichotomique :

    ```python
    def recherche_dicho(tab, cible):
        deb, fin = 0, len(tab)-1
        while fin - deb > 0:
            milieu = (deb + fin) // 2
            if tab[milieu] == cible:
                return True
            elif cible < tab[milieu]:
                fin -= 1
            else:
                deb += 1
        return False
    ```

    Dès lors les tests ci-dessous fonctionnent et ne permettent pas de détecter que la fonction n'est en réalité par correcte :

    ```python title="Tests"
    assert recherche_dicho([10, 12, 18, 25, 32], 12) == True
    assert recherche_dicho([10, 12, 18, 25, 32], 10) == True
    assert recherche_dicho([10, 12, 18, 25, 32], 20) == False
    assert recherche_dicho([], 1) == False
    ```

    En effet, le seul cas qui ne fonctionne pas ici est celui où la valeur cible se trouve en dernier élément :
    
    ```python
    assert recherche_dicho([10, 12, 18, 25, 32], 32) == True
    ```

## Pré et post conditions

_TODO_

## Invariant de boucle et preuve de correction (partielle)

???+ note "Définition"

    Une fonction, qui est la mise en œuvre d'un algorithme, est **correcte** (partiellement) si elle répond à la spécification de l'algorithme ; autrement si elle fait ce qu'on attend d'elle.

    **Note :** on parle de **correction totale** lorsqu'on a la correction partielle et la terminaison. À partir de maintenant nous ne spécifierons plus _partiel_.

???+ example "Algorithme de tri"

    La fonction `tri_selection` présentée au début de cet article est correcte, nous allons le montrer dans un moment.

???+ note "Définition"

    Un **invariant de boucle** est une propriété qui est vraie au début de la fonction et qui reste vrai à chaque _tour_ de boucle. À la fin de la fonction cet invariant est la postcondition qui spécifie l'algorithme ; qu'il soit vrai prouve que la fonction est correcte.

La première difficulté est de trouver cet invariant. c'est là qu'un légère instrumentalisation des fonctions sur lesquelles on travaille peut aider.

### Cas 1 : la fonction `somme`

Rappel de la spécification : 

- En entrée : un tableau de valeurs entières
- En valeur renvoyée : la somme des valeurs présentes dans le tableau 

Pour imaginer l'invariant, nous allons afficher un état du tableau et de la somme partielle au début de chaque boucle :

???+ tip "Fonction `somme` _instrumentée_"

    ```python linenums="1"
    def somme(tab):
        som = 0
        for i in range(len(tab)):
            affiche(tab, i, som)
            som += tab[i]
        affiche(tab, i+1, som)
        return som
    ```

Le rôle de la fonction `affiche` est de poser un _marqueur_ qui sépare le tableau parcouru en deux :

- une partie gauche des éléments déjà parcourus
- une partie droite des éléments restant à parcourir
- on peut aussi y afficher l'indice courant (de début de boucle) ainsi que des valeurs de variables comme par exemple ici la valeur de la variable `som`

Voici ce que donne l'exécution de cette version de `somme` sur un exemple _bien choisi_ :

???+ example "Exemple pour deviner l'invariant"

    ```pycon title="Console python"
    >>> somme([1, 1, 1, 1, 1, 1])
    ```

    ``` title="Affichage obtenu"
    ● 1 1 1 1 1 1 - i : 0 - som : 0
    1 ● 1 1 1 1 1 - i : 1 - som : 1
    1 1 ● 1 1 1 1 - i : 2 - som : 2
    1 1 1 ● 1 1 1 - i : 3 - som : 3
    1 1 1 1 ● 1 1 - i : 4 - som : 4
    1 1 1 1 1 ● 1 - i : 5 - som : 5
    1 1 1 1 1 1 ● - i : 6 - som : 6
    ```

???+ abstract "Invariant de la fonction `somme`"

    On constate assez simplement que la variable `som` vaut la somme des éléments déjà parcourus. Ce sera notre invariant de boucle.

???+ note "Preuve"

    Pour qu'un invariant serve à quelque chose, il faut prouver :

    1. que l'invariant est vrai au début de la fonction ; ici c'est le cas ici puisqu'au début, on n'a parcouru **aucun** élément (leur somme est donc nulle) et `som` vaut bien $0$ (au passage on montre l'importance de l'initialisation de `som` à la valeur $0$ ; avec une autre valeur notre invariant est faux)
    2. si l'invariant est vrai à l'entrée de la boucle, il le sera encore à la fin (et donc à la prochaine entrée) ; lorsqu'on entre dans la boucle on a une valeur d'indice `i`, l'hypothèse est que `som` vaut `tab[0] + ... + tab[i-1]` ; dans la boucle, nous incrémentons `som` avec `tab[i]` (ligne 5 dans la version numérotée) ; ainsi, au début du tour suivant, nous avons un compteur de boucle à `i+1` et `som` vaut bien `tab[0] + ... + tab[i]`. Notre invariant a été conservé.

    La conclusion de ceci est qu'à la fin du dernier passage dans la boucle, `i` vaut `len(tab)-1` et au début du suivant (qui n'aura pas lieu) `i` vaudrait `len(tab)` et notre invariant dit que `som` vaut `tab[0] + ... + tab[i-1]` soit `tab[0] + ... + tab[len(tab)-1]` c'est-à-dire exactement la somme de **toutes** les valeurs du tableau. Et c'est cette valeur `som` qui est renvoyée.


### Cas 2 : la fonction `tri_selection`

Rappel de la spécification : 

- En entrée : un tableau de valeurs entières
- En valeur renvoyée : aucune ; le tableau est trié **en place**

Comme pour le cas de la `somme`, nous donnons une version instrumentée de la fonction `tri_selection` :

???+ tip "Fonction `tri_selection` _instrumentée_"

    ```python linenums="1"
    def tri_selection(tab):
        for i in range(len(tab)):
            affiche(tab, i)
            ind_min = indice_du_min(tab, i)
            tab[i], tab[ind_min] = tab[ind_min], tab[i]
        affiche(tab, i+1)
    ```

???+ example "Exemple pour deviner l'invariant"

    ```pycon title="Console python"
    >>> tri_selection([5, 2, 4, 1, 3])
    ```

    ``` title="Affichage obtenu"
    ● 5 2 4 1 3 - i : 0 
    1 ● 2 4 5 3 - i : 1 
    1 2 ● 4 5 3 - i : 2 
    1 2 3 ● 5 4 - i : 3 
    1 2 3 4 ● 5 - i : 4 
    1 2 3 4 5 ● - i : 5 
    ```

Une fois encore l'invariant est assez simple à deviner :

???+ abstract "Invariant de la fonction `tri_selection`"

    **Le tableau est trié sur sa partie gauche et toutes les valeurs sont plus petites que les valeurs de la partie droite**. Comme à la fin de la boucle la partie gauche représente la totalité du tableau alors on a notre spécification.


???+ note "Preuve"

    Dans cette preuve, nous supposerons que la fonction `indice_du_min` a été prouvée correcte (voir exercice).

    1. l'invariant est vrai au début puisque la partie gauche est alors réduite au tableau vide qui est trivialement trié ;
    2. à l'entrée de la boucle on a donc une valeur de compteur à `i` ; et l'hypothèse est que les valeurs jusqu'à l'indice `i-1`  sont triées et inférieures aux valeurs à partir de l'indice `i`. 
      On exécute alors la ligne 4 et on récupère l'indice du minimum des valeurs de la partie droite ; notons $m$ ce minimum. Comme $m$ est dans la partie droite, il est supérieur à tous les éléments `tab[0]`,... `tab[i-1]`. À la ligne 5, on échange les valeurs aux indices `i` et `ind_min` dès lors, `tab[i]` prend la valeur $m$ et on a les deux propriétés suivantes :

        - `tab` est trié jusqu'à l'indice `i`
        - tous les éléments jusqu'à l'indice `i` sont plus petits que ceux à partir de `i+1`

    Dès lors l'invariant est vrai au début de la boucle suivante avec la valeur de compteur à `i+1` : il a bien été conservé




???+ exercice "Exercice"

    Par un affichage bien choisi, deviner un invariant de la fonction `indice_du_min` qui prend un tableau `tab` ainsi qu'un indice `i` en paramètres et qui va renvoyer l'indice du plus petit élément de `tab` à partir de `i` (donc dans la partie droite du tableau).

    Prouver cet invariant pour montrer la correction de la fonction.

## Variant de boucle et preuve de terminaison




[^1]: Voir par exemple [programme de MP2I](https://prepas.org/index.php?document=73), [programme de MPSI](https://prepas.org/index.php?document=72)