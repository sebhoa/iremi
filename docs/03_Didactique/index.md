# Réflexion et Partage

- Partager des constats de réussite ou d'échecs de mon enseignement avec mes étudiants de L1
- Réflexions sur l'utilisation de Python pour l'initiation
- Partage d'idées d'activités
- ...