# Évaporation des concepts de base de programmation

A la rentrée universitaire 2021, environ 45% des étudiant-es de première année de Licence Informatique ont une expérience en programmation Python d'au moins un an. Ils et elles sont 76 à passer un test de programmation consistant en trois questions, des variantes du _rainfall problem_ décrit dans l'article _Do we know how difficult the Rainfall problem is?_[^1]

Cet article expose les résultats obtenus à ce test et met en lumière un certain nombre de problèmes assez récurrents.


## Le test

D'une durée d'une heure environ, ll est constitué de trois exercices (2 programmes et 1 fonction) très proches :

??? exercice "Exercice 1"

    === "Énoncé"

        Écrire un programme Python qui demande à l'utilisateur des valeurs décimales positives jusqu'à ce que l'utilisateur entre la valeur -1. Le programme affiche alors le total des valeurs.

        **Remarques**

        Cet énoncé est un peu ambigu : j'ai en effet oublié de préciser que l'utilisateur suis les indications données et ne vas pas entrer de valeurs autres que numériques et positives ou alors -1. Ca n'était toutefois pas bloquant pour réaliser le programme.

        Néanmoins, il s'agit clairement de la version la plus simple : une simple boucle `while` et 2 variables à gérer.

    === "Solution type attendue"

        ```python linenums="1"
        STOP = -1

        sum_datas = 0
        data = int(input('Donner une valeur positive, tapez -1 pour terminer : '))
        while data != STOP:
            sum_datas += data
            data = int(input('Donner une valeur positive, tapez -1 pour terminer : '))
        print('La somme vaut :', sum_datas)
        ```


??? exercice "Exercice 2"

    === "Énoncé"

        Écrire un programme Python qui demande à l'utilisateur des valeurs décimales (pouvant être positives, nulles ou négatives) jusqu'à ce que l'utilisateur entre une certaine valeur. Le programme affiche alors la moyenne des valeurs positives.

        **Remarques**

        Cette version ajoute 3 difficultés :
        
        - le fait que la valeur _sentinelle_ est laissée au choix du programmeur et qu'elle ne peut pas être numérique
        - la gestion des valeurs négatives dans la boucle
        - la gestion de la division par zéro si aucune valeur positive n'a été fournie


    === "Solution type attendue"

        ```python linenums="1"
        STOP = ''
        
        sum_positive_datas = 0
        count_positive_datas = 0
        data = input('Entrer une valeur numérique, laisser vide pour terminer : ')
        while data != STOP:
            data = float(data)
            if data > 0:
                count_positive_datas += 1
                sum_datas += data
            data = input('Entrer une valeur numérique, laisser vide pour terminer : ')
        if count_positive_datas > 0:
            print('La moyenne des valeurs positives vaut :', sum_positive_datas/count_positive_datas)
        else:
            print('Aucune valeur positive')
        ```


??? exercice "Exercice 3"

    === "Énoncé"

        Écrire une fonction **f3** qui prend en paramètre une liste de valeurs numériques (entières ou décimales, positives, négatives ou nulles) qui calcule et retourne la moyenne des valeurs strictement positives ou rien si aucune valeur ne convient.

        **Remarque**

        La version fonction de l'exercice 2.

    === "Solution type attendue"

        ```python linenums="1"
        def f3(l_datas):
            sum_positive_datas = 0
            count_positive_datas = 0
            for data in l_datas:
                if data > 0:
                    sum_positive_datas += data
                    count_positive_datas += 1
            if count_positive_datas > 0:
                return sum_positive_datas / count_positive_datas
        ```


### Le but des tests

- L'exercice 1 mesure une connaissance minimale de programmation : 

    - enchaîner des instructions simples dans un programme pour réaliser une tâche donnée ;
    - savoir initialiser et mettre à jour des variables ;
    - savoir faire une boucle simple ;
    - afficher un résultat 

- L'exercice 2 ajoute des difficultés :

    - utiliser une instruction conditionnelle ;
    - se rendre compte et gérer qu'une variable obtenue par `input` est de type `str` ;
    - prendre une décision face à une information non complètement spécifiée dans l'énoncé ;

- L'exercice 3 mesure si le candidat sait définir une fonction et manipuler le `return` ainsi que l'utilisation de la boucle `for`


## Typologie des étudiant-es

Soixante-seize étudiant-es composaient. Leur expérience en programmation Python est diverse. Environ la moitié (36) ont suivi deux années de spécialité NSI (Première et Terminale), 1/4 sont des _redoublant-es_ de la filière L1 Informatique (19), l'autre quart sont des étudiant-es qui ont fait du Python au lycée en cours de mathématiques ou en BTS ou encore en autodidacte (3).

![origine de l'expérience](../assets/images/rainfall/origine_experience.svg){: .centrer}

## Résultats

Ils sont globalement mauvais : 21% seulement arrive à obtenir une note sur 20 supérieure ou égale à 10 contre 70% environ qui rendent une copie blanche ou presque.

![résultats](../assets/images/rainfall/resultats.svg){: .centrer}

### Les étudiant-es ayant suivi NSI en Terminale

Ce sont ceux et celles qui s'en sortent le mieux : 81% des notes au-dessus de 10 sont des candidat-es ayant deux années de spécialité. Les meilleures notes (18) sont aussi dans cette catégorie.

### Les redoublants

Ils sont les premiers à quitter la salle en rendant une copie blanche. Aucun des 19 candidats ne réussit : à part un 4/20, il n'y a que des 0 de copies blanches ou presque.

### Les autres

Parmi les candidat-es ne faisant pas partie des deux catégories précédentes, à noter 2 autodidactes qui obtiennent les deux meilleurs scores de la catégorie (13 et 14). Des élèves de classe prépa s'en sortent moyennement (7 et 12). Les 6 possesseurs d'un BAC STI2D option SIN ont tous 0.

## Exemples de copies

??? example "Avouent ne plus savoir"

    Parmi les copies vierges, beaucoup avouent ne plus se souvenir.

    ![ne sais pas](../assets/images/rainfall/ne_sais_pas.jpg){: .centrer}


??? example "Bribes de syntaxe"

    Parmi les moins de 5, on constate comme un souvenir de quelques bribes de syntaxe, sans possibilité de construire quelque chose de cohérent algorithmiquement :

    ![bribes 1](../assets/images/rainfall/bribes_syntaxe_1.JPG){: .centrer}

    --- 

    ![bribes 2](../assets/images/rainfall/bribes_syntaxe_2.JPG){: .centrer}

    --- 

    ![bribes 3](../assets/images/rainfall/bribes_syntaxe_3.JPG){: .centrer}

    Dans les deux derniers exemples, on note aussi des soucis de confusion entre `print` et `return`


??? example "Ne savent pas ce qu'est un programme"

    Il n'y en a pas eu tant que ça compte tenu du discours officiel sur les `print` et `input`. Mais 11 cas au total dont 9 parmi les NSI Terminale !

    ![pb prog 1](../assets/images/rainfall/pb_prog.JPG){: .centrer}

    --- 

    ![pb prog 2](../assets/images/rainfall/pb_prog_2.JPG){: .centrer}


??? example "Confusion `print` et `return`" 

    Problème relevé dans 6 copies sur les 41 qui n'ont pas eu 0.

    ![print return 1](../assets/images/rainfall/print_return.JPG){: .centrer}

    --- 

    ![print return 2](../assets/images/rainfall/print_return_2.JPG){: .centrer}

    --- 

    ![print return 3](../assets/images/rainfall/print_return_3.JPG){: .centrer}


??? example "Gestion de la valeur _sentinelle_" 

    Choix d'une valeur numérique arbitraire :

    ![sentinelle 1](../assets/images/rainfall/sentinelle_1.JPG){: .centrer}

    ![sentinelle 4](../assets/images/rainfall/sentinelle_4.JPG){: .centrer}

    --- 

    Choix d'une chaîne de caractères :

    ![sentinelle 2](../assets/images/rainfall/sentinelle_2.JPG){: .centrer}

    --- 

    C'est l'utilisateur qui choisit sa sentinelle :

    ![sentinelle 3](../assets/images/rainfall/sentinelle_3.JPG){: .centrer}



??? example "Complexification, programme mal structuré"

    Ci-dessous le code proposé par un étudiant autodidacte pour l'exercice n°1 :
    ```python
    # déclaration variable
    insert = 0
    sum = 0
    sum_list = []

    # définition fonction
    def decimal_entry():
        insert = float(input('Entrez un nombre décimal positif'))
        if insert < -1:
            decimal_entry()
        else sum_list.append(entry)

    # boucle de vérification
    while insert != -1:
        decimal_entry()
    for numbers in sum_list:
        sum += numbers
    print(sum)
    ```

    On note que le code mélange au milieu du code du programme la definition d'une fonction.. récursive ! 


??? example "Un peu de POO ?!"

    Ci-dessous un candidat qui d'un coup a voulu faire de la POO pour la question 3 :

    ![poo](../assets/images/rainfall/poo.JPG){: .centrer}


## Conclusion

Il est assez difficile de comparer les résultats avec ceux de l'article cité. Néanmoins dans le tableau final de cet article les résultats sont très disparates et certains pourcentages très bas (moins de 15%). D'autre part, au moins pour les tests effectués par les auteurs de l'article, les tests ont été effectués :

- sur machine (ce qui facilite grandement la tâche)
- juste après le cours d'initiation et non après 2 mois de vacances

Il ne faut donc pas dramatiser les résultats bruts et ne pas s'inquiéter de ce que deux années de NSI n'ont pas laissé plus de souvenirs. Rappelons aussi que cette génération a subit deux années de crise COVID avec les contraintes que l'on sait sur les cours.

### Conclusion bis 

Pour le quatrième et dernier contrôle continu de l'UE de _Programmes et Algorithmes_, j'ai redonné le fameux test _rainfall_.

#### Beaucoup d'abandons

Comme chaque année, un des taux les plus élevé est celui des abandons. Plus du quart (27%) des étudiant-es qui ont passé le test de rentrée, ne se sont pas présentés au contrôle n°4. Et tous, sauf 1, avaient obtenu une note inférieure à 5. Sans surprise, ces abandons se retrouvent majoritairement (57%) parmi les redoublants et les lycéen-nes des filières STI2D, BTS SIN. Le seul STI2D qui s'en sort très bien (passe de 4 à 17) avait aussi fait une année de BTS (et donc a été sélectionné, preuve de son bon dossier au départ).


**On ne rattrape pas les étudiant-es très faibles dans une L1 classique.** 



#### Des résultats bruts meilleurs...

Alors qu'ils ne sont que 21% au-dessus de 10 sur 20 au test de rentrée, cette part passe à 38%.

![notes_rentree](../assets/images/rainfall/pie_notes_rentree.png)
![notes_cc4](../assets/images/rainfall/pie_notes_cc4.png)

La note moyenne n'est pas une information très pertinente. Parmi les 179 présents au contrôle n°4, beaucoup (environ 70 soit 40%) ne viennent que valider une présence et rendent une copie blanche ou presque. Si on ne compte pas ceux là, la note moyenne est de 10,3 contre 4 pour le test de rentrée. Même si on comptabilise toutes les copies, la moyenne au contrôle n°4 reste supérieure : 6,8.

Il est heureux de constater que les quelque semaines d'enseignement ont quand même servi à quelque chose.

#### NSI jusqu'en Terminale améliore les résultats ?

La réponse est mitigée : la moyenne des présents aux deux épreuves passe de 7 à 10,5 pour les étudiant-es validant deux années de NSI. Pour les autres elle passe de 3 à 7,2.

Néanmoins, les NSI2 ne sont que 16% à ne pas s'être pésenté-es au controle contre 37% pour les autres. Leurs bases plus solides semble donc les met donc à l'abri du découragement.

Sur les 30 NSI2 qui se sont présenté-es au contrôle, six retrouvent leur mémoire et passent d'une note entre 2 et 7 à une note entre 13,4 et 18,6. Mais cette très forte progression se retrouve chez d'autres profils :

- 3 étudiant-es ayant déjà un niveau L1 : l'étudiant issu de classe préparatoire (de 7 à 14,8), celui qui avait déjà suivi une première année améliore (5 en 12,6) et l'étudiant de BTS (4 à 17) ;
- 2 étudiant-es de spécialité mathématiques (0 à 11,2 et 3 à 17).


[^1]: [Do we know how difficult the Rainfall problem is?](https://www.researchgate.net/publication/288002756_Do_we_know_how_difficult_the_Rainfall_Problem_is), O. Seppälä, P. Ihantola, E. Isohanni, J. Sorva et A. Vihavainen, _Proceedings of the 15th Koli Calling Conference on Computing Education Research_ (2015) 
