**Option Sciences du Numérique**<br>
Filière MPI

--- 

# Concours EPITA - IPSA - ESME  

???+ warning "Avertissement"

    Ce document n'est pas un corrigé du sujet officiel. D'ailleurs les parties de programmation se feront toutes en Python, et non en OCaml et C comme demandé.

## I. Mots de Dyck[^1] et arbres binaires

--8<-- "includes/epita_2023/exo_I.md"