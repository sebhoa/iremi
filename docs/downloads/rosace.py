"""
Dessine sur un cercle les tables de multiplication formant
ainsi une belle rosace
Usage : rosace.py -t n -m m -c
Dessine la table de n sur m points répartis sur un cercle
Si l'option -c est présente, le cercle sera en couleur

Auteur : Sébastien Hoarau
"""

import turtle
import argparse
import random

class Rosace:

    DEFAULT_TABLE = 2
    DEFAULT_MODULO = 10
    DEFAULT_COLOR = 'black'
    RADIUS = 200
    INITIAL_POS = 0, -RADIUS

    def __init__(self):
        self.table = Rosace.DEFAULT_TABLE
        self.modulo = Rosace.DEFAULT_MODULO
        self.pts = []
        self.vue = turtle.Turtle()


    def settings(self):
        # Réglagles de la tortue
        #
        self.vue.ht()               # masquer la tortue
        self.vue.screen.tracer(400) # ça c'est juste pour dessiner plus vite
        self.vue.screen.colormode(255)
        self.vue.color(Rosace.DEFAULT_COLOR)

        # Traitement des options
        #
        parser = argparse.ArgumentParser()
        parser.add_argument('-t', help='Un entier entre 2 et 9, la table qu\'on va dessiner')
        parser.add_argument('-m', help='Un diviseur de 360')
        parser.add_argument('-c', help='Rajoute de la couleur', action='store_true')

        args = parser.parse_args()
        if args.t:
            self.table = int(args.t)
        if args.m:
            modulo = int(args.m)
            if 360 % modulo == 0:
                self.modulo = modulo
        if args.c:
            self.vue.color(self.random_color())


    def random_color(self):
        return tuple([random.randint(0,255) for _ in range(3)])

    def mainloop(self):
        self.vue.screen.update()    # pour mettre à jour le canvas (à cause du tracer(400))
        self.vue.screen.mainloop()

    # -- FONCTIONS DE DESSIN
    # --

    def move_to(self, pos, trace=False):
        if not trace:
            self.vue.up()
        self.vue.goto(pos)
        self.vue.down()


    def draw_circle(self):
        """
        Dessine le cercle intial et récupère en même temps les 
        différents points sur ce cercle correspondant aux entiers
        0, 1, 2... self.modulo - 1
        """
        self.move_to(Rosace.INITIAL_POS)
        delta_angle = 360 // self.modulo
        for _ in range(self.modulo):
            self.pts.append(self.vue.pos())
            self.vue.circle(Rosace.RADIUS, delta_angle)


    def draw_segments(self):
        """
        Dessine la multiplication : m = n * self.table pour les
        n de 1 à self.modulo - 1, en liant les points n et m 
        """
        for n in range(1,self.modulo):
            m = (self.table * n) % self.modulo 
            self.move_to(self.pts[n])
            self.move_to(self.pts[m], True)

    def update(self):
        self.vue.screen.update()

    def reset(self):
        self.pts.clear()
        self.vue.clear()



# MAIN

rosace = Rosace()
rosace.settings()
rosace.draw_circle()
rosace.draw_segments()
rosace.mainloop()





