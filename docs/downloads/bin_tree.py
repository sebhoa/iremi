import PyGraph.pygraph as pg

class ArbreBin:

    def __init__(self, racine=None, gauche=None, droite=None):
        self.racine = racine
        if racine is not None:
            self.gauche = gauche if isinstance(gauche, ArbreBin) else ArbreBin()
            self.droite = droite if isinstance(droite, ArbreBin) else ArbreBin()

    def vide(self):
        return self.racine is None

    def hauteur(self, hauteur_vide=0):
        return hauteur_vide if self.vide() else 1 + max(self.gauche.hauteur(), self.droite.hauteur())
        
    def taille(self):
        return 0 if self.vide() else 1 + self.gauche.taille() + self.droite.taille()

    def feuille(self):
        return not self.vide() and self.gauche.vide() and self.droite.vide()
    
    def feuilles(self):
        if self.vide():
            return []
        elif self.gauche.vide() and self.droite.vide():
            return [self.racine]
        else:
            return self.gauche.feuilles() + self.droite.feuilles()


class ABR(ArbreBin):

    def __init__(self):
        ArbreBin.__init__(self)

    def ajoute(self, elt):
        if self.vide():
            self.racine = elt
            self.gauche = ABR()
            self.droite = ABR()
        elif elt < self.racine:
            self.gauche.ajoute(elt)
        elif elt > self.racine:
            self.droite.ajoute(elt)


class ArbreViz:
    
    def __init__(self, arbre, coef=1, oriente=False):
        self.arbre = arbre
        self.size = arbre.taille()
        self.hauteur = arbre.hauteur()
        self.coef = coef
        self.oriente = oriente
        self.link = {}
        self.__vue = self.__setup()

    def reset(self):
        self.__vue = self.__setup()
        
    def __setup(self):
        """crée un pygraph pour visualiser un arbre binaire"""
        g = pg.DiGraph(self.size) if self.oriente else pg.Graph(self.size)
        l_nodes = list(g.node_ids())
        l_nodes.reverse()
        racine = l_nodes.pop()
        hauteur = self.hauteur
        pos = (2 ** hauteur - 1) * self.coef, 0 
        self.__create_edges(self.arbre, g, racine, pos, hauteur, l_nodes)
        g.scale(0.6)
        g.label_on()
        return g

    
    def __create_edges(self, arbre, g, nracine, pos_racine, hauteur, l_nodes):
        """fonction interne : ajoute les arêtes et les positions des sommets dans la vue g de l'arbre"""
        x, y = pos_racine
        position = (nracine, x, y)
        g.position([position]) 
        coef = self.coef
        demi = (2 ** (hauteur - 2)) * coef
        if not arbre.vide():
            g.node_view(nracine).label = str(arbre.racine)
            self.link[arbre.racine] = nracine
            ng, nd = None, None
            if not arbre.gauche.vide():
                ng = l_nodes.pop()
                g.add_edge(nracine, ng)
            if not arbre.droite.vide():
                nd = l_nodes.pop()
                g.add_edge(nracine, nd)
            if ng is not None:
                position_g = x - demi, y-1
                self.__create_edges(arbre.gauche, g, ng, position_g, hauteur-1, l_nodes)
            if nd is not None:
                position_d = x + demi, y-1
                self.__create_edges(arbre.droite, g, nd, position_d, hauteur-1, l_nodes)
         
    @property
    def vue(self):
        return self.__vue.view
    
    def small(self):
        self.__vue.resize(0.1)
        self.__vue.label_off()
        for s in self.__vue.node_ids():
            self.__vue.node_view(s).label_off_side()
    
    def normal(self):
        self.__vue.resize(0.3)
        self.__vue.set_labels()
        
    def zoom(self, ech=1):
        self.__vue.scale(ech)
        
            
    def colore_feuille(self, label=None, color=0):
        if label is None:
            feuilles = self.arbre.feuilles()
            for node_id in feuilles:
                self.__vue.color_on(self.link[node_id], color)
        else:
            for node_id in self.__vue.node_ids():
                if self.__vue.node_view(node_id).label == label:
                    self.__vue.color_on(node_id, color)
            
        
    def color_off(self):
        self.__vue.color_off()
                
def __parcours_dicho(triees, deb=None, fin=None):
    if fin is None:
        fin = len(triees) - 1
    if deb is None:
        deb = 0
    if fin < deb:
        return []
    else:
        m = (deb + fin) // 2 
        gauche = __parcours_dicho(triees, deb, m-1)
        droite = __parcours_dicho(triees, m+1, fin)
        return [triees[m]] + gauche + droite

def abr_from(triee):
    """crée un ABR à partir d'une liste triée"""
    if len(triee) == 0:
        return ABR()
    else:
        valeurs = __parcours_dicho(triee)
        abr = ABR()
        for e in valeurs:
            abr.ajoute(e)
        return abr