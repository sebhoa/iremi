import turtle

class Circuit:

    def __init__(self, code, size=50):
        self.code = code
        self.size = size

    def avance(self):
        turtle.fd(self.size)
        turtle.stamp()
        turtle.up()
        turtle.fd(5)
        turtle.down()

    def gauche(self):
        turtle.left(90)

    def droite(self):
        turtle.right(90)

    def depart(self):
        turtle.hideturtle()
        turtle.dot()

    def show(self):
        move = {'A': self.avance, 'G': self.gauche, 'D': self.droite}
        self.depart()
        for c in self.code:
            move[c]()
        turtle.mainloop()

# Version 1re

def depart():
    """place un point au départ et masque la tortue"""
    turtle.hideturtle()
    turtle.dot()

def avance(d=50):
    """avance d'une longueur d, valant 50 par défaut puis rajoute une
    petite flèche et un petit espace à la suite du trait
    """
    turtle.fd(d)
    turtle.stamp()
    turtle.up()
    turtle.fd(5)
    turtle.down()

def gauche():
    """pivote la tortue de 90° vers la gauche, sans bouger"""
    turtle.left(90)

def droite():
    """pivote la tortue de 90° vers la droite, sans bouger"""
    turtle.right(90)

def trace(circuit):
    """trace le circuit modélisé par la liste circuit"""
    depart()
    for instruction in circuit:
        if instruction == 'A':
            avance()
        elif instruction == 'G':
            gauche()
        elif instruction == 'D':
            droite()

def tracer_circuit(circuit, d):
    for instruction in circuit:
        if instruction == 'A':
            turtle.forward(d)
        elif instruction == 'G':
            turtle.left(90)
        elif instruction == 'D':
            turtle.right(90) 

Q2 = ['A', 'A', 'D', 'A', 'D', 'A', 'G', 'A', 'D', 'A', 
                  'D', 'A', 'G', 'A', 'D', 'A', 'D', 'A']
E1 = ['A', 'G', 'A', 'D', 'A', 'G', 'A', 'G', 'A', 'A', 'G', 'A', 'A']
Q1 = ['A', 'G', 'A', 'D', 'A', 'D', 'A', 'A', 'D', 'A', 'G', 'A', 'D', 'A', 'D', 'A', 'A']
Q12 = ['G','A','D','A','D','A','A','G','A','A','G','A','G','A','A','A']

trace(Q12)

# turtle.hideturtle()
# for _ in range(4):
#     tracer_circuit(['D', 'A', 'G', 'A', 'G', 'A'], 20)

turtle.mainloop()