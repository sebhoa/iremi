import graphviz as gv

GREY = '#e0e0e0'
BLEU = 'lightblue'
ROUGE = 'salmon'

NODE_OPTS = {
    'fixedsize': 'true',
    'width': '0.25',
    'height': '0.25',
    'style': 'filled',
    'fontsize': '10',
    'fillcolor': GREY
}

def show(arbre_binaire, show_vide=True, colored=False):
    """ Renvoie un objet graphviz pour la visualisation graphique de l'arbre
    show_vide est un booléen indique si l'arbre vide doit être représenté ou pas
    colored à True permet de colorer différemment les sous arbres gauche et droit
    """
    
    def representation(dot, arbre, aretes, color):
        dot_node_id = str(arbre_id(arbre)) 
        if est_vide(arbre) and show_vide:
            dot.node(dot_node_id, 'ø', {'shape': 'plaintext', 'style': ''})
        elif not est_vide(arbre):
            dot.node(dot_node_id, str(valeur(arbre)), {'fillcolor':color if colored else GREY})
            representation(dot, gauche(arbre), aretes, BLEU)
            if not est_vide(gauche(arbre)) or show_vide:
                aretes.append((dot_node_id, str(arbre_id(gauche(arbre)))))
            representation(dot, droit(arbre), aretes, ROUGE)
            if not est_vide(droit(arbre)) or show_vide:
                aretes.append((dot_node_id, str(arbre_id(droit(arbre)))))

    dot = gv.Graph(format='svg', node_attr=NODE_OPTS)
    aretes = []
    representation(dot, arbre_binaire, aretes, GREY)
    dot.edges(aretes)
    return dot