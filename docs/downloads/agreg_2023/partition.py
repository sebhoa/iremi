class Partition:
    """Modélise une structure de union-find à l'aide de listes Python :
    - parents : tableau d'entiers (numéros de nœud) donne le parent de chacun des nœuds
    - repr : tableau de booléens, à repr[node_id] vaut True ssi node_id est un représentant
    """
    
    def __init__(self, n):
        self.parents = list(range(n))
        self.repr = [True] * n
        self.size = n

    # 2.4
    def find(self, node_id):
        """recherche et renvoie le représentant du nœud node_id, et réalise la compression"""
        if self.repr[node_id]:
            return node_id
        else:
            repr_id = self.find(self.parents[node_id])
            self.parents[node_id] = repr_id
            return repr_id
        
    def union(self, node_i, node_j):
        """réalise l'union de deux nœuds"""
        repr_i = self.find(node_i)
        repr_j = self.find(node_j)
        if repr_i != repr_j:
            self.parents[repr_i] = repr_j
            self.repr[repr_i] = False