class Buf(Partition):
    
    # 2.3
    def __init__(self, n):
        Partition.__init__(self, n)
        self.rang = [1] * n
    
    def copy(self):
        buf = Buf(self.size)
        buf.repr = self.repr.copy()
        buf.parents = self.parents.copy()
        buf.rang = self.rang.copy()
        return buf
        
    # 2.5
    def blocs(self):
        resultat = {node_id:set() for node_id in range(self.size) if self.repr[node_id]}
        for node_id in range(self.size):
            repr_id = self.find(node_id)
            resultat[repr_id].add(node_id)
        return resultat
        
    # 2.6 
    def chaine_racine(self, node_id):
        parent_id = self.parents[node_id] 
        if parent_id == node_id:
            return [node_id]
        else:
            chaine = self.chaine_racine(parent_id)
            if self.repr[node_id]:
                chaine.append(node_id)
            return chaine
        
    # 2.8 
    def retourner_chaine(self, u, v):
        chaine_u = self.chaine_racine(u)
        dernier = chaine_u[-1]
        for i in range(len(chaine_u) - 1):
            node_id, suivant_id = chaine_u[i], chaine_u[i+1]
            self.parents[node_id] = suivant_id
        return dernier

    # si on ajoute le lien du parent   
    def ajout_non_connexe(self, u, v):
        dernier = self.retourner_chaine(u, v)
        self.parents[dernier] = self.find(v)
            
    # 2.10
    def union(self, repr_i, repr_j):
        """réalise l'union des deux blocs représentés par repr_i et repr_j
        repr_j est censé avoir le plus petit rang et devenir le représentant
        de l'union, si ce n'est pas le cas on switche
        """
        if repr_i != repr_j:
            if self.rang[repr_j] > self.rang[repr_i]:
                repr_i, repr_j = repr_j, repr_i
            self.parents[repr_i] = repr_j
            self.repr[repr_i] = False
            self.rang[repr_j] += 1
        return repr_j

    # 2.11
    def fusion_chaine(self, chaine):
        exterieur = self.find(self.parents[chaine[0]])
        while len(chaine) > 1:
            v = chaine.pop()
            u = chaine.pop()
            if self.union(u, v) == u:
                chaine.append(u)
            else:
                chaine.append(v)
        repr_final = chaine[0]
        if self.repr[exterieur]:
            self.parents[repr_final] = exterieur
        else:
            self.parents[repr_final] = repr_final
    
    # 2.12
    def ajout(self, u, v):
        r_u = self.find(u)
        r_v = self.find(v)
        if r_u != r_v:
            chaine_u = self.chaine_racine(u)
            chaine_v = self.chaine_racine(v)
            if chaine_u[0] != chaine_v[0]:
            # cas de 2 composantes connexes distinctes (voir 2.8)
                dernier = chaine_u[-1]
                for i in range(len(chaine_u) - 1):
                    node_id, suivant_id = chaine_u[i], chaine_u[i+1]
                    self.parents[node_id] = suivant_id
                self.parents[dernier] = r_v
            else:
            # cas de la même composante
                k, n = 0, min(len(chaine_u), len(chaine_v))
                while k < n and chaine_u[k] == chaine_v[k]:
                    k += 1
                chaine = chaine_u[k-1:] + chaine_v[k:]
                self.fusion_chaine(chaine)