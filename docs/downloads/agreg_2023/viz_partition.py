class VizPartition:
    """Pour visualiser une Partition"""
    
    def __init__(self, partition):
        self.partition = partition
        self.size = partition.size
        self.__sagittal = pg.DiGraph(self.size, strict=True)
        self.__position = []
        self.init_sagittal()
        
    def init_sagittal(self):
        parents = self.partition.parents
        representants = self.partition.repr
        for node in range(self.size):
            parent = parents[node]
            if node != parent:
                self.__sagittal.add_edge(node, parent)
            if representants[node]:
                self.__sagittal.color_on(node, 4)

    @property
    def sagittal(self):
        return self.__sagittal.view

    @property
    def position(self):
        return self.__position
    
    @position.setter
    def position(self, iterable):
        if self.__position == []:
            self.__position = [None] * self.size
        for indice, info in enumerate(iterable):
            if len(info) == 3:
                i, x, y = info
            else:
                i = indice
                x, y = info
            self.__position[i] = i, x, y
        self.__sagittal.position(self.position, ech=0.6)
            
    def racine(self, node_id):
        parent_id = self.partition.parents[node_id]
        return node_id if parent_id == node_id else self.racine(parent_id)
    
    def check_position(self):
        return len(self.__position) == self.size and all(isinstance(pos, tuple) and len(pos) == 3 for pos in self.__position)
    
    def change_position(self, node_id, x, y, group=False):
        """Positionne le nœud node_id au point x, y et bouge toute la composante connexe si group vaut True"""
        if self.check_position():
            _, old_x, old_y = self.__position[node_id]
            dx, dy = x - old_x, y - old_y
            self.move(node_id, dx, dy, group)
    
    def move(self, node_id, dx, dy, group=False):
        if self.check_position():
            self.__sagittal.move(node_id, dx, dy, group)            
    
    
    def svg(self, filename='output'):
        self.__sagittal.write(filename=filename)
        
    def png(self, filename='output'):
        self.__sagittal.write(filename=filename, format='png')