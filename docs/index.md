# Bienvenue !

![Python de Neiges](assets/images/python_des_neiges_small.png)

Explorer grâce au langage Python. Le nom du site est un clin d'oeil au langage de G. Van Rossum et à l'[ile de la Réunion et son plus haut sommet](https://fr.wikipedia.org/wiki/Piton_des_Neiges). Merci à ma fille Raphaëlle pour l'illustration...

