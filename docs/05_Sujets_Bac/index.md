# Les exercices de programmation dans les sujets de BAC

La section suivante se propose d'étudier les exercices de programmation Python des divers sujets du Baccalauréat.

Les [sujets sont centralisés sur le forum NSI](https://mooc-forums.inria.fr/moocnsi/t/centralisation-des-sujets-de-baccalaureat/3139)

