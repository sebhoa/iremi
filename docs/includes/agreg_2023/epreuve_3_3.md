### Gestion des tournées

Dans la suite, on s'intéresse aux livraisons. Il s'agit d'attribuer à un certain nombre de camions, une liste de clients à livrer, dans un certain ordre, en partant de l'entrepôt (le nœud $0$ du graphe) et en y revenant. Les questions $2$ à $4$ s'intéressent à un algorithme naïf et ses limitations. Voici la description de cet algorithme dont l'entrée est le tableau des distances, le tableau des demandes des clients et la capacité maximale des camions.

1. on prend un premier camion et on lui affecte le client de plus proche qui n'a pas encore été planifié,
2. si la demande de ce client ne met pas le camion en surcharge alors on procède effectivement à l'affectation,
3. sinon on essaie le $2^e$ client le plus proche etc.
4. on continue ainsi jusqu'à atteindre la capacité du camion,
5. on recommence avec un autre camion, jusqu'à ce que tous les clients soient affectés à une tournée

### Modéliser

Il nous faut modéliser deux choses : les tournées de tous les clients et la tournée d'un camion.

- une _tournée_ : une liste Python de clients
- les _tournées_ : une liste de _tournée_


### Question 2

???+ question "2.1"

    Appliquer l'algorithme naïf sur les données de l'exemple 1.

??? success "Réponse"

    - On initialise une première tournée avec le client le plus proche de l'entrepôt (il s'agit du client $2$) : `[2]` ; la charge du camion est alors de $6$ (la demande du client $2$),
    - on continue : le client le plus proche de $2$ est $1$ avec une demande à $5$ ce qui porterait la charge totale du camion à $11$ on est en-dessous de la capacité max qui est $12$ ; on a donc `[2, 1]`
    - le client suivant le plus proche de $2$ est $3$ mais la demande viendrait surcharger le camion, on doit donc initier un nouveau camion, avec le client le plus proche de l'entrepôt (il se trouve que c'est toujours le client $3$) : `[3]`
    - le dernier client est pris en charge par le $2^e$ camion : `[3, 4]`

    Les tournées sont donc, avec leurs couts respectifs : 

    - `[2, 1]` pour un cout de $4$ ($0 \rightarrow 2 : 1$, $2 \rightarrow 1 : 1$, $1 \rightarrow 0 : 2$) ;  
    - `[3, 4]` pour un cout de $13.5$.

    On peut d'ailleurs écrire une fonction `cout` qui, étant donnés un tableau de distances et une tournée, renvoie le cout :


???+ tip "Une fonction pour le calcul du cout"

    ```python
    def cout(dist, tournee):
        km = dist[0][tournee[0]]
        for i in range(1, len(tournee)):
            km += dist[tournee[i-1]][tournee[i]]
        km += dist[tournee[-1]][0]
        return km
    ```

    Par exemple :

    ```python
    >>> cout(DIST, [2, 1])
    4
    >>> cout(DIST, [3, 4])
    13.5
    ```

    Le cout total peut alors se calculer ainsi :

    ```python
    def cout_total(dist, tournees):
        return sum(cout(dist, t) for t in tournees)
    ```

???+ question "2.2"

    En considérant l'objectif de minimiser la distance totale parcourus par tous les véhicules, peut-on trouver une autre solution ? Si oui, laquelle ?

??? success "Réponse"

    Il ne semble pas possible d'améliorer sur ce petit exemple.


### Question 3

On nous demande l'impact de la suppression du client $v_2$ sur le reste de la gestion. Alors sans le client $v_2$ une seule tournée suffit : $1 \rightarrow 3 \rightarrow 4$ pour une charge totale à $12$.

### Question 4

???+ question "La question"

    Il s'agit d'écrire la fonction `algo_naif` qui code la méthode de génération de tournées. 
    
    ???+ warning "Avertissements"
    
        On regrettera dans un sujet de l'agrégation le nom respect PEP8 sur le nommage de la fonction ainsi que l'indication _constante entière_ pour le paramètre `c` (`C` dans le sujet) : si c'est une constante ça n'est pas un paramètre.

Avant d'écrire la fonction `algo_naif`, nous allons présenter quelques fonction auxiliaires.

???+ tip "Quelques fonctions annexes"

    Commençons par trier les voisins d'un nœud $s$ du plus proche au plus éloigné. C'est ce que réalise la fonction `voisins_proches` ci-dessous.

    ```python
    def voisins_proches(dist, s, visites):
        return sorted((v for v in range(1, len(dist)) if v not in visites), key=lambda x: dist[s][x])
    ```

    Nous aurons aussi besoin de calculer la charge d'un véhicule :

    ```python
    def charge(tournee, demandes):
        return sum(demandes[c] for c in tournee)
    ```

??? success "Réponse"

    Notre fonction commence par :

    - préciser que l'entrepôt a été _planifié_ (ce qui permet de ne pas le considérer dans les tournées qui vont être créées) (ligne 3)
    - trouver les clients proche de l'entrepôt (ligne 4) et 
    - initialiser la liste des tournées avec une première tournée qui ne contient que le client le plus proche (ligne 5)
    - ensuite il reste à faire $n$ tours de _boucle_ (où $n$ est le nombre de **vrais** clients moins $1$, c'est-à-dire `nb_clients - 2` dans notre modèle qui comptabilise l'entrepôt parmi les clients)

    ```python linenums="1"
    def algo_naif(dist, demandes, capacite):
        nb_clients = len(dist)
        planifies = {0}
        clients = voisins_proches(dist, 0, planifies)
        tournees = [[clients[0]]]
        for _ in range(nb_clients - 2):
            trouve = False
            tournee = tournees[-1]
            charge_courante = charge(tournee, demandes)
            dernier_client = tournee[-1]
            planifies.add(dernier_client)
            voisins = voisins_proches(dist, dernier_client, planifies)
            for suivant in voisins:
                charge_a_ajouter = demandes[suivant]
                if charge(tournee, demandes) + charge_a_ajouter <= capacite:
                    tournee.append(suivant)
                    trouve = True
                    break
            if not trouve:
                voisins = voisins_proches(dist, 0, planifies)
                nlle_tournee = [voisins[0]]
                tournees.append(nlle_tournee)
        return tournees
    ```

???+ example "Appliquons sur l'exemple..."

    ```python
    >>> DEMANDES = [None, 5, 6, 4, 3]
    >>> C = 12
    >>> T1 = algo_naif(DIST, DEMANDES, C)
    >>> T1
    [[2, 1], [3, 4]]
    ```

La fin du sujet consistera à :

- montrer qu'il existe des situations où l'algorithme naïf ne donne pas la solution optimale
- coder un autre algorithme