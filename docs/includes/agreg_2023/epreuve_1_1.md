Commençons par le reproduire $G_1$, le graphe de la Figure 1 :

???+ example "Graphe de la Figure 1"

    === "$G_1$"

        ![FIG. 1](../../assets/images/agreg_2023/fig1.svg)

    === "Le code"

        Voici les instructions Python pour l'obtenir et le visualiser, dans un notebook Jupyter :

        ```python
        import PyGraph.pygraph as pg

        G1 = pg.Graph(16)
        G1_EDGES = [(0, 2), (0, 3), (2, 3), (1, 3), (3, 4), (4, 5), (4, 6), (5, 6), (6, 7), 
                    (6, 8), (6, 9), (8, 9), (10, 11), (10, 12), (11, 12), (11, 15), (13, 14), 
                    (13, 15), (14, 15)]
        G1.add_edges_from(G1_EDGES)
        
        G1_POS = [(0, 0, 0), (3, 1, 0), (4, 2, 0), (6, 3, 0), (9, 4, 0), (10, 5, 0), (11, 6, 0), 
                  (15, 7, 0), (1, 0.5, 1), (5, 2.5, 1), (7, 3.5, 1), (12, 5.5, 1), (2, 0.5, -1),
                  (8, 3.5, -1), (13, 6.5, -1), (14, 7.5, -1)]
        G1.position(G1_POS, ech=0.6)
        G1.view
        ```

Ce graphe servira de support à l'ensemble du sujet où l'on s'intéresse aux :

- **ponts** qui sont des arêtes du graphe dont la suppression fait croitre le nombre de composantes connexes ;
- **blocs** qui sont les composantes connexes restantes lorsqu'on a supprimé tous les ponts d'un graphe.

??? question "Ponts et blocs du graphe $G_1$"

    === "Quiz"

        1. Parmi les arêtes suivantes, lesquelles sont des ponts ?
            - [ ] $(0, 2)$
            - [ ] $(1, 3)$
            - [ ] $(5, 6)$
            - [ ] $(3, 4)$
            - [ ] $(8, 9)$
            - [ ] $(6, 7)$
            - [ ] $(11, 15)$
        2. Combien de blocs $G_1$ possède-il ?
            - [ ] 3
            - [ ] 2
            - [ ] 6
            - [ ] 5   
    
    === "Réponse"
 
        3. Parmi les arêtes suivantes, lesquelles sont des ponts ?
            - [ ] $(0, 2)$
            - [x] $(1, 3)$
            - [ ] $(5, 6)$
            - [x] $(3, 4)$
            - [ ] $(8, 9)$
            - [x] $(6, 7)$
            - [x] $(11, 15)$
        4. Combien de blocs $G_1$ possède-il ?
            - [ ] 3
            - [ ] 2
            - [x] 6
            - [ ] 5 


??? success "La solution en image"

    La Figure 2 du sujet, met en avant ponts et blocs du graphe exemple, nous le faisons ici, avec un peu plus de couleurs. On retrouve les $6$ blocs et les $4$ ponts (les arêtes en bleu).

    === "G1 colorisé"

        ![FIG. 2](../../assets/images/agreg_2023/fig2.svg)

    === "Le code"

        Nous définissons deux petites fonctions pour colorier les blocs d'un graphe et les ponts :

        ```python title="Coloration de blocs"
        def colorier_blocs(graphe, blocs):
            """colorie les sommets de l'itérable blocs via color_on méthode du module pygraph
            on se sert du numéro du bloc dans l'énumération comme numéro de couleur
            """
            for color_id, set_of_nodes in enumerate(blocs):
                for node_id in set_of_nodes:
                    graphe.color_on(node_id, color_id)
        ```

        ```python title="Coloration de ponts"
        def colorier_ponts(graphe, ponts, color='lightblue'):
            for a, b in ponts:
                graphe.color_on_edge(a, b, color)
        ```

        Et finalement,

        ```python
        G1b = G1.copy()
        BLOCS_G1 = [[1], [0, 2, 3], [4, 5, 6, 8, 9], [7], [10, 11, 12], [13, 14, 15]]
        PONTS_G1 = [(1, 3), (3, 4), (6, 7), (11, 15)]
        colorier_blocs(G1b, BLOCS_G1)
        colorier_ponts(G1b, PONTS_G1)
        ```