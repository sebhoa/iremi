Le sujet propose de représenter un graphe à l'aide d'une forêt qui sera une structure de type _union find_ appelée _bloc union find_ ou _buf_. Cette structure repose sur trois tableaux :

- _repr_ pour les représentants de blocs (chaque bloc a 1 représentant choisi arbitrairement)
- _parents_ pour les parents de chacun des nœuds : si $s$ est le représentant d'un bloc $b$ alors soit `parents[s] == s` et alors $s$ est racine d'un des arbres de la forêt, soit `parents[s]` indique un somment d'un bloc $b'$ tel qu'il existe un pont entre un sommet de $b$ et un sommet de $b'$ ; sinon en remontant, on arrive au représentant du bloc de $s$
- _rang_ la taille du bloc de chaque représentant.

### Exemple et premières questions

On considère le graphe $G_4$ ci-dessous :

??? example "Graphe de la Figure 4"

    === "$G_4$"

        ![FIG. 4](../../assets/images/agreg_2023/fig4.svg)

    === "Le code"

        ```python
        G4 = pg.Graph(8)
        G4_EDGES = [(0, 1), (1, 2), (2, 3), (2, 4), (3, 4), (4, 6), (5, 6), (5, 7), (6, 7)]
        G4.add_edges_from(G4_EDGES)

        G4_POS = [(0, 0, 0), (2, 1, 0), (4, 2, 0), (6, 3, 0), (1, 0.5, 1), (3, 1.5, 1), 
                  (5, 2.5, 1), (7, 3.5, 1)]
        G4.position(G4_POS, ech=0.6)
        G4.view
        ```

???+ question "Question 2.1"

    Indiquer les blocs et les ponts du graphe $G_4$.

    === "$G_4$ colorié"

        ![question 2.1](../../assets/images/agreg_2023/q2.1.svg)

    === "Le code"

        ```python
        G4bis = G4.copy()
        BLOCS_G4 = [[0], [1], [2, 3, 4], [5, 6, 7]]
        PONTS_G4 = [(0, 1), (1, 2), (4, 6)]
        colorier_blocs(G4bis, BLOCS_G4)
        colorier_ponts(G4bis, PONTS_G4)
        G4bis.view
        ```

???+ question "Question 2.2"

    Représenter graphiquement une structure _buf_ correspondant à $G_4$

    === "$F_4$ une forêt possible pour $G_4$"

        ![question 2.2](../../assets/images/agreg_2023/q2.2.svg)

    === "Le code"

        On utilise les graphes orientés disponibles dans _pygraph_ et la colorisation des sommets, pris dans chacun des blocs :

        ```python
        F4 = pg.DiGraph(8)
        F4_EDGES = [(0, 1), (2, 1), (3, 2), (4, 2), (6, 4), (5, 6), (7, 6)]
        F4.add_edges_from(F4_EDGES)

        F4_POS = [(0, 0, 3), (1, 0.5, 4), (2, 1, 3), (3, 0.5, 2), (4, 1.5, 2), (5, 1, 0), 
                  (6, 1.5, 1), (7, 2, 0)]
        F4.position(F4_POS, ech=0.6)

        F4_REPR = [bloc[0] for bloc in BLOCS_G4]
        for node_id in F4_REPR:
            F4.color_on(node_id, 4)
        F4.view
        ```

### Quelques classes Python

La structure _buf_ peut être vue comme une spécialisation d'une structure _union find_ plus classique. Voici la classe `Partition`, qui modélise cette _union find_ et utilisée dans d'autres sujets de cette agrégation 2023, la classe `VizPartition` pour visualiser ces partitions et la classe `Buf` qui spécialise `Partition` pour en faire une structure _bloc union find_. Dans la suite des questions, nous reprendrons certaines méthodes des classes `Partition` et  `Buf`. Vous pouvez télécharger ces trois classes :


  - la classe `Partition` : [partition.py](../../downloads/agreg_2023/partition.py)
  - la classe `VizPartition` : [viz_partition.py](../../downloads/agreg_2023/viz_partition.py)
  - la classe `Buf` : [buf.py](../../downloads/agreg_2023/buf.py) :warning: _encore en cours de finalisation_

!!! warning "Avertissement"

    La modélisation POO de la structure _buf_ peut servir de support à des exercices guidés ou à un projet en classe de Terminale NSI. Il faudra penser à quelques aménagements par rapport à ce qui est dit ici, comme par exemple, supprimer la notion d'héritage en ne faisant qu'une classe `Buf` reprenant le contenu de `Partition`. 


### Suite de la première section

???+ question "Question 2.3"

    Il s'agit d'écrire une fonction pour initialiser une structure _buf_ vide comportant $n$ nœuds. Compte tenu de notre structuration, cela correspond aux deux méthodes `__init__`  des classes `Partition` et `Buf`.

    Le tableau des parents est la liste de identifiants de nœuds (chaque nœud est son propre parent) ; le tableau des représentants est initialisé à `True` pour tous les nœuds (chaque nœud est le représentant du _groupe_ où il est le seul individu) ; enfin les rangs sont à $1$.

    ```python
    class Partition:

        def __init__(self, n):
            self.parents = list(range(n))
            self.repr = [True] * n
            self.size = n

    class Buf(Partition):

        def __init__(self, n):
            Partition.__init__(self, n)
            self.rang = [1] * n
    ```

Avant de passer aux deux dernières questions de cette première section, voyons l'utilisation de nos classes `Buf` et `VizPartition` pour créer et visualiser les forêts données en exemple dans le sujet.

??? example "Forêt de la Figure 3"

    === "$F_3$"

        ![FIG. 3](../../assets/images/agreg_2023/fig3.svg)

    === "Création du _buf_ "

        ```python
        B3 = Buf(16)
        B3.parents = [3, 3, 3, 3, 3, 4, 4, 6, 4, 8, 11, 11, 11, 11, 13, 13]
        B3.repr = [False, True, False, True, True, False, False, True, False, False, False, True, False, True, False, False]
        B3.rang = [3, 1, 3, 3, 5, 5, 5, 1, 5, 5, 3, 3, 3, 3, 3, 3]
        ```

    === "Code pour créer $F_3$"

        ```python
        F3 = VizPartition(B3)
        F3.position = [(0, 0, 2), (1, 1, 2), (2, 2, 2), (3, 1.5, 3), (4, 3, 2), (5, 2, 1), (6, 3, 1), 
                       (7, 3, 0), (8, 4, 1), (9, 4, 0), (10, 6, 2), (11, 7, 3), (12, 7, 2), (13, 8, 2), 
                       (14, 7.5, 1), (15, 8.5, 1)]
        ```

        Et comme avec ce positionnement, la composante connexe comportant le nœud $11$ se trouve un peu éloignée, on la rapproche en déplaçant de $1$ vers la gauche le nœud $11$ et tous ceux qui lui sont rattachés (la signification du `True` à la fin) ; et on visualise :

        ```python
        F3.move(11, -1, 0, True)
        F3.sagittal
        ```



???+ question "Question 2.4"

    La méthode `find` dans une structure de type _union find_ permet de retrouver le représentant d'un sommet. Lors de la remontée vers ce représentant, une compression à lieu : chaque sommet se voit rattaché directement au représentant. Cette méthode se trouve dans la classe `Partition`. La solution est récursive :

    - si le nœud est lui-même un représentant alors on renvoie ce nœud,
    - sinon, on recherche (récursivement) le représentant du parent et, avant de renvoyer ce représentant, on l'affecte comme parent du nœud (compression)

    ```python title="find"
    class Partition:
        ...

        def find(self, node_id):
            """recherche et renvoie le représentant du nœud node_id, et réalise la compression"""
            if self.repr[node_id]:
                return node_id
            else:
                repr_id = self.find(self.parents[node_id])
                self.parents[node_id] = repr_id
                return repr_id
    ```

???+ question "Question 2.5"

    La méthode `blocs` de la classe `Buf` doit renvoyer les informations sur les blocs de la structure. le sujet (codé en OCaml) préconise des tableaux de tableaux. On perd alors l'information du représentant de chaque bloc, ce qui est dommage. Le fait d'utiliser un tableau est aussi embêtant pour le caractère ordonné, ce que n'est pas l'ensemble des nœuds d'un bloc. Pour ces raisons, nous choisissons de renvoyer un dictionnaire dont les clés sont les représentants et les valeurs associées des ensembles (`set`)  de nœuds. 

    _Adaptation NSI_ : remplacer les `set` par de simples `list`.

    À noter que cette méthode modifie la structure puisqu'on recherche les représentants (phénomène de compression).

    ```python title="blocs"
    class Buf(Partition):
        ...
    
        def blocs(self):
            resultat = {node_id:set() for node_id in range(self.size) if self.repr[node_id]}
            for node_id in range(self.size):
                repr_id = self.find(node_id)
                resultat[repr_id].add(node_id)
            return resultat
    ```

??? example "D'autres forêts possible pour le graphe $G_1$"

    === "F3b"

        ![FIG. 3 bis](../../assets/images/agreg_2023/fig3_bis.svg)

    === "F3c"

        `B3` est le _buf_ à l'origine de `F3` et de `F3b`. On peut demander le représentant de $9$ :

        ```pycon
        >>> B3c = B3.copy()
        >>> B3c.find(9)
        4
        ```

        Mais alors la forêt représentante change :

        ![Fig. 3 ter](../../assets/images/agreg_2023/fig3_canonique.svg)
