### La matrice d'adjacence

À partir du graphe $G_1$, on peut construire sa matrice d'ajacence c'est-à-dire la matrice qui donne pour un couple $(i, j)$ la valeur du poids de l'arête associée ou $\infty$ si l'arête n'existe pas.

Nous allons donner la fonction Python `adjacence` qui prend un graphe au sens de _pygraph_ et qui renvoie cette matrice sous la forme d'un tableau de tableaux. C'est ici l'occasion de voir que _pygraph_ n'est pas que un outil de visualisation, mais permet aussi de manipuler le graphe comme structure.

???+ tip "La constante $\infty$"

    ```python
    INF = float('inf')
    ```

???+ tip "La matrice d'adjacence d'un graphe"

    Dans _pygraph_, le poids d'une arête $(u, v)$ d'un graphe $g$ est donné par `g.edge_view(u, v).weight`. `node_ids()` et `edges()` sont les deux méthodes pour parcourir les sommets et les arêtes.

    ```python
    def adjacence(g):
        """Crée la matrice d'adjacence à partir du graphe g"""
        adj = [[0 if v == s else INF for v in g.node_ids()] for s in g.node_ids()]
        for s, v in g.edges():
            poids = g.edge_view(s, v).weight
            adj[s][v] = poids
            adj[v][s] = poids
        return adj 
    ```

???+ example "Calcul de la matrice d'adjacence de $G_1$"

    ```python
    >>> A1 = adjacence(G1)
    >>> A1
    [[0, 2, 1, 4.5, 5],
    [2, 0, 1, inf, inf],
    [1, 1, 0, inf, inf],
    [4.5, inf, inf, 0, 4],
    [5, inf, inf, 4, 0]]
    ```

### Le tableau des plus courtes distances

???+ question "Question 1"

    Le sujet donne, en pseudo-langage, l'algorithme de _Floyd-Warshall_ pour ce calcul dont l'entrée est la matrice d'adjacence $A$ :

    ```javascript title="Floyd-Warshall"
    dist <- A
    foreach vertex z in V do
        foreach vertex x in V do
            foreach vertex y in V do
                if dist(x, z) ≠ ∞ and dist(z, y) ≠ ∞ and
                dist(x, z) + dist(z, y) < dist(x, y) then
                    dist(x, y) <- dist(x, z) + dist(z, y)
    return dist
    ```

    ???+ warning "Avertissements"

        1. Nul besoin de parler de `np.inf` pour l'infini, `float('inf')` fourni un infini.
        2. L'affectation de tableau de tableaux `dist <- A` est une vraie difficulté si on souhaite étudier ce sujet en Terminale NSI ; en effet il faut veiller à effectuer correctement la copie profonde.
        3. Pas besoin non plus de tester que les distances sont différentes de l'infini.


        Traduire en Python l'algorithme précédent.

??? success "Réponse"

    ```python
    def floyd_warshall(adj):
        dist = [ligne.copy() for ligne in adj]
        noeuds = list(range(len(adj)))
        for z in noeuds:
            for x in noeuds:
                for y in noeuds:
                    d = dist[x][z] + dist[z][y]
                    if d < dist[x][y]:
                        dist[x][y] = d
        return dist
    ```

???+ example "Tableau des distances de l'exemple"

    ```python
    >>> DIST1 = floyd_warshall(A1)
    >>> DIST1
    [[0, 2, 1, 4.5, 5],
    [2, 0, 1, 6.5, 7],
    [1, 1, 0, 5.5, 6],
    [4.5, 6.5, 5.5, 0, 4],
    [5, 7, 6, 4, 0]]
    ```