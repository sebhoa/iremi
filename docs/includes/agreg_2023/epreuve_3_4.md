### Question 5

Il faut exhiber deux configurations qui trompent l'algorithme naïf. Voyons déjà un :

#### Construction d'un exemple 2

???+ example "Un $2^e$ exemple"

    === "$G_2$"

        ![G2](../../assets/images/agreg_2023/exemple2.svg)

    === "Le code"

        ```python
        G2 = pg.Graph(7)
        G2.add_edges_from([(0, 1, 2), (0, 4, 4), (0, 6, 3), (1, 2, 7), (1, 3, 5),
                           (2, 3, 2), (2, 6, 1), (3, 5, 1), (4, 5, 1)])
        G2.position([(0, 3, 2), (1, 2, 1), (2, 3, 0), (3, 1, 0), (4, 1, 2),  (5, 0, 1), (6, 4, 1)], ech=0.6)
        ```

    === "Les autres informations"

        ```python
        >>> A2 = adjacence(G2)
        >>> DIST2 = floyd_warshall(A2)
        >>> D2 = [None, 5, 5, 4, 3, 4, 2] # les demandes
        ```

On peut alors  lancer l'algorithme naïf pour obtenir des tournées. 

???+ example "Solution naïve"

    === "La solution"

        Voici la solution naïve, donnant $3$ tournées pour un cout total de $25$. Grâce à une fonction de colorisation du graphe pour mettre en avant les tournées :

        ![G2 naif](../../assets/images/agreg_2023/G2_sol_naive.svg)

    === "Le code"

        ```python title="la fonction de colorisation"
        def colorise(g, tournees):
            for color_id, tournee in enumerate(tournees):
                for c in tournee:
                    g.color_on(c, color_id)
        
        >>> S2 = algo_naif(DIST2, D2)
        >>> colorise(G2, S2)
        >>> cout_total(DIST2, S2)
        25
        ```

#### Une meilleure solution

???+ example "Une autre solution"

    === "La solution"

        Les deux tournées ci-dessous offrent un meilleur cout :

        ![G2 opti](../../assets/images/agreg_2023/G2_sol_opti.svg)

    === "Le code"

    ```python
    >>> S2b = [[1, 2, 6], [3, 5, 4]]
    >>> cout_total(DIST2, S2b)
    24
    ```

### L'algorithme de Clarke et Wright

???+ note "Description de l'algorithme"

    1. on commence par $n$ tournées : $v_0 \rightarrow v_i \rightarrow v_0$, pour tout $i \lt 0$ (chaque client par 1 véhicule différent)
    2. on calcule les économies $s(i, j)$ pour fusionner deux clients $v_i$ et $v_j$ en une même tournée, pour tout $i, j \lt 0$ et $i \neq j$ : $s(i, j) = d(i, 0) + d(0, j) - d(i, j)$
    3. on trie les économies par ordre décroissant ;
    4. en parcourant la liste des économies, on fusionne les deux tournées associées si :
        1. les deux clients ne sont pas déjà dans la même tournée,
        2. aucun des deux clients n'est à l'intérieur de la tournée (donc soit 1er soit dernier),
        3. la somme des demandes des deux tournées ne dépasse pas la limite de capacité
        
    À noter que les points 4a et 4b peuvent être testés en même temps pour plus d'efficacité : en effet, si on note $i$ et $j$ les deux clients, $i$ est l'une des extrémités d'une des tournées et $j$ de l'autre.


#### La fonction `savings`

Il s'agit d'une des idées à la base de cet algorithme : lorsqu'on considère deux sommets $i$ et $j$ est-ce qu'il est préférable de faire les allers-retour depuis l'entrepôt ou une boucle incluant les deux sommets ? Dit autrement, veut-on remplacer un aller depuis l'entrepôt vers chacun des sommets par le trajet entre les sommets ? Voyons sur un petit exemple :

???+ example "Illustration du concept d'économie"

    Dans le graphe ci-dessous, il est plus rentable de parcourir la boucle $0\rightarrow 1\rightarrow 2\rightarrow 0$ (cout total $11$) que les allers-retour $0\leftrightarrow 1$ et $0\leftrightarrow 2$ (cout total $16$).

    ![ex_eco](../../assets/images/agreg_2023/ex_savings.svg)

???+ tip "La fonction `savings`"

    ```python
    def savings(dist):
    n = len(dist)
    sav = [[0] * n for _ in range(n)]
    for i in range(1, n):
        for j in range(i+1, n):
            sav[i][j] = sav[j][i] = dist[i][0] + dist[0][j] - dist[i][j]
    return sav
    ```

#### La fusion de tournées

La seconde idée de l'algorithme est le concept de fusion de tournées. Voyons voir cela sur des exemples.

???+ example "Fusion de tournées"

    === "Cas n°1"

        Dans le schéma ci-dessous, nous avons deux tournées :

        - la bleue qui **commence** par le client $i$ et en inclus $3$ autres
        - la jaune qui **commence** par le cliant $j$

        ![fusion cas 1 avant](../../assets/images/agreg_2023/fusion_cas_1_0.svg)

        Si la somme des charges ne dépasse pas la capacité maximale, on peut fusionner ces deux tournées par leurs extrémités $i$ et $j$ pour obtenir une seule (à condition que l'économie sur le couple $(i, j)$  soit effective) :

        ![fusion cas 1 après](../../assets/images/agreg_2023/fusion_cas_1_1.svg)

        Nous avons au total $4$ cas suivant que $i$ (resp. $j$) se trouve en tête ou en queue de sa tournée.

    === "Cas n°2"

        ![fusion cas 2 avant](../../assets/images/agreg_2023/fusion_cas_2_0.svg)

        Similaire au cas n°1 : $i$ est en tête de sa tournée mais cette fois $j$ est à la fin de la sienne. On obtient la même fusion que le cas n°1 à condition de renverser la tournée jaune (ou ajouter les sommets jaune en commençant par la fin).

        ![fusion cas 2 après](../../assets/images/agreg_2023/fusion_cas_1_1.svg)

    === "Cas n°3"

        ![fusion cas 3 avant](../../assets/images/agreg_2023/fusion_cas_3_0.svg)

        Similaire au cas n°1 : $j$ est bien en tête de sa tournée mais cette fois $i$ est à la fin ; la fusion se fera alors par la droite de la tournée bleue.

        ![fusion cas 3 après](../../assets/images/agreg_2023/fusion_cas_3_1.svg)

    === "Cas n°4"

        À vous de le dessiner !

        ??? success "Réponse"

            ![fusion cas 4 avant](../../assets/images/agreg_2023/fusion_cas_4_0.svg)

            Similaire au cas n°2 : $j$ est bien en fin de sa tournée mais cette fois $i$ est aussi à la fin ; la fusion se fera alors par la droite de la tournée bleue en renversant le parcours de la tournée jaune.

            ![fusion cas 4 après](../../assets/images/agreg_2023/fusion_cas_3_1.svg)

### Question 6

Cette question aborde la modélisation d'une tournée en POO. Mais bizarrement, l'énoncé parle de la classe `Tournee` et nous donne $4$ de ses méthodes, mais aucune propriété :

- `distance` : cette méthode calcule le cout total de la tournée ; elle a donc besoin du tableau des distances. Pour nous, ce tableau pourrait être l'une des propriétés de la tournée,
- `affiche` : affiche au format texte la tournée ie la liste ordonnée des clients visités, en commençant et en finissant par $0$, le code de l'entrepôt ; la méthode donne aussi le chargement total de la tournée (la somme des demandes des différents clients),
- `fusionnable` : doit permettre de déterminer si la tournée courante (celle qui appelle la méthode) peut fusionner avec une _autre tournée_ via un couple de clients (il s'agit là de vérifier les contraintes a, b et c du point 4 de l'algorithme de Clarke et Wright),
- enfin `fusion` : réalise effectivement la fusion si celle-ci a été déclarée possible.

Nous allons voir que les méthodes `fusionnables` et `fusion` ne sont pas simples, notamment, `fusionnable` ne peut pas se contenter de renvoyer un booléen : `fusion` (qui va appeler `fusionnable`) a besoin de plus d'informations. Comme nous l'avons vu, la fusion se fait selon plusieurs cas de figures et il nous faut savoir lequel est concerné au moment de fusionner. 


???+ tip "La bonne structure pour les clients"

    Les différents cas de fusion nous font choisir une structure de type _file à double entrée_ pour modéliser les clients. En effet, on doit pouvoir ajouter au début et à la fin ; la simple liste Python n'est donc pas efficace.
    
    Nous pouvons utiliser la structure `deque` (_double ended queue_) du module `collections` :

    ```python
    >>> from collections import deque
    >>> test = deque([])  # création d'une file vide
    >>> test.append('quelque chose')  # ajout à droite
    >>> test.appendleft('autre chose')  # ajout à gauche
    >>> test.pop()  # retrait d'un élément à droite
    >>> test.popleft()  # retrait d'un élément à gauche
    ```

    Toutes ces opérations sont en $O(1)$.

    
Commençons par une définition de la classe `Tournee`, son initialiseur qui donne les propriétés :

???+ success "Définition de `Tournee`"

    ```python
    class Tournee:
        
        def __init__(self, client, distances, demandes, capacite_max=12):
            self.id = client  # le client initial sert d'identifiant pour la tournée
            self.dist = distances
            self.demandes = demandes
            self.limite = capacite_max
            self.clients = deque([client])
            self.ptc = demandes[client]  # ptc pour poids total en charge
    ```

???+ success "Réponse à la question 6 : `distance` et `affiche`"

    ces deux petites méthodes ne posent pas de difficulté :

    ```python
    class Tournee:
        ...
        
        def distance(self):
            clients = self.clients
            dist = self.dist
            km = dist[0][clients[0]]
            for i in range(1, len(clients)):
                km += dist[clients[i-1]][clients[i]]
            km += dist[clients[-1]][0]
            return km

        def affiche(self):
            str_tournee = ' '.join(str(c) for c in self.clients)
            print(f'tournée 0 {str_tournee} 0, chargement {self.ptc}')
    ```

#### La méthode `fusionnable`

Voilà ce que notre méthode `fusionnable` doit réaliser :

- vérifier les points 4a et 4b de l'algorithme,
- mémoriser l'information de quel cas de figure de fusion doit être mis en œuvre dans le cas où effectivement la fusion est possible ; appelons $P$ (comme positions) cette information,
- vérifier que la fusion ne provoque pas de surcharge et transmettre dans ce cas l'information $P$

???+ tip "Deux petites méthodes auxiliaires"

    Pour vérifier les points 4a et 4b, nous avons besoin d'une autre tournée et de deux clients (ceux de la fusion potentielle). Il nous faut déterminer si les clients sont bien des extrémités de nos deux tournées mises en œuvre et, si oui, de quelles extrémités (gauche ou droite) :

    ```python
    class Tournee:
        ...

        def extremite(self, client):
            """renvoie 0 si le client est en tête de la tournée -1 s'il est dernier
            et None sinon
            """
            if self.clients[0] == client:
                return 0
            elif self.clients[-1] == client:
                return -1
    
        def points_4a_4b(self, autre_tournee, deux_clients):
            """renvoie None si les contraintes ne sont pas vérifiées
            - 0, 0 si on est dans le cas 1 de fusion
            - 0, -1 s'il s'agit du cas 2
            - -1, 0 pour le cas 3
            - -1, -1 enfin pour le cas 4
            """
            if self.id != autre_tournee.id:
                i, j = deux_clients
                for u, v in ((i, j), (j, i)):
                    position_u, position_v = self.extremite(u), autre_tournee.extremite(v)
                    if position_u is not None and position_v is not None:
                        return position_u, position_v
    ```

Tout est en place pour la méthode `fusionnable` :

???+ success "Réponse à la question 6 : `fusionnable`"

    ```python
    class Tournee:
        ...

        def fusionnable(self, autre_tournee, deux_clients):
            positions = self.points_4a_4b(autre_tournee, deux_clients)
            if positions is not None and self.ptc + autre_tournee.ptc <= self.limite:
                return positions
    ```

Si la fusion est possible, on récupère de `fusionnable` un couple $(p_1, p_2)$  parmi les quatre possibilités : $(0, 0)$, $(0, -1)$, $(-1, 0)$ et $(-1, -1)$. Dans les cas $2$ et $4$, lorsque $p_2$ vaut $-1$, nous devons parcourir à rebours les clients de la tournée correspondante ; nous utiliserons simplement la méthode `reverse` disponible sur la `deque`. Dans les cas $1$ et $2$, lorsque $p_1$ vaut $0$, nous devons insérer par la gauche. Et au final, cette méthode renvoie `True` si la fusion a eu lieu, `False` sinon. Ces considérations nous amènent à cette solution pour la méthode `fusion` :

???+ success "Réponse à la question 6 : `fusion`"

    ```python
    class Tournee:
        ...

        def fusion(self, autre_tournee, deux_clients):
            positions = self.fusionnable(autre_tournee, deux_clients)
            if positions is not None:
                p1, p2 = positions
                if p2 == -1:
                    autre_tournee.clients.reverse()
                for c in autre_tournee.clients:
                    self.ajout(c, gauche=p1 == 0)
                return True
            return False
    ```


La petite méthode `ajout` qui permet d'ajouter un client à une tournée :

- ajouter **du bon côté** le client à la file des clients,
- augmenter le poids total en charge de la valeur de la demande du client.

???+ tip "Ajouter un client"

    ```python
    class Tournee:
        ...

        def ajout(self, client, gauche=False):
            if gauche:
                self.clients.appendleft(client)
            else:
                self.clients.append(client)
            self.ptc += self.demandes[client]
    ```

L'ensemble de la définition de la classe `Tournee` est disponible ici : [tournee.py](../../downloads/agreg_2023/tournee.py). Le script intègre aussi la mise en œuvre de l'algorithme de Clarke et Wright.

