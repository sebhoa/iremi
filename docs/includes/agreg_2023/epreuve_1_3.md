Cette section s'intéresse à l'ajout d'arêtes dans la structure _buf_. On commence par calculer une information : la chaine des représentants d'un nœud, que l'on utilisera par la suite.

???+ question "Question 2.6"

    Il s'agit  d'écrire une méthode `chaine_racine` qui, pour un nœud $s$ donné, renvoie la liste des représentants depuis la racine de l'arbre du nœud $s$ jusqu'au représentant de $s$.

    Là encore, sans surprise, on adopte une solution récursive adaptée à notre structure :

    - si $s$ est son propre parent alors il est représentant et on renvoie la liste constituée uniquement de $s$
    - sinon, on calcule la chaine à partir du parent de $s$ puis, on y ajoute $s$ si ce dernier est un représentant ; on renvoie la chaine

    ```python title="chaine_racine"
    class Buf:
        ...

        def chaine_racine(self, node_id):
            parent_id = self.parents[node_id] 
            if parent_id == node_id:
                return [node_id]
            else:
                chaine = self.chaine_racine(parent_id)
                if self.repr[node_id]:
                    chaine.append(node_id)
                return chaine
    ```

L'ajout est une opération délicate et la fin du sujet consiste à nous guider vers la modélisation de celle-ci. On distingue plusieurs cas suivant que les nœuds de l'arête appartiennent ou pas à la même composante connexe.

### Arêtes entre sommets de composantes connexes distinctes

Lorsqu'on ajoute une arête entre deux sommets de deux composantes connexes distinctes, cette arête est un pont. Voici comment il faut procéder pour ajouter par exemple une arête entre les sommets $u$ et $v$ :

1. on calcule la chaine des représentants d'un des sommets (disons $u$),
2. on _retourne_ le sens des arcs de la chaine, faisant de $u$ la nouvelle racine de son arbre,
3. on change le parent du représentant de $u$ pour le faire pointer vers le représentant de $v$.

??? example "La forêt $F_5$ un exemple d'ajout d'arête"

    La Figure 5 du sujet montre le résultat de la connexion des sommets $5$ et $14$ depuis le _buf_ $B_3$.

    ![FIG. 5](../../assets/images/agreg_2023/fig5.svg)

    À votre avis, quelle chaine a été retournée ? 


???+ question "Question 2.7"

    Dessiner la forêt obtenue en _connectant_ les sommets $7$ et $12$ et en supposant que c'est la chaine issue de $7$ qui est retournée :

    ![q2.7](../../assets/images/agreg_2023/q2.7.svg)

???+ question "Question 2.8"

    On nous demande de coder les points 1 et 2 de la méthode pour connecter deux sommets de deux composantes connexes distinctes :

    ```python
    class Buf:
        ...

        def retourner_chaine(self, u, v):
            chaine_u = self.chaine_racine(u)
            dernier = chaine_u[-1]
            for i in range(len(chaine_u) - 1):
                node_id, suivant_id = chaine_u[i], chaine_u[i+1]
                self.parents[node_id] = suivant_id
            return dernier
    ```

    Avec une ligne de plus on obtient la méthode complète :

    ```python
        def ajout_non_connexe(self, u, v):
            dernier = self.retourner_chaine(u, v)
            self.parents[dernier] = self.find(v)
    ```

On peut alors donner le code utilisé pour la création de la forêt de la question 2.7 :

??? tip "Code de la forêt 2.7"

    Le _buf_ de départ est $B_3$.

    ```python
    B3d = B3.copy()
    B3d.ajout_non_connexe(7, 12)
    F3d = VizPartition(B3d)
    F3d.position = [(0, 0, 0), (1, 1, 0), (2, 2, 0), (3, 1, 1), (4, 2.5, 2), (5, 2, 1), (6, 3, 1), 
                    (7, 2.5, 3), (8, 4, 1), (9, 4, 0), (10, 3.5, 3), (11, 4, 4), (12, 4.5, 3), 
                    (13, 5.5, 3), (14, 5, 2), (15, 6, 2)]
    F3d.sagittal
    ```

### Arêtes entre blocs distincts d'une même composante connexe

Lorsqu'on connecte deux sommets de deux blocs de la même composante connexe, il va falloir fusionner les deux blocs et **tous les blocs compris entre les deux**.

??? example "La forêt $F_6$, issue de $F_5$"

    À partir de $F_5$, on connecte les sommets $1$ et $15$. Les blocs $1$, $3$, $4$ et $13$ sont donc fusionnés. Voici le résultat si on garde $4$ comme représentant :

    ![Fig. 6](../../assets/images/agreg_2023/fig6.svg)

???+ question "Question 2.9"

    À l'instar de l'exemple précédent, on demande de représenter la forêt issue de $F_5$, après connexion des sommets $7$ et $12$. On fusionne les blocs $7$, $4$, $13$ et $11$. Là encore on a gardé $4$ comme représentant :

    ![Q2.9](../../assets/images/agreg_2023/q2.9.svg)

???+ question "Question 2.10"

    Il s'agit d'écrire l'opération élémentaire de la connexion : l'union de deux blocs dont on passe les représentants en paramètres. La question précise qu'on ne se souci pas de la valeur du parent du bloc obtenu. On garde comme représentant, le représentant ayant le rang le plus petit. Le rang du bloc augmente de $1$.

    Notons que la méthode doit renvoyer le sommet qui a été retenu comme représentant.

    ```python title="union"
    class Buf:
        ...

        def union(self, repr_i, repr_j):
            """réalise l'union de deux blocs"""
            if repr_i != repr_j:
                # repr_j est censé être le bloc le plus petit, 
                # on switche si ce n'est pas le cas
                if self.rang[repr_j] > self.rang[repr_i]:
                    repr_i, repr_j = repr_j, repr_i
                self.parents[repr_i] = repr_j
                self.repr[repr_i] = False
                self.rang[repr_j] += 1
            return repr_j
    ```

??? example "Visualisons l'union"

    Pour l'instant, l'opération d'union est incomplète. En effet, souvenez-vous de la Figure 5 :

    ![FIG. 5](../../assets/images/agreg_2023/fig5.svg)

    Si on réalise l'union telle que décrite entre les sommets $4$ et $7$, alors $7$ devient le représentant du bloc. $4$ pointera donc vers $7$ qui lui continuera de pointer sur $6$ et on a perdu le lien avec le bloc $3$ :

    ![Illus. union](../../assets/images/agreg_2023/union_incomp.svg)

    Les dernières questions sont là pour terminer cette opération de fusion incomplète.


???+ question "Question 2.11"

    Maintenant qu'on sait faire l'union de deux représentants, il s'agit d'étendre à une chaine, en faisant attention au parent du représentant du bloc final. 

    On suppose que la chaine passée est ordonnée, un peu au sens de la chaine de représentants produite par `chaine_racine`. Ainsi le parent du premier représentant (qui peut être le représentant lui-même si on a affaire à une racine d'arbre) deviendra le parent du représentant du bloc issu de la fusion. Pour la fusion elle-même, voici la procédure :
    
    1. extraire les deux derniers sommets de la chaine,
    2. faire l'union des sommets,
    3. replacer le sommet issu de l'union à la fin de la chaine,
    4. recommencer jusqu'à n'avoir plus qu'un sommet.

    Cela donne le code suivant :

    ```python
    class Buf:
        ...

        def fusion_chaine(self, chaine):
            futur_parent = self.find(self.parents[chaine[0]])
            while len(chaine) > 1:
                v = chaine.pop()
                u = chaine.pop()
                if self.union(u, v) == u:
                    chaine.append(u)
                else:
                    chaine.append(v)
            repr_final = chaine[0]
            if self.repr[futur_parent]:
                self.parents[repr_final] = futur_parent
            else:
                self.parents[repr_final] = repr_final
    ```


???+ question "Question 2.12"

    Nous arrivons au terme de ce sujet avec l'opération _ultime_ : l'ajout d'une arête dans la structure _buf_. La méthode `ajout` résume les cas traités jusqu'ici. Dans le cas d'une liaison intra composante connexe, il nous faut construire la chaine de représentants à fusionner. Cela se fait de la façon suivante :

    - on calcule les chaines racines de chacun des deux sommets,
    - on calcule ensuite la chaine constituée des parties distinctes, en prenant comme départ l'ancêtre commun le plus profond

    Ce qui donne la méthode suivante :

    ```python
    class Buf:
        ...

        def ajout(self, u, v):
            r_u = self.find(u)
            r_v = self.find(v)
            if r_u != r_v:
                chaine_u = self.chaine_racine(u)
                chaine_v = self.chaine_racine(v)
                if chaine_u[0] != chaine_v[0]:
                # cas 1 : 2 composantes connexes distinctes (voir 2.8)
                    dernier = chaine_u[-1]
                    for i in range(len(chaine_u) - 1):
                        node_id, suivant_id = chaine_u[i], chaine_u[i+1]
                        self.parents[node_id] = suivant_id
                    self.parents[dernier] = r_v
                else:
                # cas 2 : même composante
                    k, n = 0, min(len(chaine_u), len(chaine_v))
                    while k < n and chaine_u[k] == chaine_v[k]:
                        k += 1
                    chaine = chaine_u[k-1:] + chaine_v[k:]
                    self.fusion_chaine(chaine)
    ```

## Conclusion

Ce sujet manipulant des arbres et des graphes est très visuel, et se prête à une adaptation pour un beau sujet de projet en Terminale NSI. En guise de conclusion, voici la reconstruction d'une forêt représentants les blocs et les ponts de $G_1$, en partant d'un _buf_ vide et en y ajoutant (au sens de la question 2.12) les arêtes, une par une.

!!! example "Reconstruction d'une forêt possible pour $G_1$"

    === "0. le BUF vide"

        ```pycon
        >>> B3 = Buf(16)
        ```

        ![Construction 0](../../assets/images/agreg_2023/F3_etape0.svg)

    === "1. bloc 0-2-3"

        ```pycon
        >>> B3.ajout(0, 2)
        >>> B3.ajout(0, 3)
        >>> B3.ajout(2, 3)
        ```

        ![Construction 1](../../assets/images/agreg_2023/F3_etape1.svg)

    === "2. bloc 4-5-6-8-9"

        ```pycon
        >>> B3.ajout(4, 5)
        >>> B3.ajout(4, 6)
        >>> B3.ajout(5, 6)
        >>> B3.ajout(6, 8)
        >>> B3.ajout(8, 9)
        >>> B3.ajout(6, 9)
        ```

        ![Construction 0](../../assets/images/agreg_2023/F3_etape2.svg)

    === "3. bloc 10-11-12"

        ```pycon
        >>> B3.ajout(10, 11)
        >>> B3.ajout(10, 12)
        >>> B3.ajout(11, 12)
        ```

        ![Construction 0](../../assets/images/agreg_2023/F3_etape3.svg)

    === "4. bloc 13-14-15"

        ```pycon
        >>> B3.ajout(13, 14)
        >>> B3.ajout(13, 15)
        >>> B3.ajout(14, 15)
        ```

        ![Construction 0](../../assets/images/agreg_2023/F3_etape4.svg)

    === "5. pont 3-4"

        ```pycon
        >>> B3.ajout(3, 4)
        ```

        ![Construction 0](../../assets/images/agreg_2023/F3_etape5.svg)

    === "6. pont 11-15"

        ```pycon
        >>> B3.ajout(11, 15)
        ```

        ![Construction 0](../../assets/images/agreg_2023/F3_etape6.svg)

    === "7. pont 7-6"

        ```pycon
        >>> B3.ajout(7, 6)
        ```

        ![Construction 0](../../assets/images/agreg_2023/F3_etape7.svg)

    === "8. pont 1-3"

        ![Construction 8](../../assets/images/agreg_2023/F3_etape8.svg)

