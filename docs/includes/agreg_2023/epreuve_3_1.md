Le sujet travaille avec des graphes pondérés que l'on peut construire avec _pygraph_. Un nœud est identifié par une variable $v_i$ dans le sujet (avec comme convention que $v_0$  représente l'entrepôt), dans notre modélisation, nous n'utiliserons que les numéros $0, 1...$

### Le graphe des distances

???+ example "Le graphe $G_1$ du sujet"

    === "$G_1$"

        ![G1](../../assets/images/agreg_2023/ep3_fig1a.svg)

    === "Le code"

        ```python
        G1 = pg.Graph(5)
        G1.add_edges_from([(0, 1, 2), (0, 2, 1), (1, 2, 1), (0, 3, 4.5), (0, 4, 5), (3, 4, 4)])
        G1.position([(4, 1, 0), (3, 0, 1), (0, 1.5, 1.5), (2, 2.5, 2.5), (1, 3.5, 1.5)], ech=0.8)
        ```

### Les autres informations

=== "Les demandes des clients"

    Donné en Figure 1b du sujet.

    | clients  | $1$ | $2$ | $3$ | $4$ |
    | -------- | --- | --- | --- | --- |
    | demandes | $5$ | $6$ | $4$ | $3$ |

=== "La matrice d'_adjacence_ du graphe"

    Donné en Figure 1c du sujet.

    $$\mathnormal{adjacence} = \left( \begin{array}{lllll}
                0 & 2 & 1 & 4,5 & 5\\
                2 & 0 & 1 & \infty & \infty\\
                1 & 1 & 0 & \infty & \infty\\
                4,5 & \infty & \infty & 0 & 4\\
                5 & \infty & \infty & 4 & 0\\
                \end{array} \right)$$


=== "Le tableaux _dist_ des plus courtes distances"

    Donné en Figure 1d du sujet.

    $$\mathnormal{dist} = \left( \begin{array}{lllll}
                0 & 2 & 1 & 4,5 & 5\\
                2 & 0 & 1 & 6,5 & 7\\
                1 & 1 & 0 & 5,5 & 6\\
                4,5 & 6,5 & 5,5 & 0 & 4\\
                5 & 7 & 6 & 4 & 0\\
                \end{array} \right)$$
