### Question 7

Enfin, nous sommes en mesure de coder l'algorithme de Clarke et Wright.

Avant de voir le code Python, voici un autre énoncé de la méthode :

???+ note "Reformulation de l'algorithme"

    1. Initialiser ce qui va être le résultat de notre fonction _une collection_ de tournées ; dans l'implémentation Python, nous choisissons un dictionnaire car lors des opérations de fusion, nous supprimerons des tournées
    2. Calculer les économies et les trier par ordre décroissant
    3. Tant qu'il y des fusions possibles :
          1. on récupère le couple de clients de la première économie
          2. on trouve les tournées impliquées
          3. on essaie de fusionner ; si la fusion est effective on retire la tournée qui a été absorbée

Voici la traduction en Python :

???+ success "Réponse"

    ```python
    def clarke_wright(dist, demandes, capacite=12):
        nb_clients = len(demandes)
        tournees = {c: Tournee(c, dist, demandes, capacite) for c in range(1, nb_clients)}
        sav = savings(dist)
        sorted_savings = sorted(((sav[i][j], i, j) for i in range(len(sav)) for j in range(i+1, len(sav))), reverse=True)
        fini = False
        while not fini:
            fini = True
            for _, i, j in sorted_savings:
                id_de_i, tournee_de_i = quelle_tournee(tournees, i)
                id_de_j, tournee_de_j = quelle_tournee(tournees, j)
                if tournee_de_i is not None and tournee_de_j is not None\
                   and tournee_de_i.fusion(tournee_de_j, (i, j)):
                    del tournees[id_de_j]
                    fini = False
        return tournees
    ```

Nous pouvons essayer notre algorithme :

???+ example "Tournées de l'exemple 2"

    On retrouve les tournées `S2b` déjà illustrée :

    ![S2b](../../assets/images/agreg_2023/G2_sol_opti.svg)

    ```python
    >>> S3 = clarke_wright(DIST2, D2)
    >>> S3[1].affiche()
    tournée 0 2 6 1 0, chargement 12
    >>> S3[4].affiche()
    tournée 0 3 5 4 0, chargement 11
    ```