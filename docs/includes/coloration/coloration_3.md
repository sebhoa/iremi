### 3.1 Préliminaires

!!! definition "$k$-coloration"

    Une $k$-_coloration_ d'un graphe $G = (S, A)$ est une fonction $c$ de $S \rightarrow \{0,\ldots k-1\}$ telle que pour toute arète $(s, t)\in A$, $c(s)\neq c(t)$.

??? example "Exemples"

    === "Une _fausse_ coloration"

        Créons le graphe $G_0$ de l'exemple permettant de montrer un exemple de non coloration et un exemple de coloration.

        ```python
        >>> G0 = pygraph.Graph(6)
        >>> G0.add_edges_from([(0, 1), (1, 2), (1, 4), (1, 3), (2, 3), (3, 5), (4, 5)])
        >>> G0.position([(1, 0, 0), (0, 0, 1), (2, -1, 0), (4, 1, 0), (3, 0, -1), (5, 1, -1)], 0.6)
        >>> G0.view
        ```

        ![G0](../../assets/images/agreg2022_S0E2/G0_brut.svg){ .centrer }

        On peut remplacer les étiquettes des sommets par un numéro de couleur et mettre en avant les deux sommets qui font que ce choix n'est pas une coloration :

        ```python
        >>> G0.set_labels('021113')
        >>> G0.label_on()
        >>> G0.color_on(2, 'lightblue')
        >>> G0.color_on(3, 'lightblue')
        >>> G0.view
        ```

        ![pas une coloration](../../assets/images/agreg2022_S0E2/G0_bad_coloration.svg){ .centrer }

    === "Une 4-coloration"

        On reprend $G_0$  pour montrer une 4-coloration :

        ```python
        >>> G0b = G0.copy()
        >>> G0b.same_position_as(G0)
        >>> G0b.scale(0.6)
        >>> G0b.set_labels('021013')
        >>> G0b.label_on()
        >>> G0b.view
        ```
         ![4-coloration](../../assets/images/agreg2022_S0E2/G0b_coloration.svg){ .centrer }       

!!! definition "$k$-colorabilité"

    Un graphe $G$ est $k$-_colorable_ s'il possède une $k$-coloration. $\chi(G)$ est le **nombre chromatique** de $G$ ie le plus petit entier $k$ tel que $G$ est $k$-colorable.

??? question "Q21"

    $\chi(G_0) = 3$. Et voici une 3-coloration possible (avec de vraies couleurs) :

    ```python
    >>> G0c = G0.copy()
    >>> G0c.same_position_as(G0)
    >>> G0c.scale(0.6)
    >>> G0c.colorise()
    3
    ```

    Et voici le graphe coloré, avec le **numéro des sommets en étiquettes** pour pouvoir les nommer :

    ![true-coloration](../../assets/images/agreg2022_S0E2/G0c_true_coloration.svg){ .centrer }       

    Dans cette 3-coloration, le sous graphe constitué des sommets 1, 2 et 3 forment un graphe complet. Utiliser 2 couleurs pour ces sommets est impossible. 3 est donc bien le plus petit entier possible pour une coloration. $\square$

??? question "Q22"

    On considère la graphe $G_1$ suivant :

    ```python
    >>> G1 = pygraph.Graph(7)
    >>> G1.add_edges_from([(0, 1), (0, 2), (0, 3), (1, 2), (2, 3), (3, 4), (4, 5), (4, 6), (3, 5), (5, 6), (1, 6)])
    >>> G1.position([(0, -1, 0.5), (4, 1, 0.5), (3, 0, 0), (2, -2, 0), (5, 2, 0), (1, -1, 1.5), (6, 1, 1.5)], 0.6)
    >>> G1.view
    ```
    ![G1](../../assets/images/agreg2022_S0E2/G1.svg){ .centrer }       

    Ci-dessous une 4-coloration obtenue via l'algorithme D-Satur. On ne peut pas faire mieux : 2 doit être de couleur différente de 0, 1 et 3... seuls 1 et 3 peuvent être de la même couleur. Cela nous donne 3 couleurs. 4 et 5 ne peuvent avoir la même couleur que 3. Ainsi 1, 4, et 5 couvrent les 3 couleurs, obligeant 6 à une 4e. $\square$

    ![G1 coloré](../../assets/images/agreg2022_S0E2/G1_coloration.svg){ .centrer }       

??? question "Q23"

    Déterminer les nombres chromatiques de $C_n$ et d'un graphe complet.

    (a) Soit $s_1 \rightarrow s_2\rightarrow\ldots \rightarrow s_n\rightarrow s_1$ le graphe $\cal{C}_n$.  Si $n$ est pair, le graphe est bi-parti : chaque sommet pair est relié à 2 sommets impairs et vice-versa ; 2 couleurs sont alors suffisante. Sinon, le dernier sommet $n$ impair est lié à un sommet pair ($n-1$) et un sommet impair (1). Il faudra donc une 3e couleur. Conclusion $\chi(\cal{C}_n) = 2 + n\,\mathtt{mod}\,2$. $\square$

    (b) On appelle $P(n)$ la propriété suivante : si $G$ est un graphe complet à $n$ sommets alors $\chi(G) = n$. $P(1)$ est vraie. Montrons que pour tout $n$, $P(n) \Rightarrow P(n+1)$. Soit $G$ le graphe complet à $n$ sommets, $n$-coloré. Ajoutons un $n+1$-ième sommet et relions le aux autres pour obtenir un graphe $G'$ complet à $n+1$ sommets. Pour colorer ce nouveau sommet je ne peux utiliser une des $n$ couleurs déjà utilisées par les voisins du noeud, il me faut donc une $n+1$-ième couleur. Ainsi $\chi(G') = n+1$. $\square$

!!! definition "Définitions"

    - Le **degré** d'un sommet est le nombre de sommet qui lui sont adjacents. 
    - Une **clique** est un ensemble de sommets 2 à 2 adjacents. 
    - Le **nombre clique** est la taille d'une clique maximale et est notée $\omega(G)$.

??? question "Q24"

    ${\cal K(T)} = \omega(G_{\cal T})$ (puisque le nombre maximum de machines en marchent simultanément est égal à la taille du plus grand sous graphe complet induit du graphe d'intervalles $G_{\cal T}$ qui est par définition le nombre clique de $G_{\cal T}$). $\square$

??? question "Q25"

    Soit $G$ un graphe connexe. Si on extrait une clique maximale $G'$ alors $G'$ est un graphe complet à $\omega(G)$ sommets et son nombre chromatique $\chi(G')$ vaut $\omega(G)$ (Q23b). Puisque $G'$ est un sous graphe de $G$, $\chi(G) \geq \chi(G')$. On a donc :

    $$\chi(G) \geq \omega(G)$$

    $\square$

    $G_1$ est un exemple de graphe pour lequel l'inégalité est stricte : $\omega(G_1) = 3$ et $\chi(G_1) = 4$. Si on retire l'arête entre les 2 sommets du haut (1 et 6), on obtient une égalité :

    ```python
    >>> G1b = G1.copy()
    >>> G1b.remove_edge(1, 6)
    >>> G1b.same_position_as(G1)
    >>> G1b.scale(0.6)
    >>> G1b.label_off()
    >>> G1b.resize(0.12)
    >>> G1b.colorise()
    >>> G1b.color_on()
    >>> G1b.view
    ```

    ![G1b coloré](../../assets/images/agreg2022_S0E2/G1b_coloration.svg){ .centrer }       

On note $\Delta(G)$ le degré maximal d'un sommet de $G$.

??? question "Q26"

    On a $\chi(G) \leq \Delta(G) + 1$. 

    Pour $G_1$ il y a égalité (le degré max est 3 et on a bien 4 couleurs) et pour le graphe $G_{1b}$ l'inégalité est stricte.

### 3.2 Calcul du nombre chromatique

??? question "Q27"

    Pour un graphe $G$ à $n$ sommets, la valeur maximale possible pour $\chi(G)$ est $n$. Parce qu'au pire $G$ est le graphe complet et on a montré qu'alors $\chi(G) = n$. $\square$

??? question "Q28"

    Pour vérifier qu'une coloration en est effectivement une, on parcourt chaque sommet $s$ de $G$, puis on vérifie qu'un voisin $v$ de $s$ n'a pas la même couleur. Si on tombe sur deux voisins de même couleur on peut arrêter le parcours et renvoyer `False`. Si on va au bout du parcours on renvoie `True`.

    === "Version _basique_"

        ```python
        def est_coloration(G, c):
            n = len(G)
            for s in range(n):
                for v in G[s]:
                    if c[s] == c[v]:
                        return False
            return True
        ```

    === "Version pythonique"

        ```python
        def est_coloration(G, c):
            n = len(G)
            return all(c[s] != c[v] for s in range(n) for v in G[s])
        ```
    
    !!! example "Exemples"

        On se donne la liste d'adjacences de $G_0$ (définit au début des préliminaires) :

        ```python
        >>> AG0 = [[1], [0, 2, 3, 4], [1, 3], [1, 2, 5], [1, 5], [3, 4]]
        ```

        On peut alors vérifier que la _coloration_ 0, 2, 1, 1, 1, 3 donnée dans les préliminaires n'en est effectivement pas une :

        ```python
        >>> est_coloration(AG0, [0, 2, 1, 1, 1, 3])
        False
        ```

        Mais la 2e si :

        ```python
        >>> est_coloration(AG0, [0, 2, 1, 0, 1, 3])
        True
        ```

??? question "Q29"

    On incrémente de 1 les valeurs de la liste, en commençant par la dernière et en passant à la précédente si l'incrémentation à fait atteindre la valeur de $k$, auquel cas on remet cette valeur à 0. On stoppe si la valeur incrémentée est inféieure à $k$ où s'il s'agit de la première. En renvoie `True` si on est sur la première valeur et qu'elle a été incrémentée à $k$ (et donc 0).

    ```python
    def incrementer(c, k):
        for i in range(len(c)-1, -1, -1):
            c[i] += 1
            if c[i] == k:
                c[i] = 0
                if i == 0:
                    return True
            else:
                return False
    ```


    === "Exemple 1"

        ```python
        >>> l1 = [2, 1, 0, 2, 2]
        >>> incrementer(l1, 3)
        False
        >>> l1
        [2, 1, 1, 0, 0]
        ```

    === "Exemple 2"

        ```python
        >>> l2 = [2, 2, 2, 2, 2]
        >>> incrementer(l2, 3)
        True
        >>> l2
        [0, 0, 0, 0, 0]
        ```

On peut alors écrire une fonction pour tester de la $k$-colorabilité :

??? question "Q30"

    ```python
    def k_colorable(G, k):
        coloration = [0] * len(G)
        est_k_colorable = est_coloration(G, coloration)
        while not est_k_colorable and not incrementer(coloration, k):
            est_k_colorable = est_coloration(G, coloration)
        return est_k_colorable
    ```

Et calculer la nombre chromatique d'un graphe (version naïve) :

??? question "Q31"

    ```python
    def chi(G):
        for k in range(2, len(G)):
            if k_colorable(G, k):
                return k
    ```

    ```python
    >>> chi(AG0)
    3
    ```

??? question "Q32"

    Comme il a été dit dans l'énoncé, pour un graphe à $n$ sommets, il y a $k^n$ colorations possibles des sommets par $k$ couleurs. Ainsi la fonction `k_colorable` à une complexité temporelle dans le pire des cas en ${\cal O}(k^n)$. Cette fonction est appelée avec $k = 2$, puis 3, ... jusqu'à $n$. Soit une complexité finale en ${\cal O}(n^n)$. $\square$
    
    Donc non, en pratique cette fonction n'est pas utilisable : 

    ```python
    >>> 20**20
    104857600000000000000000000
    >>> 50**50
    8881784197001252323389053344726562500000000000000000000000000000000000000000000000000
    ```

### 3.3 Problème de décision et NP-complétude

!!! definition "Définitions de problèmes"

    === "Problème `k-coloration`(k fixé)"

        - Entrée : un graphe $G$
        - Question : le graphe $G$ est-il $k$-colorable ?

    === "Problème "Coloration"

        - Entrée : un graphe $G$ et un entier $k$
        - Question : le graphe $G$ est-il $k$-colorable ?

??? question "Q33"

??? question "Q34"

    **Réduction de $h$-coloration en $k$-coloration**

    Soit deux entiers $h\leq k$. Soit $G = (S, A)$ un graphe. On va construire un graphe  $G'$ copie de $G$ à laquelle au rajoute un graphe complet à $k - h$ sommets et chacun de ces nouveaux sommets est relié à chacun des sommets de G. Cette construction se fait en temps polynomiale.

    - Si $G$ est $h$-colorable, soit $C$ une telle coloration. On construit alors la coloration $C'$ de $G'$ suivante : pour les sommets de $G'$ qui sont aussi des sommets de $G$ on utilise $C$ ($h$ couleurs), pour l'extension clique de $k - h$ sommets on utilise les couleurs $h + 1, \ldots k$. $G'$ est donc $k$-colorable.

    - Si $G'$ est $k$-colorable. Soit $C$ une telle coloration. Pour la clique, $C$ utilise $k - h$ couleurs et aucune n'est utilisée sur les sommets de $G$ (puisque par construction ils sont reliés à chacun des sommets utilisant ces $k - h$ couleurs). Il reste donc $h$ couleurs pour les sommets de $G$ qui est donc $h$-colorable. $\square$

??? question "Q35"

    Un parcours en profondeur, en ${\cal O}(n + m)$  où $n$ est le nombre de sommets et $m$ le nombre d'arêtes permet de construire une 2-coloration si possible :

    ```python
    def deux_colorable(G):
        a_traiter = [0]
        coloration = [-1] * len(G)
        coloration[0] = 0
        while a_traiter:
            s = a_traiter.pop()
            for v in G[s]:
                if coloration[v] == coloration[s]:
                    return False
                elif coloration[v] == -1:
                    coloration[v] = 1 - coloration[s]
                    a_traiter.append(v)
        return True
    ```

    **Exemple**

    Le graphe $C_6$ est 2-colorable :

    ```python
    >>> AC6 = [[4, 5], [3, 5], [3, 4], [1, 2], [0, 2], [0, 1]]
    >>> deux_colorable(AC6)
    True
    ```

    ```python
    >>> C6 = liste_adjacence_pygraph(AC6)
    >>> C6.colorise()
    >>> C6.view
    ```

    ![C6 coloré](../../assets/images/agreg2022_S0E2/C6_coloration.svg){ .centrer }       


??? question "Q36"

    Soit $G$ un graphe et $C$ une coloration de $k$ couleurs, un parcours de $G$ similaire à celui de la fonction `deux_colorable` permet de dire si oui ou non $C$ est une $k$-coloration de $G$. La vérification est polynomiale en ${\cal O}(n + m)$ prouvant que Coloration est bien dans NP. $\square$


!!! definition "Définitions"

    - un **littéral** est une variable ou sa négation
    - une **clause** est une disjonction de littéraux
    - une **forme normale conjonctive (FNC)** est une conjonction de clauses ; et dans une 3-FNC chaque clause contient exactement 3 littéraux.

**Exemple de 3-FNC** 

$$\varphi_0 = (a \vee b \vee c) \wedge (\bar{a} \vee c \vee \bar{d}) \wedge (b \vee \bar{c} \vee d)$$ 

!!! definition "Problème `3-SAT`"

    - Entrée : une formule booléenne $\varphi$ en 3-FNC
    - Sortie : la formule $\varphi$ est-elle satisfiable ?    

Voici le graphe d'une clause :

```python
>>> GC = pygraph.Graph(9)
>>> GC.add_edges_from([(0,3), (1,4), (2,7), (3,4), (3,5), (4,5), (5,6), (7,6), (6,8), (7,8)])
>>> GC.position([(2, 0, 0), (1, 0, 1), (0, 0, 2), (3, 1.5, 2), (4, 1.5, 1), (5, 2.5, 1.5), (6, 3.5, 1.5), (7, 3.5, 0), (8, 5, 0.75)], 0.6)
>>> GC.view
```

![GC](../../assets/images/agreg2022_S0E2/GC.svg){ .centrer }       

Le graphe d'une formule $\varphi$ est le graphe $G_\varphi$ construit sur le principe ci-dessus, pour chaque clause. 

??? question "Q37"

    On considère $\varphi_1 = (\bar{a} \vee b \vee \bar{c}) \wedge (a \vee b \vee \bar{c}) \wedge (a \vee \bar{b} \vee c)$. Voici $G_{\varphi_1}$.

    On peut construire le graphe avec pygraph... le placement devient un peu délicat :

    ??? example "Graphe de $\varphi_1$"

        ```python
        >>> PHI1 = pygraph.Graph(24)
        >>> PHI1.add_edges_from([(0,1), (0,2), (0,3), (0,4), (0,5), (0,6), (0,7), (0,23),
                    (2,3), (4,5), (6,7),
                    (2,8), (2,22), (3,10), (4,9), (4,11), (5,12), (6,13), (7,18), (7,20),
                    (8,9), (8,14), (9,14), (10,11), (10,15), (11,15), (12,13), (12,16), (13,16),
                    (14,17), (15,19), (16,21),
                    (17,18), (19,20), (21,22),
                    (17,23), (18,23), (19,23), (20,23), (21,23), (22,23),
                    (1,23)])
        >>> PHI1.position([(23, 0, 0),
              (17,-3,1), (18,-2.5,1), (19,-0.5,1), (20,0.5,1), (21,2,1), (22,3,1),
              (14,-3,1.5), (15,-0.5,1.5), (16,2,1.5),
              (8,-3.5,2), (9,-2.5,2), (10,-1,2), (11,0,2), (12,1.5,2), (13,2.5,2),
              (2,-4,3), (3,-3.5,3), (4,-1.5,3), (5,-1,3), (6,1,3), (7,1.5,3),
              (0,-3,4.5), (1,0.5,4.5)])
        >>> PHI1.set_labels('KFaȧbḃcċ               T')
        >>> PHI1.label_on()
        >>> for s in range(8, 23):
                PHI1.resize(0.12, node_id=s)
        >>> PHI1.scale(0.7)
        >>> PHI1.view
        ```

        ![PHI1](../../assets/images/agreg2022_S0E2/PHI1.svg){ .centrer }       

??? question "Q38"

    Soit $n$ la taille de la 3-FNC $\varphi$. Alors $\varphi$ possèdent $m$ littéral positif avec $m\lt 3n$. Le nombre de sommets de $G_\varphi$ sera (par construction) : $8 + 2m$. Quand au nombre d'arêtes il est de $3 + 3m + 10n$. Au total on a donc $11 + 5m + 10n$ _éléments de construction_ quantité inférieure à $11 + 25n$ justifiant ainsi une construction en temps polynomial. $\square$

??? question "Q39"

    === "Le graphe d'une 3-FNC"

        On considère la graphe suivant :

        ```python
        >>> G39 = pygraph.Graph(10)
        >>> G39.add_edges_from([(0,1), (0,2), (0,3), (0,9), (1,4), (2,5), (4,6), 
                                (5,6), (6,7), (3,8), (7,8), (7,9), (8,9), (4,5)])
        >>> G39.position([(0,0,-0.25), (3,1,0.25), (2,1,1), (1,1,2), (4,2,2), (5,2,1), 
                            (6,3,1.5), (7,4,1.5), (8,4,0.25), (9,5,-0.25)], 0.6)
        >>> G39.set_labels('Kxyz45678T')
        >>> G39.label_on()
        >>> G39.view
        ```

        ![G39](../../assets/images/agreg2022_S0E2/G39.svg){ .centrer }       

        Ci-dessous une 3-coloration qui prouve que ce graphe est 3-colorable :

        ```python
        >>> G39.colorise()
        >>> G39.color_on()
        >>> G39.view
        ```

        ![G39 coloré](../../assets/images/agreg2022_S0E2/G39_coloration.svg){ .centrer }       

    === "Réponse à la question"

        Le sommet $T$ est toujours de la même couleur que l'un des littéraux $x, y, z$.

        On suppose $T$ de couleur 0. $T$ forme un triangle avec les sommets 7 et 8, on a donc les 3 couleurs. Supposons que 8 soit de couleur 2. Dès lors $z$ est de couleur 0 ou 1. Si c'est 0 c'est comme $T$. Sinon, $z$ est de couleur 1. Ce qui impose la couleur 2 à $K$ (qui est relié à $z$ et à $T$). Dès lors $x$ et $y$ ne peuvent pas être de couleur 2. Il reste la couleur 1 ou 0. Montrons que 1 n'est pas possible. En effet, si $x$ et $y$ sont tous les deux 1 alors les noeuds 4 et 5 ne peuvent pas être 1 et donc se partagent les couleurs 0 et 2 laissant 1 pour le noeud 6 (4, 5, 6 forment un triangle). Mais alors le noeud 7 est de couleur 2 et 8 de couleur 1 ce qui n'est pas possible puisque relié à $z$. $\square$

??? question "Q40"

    Il faut montrer que $\varphi$ est satisfiable si et seulement si $G_\varphi$ est 3-colorable.

    - Soit une 3-coloration de $G_\varphi$. Pour chaque clause $C_i$ de $\varphi$, il existe un littéral $x_i$ dont le sommet associé est de même couleur que le noeud $T$. On affecte à _Vrai_ chacun de ces $x_i$.

    - Soit une affectation des variables de $\varphi$ qui satisfait la formule. On construit alors la 3-coloration suivante : chaque litteral à _vrai_ prend la couleur 0, ainsi que le sommet $T$. Les négations des littéraux prennent la couleur 1 ainsi que le noeud $F$. Les deux noeuds entre les littéraux et $T$ se partagent les 2 couleurs 1 et 2. Les triangles entre les littéraux et le triangle de $T$ prend les 3 couleurs. Quand au noeud $K$ il prend la couleur 2.

??? question "Q41"

    Puisqu'on a 3-SAT $\leq_P$ 3_coloration et que 3-SAT est NP-complet alors 3-coloration est au moins NP-difficile. $\square$

### 3.4 Algorithmes gloutons

Dans cette section, on étudie les algorithmes suivants :

```
Début algorithme
    Entréee : graphe G = (S, A)
    Pour chaque sommet s in S non coloré Faire
        Colorer s avec la plus petite couleur possible
```

??? question "Q42"

    Ma version était fausse... merci à Romain pour la correction :

    ```python
    def plus_petit_absent(L):
        vus = [0] * len(L)
        for v in L:
            if v < len(L):
                vus[v] = 1
        i = 0
        while i < len(vus) and vus[i] == 1:
            i += 1
        return i
    ```

    Test :

    ```python
    >>> plus_petit_absent([1, 0])
    2
    >>> plus_petit_absent([1, 2])
    0
    >>> plus_petit_absent([2, 0])
    1
    ```
 
??? question "Q43"

    ```python
    def colo_glouton(G):
        n = len(G)
        color = [-1] * n
        for i in range(n):
            if color[i] == -1:
                color[i] = plus_petit_absent([color[j] for j in G[i] if color[j] != -1])
        return color
    ```

??? question "Q44"

    `plus_petit_absent` est linéaire en la longueur de la liste. Cette liste ici dans le pire des cas est la totalité des arêtes. On a donc un complexité totale en ${\cal O}(n\times m)$. $\square$

??? question "Q45"

    On considère le graphe $G_2$ suivant :

    ```python
    >>> G2 = pygraph.Graph(5)
    >>> G2.add_edges_from([(0, 1), (0, 4), (1, 4), (3, 4), (1, 3), (2, 3)])
    >>> G2.position([(0, 0, 0), (1, 1, -0.75), (4, 1, 0.75), (3, 2, 0), (2, 3, 0)], 0.7)
    >>> G2.view
    ```
    ![G2](../../assets/images/agreg2022_S0E2/G2.svg){ .centrer }       

    Les listes d'adjacences de $G_2$ :

    ```python
    >>> AG2 = [[1, 4], [0, 3, 4], [3], [1, 2, 4], [0, 1, 3]]
    ```

    Et notre algo glouton de coloration va se _fourvoyer_ en état obligé d'attribuer une quatrième couleur au noeud 4 :

    ```python
    >>> colo_glouton(AG2)
    [1, 0, 1, 2, 3]
    ```

??? question "Q46"

    Soit $G$ un graphe tel que $\chi(G) = k$. Soit $C$ la $k$-coloration : $0, 1,\ldots k-1$, alors si on considère l'indexation des sommets qui suit la coloration : on commence par numéroter $0, 1,\ldots n_0$ tous les sommets avec la couleur 0, on continue $n_0+1,\ldots n_1$ tous ceux colorés avec 1 etc. L'algorithme glouton donnera alors la coloration $C$. $\square$

??? question "Q47"

    Dans $G_2$, c'est le fait de traiter le sommet 4 en dernier qui fait rater la coloration optimale. Avec l'algorithme de Walsh-Powell on a la garantie de traiter les sommets 1, 3, et 4 avant les 2 autres. Ainsi le triangle sera coloré 0, 1, 2 et le sommet 2 peut avoir une des couleurs du sommet 1 ou du sommet 4. Le noeud 0 aura la couleur du sommet 3. On garantit donc la 3-coloration. $\square$

On considère les _graphes couronnes_ $J_n$ à $2n$ sommets. Ces graphes sont dit _bi-partie_ ie qu'ils admettent une 2-coloration.

??? note "Les codes"

    === "Définition de $J_n$"

        ```python
        def jn(n):
            couronne = pygraph.Graph(2*n)
            for i in range(n):
                for j in range(n, 2*n):
                    if i%n != j%n:
                        couronne.add_edge(i, j)
            return couronne
        ```

    === "Pour afficher en ligne"

        ```python
        def jn_en_ligne(jn):
            pos = []
            n = len(jn.node_ids()) // 2
            for i in range(n):
                pos.append((i, 0, -i))
            for j in range(n, 2*n):
                pos.append((j, 1.5, -(j%n)))
            jn.position(pos, 0.7)
        ```

    === "Pour afficher en couronne"

        ```python
        from math import cos, sin, pi

        def jn_en_couronne(jn, rayon=1):
            n = len(jn.node_ids()) // 2
            pos = []
            a = 0
            for i in range(n):
                pos.append((i, rayon*cos(a), rayon*sin(a)))
                a += 2*pi/n
            a = (((n - 1)//2)*2+1) * pi/n
            for i in range(n, 2*n):
                pos.append((i, rayon*cos(a), rayon*sin(a)))
                a += 2*pi/n
            jn.position(pos)
        ```

??? example "Les exemples"

    === "J3 en ligne"

        ```python
        >>> J3 = jn(3)
        >>> jn_en_ligne(J3)
        >>> J3.view
        ```
    
        ![J3 en ligne](../../assets/images/agreg2022_S0E2/J3_ligne.svg){ .centrer }       

    === "J3 en couronne"

        ```python
        >>> jn_en_couronne(J3)
        >>> J3.view
        ```
    
        ![J3 en couronne](../../assets/images/agreg2022_S0E2/J3_couronne.svg){ .centrer }       

    === "J4 en ligne"

        ```python
        >>> J4 = jn(4)
        >>> jn_en_ligne(J4)
        >>> J4.view
        ```
    
        ![J4 en ligne](../../assets/images/agreg2022_S0E2/J4_ligne.svg){ .centrer }       

    === "J4 en couronne"

        ```python
        >>> jn_en_couronne(J4)
        >>> J4.view
        ```
    
        ![J4 en couronne](../../assets/images/agreg2022_S0E2/J4_couronne.svg){ .centrer }       

    === "J5 en ligne"

        ```python
        >>> J5 = jn(5)
        >>> jn_en_ligne(J5)
        >>> J5.view
        ```
    
        ![J5 en ligne](../../assets/images/agreg2022_S0E2/J5_ligne.svg){ .centrer }       

    === "J5 en couronne"

        ```python
        >>> jn_en_couronne(J5)
        >>> J5.view
        ```
    
        ![J5 en couronne](../../assets/images/agreg2022_S0E2/J5_couronne.svg){ .centrer }       

??? question "Q48"

    Soit $J_n = (\{x_1,\ldots x_n\}\cup \{y_1,\ldots y_n\}, S)$ un graphe couronne avec $n\geq 3$. Notons que tout sommet de $J_n$ est de degré $n - 1$ c'est donc l'indexation qui détermine l'ordre suivant lequel les sommets sont colorés.

    ##### Une 2-coloration

    La coloration $C$ obtenue en suivant l'indexation suivante est une 2-coloration : $\forall 1\leq i\leq n, I(x_i) = i$ et $\forall 1\leq i\leq n, I(y_i) = n + i$. En effet tous les $x_i$ n'étant pas liés entre eux, à chaque $x_i$ choisit, la plus petite couleur est 0. Puis, pour chaque $y_j$ il sera relié à des $x_i$ et uniquement des $x_i$, la plus petite couleur sera donc 1.

    ##### Une $n$-coloration

    On considère maintenant l'indexation suivante : $\forall 1\leq i\leq n, I(x_i) = 2i - 2$ et $\forall 1\leq i\leq n, I(y_i) = 2i - 1$. Montrons que pour tout $1\leq i\leq n$, on a $x_i$ et $y_i$ qui sont de couleur $i - 1$ (propriété que nous nommons $P(i)$).

    - $P(1)$ est vraie : en effet les premier sommet choisi sera $x_1$ et donc avec la couleur 0. Puis le deuxième numéro est 2 et c'est le sommet $y_1$ qui, n'étant pas lié à $x_1$ peut avoir aussi la couleur 0.
    - Supposons $P(i-1)$. Le prochain sommet à colorier est $x_i$ lié à chacun des $y_1,\ldots y_{i-1}$. D'après $P(i-1)$ ces sommets $y_j$ occupent les couleurs de 0 à $i-2$ et la plus petite couleur non utilisée est donc $i - 1$. Le sommet suivant est $y_i$, lié à $x_1,\ldots x_{i-1}$, mais pas à $x_i$. Les $x_j$ occupent les couleurs $0,\ldots i-2$ et la plus petite couleur utilisable est $i-1$. 

    En conséquence, $P(n)$ est vraie et prouve que l'indexation $I$ conduit l'algorithme à une $n$-coloration. $\square$

??? example "En images..."

    === "J5 en 2-coloration"

        Ordre de traitement : $0, 1,\ldots 8$ 

        ![J5 2-coloré](../../assets/images/agreg2022_S0E2/J5_2_coloration.svg){ .centrer }       

    === "J4 en 4-coloration"

        Ordre de traitement : $0, 4, 1, 5, 2, 6, 3, 7$ 

        ![J4 4-coloré](../../assets/images/agreg2022_S0E2/J4_4_coloration.svg){ .centrer }       

??? question "Q49"

    On considère le graphe suivant :

    ```python
    >>> G3 = pygraph.Graph(11)
    >>> G3.add_edges_from([(0, 1), (1, 2), (1, 3), (1, 4), (4, 5), (5, 6), (5, 7), (7, 8), (7, 9), (7, 10)])
    >>> G3.position([(0, 0, 0), (1, 1, 0), (2, 1, 1), (3, 1, -1), (4, 2, 0), (5, 3, 0), (6, 3, 1), (7, 4, 0), (8, 4, 1), (9, 4, -1), (10, 5, 0)], 0.8)
    >>> G3.view
    ```

    ![G3](../../assets/images/agreg2022_S0E2/G3.svg){ .centrer }       

    Quelle que soit l'indexation choisie, l'algorithme va prendre l'ordre suivant :

    1. les deux sommets d'ordre 4 (1 et 7 dans notre exemple)
    2. le sommet d'ordre 3 (sommet 5)
    3. le sommet d'ordre 2 (sommet 4)
    4. enfin les sommets d'odre 1, dans un ordre qui va dépendre de l'indexation choisie

    Construisons la coloration :

    1. les sommets d'ordre 4 ne sont pas reliés entre eux, ils seront choisis les premiers et auront la couleur 0, peu importe l'ordre 
    2. est ensuite choisi le sommet d'ordre 3 qui est lié à un des sommets d'ordre 4 : sa couleur sera donc 1
    3. le sommet d'ordre 2 étant lié aux sommets d'ordre 4 et 3, les couleurs 0 et 1 lui sont interdites, la plus petite possible est donc 2

    On obtient donc une $k$-coloration avec $k\geq 3$ alors que la coloration suivante est une 2-coloration :

    - On colorie en 0 : 0, 2, 3, 4, 6, 7
    - On colorie en 1 : 1, 5, 8, 9, 10

    $\square$

    Cette coloration est d'ailleurs obtenue par l'algorithme [D-SATUR](https://fr.wikipedia.org/wiki/DSATUR)

    ```python
    >>> G3.colorise()
    >>> G3.color_on()
    >>> G3.view
    ```

    ![G3 2-coloration](../../assets/images/agreg2022_S0E2/G3_2_coloration.svg){ .centrer }       

