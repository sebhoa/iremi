??? question "Q50"

    Soit $G = (S, A)$ un graphe d'intervalles et $r$ une réalisation de $G$.

    - Soit `ordo` un ordonnancement cohérent de $r$. Montrons que `ordo` est une coloration de $G$. Soit $(i, j)\in A$ alors cela signifie que les tâches associés $t_i$ et $t_j$ sont telles que $t_i\cap t_j\ne \emptyset$. Comme `ordo` est cohérent on a `ordo[i] != ordo[j]`.

    - Soit $c$ une coloration de $G$. Montrons que `c` est un ordonnancement cohérent des tâches de $r$. Soit $t_i$ et $t_j$ deux tâches telles que $i\ne j$. Si les sommets $i$ et $j$ associés sont tels que $c(i) \ne c(j)$ alors l'ordonnancement ne prévoit pas les mêmes machines pour les deux tâches et il n'y a pas d'incohérence. Si au contraire $c(i) = c(j)$ alors puisque $c$ est une coloration, cela signifie que $(i, j)\notin A$ et par conséquent $t_i\cap t_j = \emptyset$ prouvant la cohérence. $\square$


??? question "Q51"

    Soit $G = (S, A)$ un graphe d'intervalles et $r$ une réalisation de $G$. On traite les sommets du graphe dans l'ordre croissant des dates de début des intervalles correspondant dans la réalisation $r$.


### 4.1 Parcours en largeur lexicographique

On considère le graphe $G_4$ suivant. Il s'agit du graphe d'intervalles de l'ensemble de tâches $T_2$ du début de ce sujet ; c'est donc le même graphe que `GT2` mais présenté autrement :

```python
>>> G4 = pygraph.Graph(8)
>>> G4.add_edges_from([(0,4), (0,7), (0,6), (0,5), (5,1), (5,6), (6,7), (7,4), (4,3), (3,2)])
>>> G4.position([(7, -0.5, 0), (6, 0.5, 0), (0, 0, 1), (4, -1.5, 0), (5, 1.5, 0), (1, 1.5, -1), (2, 0, -1), (3, -1.5, -1)], 0.6)
>>> G4.view
```

![G4](../../assets/images/agreg2022_S0E2/G4.svg){ .centrer }       

La vue `GT2` :

![GT2](../../assets/images/agreg2022_S0E2/GT2.svg){ .centrer }


??? question "Q52"

    On va appliquer le parcours lexicographique sur $G_4$. Ci-dessous une fonction pour modifier un pygraph.Graph et faire _avancer_ l'algorithme :

    ??? note "Code"

        ```python
        etiquettes = [''] * 8
        selected = set()

        def select(g, s, i):
            g.color_on(s, 0)
            selected.add(s)
            g.node_view(s).label = str(s)
            i -= 1
            for v in g.neighbors(s):
                # on ne prend que les sommets non sélectionnés
                if v not in selected: 
                    etiquettes[v] += str(i)
                    g.node_view(v).label = f'{v}-{etiquettes[v]}'
            g.label_on()
            return i
        ```

    === "Étape 1"

        On fixe l'étiquette du sommet 0 à 8 :

        ```python
        >>> i = 8
        >>> etiquettes[0] = str(i)
        >>> G4.node_view(0).label = f'0-{etiquettes[0]}'
        >>> G4.label_on()
        >>> G4.view
        ```
        
        ![G4 Etape 1](../../assets/images/agreg2022_S0E2/G4-1.svg){ .centrer }       

    === "Étape 2"

        On sélectionne le sommet 0 et on concatène 7 à l'étiquette des voisins :

        ```python
        >>> i = select(G4, 0, i)
        >>> G4.view
        ```
        
        ![G4 Etape 2](../../assets/images/agreg2022_S0E2/G4-2.svg){ .centrer }       

    === "Étape 3"

        On sélectionne le sommet 4 (le plus petit indice avec l'étiquette maximale 7) et on concatène 6 à l'étiquette des voisins (désolé pour le souci d'affichage, un des points d'amélioration de pygraph) :

        ```python
        >>> i = select(G4, 4, i)
        >>> G4.view
        ```
        
        ![G4 Etape 3](../../assets/images/agreg2022_S0E2/G4-3.svg){ .centrer }       

    === "Étape 4"

        On sélectionne le sommet 7 et on rajoute 5 à l'étiquette des voisins (non déjà sélectionnés) :

        ```python
        >>> i = select(G4, 7, i)
        >>> G4.view
        ```
        
        ![G4 Etape 4](../../assets/images/agreg2022_S0E2/G4-4.svg){ .centrer }

    === "Étape 5"

        On sélectionne le sommet 6 et on rajoute 4 à l'étiquette des voisins (non déjà sélectionnés) :

        ```python
        >>> i = select(G4, 6, i)
        >>> G4.view
        ```
        
        ![G4 Etape 5](../../assets/images/agreg2022_S0E2/G4-5.svg){ .centrer }

    === "Étape 6"

        On sélectionne le sommet 5 et on rajoute 3 à l'étiquette des voisins :

        ```python
        >>> i = select(G4, 5, i)
        >>> G4.view
        ```
        
        ![G4 Etape 6](../../assets/images/agreg2022_S0E2/G4-6.svg){ .centrer }

    === "Étape 7"

        On sélectionne le sommet 3 et on rajoute 2 à l'étiquette des voisins :

        ```python
        >>> i = select(G4, 3, i)
        >>> G4.view
        ```
        
        ![G4 Etape 7](../../assets/images/agreg2022_S0E2/G4-7.svg){ .centrer }

        On termine avec les sommets 1 et 2 donnant l'ordre suivant : $\sigma = (0, 4, 7, 6, 5, 3, 1, 2)$.

??? question "Q53"

    L'algorithme glouton en suivant l'ordre LexBFS (donc avec le $\sigma$ précédent) donne : 

    - premier sommet 0 : couleur 0
    - puis 4 coloré en 1
    - puis 7 couleur 2
    - puis 6 couleur 1
    - puis 5 couleur 2
    - puis 3 couleur 0
    - puis 1 couleur 0
    - enfin 2 couleur 1

    On a bien une 3-coloration

    Par un parcours classique via indices croissants on aurait eu : 0 en couleur 0, 4 en couleur 1, 5 en 1, 6 en 2, 7 en 3 (puisque 0, 1 et 2 sont déjà pris par les voisins), 3 en 0, 1 en 0 et enfin 2 en 1. On n'obtient donc qu'une 4-coloration.

### 4.2 Application à la coloration de graphes d'intervalles

!!! note "Parcours simplicial"

    Un parcours $\sigma$ est _simplicial_, si pour chaque sommet, ses voisins qui le précèdent forment une clique.

??? question "Q54"

    _en cours_

??? question "Q55"

    _en cours_

#### 4.3 Implémentation

??? question "Q56"

    L'algorithme reprend donc l'algorithme glouton mais au lieu de juste parcourir les noeuds suivant les indices, ici on doit sélectionner suivant l'ordre BFS-lexicographique.

    On a besoin des 3 structures suivantes :

    - un tableau pour la coloration
    - un tableau pour les étiquettes
    - un ensemble pour les sommets à traiter (retrait et test d'appartenance en temps constant)

    ??? note "Le code"

        === "La fonction principale"

            ```python
            def lexBFS(G):
                """Réalise une coloration glouton en suivant l'ordre lexBFS"""
                n = len(G)
                coloration = [-1] * n
                etiquettes = [''] * n
                etiquette = n
                noeuds_a_traiter = set(range(n))
                while noeuds_a_traiter:
                    s = select(G, noeuds_a_traiter, etiquettes, coloration)
                    print(s) # rajouté pour vérifier l'ordre des noeuds
                    noeuds_a_traiter.remove(s)
                    couleur = plus_petit_absent([coloration[j] for j in G[s] if coloration[j] != -1])
                    coloration[s] = couleur
                    # mise à jour des étiquettes
                    etiquette -= 1
                    for v in G[s]:
                        if v in noeuds_a_traiter:
                            etiquettes[v] += str(etiquette)
                return coloration
            ```

        === "La fonction de sélection"

            ```python
            def select(g, noeuds, etiquettes, coloration):
                """retourne le noeud de g non coloré qui maximise
                le degré et l'étiquette (ordre lexico)"""
                s = None
                for v in noeuds:
                    if s is None:
                        s = v
                    elif coloration[v] == -1:
                        if len(g[v]) > len(g[s]) or len(g[v]) == len(g[s]) and etiquettes[v] > etiquettes[s]:
                            s = v
                return s
            ```

            plus pythonique :

            ```python
            def select(g, noeuds, etiquettes, coloration):
                return max([v for v in noeuds if coloration[v] == -1], key=lambda s: (len(g[s]), etiquettes[s]))
            ```

    Exécuté sur le graphe $G_4$, on obtient l'odre de parcours des sommets suivant : $(0, 4, 7, 6, 5, 3, 1, 2)$ et la coloration optimale suivante :

    ![G4 LexBFS](../../assets/images/agreg2022_S0E2/G4-56.svg){ .centrer }
