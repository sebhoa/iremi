### 1.1 Représentations des ensembles de tâches

Ci-dessous deux exemples du sujet de représentations graphiques d'ensembles de tâches :

![ensembles de tâches](../../assets/images/agreg2022_S0E2/ex_taches.png){ .centrer }

Liste d'intervalles de l'exemple $T_1$ :

```python
T1 = [(9, 11), (0, 4), (1, 7), (14, 19), (3, 12), (13, 15), (8, 17)]
```

??? question "Q1"

    La liste de l'exemple $T_2$

    ```python
    T2 = [(5, 12), (13, 15), (0, 2), (1, 4), (3, 7), (10, 14), (8, 11), (6, 9)]
    ```

??? question "Q2"

    - Pour les tâches de $T_1$, on peut constater 3 groupes de tâches 2 à 2 disjointes : $(t_2, t_6)$, $(t_4, t_5)$ et $(t_1, t_0, t_3)$ on ne peut pas faire mieux puisque par exemple pour $x = 10$ on a : $x\in t_0$ et $x\in t_4$ et $x\in t_6$ ; il nous faut donc 3 machines a minima
    - Pour les tâches de $T_2$, le minimum est également 3 et le groupement suivant fonctionne : $(t_2, t_4, t_6)$, $(t_3, t_5, t_7)$ et $(t_0, t_1)$. 

Le sujet introduit la notion d'_événement_ : _(date, indice, debut-ou-fin)_ Les événements pour $T_1$ sont donnés :

```python
Ev1  =  [(0, 1, 0), (1, 2, 0), (3, 4, 0), (4, 1, 1), (7, 2, 1), (8, 6, 0), 
        (9, 0, 0), (11, 0, 1), (12, 4, 1), (13, 5, 0), (14, 3, 0), (15, 5, 1), 
        (17, 6, 1), (19, 3, 1)]
```

??? question "Q3"

    Les 7 premiers événements pour $T_2$ :

    ```python
    REP3 = [(0, 2, 0), (1, 3, 0), (2, 2, 1), (3, 4, 0), (4, 3, 1), (5, 0, 0), (6, 7, 0)]
    ```

??? question "Q4"

    La fonction `formate` qui, à partir d'une liste d'intervalles retourne la liste triée par ordre croissant des événements :

    ??? note "Code"

        ```python
        def inserer(liste, i, elt):
            while i >= 0 and liste[i] > elt:
                liste[i+1] = liste[i]
                i -= 1
            liste[i+1] = elt
        
        def formate(intervalles):
            evenements = [None] * 2*len(intervalles)
            i = -1
            for indice, (debut, fin) in enumerate(intervalles):
                inserer(evenements, i, (debut, indice, 0))
                i += 1
                inserer(evenements, i, (fin, indice, 1))
                i += 1
            return evenements
        ```

    !!! example "Exemples"

        === "Les événements de $T_1$"

            ```python
            >>> formate(T1)
            [(0, 1, 0), (1, 2, 0), (3, 4, 0), (4, 1, 1), (7, 2, 1), (8, 6, 0), 
            (9, 0, 0), (11, 0, 1), (12, 4, 1), (13, 5, 0), (14, 3, 0), (15, 5, 1), 
            (17, 6, 1), (19, 3, 1)]
            ```
        
        === "Les événements de $T_2$"

            On peut vérifier notre réponse à la question 3 :

            ```python
            >>> Ev2 = formate(T2)
            >>> Ev2[:7] == REP3
            True
            ```

??? question "Q5"

    La fonction `formate` est un tri par sélection : on crée une liste pour accueillir les $2n$ événements correspondants aux $n$ intervalles. Puis, pour chaque intervalle $I$ on insère deux événements dans la liste triée des $k$ premiers événements déjà placés. A partir du $n^e$ événement, dans le pire des cas, la fonction `insere` sera amenée à tout décaler vers la droite, effectuant donc de l'ordre de $n^2$ opérations élémentaires. $\square$ 


### 1.2 Calcul du nombre maximum de tâches simultanées

On appelle $\cal{K}(T)$ le ombre maximum de tâches simultanément en cours d'exécution. Par exemple $\cal{K}(T_1) = 3$.

??? question "Q6"

    On a $\cal{K}(T_2) = 3$

??? question "Q7"

    Il s'agit d'écrire une fonction qui calcule $\cal{K}(T)$.

    ```python
    def nb_simultanees(evts):
        max_simul = 0
        nb_simul = 0
        for date, indice, debut_fin in evts:
            if debut_fin == 0:
                nb_simul += 1
                if nb_simul > max_simul:
                    max_simul = nb_simul
            else:
                nb_simul -= 1
        return max_simul
    ```

??? question "Q8"

    L'invariant est : à chaque début de boucle `for` la variable `max_simul` représente le nombre maximum de tâches qui on été exécutées simultanément à la date `date`. 

    L'invariant est vrai en entrant dans la boucle : aucune tâche n'a commencé, et donc on a bien 0 tâche en simultanée. 

    Supposons qu'on soit à un tour quelconque de boucle, la date est `date` et `max_simul` représente le maximum de tâches qui ont été exécutées simultanément à une date inférieure à `date`. Si l'événement qu'on explore est un début de tâche (`debut_fin` vaut 0) alors on va incrémenter `nb_simul` qui représente le nombre de tâches en cours simultanément. Et si ce nombre dépasse `max_simul` on met à jour `max_simul` ; si on explore un événement de fin de tâche, alors on décrémente `nb_simul` qui ne peut donc pas dépasser `max_simul` et effectivement une tâche se terminant, on en a une de moins en cours d'exécution. 

    Dans tous les cas, à l'amorce du prochain passage, `max_simul` représente toujours le nombre maximum de tâches qui ont été exécutées simultanément. $\square$

    **Exemple**

    ```python
    >>> nb_simultanees(Ev1)
    3
    ```

### 1.3 Calcul d'un ordonnancement optimal

On s'intéresse à l'algorithme suivant :

En s'inspirant du calcul du nombre maximum de tâches simultanées, on peut écrire un algorithme glouton qui calcule un ordonnancement pour un ensemble $T$ de tâches donné.

Pour cela, on parcourt la liste triée des événements : à chaque fois que l'on rencontre un début de tâche, on lui attribue la machine disponible de plus petit indice et cette machine ne sera à nouveau disponible qu'au moment de la date de fin de cette tâche.

??? question "Q9"

    Ordonnancement obtenu pour $T_2$ :

    ```python
    >>> ORD2 = [1, 0, 0, 1, 0, 2, 0, 2]
    ```

??? question "Q10"

    ??? note "Code"

        ```python
        def plus_petite_machine(utilisees):
            for i in range(len(utilisees)):
                if utilisees[i] == 0:
                    return i

        def ordo_glouton(evts):
            n = len(evts) // 2
            machines_utilisees = [0] * n
            ordo = [-1] * n
            for date, i_tache, debut_fin in evts:
                if debut_fin == 0:
                    i_machine = plus_petite_machine(machines_utilisees)
                    ordo[i_tache] = i_machine
                    machines_utilisees[i_machine] = 1
                else:
                    i_machine = ordo[i_tache]
                    machines_utilisees[i_machine] = 0
            return ordo
        ```

    !!! example "Exemples"

        === "$T_1$"

            ```python
            >>> ordo_glouton(Ev1)
            [1, 0, 1, 2, 2, 1, 0]
            ```
        
        === "$T_2$"

            ```python
            >>> ordo_glouton(Ev2) == ORD2
            True
            ```

??? question "Q11"

    Nous devons prouver que :

    1. une machine attribuée est libre et que c'était la plus petite possible
    2. une tâche qui se termine libère sa machine

    Le point 1 est garanti par construction du tableau des machines et la fonction `plus_petite_machine` qui parcourt les indices de machines en commençant par 0 et qui retourne cet indice dès que la valeur du tableau vaut 0 (signifiant que la machine est libre). Le point 2 est vérifié par le `else` de la boucle principale de `ordo_glouton` : dès qu'on croise une date de fin, on libère la machine qui lui était affectée. $\square$

??? question "Q12"

    Si l'algorithme n'était pas optimal, cela voudrait dire qu'à un moment, pour un intervalle de tâches $T$, une $k+1$-ième machine a été affectée à une tâche $t$ alors que $\cal{K}(T) = k$. Cela signifie qu'une tâche parmi celles qui utilisaient les machines $0, 1,\ldots k-1$ s'est terminée au moment où $t$ se lance. Mais alors, d'après le point 2 de la question précédente, la machine $j < k$ ainsi libérée aurait dûe être affectée à la tâche $t$ (point 1 de la question précédente). Il y a contradiction, l'hypothèse de départ est donc fausse : l'algorithme est optimal. $\square$

??? question "Q13"

    Pour chaque événement, on va parcourir la liste des machines (et il peut y en avoir autant que d'intervalles dans le pire des cas) donc la complexité est **quadratique** en fonction du nombre d'intervalles. $\square$