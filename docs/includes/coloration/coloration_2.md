On va utiliser le module `pygraph` pour continuer à jouer avec ce sujet.

```python
>>> import pygraph
```

On représente les ensembles d'intervalles par des graphes ; par exemple pour $T_1$ :

??? example "Le graphe pour $T_1$"

    ```python
    >>> GT1 = pygraph.Graph(7)
    >>> GT1.add_edges_from([(0, 4), (0, 6), (1, 4), (1, 2), (5, 6), (5, 3), (3, 6), (2, 4), (4, 6)])
    >>> GT1.position([(2, -0.5, 0), (5, 0.5, 0), (1, -2.5, 0), (3, 2.5, 0), (4, -1.5, 1.5), (6, 1.5, 1.5), (0, 0, 3)], 0.5)
    >>> GT1.view
    ```

    ![GT1](../../assets/images/agreg2022_S0E2/GT1.svg){ .centrer }

??? question "Q14"

    Le graphe de $T_2$ :

    ```python
    >>> GT2 = pygraph.Graph(8)
    >>> GT2.add_edges_from([(0, 4), (0, 7), (0, 6), (0, 5), (4, 7), (4, 3), (5, 6), (5, 1), (6, 7), (3, 2)])
    >>> GT2.position([(0, 0, 0), (4, 1, 0), (3, 2, 0), (2, 3, 0), (5, -1, 0), (1, -2, 0), (7, 0.5, 0.75), (6, -0.5, 0.75)], 0.7)
    >>> GT2.view
    ```

    ![GT2](../../assets/images/agreg2022_S0E2/GT2.svg){ .centrer }

??? question "Q15"

    Il s'agit d'écrire une fonction qui, à partir d'une liste d'intervalles construit le graphe associé :

    ```python
    def inter(t1, t2):
        a1, b1 = t1
        a2, b2 = t2
        return a1 < a2 < b1 or a2 < a1 < b2

    def liste_vers_graphe(T):
        n = len(T)
        graph = [[] for _ in range(n)]
        for i in range(n):
            for j in range(i+1, n):
                if inter(T[i], T[j]):
                        graph[i].append(j)
                        graph[j].append(i)
        return graph
    ```

    **Exemple**

    ```python
    >>> A2 = liste_vers_graphe(T2)
    >>> A2
    [[4, 5, 6, 7], [5], [3], [2, 4], [0, 3, 7], [0, 1, 6], [0, 5, 7], [0, 4, 6]]
    ```

??? tip "Bonus"

    Il manque des fonctionnalités à pygraph (une nouvelle version est prévue). Par exemple, le lien avec les listes d'adjacences.

    Voici une fonction que nous allons utiliser ; créer un pygraph.Graph depuis une liste de listes d'adjacences :

    ```python
    def liste_adjacence_pygraph(adj):
        n = len(adj)
        g = pygraph.Graph(n)
        for i in range(n):
            for j in adj[i]:
                if j > i:
                    g.add_edge(i, j)
        return g
    ```

    On peut alors retrouver le graphe de $T_2$ :

    ```python
    >>> G2 = liste_adjacence_pygraph(A2)
    >>> G2.same_position_as(GT2)
    >>> G2.scale(0.7)
    >>> G2.view
    ```

    ![GT2](../../assets/images/agreg2022_S0E2/GT2.svg){ .centrer }

??? question "Q16"

    On considère le graphe suivant :

    ```python
    >>> G3 = pygraph.Graph(5)
    >>> G3.add_edges_from([(0, 1), (0, 2), (0, 3), (1, 2), (2, 3), (3, 4)])
    >>> G3.position([(0, 0, 0.75), (1, -1, 0), (2, 0, -0.75), (3, 1, 0), (4, 2, 0)], 0.6)
    >>> G3.view
    ```

    ![G3](../../assets/images/agreg2022_S0E2/G3.svg){ .centrer }

    Voici une réalisation possible de ce graphe d'intervalles :

    ```
        |---t2---|
    |---t1---|  |----t3-----|
        |-----t0-------|  |-t4--| 
    |--|--|--|--|--|--|--|--|--|--->
    0  1  2  3  4  5  6  7  8  9
    ``` 

On considère les graphes cycliques.

??? tip "Bonus"

    Une fonction pour créer un pygraph.Graph cyclique d'ordre $n$ :

    ```python
    def cyclique(n):
    g = pygraph.Graph(n)
        for i in range(n):
            g.add_edge(i, (i+1)%n)
        return g
    ```

??? example "C4"

    Voici $C_4$ le graphe cyclique d'ordre 4 :

    ```python
    >>> C4 = cyclique(4)
    >>> C4.view
    ```

    ![C4](../../assets/images/agreg2022_S0E2/C4.svg){ .centrer }

??? question "Q17"

    Soit $t_0$ et $t_1$ deux intervalles de temps, avec une intersection non vide. Puisque $t_2$ doit chevaucher $t_1$ sans toucher à $t_0$ alors $t_1$ doit donc _déborder_ de $t_0$, disons par la droite (par la gauche le raisonnement reste le même), comme ceci :

    ```
        |---t1---|
    |---t0---|
    ```

    Dès lors $t_2$ vient chevaucher la partie de $t_1$ qui n'est pas en contact avec $t_0$ :

    ```
            |---t2---|
        |---t1---|
    |---t0---|
    ```

    De même, $t_3$ doit chevaucher $t_2$ sans toucher à $t_1$... mais en touchant $t_0$ ce qui n'est pas possible. On pourrait formaliser un peu plus en introduisant les bornes des intervalles : $[d_0, f_0]$, ... $[d_3, f_3]$. On a alors une contradiction sur les inégalités : 

    $$d_0\lt d_1\lt f_0\lt d_2\lt f_1\lt d_3\lt f_2$$

    et 

    $$d_2\lt f_0$$

    $\square$

??? question "Q18"

    Le raisonnement de Q17 se généralise pour un cycle d'intervalles $t_0, t_1\ldots t_{n-1}$ : chaque intervalle $t_i$ pour $2\gt i\lt n-1$ chevauche l'intervalle $t_{i-1}$ mais en ne touchant pas à $t_{i-2}$ mais $t_{n-1}$ doit chevaucher $t_{n-2}$ sans toucher $t_{n-3}$ qui se trouve plus à gauche, mais en chevauchant $t_0$ qui lui est complètement à gauche, ce qui est impossible. $\square$

??? question "Q19"

    Soit $G = (S, A)$ un graphe d'intervalles et soit $(S', A')$ un sous graphe induit de $G$. Soit $t_i$ et $t_j$ deux intervalles.

    - Si $t_i\cap t_j = \emptyset$ alors $(i, j)\notin A$ et donc $(i, j)\notin A'$
    - Si $t_i\cap t_j \neq \emptyset$, on considère les deux cas suivants :
        - $i\notin S'$ ou $j\notin S'$ alors par construction du graphe induit $(i, j)\notin A'$ et il suffit de ne pas considérer $t_i$ ou $t_j$ parmi les intervalles associés au graphe induit pour ne pas avoir d'incohérence ;
        - $i\in S'$ et $j\in S'$ et donc $i\in S$ et $j\in S$ ; comme $G$ est un graphe d'intervalles, on a $(i, j)\in A$ et par construction du graphe induit $(i, j)\in A'$

    Réciproquement, si on considère deux sommets $i$ et $j$ du graphe induit tel que $(i, j)\in A'\subset S$ alors $(i, j)\in A$ et comme $G$ est un graphe d'intervalles on a $t_i\cap t_j \neq \emptyset$. $\square$

??? question "Q20"

    Un graphe contenant un cycle de longueur supérieure ou égale à 4 sans corde ne peut pas être un graphe d'intervalles. En effet, car sinon, par le résultat de Q19, le sous graphe induit réduit au graphe cyclique serait un graphe d'intervalles ce qui contredit Q18. $\square$

