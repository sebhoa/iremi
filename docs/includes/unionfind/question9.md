### Question 9

=== "Version impérative"

    ```python
    def representant(parent, i):
        while parent[i] != i:
            i = parent[i]
        return i
    ```

=== "Version objet"

    ```python
    class Partition:
        # ...

        def representant(self, i):
            while i != self.parent[i]:
                i = self.parent[i]
            return i
    ```

Dans le pire des cas, c'est-à-dire lorsque la partition est réduite à un unique groupe contenant tous les éléments, ordonnés. La recherche du représentant du _dernier_ élément ie celui qui n'est le parent de personne, parcourra les $n$ éléments. La complexité est donc en $O(n)$.

