### Question 6

Obtenir la liste des amis d'une personne.

=== "Version Impérative"

    ```python
    def liste_des_amis_de(reseau, i):
        return [j for j in range(reseau[0]) if sont_amis(reseau, i, j)]
    ```


=== "Version Objet"

    ```python
    class Reseau:
        # ...

        def personnes(self):
            return range(self.n)

        def liste_des_amis_de(self, i):
            return [j for j in self.personnes() if self.sont_amis(i, j)]
    ```

Pour chaque personne du réseau, on recherche un lien entre cette personne et celle dont on cherche les amis. Cela fait donc une complexité en $O(n\times m)$ dans le pire des cas.