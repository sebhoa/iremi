### Question 11

Si on a une partition de $n$ singletons alors les $(n - 1)$ fusions suivantes nécessitent $1\times 2\times ... \times (n-1)$ opérations et donc de l'ordre de $n^2$ opérations.

```python
>>> fusion(1, 0)
>>> fusion(2, 1)
...
>>> fusion(n-1, n-2)
```

On remédie à cette mauvaise performance en _compressant la relation_ lors de la recherche du représentant.  La question suivante met en oeuvre la compression.