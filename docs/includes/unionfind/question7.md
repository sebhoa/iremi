### Question 7

=== "Version impérative"

    Les représentations filiales A et B :

    ```python
    >>> RFA = [5,1,1,3,4,5,1,5,5,7]
    >>> RFB = [3,9,0,3,9,4,4,7,1,9]
    ```

=== "Version objet"

    ```python
    >>> PA = Partition(10, [(i, RFA[i]) for i in range(10)]){: .centrer}
    >>> PA.graph
    ```

    ![filiale A](../assets/images/union_find/filiale_A.svg)

    ```python
    >>> PB = Partition(10, [(i, RFB[i]) for i in range(10)])
    >>> PB.graph
    ```

    ![filiale A](../assets/images/union_find/filiale_B.svg){: .centrer}