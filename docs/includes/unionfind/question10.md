### Question 10

=== "Version impérative"

    ```python
    def fusion(parent, i, j):
        parent(representant[i]) = representant[j]
    ```

=== "Version objet"

    On rappelle la partition de la Figure 2 :

    ![figure 2](../assets/images/union_find/fig2.svg){: .centrer}

    Faisons en une copie, et déplaçons un peu les sommets pour préparer la fusion (la méthode `deplace` permet de décaler la position d'un noeud et éventuellement tout son groupe) :

    ```python
    >>> FIG3 = FIG2.copy()
    >>> FIG3.deplace(3, 1.5, 0)
    >>> FIG3.deplace(10, 1, 0)
    >>> FIG3.deplace(5, -5.25, -1)
    >>> FIG3.deplace(9, 1.5, 0, False)
    >>> FIG3.place(0.7)
    >>> FIG3.graph
    ```

    ![figure 3 avant fusion](../assets/images/union_find/fig3a.svg){: .centrer}

    Effectuons la fusion :

    ```python
    class Partition:
        # ...

        def fusion(self, i, j):
            self.parent[self.representant(i)] = self.representant(j)
    ```

    ```python
    >>> FIG3.fusion(6, 14)
    >>> FIG3.place(0.7)
    >>> FIG3.graph
    ```

    ![figure 3 après fusion](../assets/images/union_find/fig3b.svg){: .centrer}


