### Question 8

Création d'une partition à $n$ groupes constitués d'un unique élément.

=== "Version impérative"

    ```python
    def creer_partition_singletons(n):
        return list(range(n))
    ```

=== "Version objet"

    Pas de fonction à écrire puisque le constructeur par défaut crée cette partition _singletons_ :

    ```python
    >>> PSINGLETONS = Partition(5)
    >>> PSINGLETONS.graph
    ```

    ![singletons](../assets/images/union_find/singletons5.svg){: .centrer}