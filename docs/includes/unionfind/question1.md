### Question 1

Il s'agit de donner la modélisation de deux réseaux A et B dont on a le schéma du graphe :

=== "Version Impérative"

    Je rappelle que j'ai remplacé les `list` par des `set` pour les arètes puisqu'elles ne sont pas orientées. 

    ```python 
    >>> RA = [5, [{0,1}, {0,2}, {0,3}, {1,2}, {2,3}]]
    >>> RB = [5, [{0,1}, {1,2}, {1,3}, {2,3}, {2,4}, {3,4}]]
    ```

=== "Version Objet"

    Création via l'objet `Reseau` et positionnement des noeuds :

    ```python
    >>> RA = Reseau(5, [{0,1}, {0,2}, {0,3}, {1,2}, {2,3}])
    >>> RA.positionne([(2,0,0), (0,-1,1), (3,-1,-1), (1,1,1), (4,1,-1)], 0.5)
    >>> RA.graph
    ```

    ![reseau A](../../assets/images/union_find/reseau_A.svg)

    ```python
    >>> RB = Reseau(5, [{0,1}, {1,2}, {1,3}, {2,3}, {2,4}, {3,4}])
    >>> RB.positionne([(2,0,0.5), (0,-2,0), (3,0,-0.5), (1,-1,0), (4,1,0)], 0.8)
    >>> RB.graph
    ```

    ![reseau B](../../assets/images/union_find/reseau_B.svg)