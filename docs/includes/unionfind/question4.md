### Question 4

Une fonction `sont_amis` qui prend un réseau en paramètre ainsi que deux entiers et retourne `True` ssi les entiers sont liés dans le réseau.

=== "Version Impérative"

    Avec ma modélisation :

    ```python
    def sont_amis(reseau, i, j):
        return {i,j} in reseau[1]
    ```

    Si on suit le sujet :

    ```python
    def sont_amis(reseau, i, j):
        return any(est_un_lien_entre(paire, i, j) for paire in reseau[1])
    ```

=== "Version Objet"

    ```python
    class Reseau:
        # ...

        def sont_amis(self, i, j):
            return {i,j} in self.liens
    ```

Dans le pire des cas, on parcourt tous les liens pour se rendre compte que celui qu'on cherche n'y est pas. La complexité en temps est donc en $O(m)$.