### Question 5

On souhaite pouvoir ajouter des liens d'amitié à nos réseaux. On constate que quelque soit la modélisation, le fait d'utiliser une liste oblige à tester l'existence du lien avant de l'ajouter.

=== "Version Impérative"

    ```python
    def declare_amis(reseau, i, j):
        if not sont_amis(reseau, i, j):
            reseau[1].append({i,j})
    ```


=== "Version Objet"

    ```python
    class Reseau:
        # ...

        def declare_amis(self, i, j):
            if not self.sont_amis(i, j):
                self.liens.append({i,j})
    ```

La complexité est donc la même que le test de l'existence du lien soit $O(m)$.