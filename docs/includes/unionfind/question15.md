### Question 15

=== "Version impérative"

    ```python
    def taille_coupe(reseau, parent):
        return len([(i,j) for i,j in reseau[1] if representant(parent, i) != representant(parent, j)])
    ```

=== "Version objet"

    ```python
    class Reseau:
        # ...

        def taille_coupe(self):
            return len([(i,j) for i,j in self.liens if self.partition.representant(i) != self.partition.representant(j)])
    ```

