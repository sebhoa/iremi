### Question 13

=== "Version impérative"

    ```python
    def liste_des_groupes(parent):
        groupes = {}
        n = len(parent)
        for i in range(n):
            if parent[i] == i:
                groupes[i] = [i]
        for i in range(n):
            if parent[i] != i:
                groupes[representant(parent, i)].append(i)
        return list(groupe.values())
    ```

=== "Version objet"

    ```python
    class Partition:
        # ...

        def liste_des_groupes(self):
            groupes = {}
            for i in self.nodes():
                if self.parent[i] == i:
                    groupes[i] = [i]
            for i in self.nodes():
                if self.parent[i] != i:            
                    groupes[self.representant(i)].append(i)
            return list(groupes.values())       
    ```

    ```python
    >>> FIG2.liste_des_groupes()
    [[3, 2, 4, 12], [5, 0, 6, 7], [9, 1, 8, 11, 13, 14, 15], [10]]
    ```