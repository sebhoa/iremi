### Question 12

=== "Version impérative"

    ```python
    def representant(parent, i):
        a = i
        while a != parent[a]:
            a = parent[a]
        ancetre = a
        a = i
        while a != parent[a]:
            b = parent[a]
            parent[a] = ancetre
            a = b
    ```

    On fait 2 parcours au lieu d'un seul, mais d'un point de vue complexité on reste en $O(n)$ ce qui permet de dire que cette opération de compression est _gratuite_.

=== "Version objet"

    ```python
    class Partition:
        # ...

        def representant(self, i, compression=False):
            if compression:
                a = i
                while a != self.parent[a]:
                    a = self.parent[a]
                ancetre = a
                a = i
                while a != self.parent[a]:
                    b = self.parent[a]
                    self.parent[a] = ancetre
                    a = b
                self.init_graph()
                return ancetre
            else:
                while i != self.parent[i]:
                    i = self.parent[i]
                return i
    ```

    Utilisation sur la relation filiale de la Figure 2, en compressant à partir du noeud 14 :

    ```python
    >>> FIG2.representant(14, True)
    >>> FIG2.graph
    ```

    ![figure 2 compressé](../assets/images/union_find/fig2compresse.svg){: .centrer}
