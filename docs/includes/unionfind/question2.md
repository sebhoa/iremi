### Question 2

Création d'un réseau vide. J'en profite pour changer le nom et adopter la notation *snake_case* conforme PEP8.

=== "Version Impérative"

    ```python
    def creer_reseau_vide(n):
        return [n, []]
    ```

=== "Version Objet"

    Il n'y a pas de fonction à écrire puisque c'est le rôle du constructeur. Par exemple pour $n = 5$ :

    ```python    
    >>> VIDE = Reseau(5)
    >>> VIDE.positionne([(0,-2,0), (1,-1,0), (2,0,0), (3,1,0), (4,2,0)], 0.8)
    >>> VIDE.graph
    ```

    ![reseau vide](../../assets/images/union_find/vide5.svg)
