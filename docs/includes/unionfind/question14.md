### Question 14

Pour profiter au maximum des listes de Python, on crée la liste de indices des liens (liste des non marqués) ; on mélange cette liste via la fonction `shuffle` du module `random` ensuite il suffira de dépiler cette liste par la fin via des `pop()`.

La longueur d'une partition est le nombre de groupes, que l'on peut calculer en comptant les éléments qui sont leur propre parent.

=== "Version impérative"

    ```python
    def coupe_minimum_randomisee(reseau):
        n = reseau[0]
        m = len(reseau[1])
        p = creer_partition_singletons(n)
        liens_non_marques = list(range(m))
        random.shuffle(liens_non_marques)
        while longueur(p) >= 3 and liens_non_marques:
            i, j = reseau[1][liens_non_marques.pop()]
            if representant(p, i) != representant(p, j):
                fusion(p, i, j)
        if longueur(p) >= 3:
            reduire(p)
        return p
    ```         

    où 

    ```python
    def longueur(parent):
        return sum(i == parent[i] for i in range(len(parent))
    ```

    et 

    ```python
    def reduire(parent):
        representants = [i for i in range(len(parent)) if i == parent[i]]
        ancetre = 0
        for i in representants[2:]:
            fusion(parent, i, representants[ancetre])
            nacetre = ancetre - 1
    ```

=== "Version objet"

    On modifie notre classe `Reseau` :

    ```python
    class Reseau:
        
        def __init__(self, ...):
            # ...

            self.partition = Partition(n)

        def coupe_minimum_randomisee(self):
            p = self.partition
            liens_non_marques = list(range(len(self.liens)))
            random.shuffle(liens_non_marques)
            while len(p) >= 3 and liens_non_marques:
                i, j = self.liens[liens_non_marques.pop()]
                if p.representant(i) != p.representant(j):
                    p.fusion(i, j)
            if len(p) >= 3:            
                p.reduire()
    ```

    Et les ajouts à la classe `Partition` :

    ```python
    class Partition:
        # ...

        def __len__(self):
            return sum(self.parent[i] == i for i in self.nodes())

        def reduire(self):
            representants = [i for i in self.nodes()]
            ancetre = 0
            for k in range(2, len(representants)):
                self.fusion(representants[k], representants[ancetre])
                ancetre = 1 - ancetre
    ``` 

Complexité :

- la création de la partition singletons est en $O(n)$
- le mélange via `shuffle` utilise probablement un algorithme efficace type [Fisher-Yates](https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle) en $O(m)$
- la boucle `while` effectue au plus $m$ passages où elle fait :
    - un calcul de représentant qui est en $O(\alpha(n))$
    - une fusion qui est aussi un calcul de representant

Au total on a donc $O(n + m.\alpha(n))$.