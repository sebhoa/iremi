### Question 3

Cette question n'a pas de sens avec ma modélisation des arètes via les ensembles. Avec des listes on doit effectivement faire ceci :

```python
def est_un_lien_entre(paire, i, j):
    return paire == [i,j] or paire == [j,i]
```