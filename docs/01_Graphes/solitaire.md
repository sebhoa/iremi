![logo solitaire](../assets/images/solitaire/logo_solitaire.png){: align=left}
# Le jeu du Solitaire

--- 

## Introduction

Jeu de plateau à un joueur (comme son nom l'indique), son origine est incertaine. Il se joue sur un plateau appelé aussi **tablier**, le plus souvent en bois mais des versions en pierre existent dans lequel on trouve des trous ou **encoches** où peuvent venir se nicher des fichets[^1]. Ces fichets prennent différentes formes : pions, fichets, mais sur les jeux modernes se sont des boules le plus souvent.

**Quelques exemples de jeux**

![exemple1](../assets/images/solitaire/ex_solitaire_01.jpg) ![exemple2](../assets/images/solitaire/ex_solitaire_02.jpg)
![exemple3](../assets/images/solitaire/ex_solitaire_03.jpg)

**But du jeu**

Le but du jeu est, partant d'un **motif** initial c'est-à-dire d'un ensemble d'encoches pleines d'obtenir un motif final en vidant certaines encoches, selon la règle : un pion peut sauter au-dessus d'un pion voisin (au-dessus, au-dessous, à gauche ou à droite) pour arriver dans une encoche vide. Le pion sauté est retiré du plateau.

Il existe différents _Solitaire_, fonction de la forme du tablier et du nombre d'encoches qu'il possède. En voici quelques uns, les deux plus connus étant le _Solitaire Anglais_ à 33 encoches et le _Solitaire Européen_ à 37 encoches :



???+ Note "Divers tabliers"

    ![divers solitaires](../assets/images/solitaire/divers_solitaires.png){align=right}
    
    1. européen, XVII<sup>e</sup> siècle, 37 trous ;
    2. J. C. Wiegleb, 1779, Allemagne, 45 trous ;
    3. asymétrique 3-3-2-2, xxe siècle ;
    4. anglais, 33 trous ;
    5. diamant, 41 trous ;
    6. triangulaire, 15 trous.

    Auteur : Júlio Reis, source : [wikimedia](https://commons.wikimedia.org/wiki/File:Peg_Solitaire_game_board_shapes.svg) 


## Historique

- Certains font remonter l'origine du Solitaire à 43 av. J.-C. avec le poète Ovide. Mais :

    > Ce qu’Ovide évoque, dans L'Art d'aimer (Ars Amatoria) et les Tristes (Tristia), c’est un jeu d’alignement, du type morpion, mérelles ou jeu du moulin. 

    _Source_ : [Wikipédia](https://fr.wikipedia.org/wiki/Solitaire_(casse-t%C3%AAte))

- Les références médiévales quant à elles parlent d'un autre jeu, de type capture mais à deux joueurs (toujours d'après Wikipédia) :

    > Les références médiévales (Roman d'Alexandre, vers 1340 ; inventaire du roi d’Angleterre Édouard IV, au xve siècle, où le jeu Fox and Geese n'est jamais que [le renard et les poules](http://www.aisling-1198.org/dossiers/jeux-medievaux/les-jeux-de-plateau/poules-et-renard/renard-et-poules-1/)) désignent des jeux de chasse pour deux joueurs. Le jeu nommé « renard » dans la longue liste des jeux énumérés par Rabelais dans Gargantua (1534) est encore ce même jeu du renard et des poules.

    Il s'agit bien d'un jeu de prise en sautant et il se pourrait que le Solitaire soit un dérivé à 1 joueur de ce jeu.

![Dame de qualité jouant au Solitaire](../assets/images/solitaire/dame.jpg){width=200; align=right}_Gravure[^2]_

- Fin 17e début 18e, des références au jeu du Solitaire sont trouvées dans la revue _Mercure Galant_. En 1710 G.W. Leibniz en consacre un paragraphe dans le Volume 1 des _Mémoires de l'Académie des Sciences de Berlin_. Leibniz joue à une variante de son cru, qu'il décrit dans une lettre adressée à M. de Montmort :

    > Le jeu nommé _le Solitaire_ m'a plu assez. Je l'ai pris d'une manière renversée, c'est-à-dire qu'au lieu de défaire un composé de pièces selon la loi de ce jeu, qui est de sauter dans une place vide et d'ôter la pièce sur laquelle on saute, j'ai cru qu'il serait beau de rétablir ce qui a été défait, en remplissant un trou sur lequel on saute ; et par ce moyen on pourrait se proposer de former une telle ou telle figure proposée, si elle est faisable [...]

- Des études plus mathématiques au 19e et 20e siècle. L'extrait de la lettre de Leibniz est tiré des [Récréations Mathématiques, d'Édouard Lucas 1891](http://edouardlucas.free.fr/oeuvres/recreations_math_01_lucas.pdf).

## Objectif

Cet article a pour objectif d'explorer la recherche de solutions du Solitaire en programmation Python et en utilisant la POO. Il pourra servir de travail préparatoire à un enseignant de Lycée (Terminale NSI ou CPGE) pour la mise au point d'une ou plusieurs activités. Les thèmes abordés sont :

- La Programmation Orientée Objet en Python
- Graphes, parcours (en largeur et en profondeur), [_beam search_](https://en.wikipedia.org/wiki/Beam_search)

L'inspiration vient de diverses sources :

1. Le [sujet d'Informatique du concours Mines et Ponts de 2021 option MP](https://www.concoursminesponts.fr/resources/InfoMP-2021.pdf) et dont le corrigé a été exploré dans cet article : [Jouer au Solitaire avec Python et les Objets](https://irem.univ-reunion.fr/spip.php?article1118) 
2. Le [_Défi C et C++ : Recherche de solution du Solitaire_](https://c.developpez.com/defis/2-Solitaire/) proposé par le site developpez.com
3. La [5<sup>e</sup> récréation mathématique d'Édouard Lucas](http://edouardlucas.free.fr/oeuvres/recreations_math_01_lucas.pdf)

## Les problèmes d'Édouard Lucas

Le mathématicien consacre un chapitre entier à l'étude du solitaire européen (37 encoches) qu'il numérote comme ceci :

![numérotation E. Lucas](../assets/images/solitaire/num_lucas.png){: .centrer}

Grâce à Python et à la programmation orientée objet, nous allons modéliser ces problèmes et tenter de les résoudre. 

### Modéliser les motifs de départ et d'arrivée

Dans le [_Défi C et C++_](https://c.developpez.com/defis/2-Solitaire/) l'entrée du programme est un fichier texte du genre :

```
  xxx
  xxx
xxxxxxx
xxx.xxx
xxxxxxx
  xxx
  xxx
```

où les caractères espaces indiquent l'absence d'encoche, les `x` représentent les encoches pleines et les `.` les encoches vides. Ici, la configuration finale est systématiquement la configuration **duale** c'est-à-dire la configuration où toutes les encoches initialement pleines se retrouvent vides et réciproquement. Mais cela limite les cas d'études puisque la configuration finale est imposée. D'autant que pour le solitaire européen il est prouvé que le passage d'une configuration à la configuration duale est impossible.

Nous optons donc pour la lecture de deux fichiers. Dans le but de simplifier les appels, nous imposons que ces deux fichiers partagent une partie du nom. Par exemple `EL01` pour _Édouard Lucas Problème n°1_ consistera en deux fichiers textes : `EL01I.txt` codant la configuration initiale et `EL01F.txt` pour la configuration finale. De plus, si nous gardons les `x` et les `.` pour les encoches pleines et vides, l'espace peu lisible pourra être remplacée par n'importe quel autre caractère.

Voici à titre d'exemple les fichiers pour le problème n°I :

=== "EL01I.txt"

    ```
    EEEEEEEEE
    EEE...EEE
    EE..x..EE
    E..xxx..E
    E...x...E
    E...x...E
    EE.....EE
    EEE...EEE
    EEEEEEEEE
    ```


=== "EL01F.txt"

    ```
    EEEEEEEEE
    EEE...EEE
    EE.....EE
    E.......E
    E...x...E
    E.......E
    EE.....EE
    EEE...EEE
    EEEEEEEEE
    ```

La première fonction utilitaire de notre outil sera donc `create_file` qui permet de créer facilement les fichiers textes des problèmes de E. Lucas. Voici par exemple l'appel pour la création du fichier `EL01I.txt` :

```python
>>> create_file('EL01', [35, 43, 44, 45, 46, 55])
```

Et celui pour le fichier `EL01F.txt` : 

```python
>>> create_file('EL01', [44], 'F')
```

Et ça selon la description de E. Lucas lui-même :

![probleme I](../assets/images/solitaire/p01.png)

Comme nous le voyons l'auteur fourni la solution par une suite de coups simples (un unique saut pour un fichet) comme des fractions : le numéro au numérateur est le fichet qui effectue le saut, le numéro au dénominateur indique l'encoche d'arrivée. Étant sous-entendu que le fichet sauté est retiré du jeu.

Nous adopterons une notation différente : les coordonnées (x, y) du fichet qui bouge et une lettre pour nommer la direction du saut. `N` pour Nord, `S` pour Sud, `E` pour Est et `W` (que l'on confond moins avec le chiffre 0 que O) pour Ouest. 

### Résoudre un problème

Une fois les fichiers texte créés, nous voulons pouvoir, dans un interprète Python interactif effectuer diverses actions :

1. Créer un problème en lui-donnant la partie du nom commune aux deux fichiers et afficher l'objectif du problème :
    
    ```python
    >>> p1 = Solitaire('EL01')
    >>> p1.goal()
    ```

    ```
    ---------- Initial ----------     ----------- Final -----------
    8                                 8
    7           ○  ○  ○               7           ○  ○  ○
    6        ○  ○  ●  ○  ○            6        ○  ○  ○  ○  ○
    5     ○  ○  ●  ●  ●  ○  ○         5     ○  ○  ○  ○  ○  ○  ○
    4     ○  ○  ○  ●  ○  ○  ○         4     ○  ○  ○  ●  ○  ○  ○
    3     ○  ○  ○  ●  ○  ○  ○         3     ○  ○  ○  ○  ○  ○  ○
    2        ○  ○  ○  ○  ○            2        ○  ○  ○  ○  ○
    1           ○  ○  ○               1           ○  ○  ○
    0                                 0
       0  1  2  3  4  5  6  7  8         0  1  2  3  4  5  6  7  8
    ``` 

2. Résoudre le problème (la réponse `True` signifie que le problème a effectivement été résolu):

    === "Résolution en largeur"

        ```python
        >>> p1.largeur()
        True
        ``` 

    === "Résolution en profondeur"

        ```python
        >>> p1.profondeur()
        True
        ``` 

    === "Résolution par _beam search_"

        ```python
        >>> p1.beam_search()
        True
        ``` 


3. Puisque le problème a été résolu, on peut afficher les motifs et les coups joués ou juste la liste des coups :

    === "La liste des coups"

        ```python
        >>> p1.coups()
        ['4,5:E', '4,3:N', '3,5:E', '6,5:W', '4,6:S']
        ```

    === "La trace complète"

        ```python
        >>> p1.trace()
        ```
        Nous donne (_cliquez pour déplier_):

        ??? example "Les 6 coups du problème I" 

            ```
            --- motif 1/6 None:
            8
            7           ○  ○  ○
            6        ○  ○  ●  ○  ○
            5     ○  ○  ●  ●  ●  ○  ○
            4     ○  ○  ○  ●  ○  ○  ○
            3     ○  ○  ○  ●  ○  ○  ○
            2        ○  ○  ○  ○  ○
            1           ○  ○  ○
            0
               0  1  2  3  4  5  6  7  8
            --- motif 2/6 (4, 5):E
            8
            7           ○  ○  ○
            6        ○  ○  ●  ○  ○
            5     ○  ○  ●  ○  ○  ●  ○
            4     ○  ○  ○  ●  ○  ○  ○
            3     ○  ○  ○  ●  ○  ○  ○
            2        ○  ○  ○  ○  ○
            1           ○  ○  ○
            0
               0  1  2  3  4  5  6  7  8
            --- motif 3/6 (4, 3):N
            8
            7           ○  ○  ○
            6        ○  ○  ●  ○  ○
            5     ○  ○  ●  ●  ○  ●  ○
            4     ○  ○  ○  ○  ○  ○  ○
            3     ○  ○  ○  ○  ○  ○  ○
            2        ○  ○  ○  ○  ○
            1           ○  ○  ○
            0
               0  1  2  3  4  5  6  7  8
            --- motif 4/6 (3, 5):E
            8
            7           ○  ○  ○
            6        ○  ○  ●  ○  ○
            5     ○  ○  ○  ○  ●  ●  ○
            4     ○  ○  ○  ○  ○  ○  ○
            3     ○  ○  ○  ○  ○  ○  ○
            2        ○  ○  ○  ○  ○
            1           ○  ○  ○
            0
               0  1  2  3  4  5  6  7  8
            --- motif 5/6 (6, 5):W
            8
            7           ○  ○  ○
            6        ○  ○  ●  ○  ○
            5     ○  ○  ○  ●  ○  ○  ○
            4     ○  ○  ○  ○  ○  ○  ○
            3     ○  ○  ○  ○  ○  ○  ○
            2        ○  ○  ○  ○  ○
            1           ○  ○  ○
            0
               0  1  2  3  4  5  6  7  8
            --- motif 6/6 (4, 6):S
            8
            7           ○  ○  ○
            6        ○  ○  ○  ○  ○
            5     ○  ○  ○  ○  ○  ○  ○
            4     ○  ○  ○  ●  ○  ○  ○
            3     ○  ○  ○  ○  ○  ○  ○
            2        ○  ○  ○  ○  ○
            1           ○  ○  ○
            0
               0  1  2  3  4  5  6  7  8
            ```


Nous laissons temporairement les problèmes de E. Lucas pour présenter les objets que nous allons utiliser dans la suite. Nous reviendrons alors sur la résolution automatique.


## Modéliser un problème

### La classe `Solitaire` 

Un objet `Solitaire` possède les propriétés suivantes :

- `initial` : le motif initial 
- `final` le motif final
- `w` : la largeur du tablier
- `h` : la hauteur du tablier
- `coordonnees` : les coordonnées valides du tablier
- `partie` : la liste des motifs qui vont du motif initial (1er élément de la liste) au motif final par une succession de motifs obtenus par un coup valide à partir du précédent
- `current_id` : l'indice d'un motif dans la liste `partie`, initialisé à 0
- `explored` : un entier initialisé à 0 et qui va nous permettre de compter les motifs explorés lors de la résolution

La création d'une instance se fait en deux temps :

1. lecture des deux fichiers texte dont on donne la racine des noms à la création ; cette étape nous fournit deux dictionnaires et vérifie la cohérence des fichiers (a-t-on bien 2 solitaires de même dimensions ? les coordonnées admissibles sont-elles les mêmes ?). On initialise alors les dimensions `w` et `h` ainsi que des coordonnées admissibles (il s'agit des clés des dictionnaires).

    Par exemple voici le dictionnaire obtenu pour le fichier `EL01I.txt` :

    ```python
    {(3, 1): 0, (4, 1): 0, (5, 1): 0, (2, 2): 0, (3, 2): 0, (4, 2): 1,
     (5, 2): 0, (6, 2): 0, (1, 3): 0, (2, 3): 0, (3, 3): 1, (4, 3): 1, 
     (5, 3): 1, (6, 3): 0, (7, 3): 0, (1, 4): 0, (2, 4): 0, (3, 4): 0, 
     (4, 4): 1, (5, 4): 0, (6, 4): 0, (7, 4): 0, (1, 5): 0, (2, 5): 0, 
     (3, 5): 0, (4, 5): 1, (5, 5): 0, (6, 5): 0, (7, 5): 0, (2, 6): 0, 
     (3, 6): 0, (4, 6): 0, (5, 6): 0, (6, 6): 0, (3, 7): 0, (4, 7): 0, 
     (5, 7): 0}
    ```

2. Création des motifs `initial` et `final` ; initialisation des autres propriétés


??? note "classe `Solitaire`"

    ```python linenums="1"
    class Solitaire:

    def __init__(self, racine_fichier, rep=DEFAULT_REP):
        init_filename = f'{rep}/{racine_fichier}I.txt'
        final_filename = f'{rep}/{racine_fichier}F.txt' 
        w, h, dico_initial = self.load(init_filename)
        w2, h2, dico_final = self.load(final_filename)
        if w == w2 and h == h2 and dico_initial.keys() == dico_final.keys():
            self.w, self.h = w, h
            self.coordonnees = dico_initial.keys()
            self.initial = Motif(self, dico_initial)
            self.final = Motif(self, dico_final)
            self.current_id = 0
            self.partie = [(self.initial, None, '')]
            self.explored = 0
        else:
            raise Exception('Fichiers incohérents')
    ```

Avant de présenter la classe `Motif`, voici une fonction bien pratique pour la suite de notre étude : `solution`, _rattachée_ à un objet `Solitaire`, permet de tester qu'une liste de coups modélisés par les couples des numéros de l'encoche de départ et de l'encoche d'arrivée est une solution au problème.

??? example "Tester une solution d'E. Lucas"

    E. Lucas dans son livre parle de ses enfants Paul et Madeleine qui, agés de 6 et 7 ans, testaient pour lui les solutions aux exercices simples. Uné méthode `solution` permet d'entrer des coups sous la forme de couple de numéros (comme fournis par l'auteur des récréations) ; la fonction retourne `True` si et seulement si les coups conduisent effectivement à une solution c'est-à-dire que le dernier motif obtenu est bien le motif final :

    ```python
    >>> p1 = Solitaire('EL01')
    >>> p1.solution([(45,65), (43,45), (35,55), (65,45), (46,44)])
    True
    ```


### La classe `Motif` 

Le motif ou encore _configuration_ c'est l'ensemble des encoches. En termes de propriétés le motif est assez minimaliste :

- `solitaire` qui est une référence vers l'objet `Solitaire` auquel appartient le motif ; c'est notamment le solitaire qui fournit les dimensions
- `encoches` un dictionnaire dont les clés sont des coordonnées admissibles et les valeurs 0 ou 1 qui modélise une encoche vide ou pleine du tablier.


??? note "classe `Motif`"

    ```python linenums="1"
    class Motif:
    
    def __init__(self, solitaire, dico):
        self.solitaire = solitaire
        h = solitaire.h
        self.encoches = {(x, h-1-y):dico[x, y]) for x, y in dico}
    ```

    !!! warning "Attention"

        Vous aurez noté que les ordonnées sont construites à l'envers : de l'indice le plus grand vers l'indice le plus faible. Cela est dû au système de coordonnées adopté pour numéroter le tablier du solitaire et qui donne les Y de bas en haut alors que lors de la lecture du fichier texte les numéros de ligne (donc les Y) vont croissant de haut en bas.

L'objet `Motif` implémente surtout pas mal de méthodes notamment pour la résolution d'un problème. Voyons quelques exemples.

#### Manipulations d'un `Motif` 

Supposons que `m` référence le motif initial du problème I. Nous allons voir d'abord quelques manipulations _à la main_. Elles ne sont pas essentielles à la résolution automatique mais dans ce genre d'exploration, il est important de pouvoir manipuler les objets créés, les visualiser.

??? example "Initialisation d'un motif"

    Il n'est pas vraiment prévu de créer un motif seul puisque le constructeur d'un `Motif` demande une référence vers un objet `Solitaire` ; le plus simple est de passer par la création d'un Solitaire, via un fichier texte :

    ```python
    >>> p1 = Solitaire('EL01')
    >>> m = p1.initial
    ```


??? example "Afficher un motif"

    ```python
    >>> print(m)
    ```

    ```
    8
    7          ○  ○  ○
    6       ○  ○  ●  ○  ○
    5    ○  ○  ●  ●  ●  ○  ○
    4    ○  ○  ○  ●  ○  ○  ○
    3    ○  ○  ○  ●  ○  ○  ○
    2       ○  ○  ○  ○  ○
    1          ○  ○  ○
    0
      0  1  2  3  4  5  6  7  8
    ```

??? example "Remplir/Vider une encoche"

    Par les coordonnées ou le numéro (donné par 10*abscisse + ordonnée)

    ```python
    >>> m.switch(4, 5)
    >>> m.switch(32)
    >>> print(m)
    ```

    ```
    8
    7          ○  ○  ○
    6       ○  ○  ●  ○  ○
    5    ○  ○  ●  ○  ●  ○  ○
    4    ○  ○  ○  ●  ○  ○  ○
    3    ○  ○  ○  ●  ○  ○  ○
    2       ○  ●  ○  ○  ○
    1          ○  ○  ○
    0
      0  1  2  3  4  5  6  7  8
    ```

??? example "Créer et modifier la copie d'un motif"

    ```python
    >>> m2 = m.copy()
    >>> print(m2)
    ```

    ```
    8
    7          ○  ○  ○
    6       ○  ○  ●  ○  ○
    5    ○  ○  ●  ○  ●  ○  ○
    4    ○  ○  ○  ●  ○  ○  ○
    3    ○  ○  ○  ●  ○  ○  ○
    2       ○  ●  ○  ○  ○
    1          ○  ○  ○
    0
      0  1  2  3  4  5  6  7  8
    ```

    ```python
    # transforme m2 en son dual
    >>> for encoche in m2.encoches.values():
    ...     encoche.switch()
    >>> print(m2)
    ```

    ``` 
    8
    7           ●  ●  ●
    6        ●  ●  ○  ●  ●
    5     ●  ●  ○  ●  ○  ●  ●
    4     ●  ●  ●  ○  ●  ●  ●
    3     ●  ●  ●  ○  ●  ●  ●
    2        ●  ○  ●  ●  ●
    1           ●  ●  ●
    0
       0  1  2  3  4  5  6  7  8
    ```

    ```python
    # m n'a pas changé
    >>> print(m)
    ```
    
    ```
    8
    7          ○  ○  ○
    6       ○  ○  ●  ○  ○
    5    ○  ○  ●  ○  ●  ○  ○
    4    ○  ○  ○  ●  ○  ○  ○
    3    ○  ○  ○  ●  ○  ○  ○
    2       ○  ●  ○  ○  ○
    1          ○  ○  ○
    0
      0  1  2  3  4  5  6  7  8
    ```

??? example "Jouer un coup simple..."

    ... en donnant une direction soit un caractère parmi `'NSEW'` et les coordonnées ou le numéro d'une encoche pleine. L'action retourne un couple : le nouveau motif et les coordonnées de l'encoche d'arrivée :

    ```python
    >>>  m.joue_un_coup('N', 4, 3)
    (<__main__.Motif at 0x1e4ad92f9d0>, (4, 5))
    # m n'a pas été affecté
    >>> print(m)
    ```

    ```
    8
    7          ○  ○  ○
    6       ○  ○  ●  ○  ○
    5    ○  ○  ●  ○  ●  ○  ○
    4    ○  ○  ○  ●  ○  ○  ○
    3    ○  ○  ○  ●  ○  ○  ○
    2       ○  ●  ○  ○  ○
    1          ○  ○  ○
    0
      0  1  2  3  4  5  6  7  8
    ```

    ```python
    >>>  m3, arrivee = m.joue_un_coup('N', 4, 3)
    >>> print(m3)
    ```

    ```
    8
    7          ○  ○  ○
    6       ○  ○  ●  ○  ○
    5    ○  ○  ●  ●  ●  ○  ○
    4    ○  ○  ○  ○  ○  ○  ○
    3    ○  ○  ○  ○  ○  ○  ○
    2       ○  ●  ○  ○  ○
    1          ○  ○  ○
    0
      0  1  2  3  4  5  6  7  8
    ```

### La classe `Encoche`

Existait dans une première version de notre solitaire ; a été simplifiée en un simple entier 0 ou 1.

## Résolution automatique : principe général

### Un parcours de graphe

Le principe de la résolution est basique il s'agit de partir du motif initial, de construire tous les motifs atteignables c'est-à-dire ceux obtenus par un coup valide. E. Lucas donne toujours ses solutions par des successions de **coups simples** ie un unique saut. Dans le sujet des Mines 2021, il est aussi question de **coups composés** : succession de coups simples où le fichet à l'arrivée est celui qui effectue le nouveau saut et ainsi de suite.

Notre résolution effectuera des coups composés, même si parfois (souvent) ils sont réduits à un seul saut et donc correspondent à un coup simple. 

On appellera **graphe d'exploration** le graphe dont les sommets sont constitués de l'ensemble des motifs atteignables et les arcs orientés, vont d'un motif M<sub>a</sub> vers un motif M<sub>b</sub> s'il existe un coup simple ou composé permettant de passer de M<sub>a</sub> à M<sub>b</sub>.

??? example "Graphe d'exploration"

    Ci-dessous une partie du graphe d'exploration du problème n°I d'E. Lucas. Les motifs teintés de mauves sont terminaux (pas de continuation possible). On y voit le motif final et on constate que plusieurs chemins y mènent. Si on ne considère que les coups simples, il est assez trivial de voir que toutes les solutions sont équivalentes puisque le nombre de coups simples est égal à la différence d'encoches pleines entre la configuration finale et la configuration initiale. Dans le sujet des Mines, l'idée était de comptabiliser un coup composé comme 1 et donc de rechercher le chemin optimal. 

    _Note : tous les ars n'ont pas été représentés_

    On remarque que ce graphe est presqu'un arbre : certains motifs peuvent avoir plusieurs arcs entrants. Mais on parlera quand même de branche pour un chemin entre deux motifs.

    ![graphe P1](../assets/images/solitaire/graphe_p1.svg)

!!! abstract "Parcours en profondeur"

    Le **Backtracking** consiste à explorer le graphe par un parcours en profondeur : on part du motif initial et on suit une des branches jusqu'à atteindre le motif final où une impasse. Dans le cas d'une impasse, on remonte au dernier motif où un choix était possible et on descend dans une autre branche.

    **Avantage**

    _Avec de la chance_ on peut atteindre la configuration finale en explorant qu'une toute petite partie du graphe.

    **Inconvénients**

    - On peut ne pas obtenir la solution optimale si on compte les coups composés.
    - Explosion combinatoire si l'ordre de sélection des branches n'est pas favorable


!!! abstract "Parcours en largeur"

    Dans le sujet des Mines, comme on peut passer d'un motif à un autre par un coup composé, certaines branches sont plus courtes que d'autres pour atteindre le motif final. En conséquence, pour avoir la certitude d'obtenir le plus court chemin, on effectue un parcours en largeur : tous les motifs qui se trouvent à 1 coup du motif initial, puis à 2 coups etc. jusqu'à tomber sur le motif final (s'il est atteignable bien sûr).

    **Avantage**

    On est certain de tomber sur la solution la plus courte si on compte les coups composés comme 1 seul coup.

    **Inconvénient**

    L'exploration va traiter le graphe entier. Sur des problèmes non triviaux l'explosion combinatoire est à prévoir.


### Solution 1 : le parcours en largeur

!!! abstract "Principe de l'algorithme"

    - On explore 1 à 1 les motifs rangés dans une structure de file initialisée avec le motif initial. On stocke dans une structure _résultat_ les motifs explorés avec comme information associée le motif dont ils sont issus, les coordonnées de l'encoche qui a effectué les sauts et les directions des différents sauts 
    - Tant que la file possède un motif et qu'on n'a pas rencontré le motif final :

        1. on retire le 1er élément de la file
        2. si le motif en question ne fait pas partie de notre _résultat_ on le rajoute
        3. on calcule tous les motifs atteignables (on mémorisera aussi l'encoche et les directions jouées pour atteindre chacun de ces motifs)
        4. on ajoute à la file les motifs qui n'y sont pas déjà
    
    - quand on sort de notre boucle, si le motif final a été atteint, on construit le chemin en remontant vers le motif initial et on retourne `True` sinon on retourne `False`.

Avant de nous pencher sur les opérations à effectuer sur les motifs pour calculer les motifs atteignables, voyons les structures dont nous avons parlé : la file et la structure _résultat.

#### La file

Il s'agit bien d'une file au sens de structure _FIFO_. Implémentée par une simple liste Python sur laquelle on fera des `append` pour ajouter d'un côté de la liste et des `pop(0)` pour retirer de l'autre... Bien sûr ce n'est pas la solution la plus optimale : on aura avantage à utiliser un module comme `dequeue` ou coder une structure de file.

Les éléments de cette file sont des objets `Motif`. Mais pour chaque motif nous avons aussi besoin de savoir de quel motif il vient, par le mouvement de quelle encoche et avec quels sauts. A chaque motif on associe donc un triplet _(m, (x, y), directions)_ dans un dictionnaire Python. Et là se pose une première difficulté : les clés d'un dictionnaire doivent être des objets _hashable_. Il nous faut donc une sorte d'encodage ou de signature d'un motif vers un objet non mutable, un `int` par exemple. C'est ce que nous donne le codage adopté dans le sujet des Mines : 

$$\Sigma_{n} 2^n$$ 

où _n_ est le numéro d'une encoche pleine du motif.

??? note "Encodage d'un motif"

    ```python
    class Motif:
        # ...
        def num(self):
            return sum(encoche.encode() for encoche in self.encoches.values() if encoche.on())

    class Encoche:
        # ...
        def encode(self):
            return 1 << self.num
    ```

### Solution 2 : le parcours en profondeur

Il suffit de changer la structure de file par une pile. Dans ce parcours, la limite du nombre de motifs voisins à explorer n'a pas de sens puisqu'on va explorer jusqu'à tomber sur la solution. 

L'ordre de rangement des motifs par contre est fondamental : la qualité de l'heuristique a un impact énorme sur la performance comme nous le verrons.

Maintenant que nous avons vu le principe général les structures utilisées, nous allons repartir du bas : comment à partir d'un motif, on obtient l'ensemble des motifs atteignables.


## Résolution automatique : les diverses opérations

Cette section détaille toutes les méthodes nécessaire à la résolution automatique. Elle est assez longue et peut éventuellement être ignorée dans une lecture rapide de l'article pour aller directement à la section des résultats.

### Jouer un coup

Étant donnée une encoche (par ses coordonnées ou son numéro), ainsi qu'une direction (par `'NSEW'`), il s'agit de retourner `None` si le coup n'est pas valide ou le couple formé du nouveau motif et des coordonnées de l'encoche d'arrivée :

??? note "joue_un_coup"

    ```python linenums="1"
    class Motif:
        # ...
        def joue_un_coup(self, direction, *args):
            x, y = to_coords(*args)
            motif = None
            dx, dy = DIRECTIONS[direction.upper()]
            voisin = x+dx, y+dy
            arrivee = x+2*dx, y+2*dy
            if self.on(*voisin) and self.off(*arrivee):            
                motif = self.copy()
                motif.switch(x, y)
                motif.switch(*voisin)
                motif.switch(*arrivee)
            return motif, arrivee
    ```

    **Explications**

    - ligne 4 : on calcule les coordonnées de l'encoche qui va _bouger_
    - ligne 5 : le nouveau motif, `None` pour l'instant car on ne sait pas encore s'il va réellement exister
    - ligne 6 : on récupère le couple des variations à faire sur x et y en fonction de la direction donnée
    - ligne 7 : on calcule les coordonnées du voisin immédiat, l'encoche qui va être sautée et donc vidée
    - ligne 8 : on calcule les coordonnées de l'encoche d'arrivée celle qui va être remplie
    - ligne 9 : la condition pour que le coup soit jouable :
        - le voisin immédiat doit être dans les coordonnées valides et plein
        - l'arrivée doit être dans les coordonnées valides et vide
    - ligne 10 : les conditions sont réunies, on crée une copie du motif qui réalise le coup
    - ligne 11 à 13 : on change les états des encoches concernées
    - ligne 14 : on retourne le nouveau motif et les coordonnées de l'encoche d'arrivée (qui servira d'encoche de départ dans le cas d'un coup composé)


### Coup simple

Il nous faut obtenir la liste des coups possibles dans chacune des directions. La méthode `coup_simple` calcule la liste des triplets _(m, d, (x, y))_ où _m_ est le nouveau motif obtenu par le coup joué dans la direction _d_ depuis l'encoche de coordonnées _(x, y)_.

??? note "coup_simple"

    ```python linenums="1"
    class Motif:
        # ...
        def coup_simple(self, *args):
            coups = []
            if self.on(*args):
                x, y = to_coords(*args)
                for direction in DIRECTIONS:
                    motif, arrivee = self.joue_un_coup(direction, x, y)
                    if motif is not None:
                        coups.append((motif, direction, arrivee))
            return coups 
    ```

??? example "Exemple de coup simple"

    ```python
    >>> print(m)
    ```

    ```
    8
    7           ○  ○  ○
    6        ○  ○  ●  ○  ○
    5     ○  ○  ●  ●  ●  ○  ○
    4     ○  ○  ○  ●  ○  ○  ○
    3     ○  ○  ○  ●  ○  ○  ○
    2        ○  ○  ○  ○  ○
    1           ○  ○  ○
    0
       0  1  2  3  4  5  6  7  8
    ```

    ```python
    >>> m.coup_simple(4, 5)
    [(<__main__.Motif at 0x1e4ad8f5100>, 'N', (4, 7)),
     (<__main__.Motif at 0x1e4aefa8160>, 'E', (6, 5)),
     (<__main__.Motif at 0x1e4aef950a0>, 'W', (2, 5))]
    ```

### Coup composé

Comme pour le coup simple, on part d'une encoche mais cette fois, on s'autorise plusieurs sauts d'affilée. Ici la direction sera donc une chaîne de direction simple.  On retrouve le principe de la file de la résolution globale :

- on commence par ranger dans une file les coups simples obtenus depuis l'encoche concernée
- tant que la file n'est pas vide, on retire un élément et :
    - on stocke dans la liste résultat des coups possibles le triplet constitué du motif, de la direction et des coordonnées de l'arrivée
    - on calcule les coups simples possibles à partir de chacun de ces coups et on stocke dans la file le triplet motif obtenu, la direction qui est la direction de l'élément retiré à laquelle on concatène la direction du nouveau coup et bien sûr les coordonnées de l'arrivée

Ce qui donne : 


??? note "coup_compose"

    ```python linenums="1"
    class Motif:
        # ...
        def coup_compose(self, *args):
            coups = []
            if self.on(*args):
                a_traiter = self.coup_simple(*args)
                while a_traiter:
                    coup, direction, arrivee = a_traiter.pop(0)
                    coups.append((coup, direction, arrivee))
                    for motif, one_dir, arr in coup.coup_simple(*arrivee):
                        voisin = motif, direction+one_dir, arr
                        if voisin not in a_traiter and voisin not in coups:
                            a_traiter.append(voisin)        
            return coups
    ```

??? example "Exemple de coup composé"

    ```python
    >>> print(m2)
    ```

    ```
    8
    7           ○  ○  ○
    6        ○  ○  ●  ○  ○
    5     ○  ○  ●  ○  ●  ○  ○
    4     ○  ○  ○  ●  ○  ○  ○
    3     ○  ○  ○  ●  ○  ○  ○
    2        ○  ○  ○  ○  ○
    1           ○  ○  ○
    0
       0  1  2  3  4  5  6  7  8
    ```

    ```python
    >>> m2.coup_compose(43)
    [(<__main__.Motif at 0x1e4ae9b47f0>, 'N', (4, 5)),
     (<__main__.Motif at 0x1e4aeeb9370>, 'NN', (4, 7)),
     (<__main__.Motif at 0x1e4aefad550>, 'NE', (6, 5)),
     (<__main__.Motif at 0x1e4aefaa3d0>, 'NW', (2, 5))]
    ```

    Notez que `coup_compose` fournit aussi le coup simple vers le Nord. Dans certains test, nous pourrons désactiver la recherche par coups composés qui peut augmenter le nombre de motifs explorés.


### Mouvements

A partir d'un motif _m_, on souhaite obtenir la liste des triplets _(m', (x, y), d)_ où : _m'_ est un motif atteignable depuis _m_ en jouant le coup simple/composé depuis l'encoche de coordonnées _(x, y)_ et dans les directions de la chaîne des directions simples _d_.

??? note "mouvements"

    ```python
    class Motif:
        # ...
        def mouvements(self):
            return [(m1, (x, y), directions) for (x, y) in self.encoches 
                                    for (m1, directions, _) in self.coup_simple(x, y)]
    ```

??? example "Exemple de mouvements"

    ```python
    >>> print(m2)
    ```

    ```
    8
    7           ○  ○  ○
    6        ○  ○  ●  ○  ○
    5     ○  ○  ●  ○  ●  ○  ○
    4     ○  ○  ○  ●  ○  ○  ○
    3     ○  ○  ○  ●  ○  ○  ○
    2        ○  ○  ○  ○  ○
    1           ○  ○  ○
    0
       0  1  2  3  4  5  6  7  8
    ```

    ```python
    >>> m2.mouvements()
    [(<__main__.Motif at 0x1e4aeef8460>, (4, 4), 'S'),
     (<__main__.Motif at 0x1e4aeebf580>, (4, 3), 'N')]
    ```

### `solve` : le coeur de la résolution

Le dictionnaire `d_pred` associe un triplet _(m', (x, y), d)_ au numéro d'un motif _m_ tel que : `(m, (x, y), d) in m'.mouvements()`. C'est ce dictionnaire qu'on va vider progressivement en suivant l'ordre des motifs de notre file, pour créer un dictionnaire _d_resultat_ :

??? note "solve"

    ```python
    class Solitaire:
        # ...
        def solve(self):
            d_resultat = {}
            d_pred = {self.initial.num():(None, None, '')}
            file_a_traiter = [self.initial]
            num_final = self.final.num()
            while num_final not in d_resultat and file_a_traiter:
                m = file_a_traiter.pop(0)
                num = m.num()
                pred, coords, directions = d_pred.pop(num)
                if num not in d_resultat:
                    d_resultat[num] = pred, coords, directions
                file_a_traiter.extend(self.strate(d_pred, [m]))
            if num_final in d_resultat:
                self.create_path(d_resultat)
                return True
            else:
                return False
    ```

    La méthode `strate` sert à mettre à jour le dictionnaire `d_pred` et à retourner dans une liste les motifs effectivement obtenus (la liste peut être vide s'il n'y a plus d'extension possible à partir du motif courant) : ces motifs viennent se ranger dans la file d'attente (`file_a_traiter`).

    La version proposée dans le script final a évolué et propose trois méthodes de résolutions qui remplacent la méthode `solve` (rebaptisée `largeur`) : `largeur`, `profondeur` et `beam_search`. 


## Premiers résultats et amélioration nécessaire

Voici les résultats de notre programme sur les 6 premiers problèmes de E. Lucasavec un parcours en largeur.

--8<--
includes/solitaire/pb01.md
includes/solitaire/pb02.md
includes/solitaire/pb03.md
includes/solitaire/pb04.md
includes/solitaire/pb05.md
includes/solitaire/pb06.md
--8<--


### Explosion combinatoire

Avec P6 on constate que le nombre de motifs explorés atteint déjà une taille conséquente, compte tenu qu'il s'agit d'un problème simple. Une solution consiste à ne pas explorer tous les motifs d'un niveau.

#### Solution 3 : limiter la largeur de l'arbre

Le principe très simple consiste à associer à un motif un _score_ généralement nommé **coût heuristique** et de ne sélectionner pour la recherche que les _n_ meilleurs candidats. Plus _n_ est grand, moins on ignore de candidats mais plus la taille de l'arbre exploré est grande, et plus _n_ est petit plus la recherche va aller vite en ignorant des candidats mais en contre-partie la garantie de trouver la solution n'est plus vraie : il s'agit d'un algorithme glouton. Si _n_ est $\infty$ alors la recherche redevient un parcours en largeur classique.

Le score que nous utilisons est le rapport entre le nombre d'encoches pleines et le nombre d'encoches _bloquées_ (ie ne pouvant pas bouger).

??? "score"

    ```python linenums="1"
    def blocked(self, x, y):
        return all(not self.coup_possible(d, x, y) for d in DIRECTIONS)

    def score(self):
        nb_pleins, nb_blocked = 0, 1
        for encoche in self.encoches.values():
            if encoche.on():
                nb_pleins += 1
                if self.blocked(encoche.x, encoche.y):
                    nb_blocked += 1
        return nb_pleins / nb_blocked
    ```

#### Solution 3 bis : le _beam search_

En français [recherche en faisceau](https://fr.wikipedia.org/wiki/Algorithme_de_recherche_en_faisceau). Dans la solution n°3, le parcours en largeur limite à _n_ les voisins de chacun des motifs d'un niveau. Le nombre de motifs restent donc exponentiel de l'ordre de $n^{p}$ où _p_ est la profondeur de l'arbre.

Le principe du _beam search_ est de limiter à _n_ chaque niveau. Et d'augmenter progressivement le _n_ jusqu'à trouver la solution.

??? "beam search"

    ```python linenums="1"
    class Solitaire:
        # ...

        def beam(self, n, d_pred, sauf=None):
            d_resultat = {}
            gen = [self.initial]
            next_gen = []
            num_final = self.final.num()
            while num_final not in d_resultat and gen:
                next_gen = sorted(self.strate(d_pred, gen), key=lambda t: t.score(sauf), reverse=True)
                for m in gen:
                    self.explored += 1
                    num = m.num()
                    pred, coords, directions = d_pred[num]
                    if num not in d_resultat:
                        d_resultat[num] = pred, coords, directions
                gen = [next_gen[i] for i in range(min(n, len(next_gen)))]
            if num_final in d_resultat:
                self.create_path(d_resultat)
                return True
            else:
                return False
            
        def beam_search(self, n=2):
            num_initial = self.initial.num() 
            d_pred = {num_initial:(None, None, '')}
            while not self.beam(n, d_pred):
                d_pred = {num_initial:(None, None, '')}
                n *= 2
            return n
    ```


#### Les problèmes 7 à 12 

Ils sont plus difficiles, et la recherche naïve par un parcours en largeur n'aboutit pas.

--8<--
includes/solitaire/pb07.md
includes/solitaire/pb08.md
includes/solitaire/pb09.md
includes/solitaire/pb10.md
includes/solitaire/pb11.md
includes/solitaire/pb12.md
--8<--


| problème | larg. inf | larg n/nb | profondeur |  bs n/nb  |
| :------: | --------: | --------: | ---------: | --------: |
|    1     |        23 |        14 |         12 |      2/11 |
|    2     |       150 |        66 |         23 |    16/169 |
|    3     |       540 |     3/198 |         35 |    16/201 |
|    4     |      1029 |       166 |       4953 |      4/55 |
|    5     |     10195 |       980 |       6277 |    32/658 |
|    6     |     83558 |      2939 |         22 |      4/87 |
|    7     |    606160 |     25303 |       1913 |     8/255 |
|    8     |     77381 |     11883 |        492 |    16/524 |
|    9     |         - |    259223 |      20401 |    16/647 |
|    10    |         - |    235548 |      83677 |   64/2613 |
|    11    |         - |   2890929 |            |     4/158 |
|    12    |         - |   1065923 |            | 256/13917 |

La deuxième colonne donne le nombre de motifs explorés sans limitation c'est-à-dire lors du parcours en largeur classique et la troisième donne la même information mais avec une limite à 2 motifs par niveau sauf pour les cas où 2 n'a pas donné de solution (on donne alors la valeur de la limite pour laquelle une solution a été trouvée). 

Dans la quatrième colonne figure le nombre d'exploration lors de la recherche en profondeur. Cette recherche donne de bons résultats sur certains problèmes (2, 3, 6, 7, 8, 9). 

Et la dernière colonne donne les résultats du _beam search_ : la valeur de _n_ qui a permis de trouver une solution et le nombre total de motifs explorés (les motifs ont été explorés plusieurs fois pour n > 2). On constate que cet algorithme est de loin le plus efficace. 


## Suite des problèmes : de 13 à 30

En guise de conclusion, nous terminons par les derniers problèmes de E. Lucas. 

Ils ont en commun (sauf le dernier) le motif de départ qui est constitué des 37 encoches pleines exceptées la centrale 44 qui est vide. Chaque problème doit alors créer un motif propre, chacun ayant un nom assez poétique donné par E. Lucas (_Adam et Eve dans le Paradis Terrestre_, _Trois cavaliers cernés par seize soldats_, _La Pyramide_ ...) :

--8<--
includes/solitaire/pb13.md
includes/solitaire/pb14.md
includes/solitaire/pb15.md
includes/solitaire/pb16.md
includes/solitaire/pb17.md
includes/solitaire/pb18.md
includes/solitaire/pb19.md
includes/solitaire/pb20.md
includes/solitaire/pb21.md
includes/solitaire/pb22.md
includes/solitaire/pb23.md
includes/solitaire/pb24.md
includes/solitaire/pb25.md
includes/solitaire/pb26.md
includes/solitaire/pb27.md
includes/solitaire/pb28.md
includes/solitaire/pb29.md
includes/solitaire/pb30.md
--8<--

Ces problèmes mettent à mal l'heuristique du nombre relatif d'encoches bloquées car :

- certains offrent un motif final particulier où le pourtour du tablier reste plein,
- les derniers demandent de ne laisser que 1 à 3 encoches pleines et sont donc fortement combinatoires.

Il faudrait trouver une autre heuristique pour les problèmes qui justement laissent des encoches pleines sur le pourtour du solitaire. Et pour aller encore plus loin, il serait probablement nécessaire de passer à une implémentation en C et trouver d'autres optimisations, comme par exemple les symétries dont fait état E. Lucas dans son ouvrage.

Notons que le _beam search_ permet de résoudre les problèmes _classiques_ de Solitaire :

--8<--
includes/solitaire/pb30sol.md
includes/solitaire/ang.md
includes/solitaire/pb30v.md
--8<--


## Certains problèmes ne sont pas solubles

Il existe une preuve élégante basée sur le _typage_ des encoches en 3 types : A, B, C. L'idée serait de Hans Zantema mais impossible de trouver une référence à cette preuve. 

Bien sûr le typage ne doit pas se faire n'importe comment car on souhaite conserver l'invariant suivant : à chaque coup 2 des types diminuent de 1 et le 3e augmente de 1. Ainsi la parité de chaque type change à chacun des coups.

Un balayage de haut en bas et de gauche à droite en alternant A, B et C (en réalité 0, 1 et 2) convient :

??? "typer"

    ```python linenums="1"
    class Solitaire:
        # ...
        def typer(self):
            self.types = {}
            d_tmp = {}
            typ = -1
            for y in range(self.h):
                typ = 0 if typ == -1 else (d_tmp[0, y-1] + 1) % 3
                for x in range(self.w):
                    d_tmp[x, y] = typ
                    if self.inside(x, y):
                        self.types[x, y] = typ
                    typ = (typ + 1) % 3
    ```

Voici ce que donne ce typage sur un solitaire européen[^3] :

??? "Signature Solitaire européen"


    ```python
    >>> impossible = Solitaire('No37')
    >>> impossible.signature(True)
    ```

    ```
    8
    7           B  C  A
    6        C  A  B  C  A
    5     A  B  C  A  B  C  A
    4     C  A  B  C  A  B  C
    3     B  C  A  B  C  A  B
    2        B  C  A  B  C
    1           B  C  A
    0
       0  1  2  3  4  5  6  7  8
    ```

Dès lors on peut typer un motif par :

- la parité du nombre de coups simples depuis le motif initial
- le triplet des quantités d'encoches de type A, B et C

??? "Signature d'un motif"

    ```python linenums="1"
    class Motif:
        # ...
        def signature(self):
            parite = (sum(self.solitaire.initial.encoches.values()) - sum(self.encoches.values())) % 2
            sig = [0, 0, 0]
            for x, y in self.encoches:
                if self.encoches[x, y] == PLEIN:
                    sig[self.solitaire.types[x,y]] += 1
            return parite, sig
    ```


    ```python
    >>> impossible.goal()
    ```
    ```
    ---------- Initial ----------     ----------- Final -----------
    8                                 8
    7           ●  ●  ●               7           ○  ○  ○
    6        ●  ●  ●  ●  ●            6        ○  ○  ○  ○  ○
    5     ●  ●  ●  ●  ●  ●  ●         5     ○  ○  ○  ○  ○  ○  ○
    4     ●  ●  ●  ○  ●  ●  ●         4     ○  ○  ○  ●  ○  ○  ○
    3     ●  ●  ●  ●  ●  ●  ●         3     ○  ○  ○  ○  ○  ○  ○
    2        ●  ●  ●  ●  ●            2        ○  ○  ○  ○  ○
    1           ●  ●  ●               1           ○  ○  ○
    0                                 0
       0  1  2  3  4  5  6  7  8         0  1  2  3  4  5  6  7  8
    ```

    ```python
    >>> impossible.initial.signature()
    (0, [12, 12, 12])
    >>> impossible.final.signature()
    (1, [0, 0, 1])
    ```


A chaque coup simple, la parité de chacun des types A, B et C change. Ainsi, on peut prévoir en fonction de la signature des motifs initial et final et de la parité du nombre de coups nécessaire pour passer du premier au second que le problème est insoluble. Attention, ne pas répondre `True` à la question de l'insolubilité du problème ne garantit pas qu'on puisse répondre `True`.

??? "nopath"

    ```python linenums="1"
    class Motif:
        # ...

        def nopath(self, m):
            p1, s1 = self.signature()
            p2, s2 = m.signature()
            s1 = parite(s1)
            s2 = parite(s2)
            if (p1 == p2 and s1 != s2) or (p1 == (not p2) and s1 != non(s2)):
                return True
    ```

    ```python linenums="1"
    class Solitaire:
        # ...
        def nopath(self):
            if self.initial.nopath(self.final):
                return True
    ```

    Commencer avec l'encoche vide au centre et terminer avec l'encoche pleine au même endroit est impossible :

    ```python
    >>> impossible.nopath()
    True
    ```


## Téléchargement

- Le script (à l'état de brouillon) est disponible ici : [Solitaire.zip](../downloads/Solitaire.zip). Cette version ne code que le parcours en largeur.

- une version 2 avec parcours en profondeur et _beam search_, permettant de jouer avec la signature d'un solitaire et d'un motif, la symétrie etc. est disponible ici : [Solitaire2.zip](../downloads/Solitaire2.zip).


[^1]: n.m. petit bâton servant de marque (source : [Larousse en ligne](https://www.larousse.fr/dictionnaires/francais/fichet/33577))
[^2]: Extrait de [gravure _Dame de qualité jouant au Solitaire_](http://ag.louvre.fr/detail/oeuvres/10/578297-Dame-de-qualite-jouant-au-solitaire-max) 
[^3]: Disponible dans la version 2 à télécharger
