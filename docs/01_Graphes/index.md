# Les Graphes

Thème omniprésent en Informatique. Il fait partie des programmes officiels dès la Terminale et en post BAC. Nous explorons ce thème notamment au travers de jeux à 1 ou 2 joueurs.

Ci-dessous un exemple de graphe d'exploration d'un jeu : chaque sommet du graphe est lui-même un graphe, modélisant une configuration de jeu dans le [jeu des parkings](https://irem.univ-reunion.fr/spip.php?article983)


![exemple graphe de jeu](../assets/images/intro_graphes_small.png)

 