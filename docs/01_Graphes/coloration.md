# Autours de la coloration de graphes

L'épreuve n°2 du sujet 0 de la future Agrégation externe d'Informatique (2022) dont vous pouvez télécharger [le sujet au format PDF](sujet0_epreuve2.pdf) est une étude du problème de coloration de graphes, appliqué à l'ordonnancement de tâches.

Des graphes, de la manipulation, de la visualisation, tous les ingrédients pour utiliser [pygraph](https://gitlab.com/sebhoa/pygraph).

Nous vous proposons d'explorer le 56 questions de ce sujet. Pour certaines questions concernant la NP-complétude aucune garantie sur l'exactitude des réponses données : mes souvenirs de DEA sont loin.

Le [notebook de cet article est disponible ici au téléchargement](agreg_2022_sujet0_ep2.ipynb) (pensez à télécharger `pygraph` aussi)

## 1 Ordonnancement de tâches

--8<-- "includes/coloration/coloration_1.md"

## 2 Lien avec les graphes

--8<-- "includes/coloration/coloration_2.md"

## 3 Coloration de graphes

--8<-- "includes/coloration/coloration_3.md"

## 4 Coloration de graphes d'intervalles

--8<-- "includes/coloration/coloration_4.md"
