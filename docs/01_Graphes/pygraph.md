![logo](../assets/images/graphes/tiny.png){: align=left}
# Tutoriel Simple PyGraph[^1]

--- 

**:warning: Ce travail est en cours de modification sur pypi -- une étude est en cours pour intégrer le module à Capytale**

## Introduction et téléchargement

Dès qu'on souhaite _jouer_ un peu avec les graphes, on a envie, de façon **simple** :

- créer un graphe en spécifiant le nombre de sommets
- rajouter des arètes/arcs, aléatoirement ou pas ; en retirer
- afficher ce graphe ie le dessiner
- sauver le dessin de notre graphe dans un fichier
- bouger les sommets pour améliorer le dessin
- utiliser un algorithme connu sur notre graphe
- ...

Cette liste n'est pas exhaustive.

Il existe deux ténors parmi les modules Python traitant de graphes :

??? note "Networkx"
    [`networkx`](https://networkx.org/) qui est un formidable outil de manipulation de graphe au sens _structure mathématique_. On peut par exemple manipuler des graphes prédéfinis (aléatoires, de [Petersen](https://fr.wikipedia.org/wiki/Graphe_de_Petersen)), connaître le degré d'un sommet, ajouter et retirer sommets et arcs etc. Appliquer des algorithmes connus (recherche de plus court chemin, coloration, etc.). On peut même faire des opérations comme l'union de graphes etc. Bref c'est un module extrêmement riche, trop dans bien des cas. 

??? note "Graphviz"

    [`graphviz`](https://graphviz.readthedocs.io/) est le portage sous Python de l'outil [Graphviz](https://www.graphviz.org/) qui est un outil de dessin de graphes utilisant le langage [DOT](https://www.graphviz.org/doc/info/lang.html). Mais graphviz ne sert pas du tout à modéliser un graphe au sens structure mathématique. 

L'idée de ce module `simplepygraph` est de créer un objet `Graph` qui soit la réunion de deux objets :

- un `model` qui est un graphe au sens de _networkx_
- une `view` qui est un graphe au sens de _graphviz_

Ajouté à cela un (petit) ensemble de méthodes permettant les opérations usuelles sur les graphes et en essayant de rester simple. L'outil devrait pouvoir aider tout enseignant souhaitant préparer une ressource pédagogique sur le thème des graphes.

**Attention**, avant d'installer ce module, il faut au préalable avoir installé l'outil GraphViz (pas le module python, l'outil, basé sur le langage dot) : [graphviz](https://graphviz.org/download/). Ensuite, le plus simple reste l'installation via la commande `pip` :

```bash
pip3 install simplepygraph
```

Vous pouvez aussi télécharger le projet :

- via le dépôt gitlab du projet : [gitlab.com/sebhoa/pygraph/](https://gitlab.com/sebhoa/pygraph)
- via Pypi : [pypi.org/project/simplepygraph/](https://pypi.org/project/simplepygraph/)


## Création d'un graphe et visualisation

Après installation, l'import du module pour utilisation se fait comme ceci :

```python
import simplepygraph as sp
```

### `Graph(...)`, `DiGraph(...)` et `BiPartite(...)`

Notre modèle propose trois type de graphes :

- `Graph` : graphe non orienté aléatoire ou pas
- `DiGraph` : graphe orienté
- `BiPartite` : graphe bi-partie 

Dans la suite nous parlerons d'un objet _graphe_ (en italique)  pour nous référer à l'un quelconque de nos trois objets. De façon analogue nous parlerons d'un objet _graphviz_ ou _networkx_ pour faire référence à une des versions orientée ou non orientée des graphes de ces modules.

La visualisation se fait via la propriété `view` qui est l'objet _graphviz_ associé. Dans un _Jupyter notebook_, demander à l'interprète _ipython_ de nous afficher le graphe va effectivement dessiner le graphe dans une cellule de sortie, ce qui est très pratique :

![graphviz notebook](../assets/images/graphes/graphviz_notebook.png){: .centrer}

### `write(filename='output', format='svg')`

Sinon, il faut appeler la méthode qui écrit le graphe dans un fichier. Cette méthode s'appelle `write`, elle prend un paramètre facultatif qui est le nom du fichier. On peut aussi préciser le format d'enregistrement (`'svg'` qui est la valeur par défaut, `'png'`, `'jpg'` etc.). Si on ne précise aucun nom de fichier, le fichier se nommera `output.xxx` ; _xxx_ étant le format choisi.

Pour le dessin de votre graphe, l'objet _graphviz_ se sert d'un _moteur_ de rendu et gère seul le placement des sommets et des arcs/arètes. Nous verrons les diverses possibilités d'intervention.

### Exemples

Les graphes ci-dessous ne sont pas en tailles réelles mais réduits pour ne pas prendre trop de place.

??? example "Graphe à 5 sommets, aléatoire"

    ```python
    >>> G1 = Graph(5, random=True)
    >>> G1.view  # ou G1.write() si on n'est pas dans un notebook
    ```

    ![G1](../assets/images/graphes/g1.svg){: .centrer width="100px"}

    Nous verrons comment réorganiser la position des sommets, par exemple pour afficher ce graphe plutôt à l'horizontale.

??? example "Graphe à 4 sommets, _manuel_"

    Ici un graphe vide, pour le moment...

    ```python
    >>> G2 = Graph(4)
    >>> G2.view  
    ```

    ![G2 sans arètes](../assets/images/graphes/g2sans.svg){: .centrer width="100px"}

    Ajoutons quelques arètes (nous verrons en détail plus tard cette fonctionnalité) :

    ```python
    >>> G2.add_edges_from([(0,1), (0,3), (1,2), (2,3)])
    >>> G2.view
    ```

    ![G2](../assets/images/graphes/g2.svg){: .centrer width="120px"}

??? example "Graphe orienté à 6 sommets, _manuel_"

    Pour l'instant, les graphes orientés ne peuvent pas être aléatoires, on doit ajouter explicitement les arcs. La raison est que mes graphes aléatoires s'appuient sur le modèle `erdos_renyi_graph` du module `networkx` qui génère un graphe non orienté.

    ```python
    >>> DG3 = DiGraph(6)
    >>> DG3.add_edges_from([(0,1), (0,4), (1,3), (2,3), (2,4), (5,2), (4,2), (3,0)])
    >>> DG3.view  
    ```

    ![DG3](../assets/images/graphes/dg3.svg){: .centrer width="140px"}

??? example "Graphe bi-partie 3, 5"

    Un graphe bi-partie (_bipartite graph_ en anglais) se construit en 2 temps :
    
    1. construire du bi-partie complet en spécifiant le nombre de sommets de chacune des parties
    2. supprimer un certain nombre d'arètes  

    ```python
    >>> B35 = BiPartite(6)
    >>> B35.view
    ```

    Le moteur par défaut n'affiche pas très bien le bi-partie complet (mais ne nous préoccupons pas de cela pour le moment):

    ![B35 complet](../assets/images/graphes/b35complete.svg){: .centrer width="140px"}

    Supprimons 4 arètes :

    ```python
    >>> B35.remove_random_edges(4)
    >>> B35.view  
    ```

    ![B35](../assets/images/graphes/b35.svg){: .centrer width="160px"}


## Principe général

Nous l'avons vu, un objet _graphe_ repose sur un modèle et une vue. Si `G` désigne un _graphe_, alors `G.model` est l'objet _networkx_ associé et `G.view` l'objet _graphviz_ associé.

Dans le modèle _networkx_, lors de la création des sommets, on peut leur attacher des informations complémentaires via un dictionnaire. C'est ce que nous faisons. Le dictionnaire étant :

```python
{'view': NodeView(...)}
```

où `NodeView` est un objet permettant la gestion des informations pour la visualisation des sommets. On y trouvera par exemple l'étiquette du sommet, sa couleur de remplissage, les coordonnées de sa position etc.

Ensuite, toute modification du modèle du graphe va entraîner une mise à jour de la vue via cet objet `NodeView`.

## L'attribut [_engine_](https://graphviz.readthedocs.io/en/stable/manual.html#engines)

Il s'agit d'un attribut important des objets _graphviz_ c'est lui qui détermine l'algorithme utilisé pour l'affichage du graphe. Par défaut le moteur utilisé par notre objet _graphe_ sera `neato`. On peut préciser à la création du _graphe_ :

```python
>>> mon_graphe = Graph(7, random=True, engine='circo')
```

ou alors lors de la réinitialisation de la vue :

```python
>>> mon_graphe.reset_view(engine='dot')
```

Les moteurs les plus connus sont :

=== "`neato`" 

    Essaye de donner la même longueur à chaque arête.

    ![neato](../assets/images/graphes/neato.svg){: .centrer}

===  "`dot`" 

    Essaye de faire un tri topologique donc fait des graphes allongés

    ![dot](../assets/images/graphes/dot.svg){: .centrer}

===  "`twopi`" 

    Disposition radiale des sommets 

    ![twopi](../assets/images/graphes/twopi.svg){: .centrer}


=== "`circo`"

    Disposition en cercle

    ![circo](../assets/images/graphes/circo.svg){: .centrer}
    

## Positionner les sommets sur une _grille_

### `position(iterable, ech=1)`, `scale(ech)`

Graphviz permet de positionner les sommets de notre graphe en fournissant des coordonnées $(x, y)$ pour chacun des points. 

Une méthode `position` exploite cette possibilité. Voici un exemple :

=== "Sans positionnement"

    ```python
    >>> G = Graph(6, random=True)
    >>> G.view 
    ```

    ![sans positionnement](../assets/images/graphes/sans_position.svg){: .centrer}

=== "Avec positionnement"

    ```python
    >>> G.position([(1, -0.5, 0), (5, 0.5, 0), (0, -1.5, 0), (3, 1.5, 0), (2, -0.5, -1), (4, 0.5, -1)])
    >>> G.view
    ```
    ![positionnement 1](../assets/images/graphes/position1.svg){: .centrer}

    Un autre positionnement :

    ```python
    >>> G.position([(5, -0.5, 0), (1, 0.5, 1), (0, -1.5, 0), 
    ... (3, 0.5, 0), (2, -0.5, -1), (4, 1.5, -1)])
    >>> G.view
    ```

    ![positionnement 2](../assets/images/graphes/position2.svg){: .centrer}

    On peut changer l'écartement des sommets en utilisant le paramètre nommé `ech`, qui par défaut vaut 1 :

    ```python
    >>> G.position([(5, -0.5, 0), (1, 0.5, 1), (0, -1.5, 0), 
    ... (3, 0.5, 0), (2, -0.5, -1), (4, 1.5, -1)], ech=0.7)
    >>> G.view
    ```

    ![positionnement 3](../assets/images/graphes/position3.svg){: .centrer}

    La méthode `position` fait appelle à `scale` qui prend `ech` en paramètre et que l'on peut utiliser directement pour écarter ou resserrer un graphe déjà positionné :

    ```python
    >>> G.scale(1.2)
    >>> G.view
    ```

    ![positionnement 4](../assets/images/graphes/position4.svg){: .centrer}

!!! warning "Attention"

    Le positionnement des sommets ne fonctionne pas avec le moteur `dot`. 

## Changer les étiquettes

### `set_labels(str_labels)`, `label_on()`

Par défaut, l'étiquette d'un sommet est son _id_ version chaîne de caractère (l'_id_ étant un entier). Par la méthode `set_labels` qui accepte une chaîne de caractères en paramètre, on peut personnaliser l'étiquette de chacun des sommets. 

Une fois le changement fait, il faut actualiser les étiquettes en appelant la méthode `label_on()`. 

Prenons par exemple le graphe suivant :

```python
>>> reseau = Graph(8)
>>> reseau.add_edges_from([(1, 3), (3, 2), (2, 0), (0, 3), (2, 1), 
... (4, 5), (5, 7), (7, 6), (6, 4), (7, 4), (6, 5), (2, 4), (5, 3)])
>>> reseau.position([(0,-3,1), (2, -1, 1), (4, 1, 1), (6, 3, 1), 
... (1,-3,-1), (3, -1, -1), (5, 1, -1), (7, 3, -1)], 0.5) 
>>> reseau.view
```

![reseau anonyme](../assets/images/graphes/reseau_anonyme.svg){: .centrer}

Ce graphe représentant un réseau social, un ami m'a suggérer des étiquettes plus _sympas_, suggérant le côté _social_ du graphe. Voici comment faire[^2] :

```python
>>> PEOPLE = '🧑🧒🧓🧔👦👧👨👩👴👵👶'
>>> reseau.set_labels(PEOPLE)
>>> reseau.label_on()
>>> reseau.view
```

![reseau humain](../assets/images/graphes/reseau_humain.svg){: .centrer}

### `label_off()`

On peut aussi masquer les étiquettes à l'affichage :

```python
>>> reseau.label_off()
>>> reseau.view
```

![reseau humain masque](../assets/images/graphes/reseau_humain_masque.svg){: .centrer}


## Changer la couleur

Chaque sommet possède dans son _NodeView_ un numéro de couleur `color_id` (de type `int`). Par défaut, à la création du _NodeView_, la valeur est -1, pour la couleur blanche.

Associé à cet numérotation des couleurs, une constante `COLORS` est un tuple de noms de couleurs (voir par exemple la [page de la documentation de graphviz à propos des couleurs](https://graphviz.org/doc/info/colors.html)). On veillera à ce que ce tuple ait toujours la couleur `white` en dernière valeur.

### `color_on(s, color)`, `color_off()` 

`color_on` permet à la fois d'afficher les couleurs de tous les sommets, chacun utilisant la couleur déterminée par son propre `color_id` et à la fois de changer la couleur d'un sommet.

Pour changer la couleur d'un sommet on peut utiliser un numéro de couleur (il s'agira de l'indice dans la constante `COLORS` ou un nom).

??? example "Un graphe tout blanc"

    ```python
    >>> CG = Graph(6, random=True)
    >>> CG.view
    ```

    ![CG white](../assets/images/graphes/cg_white.svg){: .centrer}


??? example "Avec un numéro de couleur"

    ```python
    >>> CG.color_on(4, 2)
    >>> CG.view
    ```

    ![aquamarine3 sur noeud 4](../assets/images/graphes/cg_color_id2.svg){: .centrer}



??? example "Avec un nom de couleur"

    ```python
    >>> CG.color_on(4, 'aquamarine3')
    >>> CG.view
    ```

    ![aquamarine3 sur noeud 4](../assets/images/graphes/cg_aquamarine3.svg){: .centrer}


Si les sommets ont une couleur (`color_id` différent de -1), on peut _allumer_ les couleurs avec un appel sans argument à `color_on()`. 

On peut modifier _à la main_ la valeur de `color_id` pour un nœud `node_id` comme ceci :

??? example "Changer le `color_id` d'un nœud"

    ```python
    >>> GC.node_view(0).color_id = 3
    ```

On peut aussi utiliser un outil de colorisation du graphe. Par exemple, on pourra dans le module `algorithms` utiliser la méthode `dsatur` de la classe `Coloring` pour réaliser une coloration du graphe en utilisant l'algorithme [DSATUR](https://fr.wikipedia.org/wiki/DSATUR) :


??? example "Colorer tout le graphe"

    ```python
    >>> from Pygraph.algorithms import Coloring
    >>> coloring = Coloring(CG)
    >>> coloring.dsatur()
    >>> CG.view
    ```

    ![CG après coloration](../assets/images/graphes/colorise.svg){: .centrer}


!!! warning "Attention"

    `color_on(...)`  ne change pas la valeur de `color_id` d'un sommet. 

??? example "Masquer les couleurs"

    ```python
    >>> CG.color_off()
    >>> CG.view
    ```

    ![CG blanc](../assets/images/graphes/cg_white.svg){: .centrer}

    Comme `color_on`, cette méthode ne change pas la valeur des `color_id`. Et un simple appel à `color_on` affiche les couleurs _mémorisées_ par les `color_id` :

    ```python
    >>> CG.color_on()
    >>> CG.view
    ```

    ![CG après coloration](../assets/images/graphes/colorise.svg){: .centrer}


## Le modèle

Nous l'avons déjà évoqué en introduction, le modèle PyGraph repose sur 3 objets :

- un graphe _networkx_ via l'attribut `model`
- un graphe _graphviz_ via l'attribut `view` 
- un objet _maison_ appelé `NodeView` qui sert à mémoriser les propriétés utilisées par la `view` lors de l'affichage. L'attribut qui référence cet objet pour chaque sommet se nomme `view` tout simplement. C'est cet objet pour le sommet `s` d'un graphe `g` que retourne l'appel `g.node_view(s)`.

Les propriétés sauvegardées pour chaque sommet dans l'objet `NodeView` sont :

- `label` la chaîne de caractère servant d'étiquette au sommet (par défaut `str(node_id)`)
- `color_id` l'identifiant de la couleur (par défaut -1)
- `pos` les coordonnées de la position (par défaut `None`)
- `ech` l'échelle à appliquer lors de placement (par défaut 1)
 
### La création

Nous ne reviendrons pas sur les façons de créer un graphe, nous l'avons déjà explicité dans la section [Création et Visualisation](#creation-dun-graphe-et-visualisation)

Notons tout de même la création par copie, qui conserve le positionnement et l'échelle du graphe copié :

??? example "Copie de graphe"

    Créons un graphe orienté...

    ```python
    >>> DG = DiGraph(5)
    >>> DG.add_edges_from([(0,1), (1,0), (1,3), (2,3), (3,4), (4,2)])
    >>> DG.view
    ``` 
    ![copie graphe 1](../assets/images/graphes/copie_dg_1.svg){: .centrer}

    Repositionnons les sommets...

    ```python
    >>> DG.position([(0, 0, 0), (1, -1, 0), (3, -2, 0), (2, -3, -0.5), (4, -3, 0.5)], 0.7)
    >>> DG.view
    ```
    ![copie graphe 2](../assets/images/graphes/copie_dg_2.svg){: .centrer}

    Copions...

    ```python
    >>> DH = DG.copy()
    >>> DH.view
    ```
    ![copie graphe 3](../assets/images/graphes/copie_dg_2.svg){: .centrer}



### Le graphe `model`

Lorsqu'on crée un _PyGraph_, `G` on a alors accès à `G.model` qui est un graphe _networkx_. L'interface _PyGraph_ utilise ce graphe et une partie des outils disponibles via networkx.

```python
>>> G = DiGraph(5)
>>> G.model
<networkx.classes.digraph.DiGraph at 0x26d132981c0>
```

??? note "Autres informations sur le graphe"

    ```python
    >>> G.number_of_nodes()
    5
    >>> G.number_of_edges()
    0
    >>> list(G.node_ids())
    [0, 1, 2, 3, 4]
    ``` 


### Ajouter des liens et des sommets

#### `add_edge(s1, s2)`, `add_edges_from(iterable)`

Ajoute soit un lien spécifié par les deux sommets $s_1$ et $s_2$ concernés soit une série de liens pour tous les couples de l'itérable passé en paramètre.

??? example "Ajouter des arètes"

    ```python
    >>> G = Graph(4)
    >>> G.view
    ```
    ![graphe vide](../assets/images/graphes/empty.svg){: .centrer}

    Ajouter une seule arète...

    ```python
    >>> G.add_edge(0, 1)
    >>> G.view
    ``` 
    ![ajout une arete](../assets/images/graphes/une_arete.svg){: .centrer}

    ou plusieurs :

    ```python
    >>> G.add_edges_from([(0, 2), (1, 3)])
    >>> G.view
    ``` 
    ![ajout d'aretes](../assets/images/graphes/des_aretes.svg){: .centrer}

#### `add_nodes(nodes_count)` 

L'ajout de sommets se fait en indiquant le nombre de sommets à ajouter. L'outil se charge de leur donner un identifiant en partant de l'identifiant max déjà utilisé et en incrémentant de 1 à chaque sommet ajouté. Si un sommet d'identifiant inférieur a, entre-temps été retiré, cet identifiant n'est pas réutilisé. Pour l'instant on ne peut pas non plus ajouter un sommet en spécifiant l'identifiant.

### Retrait

#### `remove_node(s)`, `remove_nodes_from(iterable)`

On peut spécifier un sommet ou un itérable de sommets. Les sommets sont retirés ainsi que **tous** les liens qui concernent les sommets retirés.

??? example "Retrait de sommets"

    On repart de ce graphe...

    ![ajout d'aretes](../assets/images/graphes/des_aretes.svg){: .centrer}

    auquel on retire le sommet 1 :

    ```python
    >>> G.remove(1)
    >>> G.view
    ```
    ![ajout d'aretes](../assets/images/graphes/retrait_sommet.svg){: .centrer}

    On peut retirer tous les sommets :

    ```python
    >>> G.remove_nodes_from(list(G.node_ids()))
    >>> G.number_of_nodes()
    0
    ```

#### `remove_edge(s1, s2)`, `remove_edges_from(iterable)`

On passe un ou des couples et les arètes ou les arcs concernés sont retirés. Attention, s'il s'agit d'un graphe orienté, l'ordre dans lequel on donne les éléments du couple pour les liens a bien entendu une importance :

??? example "Retrait d'arc"

    On considère à nouveau le graphe orienté suivant :

    ![graphe orienté](../assets/images/graphes/copie_dg_2.svg){: .centrer}

    ```python
    >>> DG.remove_edge(0, 1)
    >>> DG.view
    ```
    ![retrait arc](../assets/images/graphes/dg_remove_edge.svg){: .centrer}

    ```python
    >>> DG.remove_edges_from([(2, 3), (3, 4)])
    >>> DG.view
    ```
    ![retraits arcs](../assets/images/graphes/dg_remove_edges_from.svg){: .centrer}

## Les algorithmes

:warning: _modification de section en cours_

Le sous-module  `algorithms` permet de manipuler deux algorithmes pour le moment :
- un algorithme de coloration de graphe (DSATUR)
- un algorithme de recherche de plus court chemin (DIJKSTRA)

Pour importer le sous-module :

```python
import PyGraph.algorithms as algo
```

### Coloration

L'objet permettant de _jouer_ avec la coloration de graphe se nomme `Coloring`.

#### La méthode `dsatur()` 

L'implantation _maison_ de l'algorithme [DSATUR](https://fr.wikipedia.org/wiki/DSATUR).

```python
>>> coloring = Coloring(CG)
>>> coloring.dsatur()
>>> CG.view
```

![CG après coloration](../assets/images/graphes/colorise.svg){: .centrer}


#### `greedy_color(strategy)`

La version _networkx_ de l'algorithme de coloration. Une stratégie doit être choisie parmi : [cette liste](https://networkx.org/documentation/latest/reference/algorithms/generated/networkx.algorithms.coloring.greedy_color.html#networkx.algorithms.coloring.greedy_color). Le choix `DSATUR` correspond à l'algorithme du même nom.  




[^1]: Attention le projet pygraph existait avant le mien, je ne le savais pas et j'ai du changer de nom ; d'où l'apparition du suffixe _simple_ : le nom du module est donc : **simplepygraph**

[^2]: Les caractères unicode peuvent ne pas apparaître avec certains navigateurs. On les trouve par exemple sur cette page : [unicode-table.com](https://unicode-table.com/fr/blocks/miscellaneous-symbols-and-pictographs/)