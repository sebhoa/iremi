# Fondements de l'Informatique : les réseaux de neurones

![xor1](../../assets/images/agreg_2023/xor1.png){ width="10%" align=left } 

Vous trouverez le corrigé et les commentaires de cette partie sur le site de l'IREMI de la Réunion : [réseaux de neurones](https://irem.univ-reunion.fr/spip.php?article1173)