---
hide:
    - toc
--- 

# Introduction

Nous allons parcourir et commenter quelques uns des exercices des 4 épreuves. Nous nous intéressons aux exercices qui manipulent des graphes, et cette année ils portaient pour beaucoup d'entre eux sur la structure d'_union find_ présenté dans un précédent article [Réseau social et Coupe minimale](../../01_Graphes/union_find.md). 

L'article se décompose donc en 4 parties : deux détaillées ici, sur ce site utilisant l'outil _mkdocs-material_ et les deux autres sur le site de l'IREMI de la Réunion : [Les graphes à l'agrégation d'informatique en 2023](https://irem.univ-reunion.fr/spip.php?article1173).

Tous les visuels de graphes présentés ici ont été réalisés à l'aide du module `simple-pygraph` qui permet, dans des notebook jupyter, de créer, modifier, visualiser des graphes. Pour utiliser ce module (notamment en reproduisant les codes que nous donnerons ici) :

1. installer au préalable la suite _graphviz_ (voir [graphviz.org/download/](https://graphviz.org/download/))
2. installer `simple-pygraph` via pip : `pip3 install simple-pygraph`

Voici les quatre sujets traités (vous pouvez accéder aux sujets officiels au format PDF en suivant les liens) :

- [Épreuve 1 -- Composition d'Informatique](https://media.devenirenseignant.gouv.fr/file/Agreg_externe/90/0/s2023_agreg_externe_informatique_1_1429900.pdf) : la partie 2 à propos des ponts d'un graphe non orienté
- [Épreuve 2 -- Étude d'un problème informatique](https://media.devenirenseignant.gouv.fr/file/Agreg_externe/90/1/s2023_agreg_externe_informatique_2_1429901.pdf) : il s'agit de l'étude d'un langage rationnel
- [Épreuve 3](https://media.devenirenseignant.gouv.fr/file/Agreg_externe/90/2/s2023_agreg_externe_informatique_3_1429902.pdf) :
    - l'étude de cas : tournées de véhicules
    - les fondements de l'Informatique : réseaux de neurones