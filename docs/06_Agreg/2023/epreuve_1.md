# PARTIE II : Ponts d'un graphe

??? info

    Cet article utilise, pour sa mise en forme, de _petites boites_ pour encapsuler les différents éléments structurels. La plupart sont repliables, facilitant la navigation.

    _Convention de nommage_ : dans toute la suite, nous nommons $G_x$ où $x$ est un entier, le graphe relatif à la figure `x` du sujet (la variable python associée sera donc `Gx`), $B_x$ (`Bx`) pour le _bloc union find_ correspondant et enfin $F_x$ (`Fx`) pour les forêts. Chacun de ces graphes pourra présenter plusieurs variantes _b_, _c_ etc. (par exemple `G4` et `G4b`, `F3` et `F3b`, `F3c`).

    Bonne lecture...

## Ponts et Blocs dans un graphe non orienté

--8<-- "includes/agreg_2023/epreuve_1_1.md"


## Représentation sous la forme de forêt

--8<-- "includes/agreg_2023/epreuve_1_2.md"

## Ajout d'arêtes

--8<-- "includes/agreg_2023/epreuve_1_3.md"