# Étude d'un problème informatique

![nerode0](../../assets/images/agreg_2023/nerode0.png){ width="10%" align=left } 

Vous trouverez le corrigé et les commentaires de cette partie sur le site de l'IREMI de la Réunion : [expressions régulières et automates](https://irem.univ-reunion.fr/spip.php?article1173)