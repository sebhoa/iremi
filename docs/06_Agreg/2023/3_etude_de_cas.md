# PARTIE I : Déterminer les tournées de véhicules

L'étude porte sur la livraison de produits à des clients, via des tournées de véhicules (des camions par exemple). Nous résumons ci-dessous la modélisation par un graphe pondéré, non orienté $G = (V, E)$ :

- chaque somment $v$ représente un _lieu_ : le sommet $v_0$  est l'entrepôt quand les autres $v_i$ sont les clients ;
- chaque $v_i$ est accessible à partir de $v_0$ ; $G$ est donc connexe ;
- chaque arête $e_{ij}$ est pondérée par la distance $d_{ij}$ entre les sommets $v_i$ et $v_j$ ;
- chaque client $v_i$, $i\gt 0$, commande $w_i$ produits ;
- enfin, chaque camion a une capacité maximale $C$.

## Exemple 1

--8<-- "includes/agreg_2023/epreuve_3_1.md"

## Génération des informations

--8<-- "includes/agreg_2023/epreuve_3_2.md"

## Calcul des tournées : algorithme naïf

--8<-- "includes/agreg_2023/epreuve_3_3.md"

## Calcul des tournées : algorithme de Clarke et Wright (I)

--8<-- "includes/agreg_2023/epreuve_3_4.md"

## Calcul des tournées : algorithme de Clarke et Wright (II)

--8<-- "includes/agreg_2023/epreuve_3_5.md"


## Fin et conclusion

L'article étant déjà très long, nous ne traitons pas les dernières questions (8, 9, 10 et 11) qui sont des questionnements d'extension sur la modélisation POO avec une séparation de la gestion des véhicules de la partie tournée via une interface abstraite nommée transport.

Comme pour les autres sujets traités dans cet article en quatre sections, le but était avant tout d'explorer les graphes (thème de notre groupe de recherche IREM) avec l'idée d'en tirer des activités faisables dans d'autres niveaux que les classes préparatoires.

Enfin, pour terminer, nous avons voulu tester sur un exemple trouvé sur internet. [Cette vidéo sur youtube](https://www.youtube.com/watch?v=7_-Xuq2xKdc) montre la résolution du problème de tournées de véhicule en utilisant l'outil d'optimisation Gurobi et son extension Python gurobipy :

<iframe width="560" height="315" src="https://www.youtube.com/embed/7_-Xuq2xKdc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Génération du graphe

En utilisant le module `numpy`, l'auteur génère un graphe de 11 nœuds (le nœud $0$ représentant toujours l'entrepôt) par leurs coordonnées dans le plan, coordonnées comprises entre $0$ et $200$ pour les abscisses et $0$ et $100$ pour les ordonnées. Chaque point est (virtuellement) relié à chacun des autres par une arête dont le poids est la distance euclidienne entre les points. Voici sa représentation avec le module `matplotlib` (5:30 de la vidéo) :

???+ example "G10 : un exemple tiré du net"

    ![ex_youtube_matplotlib](../../assets/images/agreg_2023/ex_youtube_plt.png)

Construisons cet exemple à l'aide de nos outils :

???+ example "G10 à l'aide pygraph"

    === "G10"

        ![g10](../../assets/images/agreg_2023/ex_youtube_pg.svg)

    === "Le code"

        Il a fallu générer les points via leurs coordonnées dans le plan. Pour obtenir les mêmes positions, nous avons utilisé la même technique que l'auteur mais avons divisé par $10$ toutes les coordonnées.

        ```python
        import numpy as np

        NB = 10
        np.random.seed(0)  # pour initialiser la séquence pseudo aléatoire
        XC = [np.random.rand() * 20 for _ in range(NB + 1)]
        YC = [np.random.rand() * 10 for _ in range(NB + 1)]
        ```

        Puis créer le graphe :

        ```python
        G10 = pg.DiGraph(NB + 1)
        G10.position([(i, XC[i], YC[i]) for i in range(NB + 1)], ech=0.4)
        ```

        Pourquoi un graphe orienté ? Nous représentons le graphe initial des clients sans arêtes puisque toutes les arêtes sont possibles, ce qui rend le graphe difficile à lire. Nous ne ferons apparaitre à la fin que les arcs pour représenter les tournées.

???+ info "Les autres informations"

    - La matrice d'adjacence `A10` créée à partir des distances euclidiennes (appelée aussi _hypot_ dans `numpy`) :
        ```python
        def hypot(x1, y1, x2, y2):
            return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

        A10 = [[hypot(XC[i], YC[i], XC[j], YC[j]) for j in range(NB + 1)] for i in range(NB + 1)]
        ```
    - Le tableau des distances `DIST10` est identique à la matrice d'adjacence
    - La capacité maximale choisie par l'auteur est $20$
    - Le tableau des demandes : `#!py D10 = [None, 4, 4, 4, 8, 1, 2, 1, 5, 8, 4]`

???+ success "La solution"

    === "Notre solution"
    
        Les tournées calculées par l'algorithme de Clarke et Wright :

        ![solution S10](../../assets/images/agreg_2023/ex_youtube_routes.svg)

    === "Le code"

        ```python
        S10 = clarke_wright(DIST10, D10, 20)
        ```

        On peut alors écrire une petite fonction qui prend un graphe orienté et une collection de tournées en paramètres et qui va créer les arcs des différentes tournées et colorier les sommets :

        ```python
        def add_path(g, d_tournees):
            for color_id, tid in enumerate(d_tournees):
                sommets = d_tournees[tid].clients
                g.add_edge(0, sommets[0])
                g.add_edge(sommets[-1], 0)
                for i in range(len(sommets) - 1):
                    noeud, suivant = sommets[i], sommets[i+1]
                    g.add_edge(noeud, suivant)
                    g.color_on(noeud, color_id)
                g.color_on(suivant, color_id)
        ```

        Et l'appliquer :

        ```python
        add_path(G10, S10)
        ```

    === "La solution de l'auteur"

        (à 21:17 environ sur la vidéo)

        ![ex_youtube_routes_originales](../../assets/images/agreg_2023/ex_youtube_routes_orig.png)

        la tournée en haut à droite est la même ; mais il semble y avoir une différence pour le reste des clients. La représentation de l'auteur n'est pas très lisible puisqu'on ne sait pas exactement combien il y a de tournées. Mais, à cause de la capacité maximale de $20$, il n'est pas possible de grouper dans une seule tournée les clients $5, 3, 4, 9, 6$. La tournée $5, 3, 4$ d'une charge de $13$ pourrait éventuellement intégrer le client $6$ qui a une demande à $2$ (mais pas $9$ dont la demande est à $8$).

        Mais la tournée résultante intègre le trajet $4\rightarrow 6$ très couteux (on le voit sur le graphe). En calculant le cout total de cette solution (trois tournées $(1, 7, 8, 10, 2)$, $(5, 3, 4, 6)$ et $(9)$) on obtient $49$ (arrondi à l'entier le plus proche) contre $47$ pour la solution `S10`. 