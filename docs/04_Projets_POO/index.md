# Projets 

## En NSI

Les projets sont censés représenter 1/4 de l'horaire de la spécialité. Ça semble beaucoup néanmoins, de nombreux évènements permettent de valoriser ce travail. Au niveau National, on peut citer [la Nuit du Code](https://www.nuitducode.net/), [les Trophées NSI](https://trophees-nsi.fr/)...

## En L1 Informatique

_en cours_