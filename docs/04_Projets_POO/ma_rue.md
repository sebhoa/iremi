# Dessiner ma rue...

Ce projet a été proposé par Adrien Willm comme (gros) TP puis transformé en projet de groupe par Sébastien Chanthery.

Cette version n'est pas objet et l'énoncé est téléchargeable ici : [TP Modularité avec Turtle](../downloads/MaRue/tp_modularite_turtle.pdf).

Voyons ce qu'apporte les objets.

## Quelques fonctions utilitaires pour commencer

_à venir_