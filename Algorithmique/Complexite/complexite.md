# Complexité algorithmique et efficacité

Anissa et Hugo discutent de la résolution de l'exercice _Anagrammes_ donné par leur professeur d'Informatique. Il s'agit de définir en langage Python une fonction `anagrammes` qui prend deux chaînes de caractères en paramètres et qui retourne `True` si les chaînes sont anagrammes l'une de l'autre et `False` sinon.

## Définition de anagramme

Deux chaînes de caractères sont des **anagrammes** si tout caractère apparaissant _n_ fois dans une des chaînes apparaît aussi _n_ fois dans l'autre, pas nécessairement aux mêmes emplacements.

### Exemples et contre-exemples

- `niche` et `chien` sont anagrammes
- `python` et `python` sont anagrammes
- `ballet` et `table` ne sont pas anagrammes (le `l` n'apparaît qu'une fois dans `table`)

## Discussion

**Hugo :**

> Je pense que je vais trier les lettres des mots dans des listes et si j'obtiens les mêmes listes alors c'est que les mots sont des anagrammes. Simple cet exercice... Je vais utiliser la fonction prédéfinie `sorted` qui prend un itérable et retourne une liste triée. Voilà ma fonction, qu'est-ce que tu en penses Anissa ? pas mal non ? :

```python
def anagrammes_hugo(w, v):
    return sorted(w) == sorted(v)
```

**Anissa :**

> Ah oui c'est pas mal ; c'est court... mais ton algorithme repose sur deux tris donc en terme de complexité moyenne en temps il est en _O(nlogn)_. Je pense qu'on peut faire mieux. Je pense parcourir le premier mot, et compter les apparitions des différents caractères que je vais ranger dans un tableau... le prof a dit qu'on se limitait à des mots sur les lettres minuscules. 
>
> Ensuite je parcours le deuxième mot et je _décompte_ les lettres de mon tableau. A la fin toutes les cases de mon tableau d'apparitions doivent être à zéro, sinon c'est que je n'ai pas des anagrammes. Voilà ma fonction :

```python
def compter(mot, apparitions):
    for lettre in mot:
        apparitions[ord(lettre) - ord('a')] += 1

def decompter(mot, apparitions):
    for lettre in mot:
        apparitions[ord(lettre) - ord('a')] -= 1

def anagrammes_anissa(w, v):
    if len(w) != len(v):
        return False
    else:
        apparitions = [0] * 26
        compter(w, apparitions)
        decompter(v, apparitions)
        return all(n_apparition == 0 for n_apparition in apparitions)
```

**Hugo :**

> Ah mais ton code est beaucoup trop long ! Ma fonction est bien plus efficace que la tienne.

**Anissa :**

> Mais pas du tout. Mon code a certes plus de lignes mais il est lisible et surtout mon algorithme est **linéaire** ! Faisons quelques tests...


## Tests

**Temps d'exécution en s**

|              | | | | | | |
| ------------ | -- | --- | ---- | ---- | -- | --- |
| Longueur mot | 10 | 100 | 1000 | 0.1M | 1M | 10M |
| Hugo (en NlogN) | 0.0 | 0.0 | 0.0 | 0.020 | 0.20 | 2.04 |
| Anissa (lineaire) | 0.0 | 0.0 | 0.0 | 0.021 | 0.21 | 2.10 |

![graphique](graphique.png)

Anissa est extrêmement déçue par ces tests qui donnent raison à Hugo.

## Conclusion

Il faut bien distinguer l'étude algorithmique théorique des mesures de temps effectif d'exécution. 

Le premier consiste à se donner un modéle de machine et d'opérations élémentaires puis à compter ou estimer le nombre de ces opérations effectués au cours de l'algorithme. 

On ne peut ensuite qu'espérer que les résultats théoriques de complexité en temps se traduisent par le même comportement en termes de temps effectif de calcul sur la machine. Mais bien souvent de nombreux facteurs extérieurs non pris en compte lors de l'étude théorique de la complexité font qu'il n'y a pas adéquation. Dans la petite étude menée ici, l'utilisation de la fonction prédéfinie `sorted` dont l'implémentation en C est probablement très optimisée a largement contre balancé la complexité théorique en O(nlogn) par rapport à la solution linéaire en théorie mais faisant appel à des fonctions utilisateurs.

Dans le cadre d'un enseignement de NSI, il faudra donc bien distinguer les situations d'apprentissage :

1. l'exercice est donné dans le but de faire manipuler des tableaux et réfléchir à un algorithme : on interdit alors d'utiliser la fonction prédéfinie `sorted`
2. la fonction doit être écrite pour participer à un projet plus global ; là l'utilisation de la version courte et très efficace est à privilégier
3. si on écrit les deux versions pour une étude de complexité alors il faut se garder de réaliser des test de temps d'exécution.