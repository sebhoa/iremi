"""
Script de manipulation des Union Find
Exploration du sujet X-ENS 2016 -- Version Impérative

Auteur : Sébastien Hoarau
Date   : 2021-08-17
"""

# Q1
# les arètes sont des set chez moi (plus logique puisque non orienté)

RA = [5, [{0,1}, {0,2}, {0,3}, {1,2}, {2,3}]]
RB = [5, [{0,1}, {1,2}, {1,3}, {2,3}, {2,4}, {3,4}]]
